/********************************************************************
  created:  2004/01/03
  filename: ELogger.h
  author:    Dmitry Orlovsky
*********************************************************************/

#ifndef __ELogger_h__
#define __ELogger_h__

#include <fstream>
#include <sstream>
#include <stack>
#include <string>
#include <vector>

#include "ELib/EXception.h"

namespace ELib {

/*!
  LoggerPush can be used as manipulator for ILogger message attribute policy
  managing. Stores the current msgAttr value to an internal stack and sets
  the specified value of msgAttr.
*/
class LoggerPush {
  unsigned int m_msgAttr;

 public:
  explicit inline LoggerPush(unsigned int msgAttr) : m_msgAttr(msgAttr) {}

  inline unsigned int MsgAttr() const { return m_msgAttr; }
};

/*!
  LoggerPop can be used as manipulator for ILogger level policy
  managing. Restores the last msgAttr value from an internal stack.
*/
class LoggerPop {};

/*!
  Provides the stream-like interface for logging and the level policy.
  Logger operates with the following terms:
    - msgAttr     -- binary mask showing the message attitude to a message group
    - acceptMask  -- binary mask representing the message groups currently
  accepted,
                      message is accepted iff masgAttr & acceptMask != 0
*/
class ILogger {
 public:
  // message attributes
  static const unsigned int MA_NONE = 0x0000;

  static const unsigned int MA_WORKFLOW =
      0x0001;  // Milestones, stage descriptions
  static const unsigned int MA_WORKFLOW_MINOR =
      0x0002;  // Workflow messages inside the stage
  static const unsigned int MA_DIAG = 0x0004;  // diagnostic messages

  static const unsigned int MA_DUMP_INPUT = 0x0010;  // input data
  static const unsigned int MA_DUMP =
      0x0020;  // intermediate results of major importance
  static const unsigned int MA_DUMP_MINOR =
      0x0040;  // intermediate results of minor importance
  static const unsigned int MA_DUMP_RESULTS = 0x0080;  // module results

  static const unsigned int MA_LIB_SHIFT = 16;

  static const unsigned int MA_LIB_WORKFLOW = MA_WORKFLOW << MA_LIB_SHIFT;
  static const unsigned int MA_LIB_WORKFLOW_MINOR = MA_WORKFLOW_MINOR
                                                    << MA_LIB_SHIFT;
  static const unsigned int MA_LIB_DIAG = MA_DIAG << MA_LIB_SHIFT;

  static const unsigned int MA_LIB_DUMP_INPUT = MA_DUMP_INPUT << MA_LIB_SHIFT;
  static const unsigned int MA_LIB_DUMP = MA_DUMP << MA_LIB_SHIFT;
  static const unsigned int MA_LIB_DUMP_MINOR = MA_DUMP_MINOR << MA_LIB_SHIFT;
  static const unsigned int MA_LIB_DUMP_RESULTS = MA_DUMP_RESULTS
                                                  << MA_LIB_SHIFT;

  // accept masks
  static const unsigned int LM_NONE = 0x0000;
  static const unsigned int LM_USER = 0x00ff;
  static const unsigned int LM_LIB = LM_USER << MA_LIB_SHIFT;
  static const unsigned int LM_MAJOR =
      MA_WORKFLOW | MA_DUMP_INPUT | MA_DUMP | MA_DUMP_RESULTS;
  static const unsigned int LM_LIB_MAJOR = LM_MAJOR << MA_LIB_SHIFT;
  static const unsigned int LM_ALL = LM_USER | LM_LIB;

  virtual ~ILogger();

  virtual bool IsActive() const = 0;

  ILogger& Log(unsigned int nLevelMsg, const std::string& s);

  ILogger& operator<<(char ch);
  ILogger& operator<<(const char* szMsg);
  ILogger& operator<<(const std::string& str);
  ILogger& operator<<(LoggerPush attr);
  ILogger& operator<<(LoggerPop);

  template <class T>
  ILogger& operator<<(const T& val) {
    if (IsActive()) {
      std::stringstream stream;
      stream << val;
      Log(GetMsgAttr(), stream.str());
    }
    return *this;
  }

  template <class T>
  ILogger& operator<<(const std::vector<T>& v) {
    (*this) << '[' << v.size() << "]{ ";

    for (auto it = v.begin(); it != v.end(); ++it) {
      if (it != v.begin()) (*this) << ", ";

      (*this) << (*it);
    }

    (*this) << "} ";

    return *this;
  }

  ILogger& Push(unsigned int msgAttr);
  ILogger& Pop();

  inline unsigned int GetAcceptMask() const { return m_acceptMask; }

  inline ILogger* GetShadowLogger() const { return m_pShadowLogger; }

  inline unsigned int GetMsgAttr() const { return m_msgAttr; }

  inline void SetAcceptMask(unsigned int acceptMask) {
    m_acceptMask = acceptMask;
  }

  inline void SetShadowLogger(ILogger& shadowLogger) {
    m_pShadowLogger = &shadowLogger;
  }

  inline void SetMsgAttr(unsigned int msgAttr) { m_msgAttr = msgAttr; }

 protected:
  ILogger(unsigned int acceptMask = LM_MAJOR, ILogger* pShadowLogger = NULL);

  virtual ILogger& Write(const std::string& s) = 0;

 private:
  unsigned int m_msgAttr;
  unsigned int m_acceptMask;

  ILogger* m_pShadowLogger;

  std::stack<unsigned int> m_attrStack;
};

//! Pushes message attribute to a logger, and pops it on destruction
class LogAutoPusher {
  ILogger& m_logger;

 public:
  inline LogAutoPusher(ILogger& logger, unsigned int msgAttr)
      : m_logger(logger) {
    logger.Push(msgAttr);
  }

  inline ~LogAutoPusher() { m_logger.Pop(); }
};

//! Birth-and-death logger
class BNDLogger {
 public:
  BNDLogger(ILogger& logger, const std::string& sLogMessage,
            unsigned int msgAttr = ILogger::MA_WORKFLOW);

  virtual ~BNDLogger();

 protected:
  inline const char* GetLogMessage() const { return m_sLogMessage.c_str(); }

  inline unsigned int GetMsgLevel() const { return m_msgAttr; }

  inline ILogger& GetLogger() { return m_logger; }

 private:
  ILogger& m_logger;
  unsigned int m_msgAttr;
  std::string m_sLogMessage;
};

//=============================================================================
class DummyLogger : public ILogger {
 public:
  DummyLogger();
  virtual bool IsActive() const;
  static DummyLogger& Instance();

 protected:
  virtual ILogger& Write(const std::string& s);
};

//=============================================================================
class OStreamLogger : public ILogger {
 public:
  explicit OStreamLogger(std::ostream& oStream,
                         unsigned int acceptMask = ILogger::LM_MAJOR,
                         ILogger* pShadowLogger = NULL);

  inline void Activate(bool state = true) { m_isActive = state; }

  virtual bool IsActive() const;

 protected:
  virtual ILogger& Write(const std::string& s);

 private:
  std::ostream& m_oStream;
  bool m_isActive;
};

//=============================================================================
class OFStreamLogger : public OStreamLogger {
 public:
  explicit OFStreamLogger(const std::string& fileName,
                          unsigned int acceptMask = ILogger::LM_MAJOR,
                          ILogger* pShadowLogger = NULL);

 private:
  std::ofstream m_oFStream;
};

//=============================================================================
//! Mixture for logger selecting and accessing support
class Logged {
 public:
  inline void SetLogger(ILogger& logger) { m_pLogger = &logger; }

  inline ILogger& GetLogger() const { return *m_pLogger; }

 protected:
  inline explicit Logged(ILogger& logger = DummyLogger::Instance())
      : m_pLogger(&logger) {}

 private:
  ILogger* m_pLogger;
};

}  // namespace ELib

#endif  //__ELogger_h__
