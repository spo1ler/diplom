/********************************************************************
  created:  2005/01/21
  filename: EProgressor.h
  author:   Dmitry Orlovsky

  purpose:  Progressors definition
*********************************************************************/

#ifndef __EProgressor_h__
#define __EProgressor_h__

#include <string>

namespace ELib {

//! The base for progress monitors
class IProgressor {
 public:
  virtual ~IProgressor() {}

  virtual void SetProgressMsg(const std::string& sCaption) = 0;
  virtual void SetProgress(int iProgress) = 0;

  inline void SetStatus(const std::string& sCaption, int iProgress) {
    SetProgressMsg(sCaption);
    SetProgress(iProgress);
  }
};

//*****************************************
//! Dummy progress monitors. Does nothing
//*****************************************
class NullProgressor : public IProgressor {
 public:
  virtual ~NullProgressor() {}

  virtual void SetProgressMsg(const std::string&);
  virtual void SetProgress(int);

  static NullProgressor& Instance();
};

//*******************************************************************
//!  Progressored is the base for classes with progress monitoring
//*******************************************************************
class SubProgressor;

class Progressored {
 public:
  inline void SetProgressor(IProgressor& progressor) {
    m_pProgressor = &progressor;
  }

  inline IProgressor& GetProgressor() const { return *m_pProgressor; }

 protected:
  inline Progressored(IProgressor& progressor = NullProgressor::Instance())
      : m_pProgressor(&progressor) {}

 private:
  mutable IProgressor* m_pProgressor;
};

/*!
  Subprogressor can be used as progressor for some subtask:
  it converts 0-100 progress to specified interval, adds prefix
  to caption and then translates it all to destination progressor
*/

class SubProgressor : public IProgressor, public Progressored {
 public:
  SubProgressor(ELib::IProgressor& destProgressor,
                const std::string& sCaptionPrefix = "", int iLowerBound = 0,
                int iUpperBound = 100);

  virtual ~SubProgressor() {}

  void Configure(const std::string& sCaptionPrefix = "", int iLowerBound = 0,
                 int iUpperBound = 100);

  virtual void SetProgressMsg(const std::string& sCaption);
  virtual void SetProgress(int iProgress);

 private:
  std::string m_sCaptionPrefix;
  int m_iLowerBound;
  double m_rRatio;
};

}  // namespace ELib

#endif  //__EProgressor_h__