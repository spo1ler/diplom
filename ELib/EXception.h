/********************************************************************
  created:  2004/11/11
  filename: EXception.h
  author:   Dmitry Orlovsky

  purpose:  Exception class definition
*********************************************************************/

#ifndef __EXception_h__
#define __EXception_h__

#include <exception>
#include <string>

namespace ELib {

class Exception : public std::exception {
  std::string m_sErrMsg;

 public:
  Exception();
  Exception(const std::string& sErrMsg);
  Exception(const char* szFormat, va_list argList);
  Exception(const char* szFormat, ...);

  inline const std::string& GetMessage() const { return m_sErrMsg; }

  virtual const char* what() const throw();
};

}  // namespace ELib

#endif  //__EXception_h__
