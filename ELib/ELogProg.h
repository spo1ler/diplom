/********************************************************************
        created:	8:11:2005   18:03
        filename: ELogProg.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __ELogProg_h__
#define __ELogProg_h__

#include "ELib/ELogger.h"
#include "ELib/EProgressor.h"

namespace ELib {

// Base for classes with both logger and progressor support.
class LogProg : public Logged, public Progressored {
 public:
  LogProg& operator=(const LogProg& lp) {
    SetLogger(lp.GetLogger());
    SetProgressor(lp.GetProgressor());

    return *this;
  }

 protected:
  LogProg(ILogger& logger, IProgressor& progressor)
      : Logged(logger), Progressored(progressor) {}

  LogProg(const LogProg& lp)
      : Logged(lp.GetLogger()), Progressored(lp.GetProgressor()) {}
};

}  // namespace ELib;

#endif  //__ELogProg_h__
