cmake_minimum_required(VERSION 2.8)
project(sirius)

add_library(
    elib_all
    ECopyProtected.h
    ELogger.cpp
    ELogger.h
    ELogProg.h
    EProgressor.cpp
    EProgressor.h
    EStrUtils.cpp
    EStrUtils.h
    ETypedefs.h
    EXception.cpp
    EXception.h)
