/********************************************************************
  created:  2004/11/11
  filename: EXception.cpp
  author:   Dmitry Orlovsky

  purpose:  Exception class implementation
*********************************************************************/

#include "ELib/EXception.h"

#include "ELib/EStrUtils.h"

#include <cstdarg>

namespace ELib {
Exception::Exception() {}

Exception::Exception(const std::string& sErrMsg) : m_sErrMsg(sErrMsg) {}

Exception::Exception(const char* szFormat, va_list argList) {
  m_sErrMsg = FormatStr(szFormat, argList);
}

Exception::Exception(const char* szFormat, ...) {
  va_list argList;

  va_start(argList, szFormat);
  m_sErrMsg = FormatStr(szFormat, argList);
  va_end(argList);
}

const char* Exception::what() const throw() { return m_sErrMsg.c_str(); }

}  // namespace ELib
