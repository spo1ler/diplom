/********************************************************************
  created:  2004/01/03
  filename: ELogger.cpp
  author:   Dmitry Orlovsky
*********************************************************************/

#include "ELib/ELogger.h"
#include "ELib/EStrUtils.h"

namespace ELib {

//===================================================================
ILogger::~ILogger() {}

ILogger::ILogger(unsigned int acceptMask, ILogger* pShadowLogger /* = NULL */)
    : m_msgAttr(MA_WORKFLOW),
      m_acceptMask(acceptMask),
      m_pShadowLogger(pShadowLogger) {}

ILogger& ILogger::Log(unsigned int msgAttr, const std::string& s) {
  if (IsActive()) {
    if (GetAcceptMask() & msgAttr) Write(s);

    if (GetShadowLogger()) GetShadowLogger()->Log(msgAttr, s);
  }

  return *this;
}

ILogger& ILogger::Push(unsigned int msgAttr) {
  m_attrStack.push(GetMsgAttr());
  SetMsgAttr(msgAttr);

  return *this;
}

ILogger& ILogger::Pop() {
  if (m_attrStack.empty())
    throw Exception("ILogger: LoggerPop w/o matching LoggerPush found.");

  SetMsgAttr(m_attrStack.top());
  m_attrStack.pop();

  return *this;
}

ILogger& ILogger::operator<<(char ch) {
  Log(GetMsgAttr(), std::string(1, ch));
  return *this;
}

ILogger& ILogger::operator<<(const char* szMsg) {
  Log(GetMsgAttr(), std::string(szMsg));
  return *this;
}

ILogger& ILogger::operator<<(const std::string& str) {
  Log(GetMsgAttr(), str);
  return *this;
}

ILogger& ILogger::operator<<(LoggerPush attr) { return Push(attr.MsgAttr()); }

ILogger& ILogger::operator<<(LoggerPop) { return Pop(); }

//=============================================================================
DummyLogger::DummyLogger() {}

bool DummyLogger::IsActive() const { return false; }

ILogger& DummyLogger::Write(const std::string& s) { return *this; }

DummyLogger& DummyLogger::Instance() {
  static DummyLogger logger;
  return logger;
}

//=============================================================================
BNDLogger::BNDLogger(ILogger& logger, const std::string& sLogMessage,
                     unsigned int msgAttr)
    : m_logger(logger), m_msgAttr(msgAttr), m_sLogMessage(sLogMessage) {
  FormatStr sMsg("+%s\n", GetLogMessage());

  m_logger.Log(GetMsgLevel(), sMsg);
}

BNDLogger::~BNDLogger() {
  FormatStr sMsg("-%s \n", GetLogMessage());
  m_logger.Log(GetMsgLevel(), sMsg);
}

//=============================================================================
OStreamLogger::OStreamLogger(std::ostream& oStream,
                             unsigned int acceptMask /*= ILogger::LM_MAJOR*/,
                             ILogger* pShadowLogger /* = NULL */)
    : ILogger(acceptMask, pShadowLogger),
      m_oStream(oStream),
      m_isActive(true) {}

bool OStreamLogger::IsActive() const { return m_isActive; }

ILogger& OStreamLogger::Write(const std::string& s) {
  if (m_isActive) {
    m_oStream.write(s.c_str(), s.length());
    m_oStream.flush();
  }

  return *this;
}

//=============================================================================
OFStreamLogger::OFStreamLogger(const std::string& fileName,
                               unsigned int acceptMask /*= ILogger::LM_MAJOR*/,
                               ILogger* pShadowLogger /* = NULL */)
    : OStreamLogger(m_oFStream, acceptMask, pShadowLogger),
      m_oFStream(fileName.c_str()) {}

}  // namespace ELib
