#ifndef __ETypedefs_h__
#define __ETypedefs_h__

namespace ELib {

template <bool Predicate, class IfTrue, class IfFalse>
struct If;

template <class IfTrue, class IfFalse>
struct If<true, IfTrue, IfFalse> {
  typedef IfTrue T;
};
template <class IfTrue, class IfFalse>
struct If<false, IfTrue, IfFalse> {
  typedef IfFalse T;
};

template <class DataType>
struct ConstTraits {
  typedef DataType value_type;
  typedef const DataType* pointer;
  typedef const DataType& reference;

  template <class DataType2>
  struct TraitsT : public ConstTraits<DataType2> {};
};

template <class DataType>
struct NonConstTraits {
  typedef DataType value_type;
  typedef DataType* pointer;
  typedef DataType& reference;

  template <class DataType2>
  struct TraitsT : public NonConstTraits<DataType2> {};
};

}  // namespace ELib

#endif  //__ETypedefs_h__
