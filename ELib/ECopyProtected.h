/********************************************************************
        created:	2006/02/11   16:46
        filename: ECopyProtected.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __ECopyProtected_h__
#define __ECopyProtected_h__

namespace ELib {

class CopyProtected {
 protected:
  CopyProtected() = default;
  ~CopyProtected() = default;

 private:
  CopyProtected(const CopyProtected&) = delete;
  CopyProtected& operator=(const CopyProtected&) = delete;
};

}  // namespace ELib

#endif  //__ECopyProtected_h__
