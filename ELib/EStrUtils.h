/********************************************************************
 created:   2004/11/11
 filename: StrUtils.h
 author:   Dmitry Orlovsky

 purpose:  String utilities
*********************************************************************/

#ifndef __EStrUtils_h__
#define __EStrUtils_h__

#include <string>
#include <cstdio>

namespace ELib {

class FormatStr : public std::string {
 public:
  FormatStr();
  FormatStr(const char* szFormat, va_list argList);
  FormatStr(const char* szFormat, ...);
  void Format(const char* szFormat, va_list argList);
  void Format(const char* szFormat, ...);
};

}  // namespace ELib

#endif  //__EStrUtils_h__
