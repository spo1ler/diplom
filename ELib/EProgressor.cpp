/********************************************************************
created:  2005/01/21
filename: EProgressor.cpp
author:   Dmitry Orlovsky

purpose:  Progressors implementation
*********************************************************************/

#include "ELib/EProgressor.h"

#include <assert.h>

namespace ELib {

//=============================================================================
void NullProgressor::SetProgressMsg(const std::string&) {}

void NullProgressor::SetProgress(int) {}

NullProgressor& NullProgressor::Instance() {
  static NullProgressor progressor;
  return progressor;
}

//=============================================================================
SubProgressor::SubProgressor(ELib::IProgressor& destProgressor,
                             const std::string& sCaptionPrefix, int iLowerBound,
                             int iUpperBound)
    : Progressored(destProgressor) {
  Configure(sCaptionPrefix, iLowerBound, iUpperBound);
}

void SubProgressor::Configure(const std::string& sCaptionPrefix,
                              int iLowerBound, int iUpperBound) {
  assert(0 <= iLowerBound && iLowerBound < iUpperBound && iUpperBound <= 100);

  m_sCaptionPrefix = sCaptionPrefix;
  m_iLowerBound = iLowerBound;
  m_rRatio = (iUpperBound - m_iLowerBound) / 100.;
}

void SubProgressor::SetProgressMsg(const std::string& sCaption) {
  GetProgressor().SetProgressMsg(m_sCaptionPrefix + sCaption);
}

void SubProgressor::SetProgress(int iProgress) {
  GetProgressor().SetProgress(m_iLowerBound +
                              static_cast<int>(iProgress * m_rRatio));
}

}  // namespace ELib
