/********************************************************************
created:   2004/11/11
filename: StrUtils.cpp
author:   Dmitry Orlovsky

purpose:  String utilities
*********************************************************************/

#include "ELib/EStrUtils.h"

#include <cstdarg>
#include <memory>

namespace ELib {

FormatStr::FormatStr() {}

void FormatStr::Format(const char* szFormat, va_list argList) {
  // It's possible for methods that use a va_list to invalidate
  // the data in it upon use.  The fix is to make a copy
  // of the structure before using it and use that copy instead.
  va_list backupArgList;
  va_copy(backupArgList, argList);
  const int buffSize = vsnprintf(nullptr, 0, szFormat, backupArgList) + 1;
  va_end(backupArgList);

  std::unique_ptr<char[]> buff(new char[buffSize]);

  va_copy(backupArgList, argList);
  vsnprintf(buff.get(), buffSize, szFormat, backupArgList);
  va_end(backupArgList);

  assign(buff.get());
}

void FormatStr::Format(const char* szFormat, ...) {
  va_list argList;

  va_start(argList, szFormat);
  Format(szFormat, argList);
  va_end(argList);
}

FormatStr::FormatStr(const char* szFormat, va_list argList) {
  Format(szFormat, argList);
}

FormatStr::FormatStr(const char* szFormat, ...) {
  va_list argList;

  va_start(argList, szFormat);
  Format(szFormat, argList);
  va_end(argList);
}

}  // namespace ELib
