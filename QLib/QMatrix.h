/********************************************************************
        created:	8:11:2005   18:04
        filename: QMatrix.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QMatrix_h__
#define __QMatrix_h__

#include "ELib/ETypedefs.h"

#include "QLib/QBaseMatrix.h"
#include "QLib/QSharedVectorWrapper.h"
#include "QLib/QPolynomial.h"

#include <ostream>

namespace QLib {

typedef SharedVectorWrapper<double> DSharedVectorWrapper;

class ConfigFile;
class ConfigSection;

class DMatrix;
class DMatrixDiag;
class DMatrixConst;
class DMatrixCDiag;

//  ******************
//  *     DMatrix    *
//  ******************
class DMatrix : public BaseMatrix {
 public:
  typedef double* pointer;
  typedef const double* const_pointer;
  typedef double* iterator;
  typedef const double* const_iterator;

  DMatrix();
  DMatrix(unsigned int nMSize, unsigned int nNSize);
  DMatrix(unsigned int nMSize, unsigned int nNSize, const double r);
  // Constructs matrix from its dimensions and entries, in row-major order.
  DMatrix(unsigned int nMSize, unsigned int nNSize,
          const std::vector<double> entries);

  DMatrix& Assign(const DMatrix& m);
  DMatrix& Clone(const DMatrix& m);

  void Init();
  void Init(unsigned int nMSize, unsigned int nNSize);
  void Init(unsigned int nMSize, unsigned int nNSize, const double r);

  void InitZero(unsigned int nMSize, unsigned int nNSize);
  void FillZero();

  void InitI(unsigned int dim);
  static DMatrix I(unsigned int dim);

  DMatrix GetRow(unsigned int i) const;
  DMatrix GetCol(unsigned int j) const;

  DMatrix T() const;     // Returns transposed matrix
  DMatrix& Transpose();  // Transposes the matrix

  double* Data() { return m_data.Data(); }
  const double* Data() const { return m_data.Data(); }
  size_t GetDLen() const { return m_data.GetDLen(); }

  iterator Begin() { return Data(); }
  const_iterator Begin() const { return Data(); }
  iterator End() { return Data() + GetDLen(); }
  const_iterator End() const { return Data() + GetDLen(); }

  void Clip() { m_data.Clip(); }

#ifndef _DEBUG
  inline const double* PtrAtV(unsigned int i) const { return Data() + i; }

  inline double* PtrAtV(unsigned int i) { return Data() + i; }

  inline const double* PtrAt(unsigned int i, unsigned int j) const {
    return Data() + j + i * GetN();
  }

  inline double* PtrAt(unsigned int i, unsigned int j) {
    return Data() + j + i * GetN();
  }
#else
  const double* PtrAtV(unsigned int i) const;
  double* PtrAtV(unsigned int i);

  const double* PtrAt(unsigned int i, unsigned int j) const;
  double* PtrAt(unsigned int i, unsigned int j);
#endif

  inline double GetV(unsigned int i) const { return AtV(i); }

  inline void SetV(unsigned int i, const double r) { AtV(i) = r; }

  inline double& AtV(unsigned int i) { return *PtrAtV(i); }

  inline double AtV(unsigned int i) const { return *PtrAtV(i); }

  inline double Get(unsigned int i, unsigned int j) const { return At(i, j); }

  inline void Set(unsigned int i, unsigned int j, const double r) {
    At(i, j) = r;
  }

  inline double At(unsigned int i, unsigned int j) const {
    return *PtrAt(i, j);
  }

  inline double& At(unsigned int i, unsigned int j) { return *PtrAt(i, j); }

 private:
  DSharedVectorWrapper m_data;
};

std::ostream& operator<<(std::ostream& out, const DMatrix& m);

void CfgGet(DMatrix& m, const ConfigSection* pSection,
            const std::string& sRecName);

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrix& m);

/*********************************
 *    DMatrixDiag Definition     *
 *********************************/
class DMatrixDiag : public BaseMatrix {
 public:
  typedef double* pointer;
  typedef const double* const_pointer;
  typedef double* iterator;
  typedef const double* const_iterator;

  DMatrixDiag();
  explicit DMatrixDiag(unsigned int nMSize);
  DMatrixDiag(unsigned int nMSize, const double r);

  DMatrixDiag& Assign(const DMatrixDiag& m);
  DMatrixDiag& Clone(const DMatrixDiag& m);

  void Init(unsigned int nMSize = 1);
  void Init(unsigned int nMSize, const double r);

  void InitZero(unsigned int nMSize);
  void FillZero();

  void InitI(unsigned int dim);
  static DMatrixDiag I(unsigned int dim);

  DMatrix GetRow(unsigned int i) const;
  DMatrix GetCol(unsigned int j) const;

  DMatrix GetAsOrdinary() const;

  double GenericGet(unsigned int i, unsigned int j)
      const;  // Universal method to retrieve the element

  inline const double Get(unsigned int i) const { return At(i); }

  inline void Set(unsigned int i, const double r) { At(i) = r; }

  inline double& At(unsigned int i) { return *PtrAt(i); }

  inline double At(unsigned int i) const { return *PtrAt(i); }

  double* Data() { return m_data.Data(); }
  const double* Data() const { return m_data.Data(); }
  size_t GetDLen() const { return m_data.GetDLen(); }

  iterator Begin() { return Data(); }
  const_iterator Begin() const { return Data(); }
  iterator End() { return Data() + GetDLen(); }
  const_iterator End() const { return Data() + GetDLen(); }

  void Clip() { m_data.Clip(); }

#ifndef _DEBUG
  inline double* PtrAt(unsigned int i) { return Data() + i; }

  inline const double* PtrAt(unsigned int i) const { return Data() + i; }
#else   // #ifndef _DEBUG
  double* PtrAt(unsigned int i);
  const double* PtrAt(unsigned int i) const;
#endif  // #ifndef _DEBUG

 private:
  DSharedVectorWrapper m_data;
};

std::ostream& operator<<(std::ostream& out, const DMatrixDiag& m);

void CfgGet(DMatrixDiag& m, const ConfigSection* pSection,
            const std::string& sRecName);

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixDiag& m);

/*********************************
*    DMatrixConst Definition     *
**********************************/
class DMatrixConst : public BaseMatrix {
 public:
  DMatrixConst() = default;
  DMatrixConst(unsigned int nMSize, unsigned int nNSize, const double r = 0);

  DMatrixConst& Assign(const DMatrixConst& m);

  double& Value() { return val_; }
  double GetValue() const { return val_; }

  void Init(unsigned int nMSize, unsigned int nNSize, const double r);
  void InitZero(unsigned int nMSize, unsigned int nNSize);

  void FillZero();

  DMatrixConst GetRow(unsigned int i) const;
  DMatrixConst GetCol(unsigned int j) const;
  DMatrix GetAsOrdinary() const;

  DMatrixConst T() const;     // Returns transposed matrix
  DMatrixConst& Transpose();  // Transposes the matrix

  double GenericGet(unsigned int i, unsigned int j) const;

 private:
  double val_ = 0;
};

std::ostream& operator<<(std::ostream& out, const DMatrixConst& m);

void CfgGet(DMatrixConst& m, const ConfigSection* pSection,
            const std::string& sRecName);

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixConst& m);

/*********************************
*    DMatrixCDiag Definition     *
**********************************/
class DMatrixCDiag : public BaseMatrix {
 public:
  DMatrixCDiag() = default;
  DMatrixCDiag(unsigned int dim, const double r = 0);

  double& Value() { return val_; }
  double GetValue() const { return val_; }

  DMatrixCDiag& Assign(const DMatrixCDiag& m);

  void Init(unsigned int dim, const double r = 0);
  void InitZero(unsigned int dim);

  void FillZero();

  void InitI(unsigned int dim);

  static inline DMatrixCDiag I(unsigned int dim) {
    return DMatrixCDiag(dim, 1.);
  }

  DMatrix GetRow(unsigned int i) const;
  DMatrix GetCol(unsigned int j) const;

  DMatrix GetAsOrdinary() const;
  DMatrixDiag GetAsDiagonal() const;

  double GenericGet(unsigned int i, unsigned int j) const;

 private:
  double val_ = 0;
};

std::ostream& operator<<(std::ostream& out, const DMatrixCDiag& m);

void CfgGet(DMatrixCDiag& m, const ConfigSection* pSection,
            const std::string& sRecName);

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixCDiag& m);

typedef std::vector<DMatrix> VectorDM;
typedef std::vector<DMatrixDiag> VectorDMDiag;
typedef std::vector<DMatrixConst> VectorDMConst;
typedef std::vector<DMatrixCDiag> VectorDMCDiag;

typedef std::vector<VectorDM> VectorDM2D;
typedef std::vector<VectorDMDiag> VectorDMDiag2D;
typedef std::vector<VectorDMConst> VectorDMConst2D;
typedef std::vector<VectorDMCDiag> VectorDMCDiag2D;

typedef Polynomial<DMatrix> PolyDM;
typedef Polynomial<DMatrixDiag> PolyDMDiag;
typedef Polynomial<DMatrixConst> PolyDMConst;
typedef Polynomial<DMatrixCDiag> PolyDMCDiag;

}  // namespace QLib

#endif  //__QMatrix_h__
