/********************************************************************
        created:	8:11:2005   18:03
        filename: QBMAP.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QBMAP_H_
#define __QBMAP_H_

#include <string>
#include <vector>

#include "QLib/QMatrix.h"
#include "QLib/QCfgObject.h"

namespace QLib {

class BMAP : public PolyDM, public ICfgObject {
 public:
  virtual ~BMAP() = default;

  inline unsigned int GetDim() const;

  void Validate() const;

  //! Returns arrival intensity
  double GetIntensity() const;

  //! Multiplicates the BMAP matrices by constant to obtain a necessary
  // intensity
  void SetIntensity(double intensity);

  //! Returns the column-vector of the BMAPs directing process stationary
  // probabilities
  DMatrix GetStationarity() const;

  //! Returns group arrival intensity
  double GetGroupIntensity() const;

  //! Returns variation of interarrival time
  double GetVariation() const;

  //! Returns correlation between the interarrival times
  double GetCorrelation() const;

  // ICfgObject interface
  virtual void CfgGet(const ConfigFile& config, const std::string& section);
  virtual void CfgSet(ConfigFile& config, const std::string& section) const;
  virtual void CfgSetInfo(ConfigFile& config, const std::string& section) const;

  virtual void Dump(ELib::ILogger& logger) const;
  virtual void DumpInfo(ELib::ILogger& logger) const;

 protected:
  void CheckNotEmpty() const;
};

inline unsigned int BMAP::GetDim() const {
  return size() == 0 ? 0 : front().GetM();
}

}  // namespace QLib

#endif  //__QBMAP_H_
