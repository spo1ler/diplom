/********************************************************************
        created:	8:11:2005   18:05
        filename: QConfigFile.h
        author:		Dmitry Orlovsky
*********************************************************************/

#ifndef __QConfigFile_h__
#define __QConfigFile_h__

#include <string>
#include <vector>

namespace QLib {

/*
* ConfigFile(name)
*     : Section(name)
*         : Record(name)
*             : Value
*             | Value[]
*/

class ConfigRecord {
  typedef std::vector<std::string> StringArray;

 protected:
  union ConfigRecordVal {
    std::string* str;
    StringArray* array;
  };

 public:
  enum ConfigRecordType { cfgUnknown, cfgString, cfgArray };

  std::string name;

  ConfigRecord(ConfigRecordType t = cfgString);
  ConfigRecord(const ConfigRecord& crec);
  ConfigRecord& operator=(const ConfigRecord& crec);

  virtual ~ConfigRecord();

  inline void SetStart(unsigned int siStart) { pos_start = siStart; }
  inline void SetEnd(unsigned int siEnd) { pos_end = siEnd; }
  inline unsigned int GetStart() const { return pos_start; }
  inline unsigned int Ind() const { return pos_end; }

  inline ConfigRecordType GetType() const { return type; }
  inline const ConfigRecordVal& GetValue() const { return value; }

  void SetValue(const std::string& val);
  void SetValue(const StringArray& val);

 private:
  unsigned int pos_start;
  unsigned int pos_end;

  ConfigRecordType type;
  ConfigRecordVal value;
};

class ConfigSection : protected std::vector<ConfigRecord> {
  typedef std::vector<ConfigRecord> ConfigSectionBase;
  typedef std::vector<std::string> StringArray;

  friend class ConfigFile;

 public:
  std::string name;

  ConfigSection();
  ConfigSection(const std::string& nstr, int npos, unsigned int nlen);
  ConfigSection(const ConfigSection& csec);

  ConfigSection& operator=(const ConfigSection& csec);

  void AddValue(const std::string& n, const std::string& value,
                unsigned int start = 0, unsigned int end = 0);

  void AddValueArray(const std::string& n, const StringArray& value,
                     unsigned int start = 0, unsigned int end = 0);

  const ConfigRecord* RecordByName(const std::string& sName) const;
  ConfigRecord* RecordByName(const std::string& sName);

  unsigned int Size() const { return size(); }
  const ConfigRecord& At(unsigned int i) const { return at(i); }

  int GetStart() const { return pos_start; }
  int Ind() const { return pos_end; }
  int GetContentStart() const { return pos_contentstart; }
  int GetContentEnd() const { return pos_contentend; }

  void SetStart(unsigned int pos) { pos_start = pos; }
  void SetEnd(unsigned int pos) { pos_end = pos; }
  void SetContentStart(unsigned int pos) { pos_contentstart = pos; }
  void SetContentEnd(unsigned int pos) { pos_contentend = pos; }

 private:
  unsigned int pos_start, pos_end;
  unsigned int pos_contentstart, pos_contentend;
};

class ConfigFile : protected std::vector<ConfigSection> {
  enum { CONF_FSET, CONF_FNSET };

  typedef std::vector<ConfigSection> DataListBase;
  typedef std::vector<std::string> StringArray;

 public:
  ConfigFile(const std::string& fn = "");
  ~ConfigFile();

  void Read();
  // Write Value to the configuration file
  void Write(const std::string& sSectionName, const std::string& sName,
             const std::string& value);

  // Retrieve section container.
  ConfigSection* SectionByName(const std::string& sName);
  const ConfigSection* SectionByName(const std::string& sName) const;

  // Delete all the section contents.
  void SectionCleanup(const std::string& sName);

  // Delete the section with contents.
  void SectionDelete(const std::string& sName);

  // Delete record from section
  void RecordDelete(const std::string& sSectionName, const std::string& sName);

 protected:
  // Description:
  //  Skip leading spaces in 'text' with length no more
  //  than 'len' from position 'i' including comments.
  int SkipSpaces(const char* text, unsigned int len, int i);

  // Description:
  //  Fetching next token from the input text buffer 's',
  //  which length is 'len' and scanning started at 'pos'.
  //  The found token offset is returned at 'tpos' and
  //  its length at 'tlen'.
  //  Returned value is the next possible token pos, i.e. *tpos+*tlen.
  int GetToken(const char* s, unsigned int len, int pos, int* tpos,
               unsigned int* tlen);

  // Description:
  //  Actual parsing of the configuration file, which content of the
  //  length 'lsize' is already readed into 'text'.
  int ParseConfigText(const char* text, unsigned int lsize);

  FILE* BindQConfigFile();
  void UnbindQConfigFile();

 private:
  int state;
  std::string filename;
  FILE* fp;

  unsigned int startat = 0;
  unsigned int lineno = 0;
};

//================= config IO routines for basic types ====================
void ThrowCfgGet(const std::string& sInfo, const ConfigSection* pSection,
                 const std::string& sRecName);

// Note: writes quotated
void CfgSetStr(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, const std::string& sVal);

void CfgSetDbl(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, double rVal);

void CfgSetInt(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, int iVal);

void CfgSetUInt(ConfigFile& config, const std::string& sSectionName,
                const std::string& sRecordName, unsigned int iVal);

void CfgSetBool(ConfigFile& config, const std::string& sSectionName,
                const std::string& sRecordName, bool bVal);

std::string CfgGetStr(const ConfigSection* pSection,
                      const std::string& sRecordName);

std::string CfgGetStr(const ConfigSection* pSection,
                      const std::string& sRecordName,
                      const std::string& defaultValue);

double CfgGetDbl(const ConfigSection* pSection, const std::string& sRecordName);

double CfgGetDbl(const ConfigSection* pSection, const std::string& sRecordName,
                 double defaultValue);

int CfgGetInt(const ConfigSection* pSection, const std::string& sRecordName);

int CfgGetInt(const ConfigSection* pSection, const std::string& sRecordName,
              int defaultValue);

unsigned int CfgGetUInt(const ConfigSection* pSection,
                        const std::string& sRecordName);

unsigned int CfgGetUInt(const ConfigSection* pSection,
                        const std::string& sRecordName,
                        unsigned int defaultValue);

bool CfgGetBool(const ConfigSection* pSection, const std::string& sRecordName);

bool CfgGetBool(const ConfigSection* pSection, const std::string& sRecordName,
                bool defaultValue);

}  // namespace QLib

#endif  //__QConfigFile_h__
