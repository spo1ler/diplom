#ifndef __QSharedVectorWrapper_h__
#define __QSharedVectorWrapper_h__

#include <algorithm>
#include <memory>
#include <vector>

namespace QLib {

template <class Type>
class SharedVectorWrapper {
 public:
  typedef typename std::vector<Type>::pointer pointer;
  typedef typename std::vector<Type>::const_pointer const_pointer;
  typedef typename std::vector<Type>::iterator iterator;
  typedef typename std::vector<Type>::const_iterator const_iterator;

  SharedVectorWrapper() : SharedVectorWrapper(0, 0) {}
  explicit SharedVectorWrapper(unsigned int len)
      : SharedVectorWrapper(len, 0) {}
  SharedVectorWrapper(unsigned int len, const Type& r)
      : m_vec(new std::vector<Type>(len, r)) {}

  void Init(unsigned int len) { Init(len, 0); }
  void InitZero(unsigned int len) { Init(len, 0); }
  void Init(unsigned int len, const Type& r);

  //! Equivalent to = operator, copy via reference counting
  void Assign(const SharedVectorWrapper<Type>& svec);

  //! Physical copy
  void Clone(const SharedVectorWrapper<Type>& svec);

  //! Make sure object stores its own copy of data not shared with anyone.
  void Clip();

  pointer Data() { return &((*m_vec)[0]); }
  const_pointer Data() const { return &((*m_vec)[0]); }
  unsigned int GetDLen() const { return m_vec->size(); }

  iterator Begin() { return m_vec->begin(); }
  iterator End() { return m_vec->end(); }

  const_iterator Begin() const { return m_vec->begin(); }
  const_iterator End() const { return m_vec->end(); }

 private:
  std::shared_ptr<std::vector<Type>> m_vec;
};

template <class Type>
void SharedVectorWrapper<Type>::Clip() {
  if (!m_vec.unique()) {
    m_vec = std::make_shared<std::vector<Type>>(*m_vec);
  }
}

template <class Type>
void SharedVectorWrapper<Type>::Init(unsigned int len, const Type& r) {
  m_vec = std::make_shared<std::vector<Type>>(len, r);
}

template <class Type>
void SharedVectorWrapper<Type>::Assign(const SharedVectorWrapper<Type>& svec) {
  m_vec = svec.m_vec;
}

template <class Type>
void SharedVectorWrapper<Type>::Clone(const SharedVectorWrapper<Type>& svec) {
  m_vec = std::make_shared<std::vector<Type>>(*svec.m_vec);
}

}  // namespace QLib

#endif  //__QSharedVectorWrapper_h__
