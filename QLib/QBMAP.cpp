/********************************************************************
        created:	8:11:2005   18:01
        filename: QBMAP.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/
#include "QLib/QBMAP.h"

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QConfigFile.h"
#include "QLib/QMatrixArithm.h"
#include "QLib/QLUP.h"

namespace QLib {

using namespace ELib;

//---------------------------------------------------------
void BMAP::CheckNotEmpty() const {
  if (size() <= 1) throw Exception("BMAP must contain at least two matrices");
}

void BMAP::Validate() const {
  CheckNotEmpty();
  if (GetDim() == 0) throw Exception("D_k matrices can't be empty");

  for (const_iterator it = begin(); it != end(); ++it) {
    if (it->GetM() != GetDim() || it->GetN() != GetDim())
      throw Exception("D_k matrices must be square and of the same size");
  }

  std::string err;
  DMatrix sum = t1(GetSum());

  for (unsigned int m = 0; m < sum.GetDLen(); ++m) {
    if (fabs(sum.GetV(m)) > 1e-8) err += FormatStr("%d (%g), ", m, sum.GetV(m));
  }

  if (!err.empty()) throw Exception("Non-zero sums detected at rows:" + err);
}

DMatrix BMAP::GetStationarity() const {
  CheckNotEmpty();

  return LSolveRowNorm(GetSum());
}

double BMAP::GetIntensity() const {
  return QLib::Sum(GetStationarity() * GetDerivAtPoint(1.));
}

void BMAP::SetIntensity(double intensity) {
  if (intensity <= 0)
    throw Exception(
        "BMAP::SetIntensity(intensity[%g]) failed: intensity must be positive",
        intensity);

  const double mul = intensity / GetIntensity();
  for (iterator it = begin(); it != end(); ++it) (*it) *= mul;
}

double BMAP::GetGroupIntensity() const {
  CheckNotEmpty();
  return -QLib::Sum(GetStationarity() * at(0));
}

double BMAP::GetVariation() const {
  return -2 * GetGroupIntensity() *
             QLib::Sum(GetStationarity() * Inversion(at(0))) -
         1;
}

double BMAP::GetCorrelation() const {
  const DMatrix cmD0Inv = Inversion(at(0));
  const double tmp =
      GetGroupIntensity() *
      QLib::Sum(GetStationarity() * cmD0Inv * GetSum(1) * cmD0Inv);

  return (tmp - 1) / GetVariation();
}

//---------------------------------------------------------
// config file support
void BMAP::CfgGet(const ConfigFile& config, const std::string& section) {
  if (const ConfigSection* pBMAPSection = config.SectionByName(section)) {
    clear();
    resize(pBMAPSection->Size());

    for (unsigned int i = 0; i < pBMAPSection->Size(); i++) {
      std::string tok = pBMAPSection->At(i).name;
      if ((tok[0] == 'D') && (tok[1] == '[') &&
          (tok.find(']') + 1 == tok.length())) {
        const unsigned int pos = atoi(tok.c_str() + 2);

        if (pos < size())
          QLib::CfgGet(at(pos), pBMAPSection, tok.c_str());
        else
          throw Exception("BMAP::CfgGet(..): invalid D_k index found, k = %d",
                          pos);
      } else
        throw Exception("BMAP::CfgGet(..): invalid record '%s'", tok.c_str());
    }
  } else
    throw Exception("BMAP::CfgGet(..): can`t find section '%s'.",
                    section.c_str());

  Validate();
}

void BMAP::CfgSet(ConfigFile& config, const std::string& section) const {
  config.SectionCleanup(section);

  for (size_type i = 0; i < size(); ++i)
    QLib::CfgSet(config, section, FormatStr("D[%d]", i), at(i));
}

void BMAP::Dump(ILogger& logger) const {
  logger << "Dumping BMAP:\n";
  for (size_type i = 0; i < size(); ++i)
    logger << "D[" << i << "] = {" << at(i) << "\n}\n";
}

void BMAP::CfgSetInfo(ConfigFile& config, const std::string& section) const {
  CfgSetDbl(config, section, "BMAP_Intensity", GetIntensity());
  CfgSetDbl(config, section, "BMAP_GroupIntensity", GetGroupIntensity());
  CfgSetDbl(config, section, "BMAP_Variation", GetVariation());
  CfgSetDbl(config, section, "BMAP_Correlation", GetCorrelation());
}

void BMAP::DumpInfo(ILogger& logger) const {
  logger << "Dumping BMAP info:\n";
  logger << "BMAP_Intensity" << GetIntensity();
  logger << "BMAP_GroupIntensity" << GetGroupIntensity();
  logger << "BMAP_Variation" << GetVariation();
  logger << "BMAP_Correlation" << GetCorrelation();
}

}  // namespace QLib
