/********************************************************************
        created:	16:2:2006   14:27
        filename: QMatrix.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QMatrix.h"

#include <cstdlib>
#include <sstream>

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QMatrixViews.h"
#include "QLib/QConfigFile.h"

namespace QLib {

const std::streamsize ciOutputPrecision = 16;
const std::streamsize ciOutputWidth = 21;

// ===================== DMatrix Implementation ====================
DMatrix::DMatrix() {}

DMatrix::DMatrix(unsigned int nMSize, unsigned int nNSize)
    : BaseMatrix(nMSize, nNSize), m_data(nMSize * nNSize) {
  if (std::numeric_limits<unsigned int>::max() / nMSize <= nNSize)
    throw ELib::Exception("DMatrix (%dx%d) doesn't fit in memory", nMSize,
                          nNSize);
}

DMatrix::DMatrix(unsigned int nMSize, unsigned int nNSize, const double r)
    : BaseMatrix(nMSize, nNSize), m_data(nMSize * nNSize, r) {
  if (std::numeric_limits<unsigned int>::max() / nMSize <= nNSize)
    throw ELib::Exception("DMatrix (%dx%d) doesn't fit in memory", nMSize,
                          nNSize);
}

DMatrix::DMatrix(unsigned int nMSize, unsigned int nNSize,
                 const std::vector<double> entries)
    : DMatrix(nMSize, nNSize) {
  for (size_t i = 0; i < entries.size(); ++i) {
    SetV(i, entries[i]);
  }
}

DMatrix& DMatrix::Assign(const DMatrix& m) { return (*this) = m; }

DMatrix& DMatrix::Clone(const DMatrix& m) {
  BaseMatrix::SetDimensions(m.GetM(), m.GetN());
  m_data.Clone(m.m_data);

  return *this;
}

// Initialization of the matrix
void DMatrix::Init() {
  Init(1, 1);  // TODO: legalize 0x0 empty matrices
}

void DMatrix::Init(unsigned int nMSize, unsigned int nNSize) {
  BaseMatrix::SetDimensions(nMSize, nNSize);
  m_data.Init(nMSize * nNSize);
}

void DMatrix::Init(unsigned int nMSize, unsigned int nNSize, const double r) {
  BaseMatrix::SetDimensions(nMSize, nNSize);
  m_data.Init(nMSize * nNSize, r);
}

void DMatrix::InitZero(unsigned int nMSize, unsigned int nNSize) {
  Init(nMSize, nNSize, 0.);
}

void DMatrix::FillZero() { std::fill(Begin(), End(), 0.); }

void DMatrix::InitI(unsigned int s_Size) {
  InitZero(s_Size, s_Size);
  MViewDiag<DMatrix> viewDiag(*this);

  std::fill(viewDiag.Begin(), viewDiag.End(), 1.);
}

DMatrix DMatrix::I(unsigned int dim) {
  DMatrix i;

  i.InitI(dim);

  return i;
}

// Returns i-th row of *this
DMatrix DMatrix::GetRow(unsigned int i) const {
  CheckMSizeIn(i);

  DMatrix ret(1, GetN());
  MViewRowConst<DMatrix> rowView(*this, i);

  std::copy(rowView.Begin(), rowView.End(), ret.Begin());

  return ret;
}

// Returns i-th column of *this
DMatrix DMatrix::GetCol(unsigned int j) const {
  CheckNSizeIn(j);

  DMatrix ret(GetM(), 1);
  MViewColumnConst<DMatrix> colView(*this, j);

  std::copy(colView.Begin(), colView.End(), ret.Begin());

  return ret;
}

// Returns transposed matrix
DMatrix DMatrix::T() const {
  DMatrix temp(GetN(), GetM());

  for (unsigned int i = 0; i < GetM(); ++i)
    for (unsigned int j = 0; j < GetN(); ++j) temp.Set(j, i, Get(i, j));

  return temp;
}

// replaces matrix with transposed one
DMatrix& DMatrix::Transpose() {
  Clip();

  if (GetM() == 1 || GetN() == 1)
    SetDimensions(GetN(), GetM());
  else if (GetM() == GetN()) {
    unsigned int nxtColInc = 2;

    for (iterator itLo = Begin(), itUp = itLo, itLast = Begin() + (GetM() - 1);
         itLo != itLast; itLo += nxtColInc, ++nxtColInc, itLast += GetM()) {
      do {
        ++itLo;
        itUp += GetM();

        const double r = *itLo;
        *itLo = *itUp;
        *itUp = r;
      } while (itLo != itLast);
    }
  } else
    Assign(T());

  return *this;
}

#ifdef _DEBUG
const double* DMatrix::PtrAt(unsigned int i, unsigned int j) const {
  CheckMNSizeIn(i, j);

  return Data() + i * GetN() + j;
}

double* DMatrix::PtrAt(unsigned int i, unsigned int j) {
  CheckMNSizeIn(i, j);

  return Data() + i * GetN() + j;
}

const double* DMatrix::PtrAtV(unsigned int i) const {
  if (i >= GetDLen())
    throw ELib::Exception(
        "DMatrix::PtrAtV(i) range check failed: i[%d] < GetDLen()[%d]", i,
        GetDLen());

  return Data() + i;
}

double* DMatrix::PtrAtV(unsigned int i) {
  if (i >= GetDLen())
    throw ELib::Exception(
        "DMatrix::PtrAtV(i) range check failed: i[%d] < GetDLen()[%d]", i,
        GetDLen());

  return Data() + i;
}
#endif  //_DEBUG

std::ostream& operator<<(std::ostream& out, const DMatrix& m) {
  const std::streamsize iOldPrecision = out.precision();
  const std::streamsize iOldWidth = out.width();

  out << m.GetM() << ' ' << m.GetN() << std::endl;

  out.precision(ciOutputPrecision);
  for (unsigned int i = 0; i < m.GetM(); ++i) {
    for (unsigned int j = 0; j < m.GetN(); ++j) {
      out.width(ciOutputWidth);
      out << m.Get(i, j) << ' ';
    }
    if (i < m.GetM() - 1) out << std::endl;
  }

  out.precision(iOldPrecision);
  out.width(iOldWidth);

  return out;
}

void CfgGet(DMatrix& m, const ConfigSection* pSection,
            const std::string& sRecName) {
  bool mustThrow = true;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecName)) {
    std::vector<std::string>* pArray = pRec->GetValue().array;

    if (pArray->size() >= 2) {
      m.Init(atoi(pArray->at(0).c_str()), atoi(pArray->at(1).c_str()));

      if (pArray->size() >= m.GetDLen() + 2) {
        for (unsigned int i = 0; i < m.GetDLen(); i++)
          m.SetV(i, atof(pArray->at(i + 2).c_str()));

        mustThrow = false;
      }
    }
  }

  if (mustThrow) ThrowCfgGet("(DMatrix&, ", pSection, sRecName.c_str());
}

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrix& m) {
  std::stringstream strout;

  strout << "{ " << m << std::endl << "    }" << std::ends;
  config.Write(sSecName, sRecName, strout.str());
}

// DMatrixDiag Implementation
DMatrixDiag::DMatrixDiag() {}

DMatrixDiag::DMatrixDiag(unsigned int nMSize)
    : BaseMatrix(nMSize, nMSize), m_data(nMSize) {}

DMatrixDiag::DMatrixDiag(unsigned int nMSize, const double r)
    : BaseMatrix(nMSize, nMSize), m_data(nMSize, r) {}

DMatrixDiag& DMatrixDiag::Assign(const DMatrixDiag& m) { return (*this) = m; }

DMatrixDiag& DMatrixDiag::Clone(const DMatrixDiag& m) {
  BaseMatrix::SetDimensions(m.GetM(), m.GetN());
  m_data.Clone(m.m_data);

  return *this;
}

// Initialize the matrix created by default constructor
void DMatrixDiag::Init(unsigned int nMSize) {
  BaseMatrix::SetDimensions(nMSize, nMSize);
  m_data.Init(nMSize);
}

void DMatrixDiag::Init(unsigned int nMSize, const double r) {
  Init(nMSize);
  std::fill(Begin(), End(), r);
}

void DMatrixDiag::InitZero(unsigned int nMSize) { Init(nMSize, 0.); }

void DMatrixDiag::FillZero() { std::fill(Begin(), End(), 0.); }

void DMatrixDiag::InitI(unsigned int dim) { Init(dim, 1.); }

DMatrixDiag DMatrixDiag::I(unsigned int dim) { return DMatrixDiag(dim, 1.); }

// Returns i-th row of *this
DMatrix DMatrixDiag::GetRow(unsigned int i) const {
  CheckMSizeIn(i);

  DMatrix Ret(1, GetN(), 0.);
  Ret.SetV(i, Get(i));

  return Ret;
}

// Returns i-th column of *this
DMatrix DMatrixDiag::GetCol(unsigned int j) const {
  CheckNSizeIn(j);

  DMatrix Ret(GetN(), 1, 0.);
  Ret.SetV(j, Get(j));

  return Ret;
}

DMatrix DMatrixDiag::GetAsOrdinary() const {
  DMatrix temp(GetM(), GetN(), 0.);
  MViewDiag<DMatrix> viewDiag(temp);

  std::copy(Begin(), End(), viewDiag.Begin());

  return temp;
}

// Universal method to retrieve the element
double DMatrixDiag::GenericGet(unsigned int i, unsigned int j) const {
  return i == j ? Get(i) : 0.;
}

#ifdef _DEBUG

double* DMatrixDiag::PtrAt(unsigned int i) {
  CheckMSizeIn(i);

  return Data() + i;
}

const double* DMatrixDiag::PtrAt(unsigned int i) const {
  CheckMSizeIn(i);

  return Data() + i;
}
#endif  //#ifdef _DEBUG

std::ostream& operator<<(std::ostream& out, const DMatrixDiag& m) {
  const std::streamsize iOldPrecision = out.precision();
  const std::streamsize iOldWidth = out.width();

  out << "diag: " << m.GetM() << std::endl;

  out.precision(ciOutputPrecision);
  for (DMatrixDiag::const_iterator it = m.Begin(); it != m.End(); ++it) {
    out.width(ciOutputWidth);
    out << *it << ' ';
  }

  out.precision(iOldPrecision);
  out.width(iOldWidth);
  out << std::endl;

  return out;
}

void CfgGet(DMatrixDiag& m, const ConfigSection* pSection,
            const std::string& sRecName) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecName)) {
    m.Init(atoi(pRec->GetValue().array->at(0).c_str()));
    for (unsigned int i = 0; i < m.GetDLen(); i++)
      m.Set(i, atof(pRec->GetValue().array->at(i + 1).c_str()));
  } else
    ThrowCfgGet("(DMatrixDiag&, ", pSection, sRecName);
}

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixDiag& m) {
  std::stringstream strout;

  strout << "{ " << m.GetDLen() << std::endl;
  strout.precision(ciOutputPrecision);

  for (DMatrixDiag::const_iterator it = m.Begin(); it != m.End(); ++it) {
    strout.width(ciOutputWidth);
    strout << *it << ' ';
  }
  strout << std::endl << "    } // Diagonal" << std::ends;

  config.Write(sSecName, sRecName, strout.str());
}

// ===================== DMatrixConst Implementation ====================
DMatrixConst::DMatrixConst(unsigned int nMSize, unsigned int nNSize,
                           const double r /* = 0 */)
    : BaseMatrix(nMSize, nNSize), val_(r) {}

DMatrixConst& DMatrixConst::Assign(const DMatrixConst& m) {
  return DMatrixConst::operator=(m);
}

// Initialize the matrix created by default constructor
void DMatrixConst::Init(unsigned int nMSize, unsigned int nNSize,
                        const double r) {
  BaseMatrix::SetDimensions(nMSize, nNSize);
  val_ = r;
}

void DMatrixConst::InitZero(unsigned int nMSize, unsigned int nNSize) {
  Init(nMSize, nNSize, 0.);
}

void DMatrixConst::FillZero() { val_ = 0.; }

// Universal method to retrieve the element
double DMatrixConst::GenericGet(unsigned int i, unsigned int j) const {
  CheckMNSizeIn(i, j);
  return GetValue();
}

// Returns i-th row of *this
DMatrixConst DMatrixConst::GetRow(unsigned int i) const {
  CheckMSizeIn(i);

  DMatrixConst Ret(1, GetN(), GetValue());

  return Ret;
}

// Returns i-th column of *this
DMatrixConst DMatrixConst::GetCol(unsigned int j) const {
  CheckNSizeIn(j);

  DMatrixConst Ret(GetM(), 1, GetValue());

  return Ret;
}

// Returns transposed matrix
DMatrixConst DMatrixConst::T() const {
  return DMatrixConst(GetN(), GetM(), GetValue());
}

// replaces matrix with transposed
DMatrixConst& DMatrixConst::Transpose() {
  SetDimensions(GetN(), GetM());

  return *this;
}

DMatrix DMatrixConst::GetAsOrdinary() const {
  return DMatrix(GetM(), GetN(), GetValue());
}

std::ostream& operator<<(std::ostream& out, const DMatrixConst& m) {
  out << "const: " << m.GetM() << ' ' << m.GetN();

  const std::streamsize iOldPrecision = out.precision(ciOutputPrecision);
  const std::streamsize iOldWidth = out.width(ciOutputWidth);

  out << m.GetValue();

  out.precision(iOldPrecision);
  out.width(iOldWidth);

  out << std::endl;

  return out;
}

void CfgGet(DMatrixConst& m, const ConfigSection* pSection,
            const std::string& sRecName) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecName)) {
    m.Init(atoi(pRec->GetValue().array->at(0).c_str()),
           atoi(pRec->GetValue().array->at(1).c_str()),
           atof(pRec->GetValue().array->at(2).c_str()));
  } else
    ThrowCfgGet("(DMatrixConst&, ", pSection, sRecName);
}

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixConst& m) {
  std::stringstream strout;

  strout << "{ " << m.GetM() << ' ' << m.GetN() << std::endl;
  strout.precision(ciOutputPrecision);
  strout.width(ciOutputWidth);
  strout << m.GetValue() << std::endl << "    } // Constant" << std::ends;

  config.Write(sSecName, sRecName, strout.str());
}

// DMatrixCDiag implementation
DMatrixCDiag::DMatrixCDiag(unsigned int dim, const double r /*= 0.*/)
    : BaseMatrix(dim, dim), val_(r) {}

DMatrixCDiag& DMatrixCDiag::Assign(const DMatrixCDiag& m) {
  return operator=(m);
}

// Initialize the matrix created by default constructor
void DMatrixCDiag::Init(unsigned int dim, const double r /*= 0.*/) {
  BaseMatrix::SetDimensions(dim, dim);
  val_ = r;
}

void DMatrixCDiag::InitZero(unsigned int dim) { Init(dim, 0.); }

void DMatrixCDiag::FillZero() { val_ = 0.; }

void DMatrixCDiag::InitI(unsigned int dim) { Init(dim, 1.); }

// Returns i-th row of *this
DMatrix DMatrixCDiag::GetRow(unsigned int i) const {
  CheckMSizeIn(i);

  DMatrix Ret(1, GetN(), 0.);
  Ret.SetV(i, GetValue());

  return Ret;
}

// Returns i-th column of *this
DMatrix DMatrixCDiag::GetCol(unsigned int j) const {
  CheckNSizeIn(j);

  DMatrix Ret(GetN(), 1, 0.);
  Ret.SetV(j, GetValue());

  return Ret;
}

DMatrix DMatrixCDiag::GetAsOrdinary() const {
  DMatrix temp(GetM(), GetN(), 0);
  MViewDiag<DMatrix> viewDiag(temp);

  std::fill(viewDiag.Begin(), viewDiag.End(), GetValue());

  return temp;
}

DMatrixDiag DMatrixCDiag::GetAsDiagonal() const {
  DMatrixDiag temp(GetM(), GetN());

  std::fill(temp.Begin(), temp.End(), GetValue());

  return temp;
}

// Universal method to retrieve the element
double DMatrixCDiag::GenericGet(unsigned int i, unsigned int j) const {
  return i == j ? GetValue() : 0;
}

std::ostream& operator<<(std::ostream& out, const DMatrixCDiag& m) {
  out << "const diagonal: " << m.GetM() << ' ' << m.GetN();

  const std::streamsize iOldPrecision = out.precision(ciOutputPrecision);
  const std::streamsize iOldWidth = out.width(ciOutputWidth);

  out << m.GetValue();

  out.precision(iOldPrecision);
  out.width(iOldWidth);

  out << std::endl;

  return out;
}

void CfgGet(DMatrixCDiag& m, const ConfigSection* pSection,
            const std::string& sRecName) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecName)) {
    m.Init(atoi(pRec->GetValue().array->at(0).c_str()),
           atof(pRec->GetValue().array->at(1).c_str()));
  } else
    ThrowCfgGet("(DMatrixCDiag&, ", pSection, sRecName);
}

void CfgSet(ConfigFile& config, const std::string& sSecName,
            const std::string& sRecName, const DMatrixCDiag& m) {
  std::stringstream strout;

  strout << "{ " << m.GetM() << std::endl;

  strout.precision(ciOutputPrecision);
  strout.width(ciOutputWidth);

  strout << m.GetValue() << std::endl << "    } // Constant diagonal"
         << std::ends;

  config.Write(sSecName, sRecName, strout.str());
}

}  // namespace QLib;
