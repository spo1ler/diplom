/********************************************************************
        created:	8:11:2005   18:03
        filename: QBaseMatrix.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QBaseMatrix_h__
#define __QBaseMatrix_h__

namespace QLib {

//!  Base class for all DMatrix classes.
/*!
  Provides dimension management, arithmetics and Get/Print functions.
*/

class BaseMatrix {
 public:
  virtual ~BaseMatrix() = default;

  inline unsigned int GetM() const { return m_nMSize; }
  inline unsigned int GetN() const {
    return m_nNSize;
  };

  void CheckMSizeIn(unsigned int m) const;
  void CheckNSizeIn(unsigned int n) const;
  void CheckMNSizeIn(unsigned int m, unsigned int n) const;

 protected:
  inline BaseMatrix();
  inline BaseMatrix(unsigned int nMSize, unsigned int nNSize);
  inline BaseMatrix(const BaseMatrix& M);

  void SetDimensions(unsigned int nMSize, unsigned int nNSize);

 private:
  unsigned int m_nMSize;
  unsigned int m_nNSize;
};

void CheckAllowSum(const BaseMatrix& m1, const BaseMatrix& m2);
void CheckAllowMul(const BaseMatrix& m1, const BaseMatrix& m2);
void CheckAllowKSum(const BaseMatrix& m1, const BaseMatrix& m2);

//    *************************************
//    *      BaseMatrix Implementation    *
//    *************************************

inline BaseMatrix::BaseMatrix() : m_nMSize(0), m_nNSize(0) {}

inline void BaseMatrix::SetDimensions(unsigned int nMSize,
                                      unsigned int nNSize) {
  m_nMSize = nMSize;
  m_nNSize = nNSize;
}

inline BaseMatrix::BaseMatrix(unsigned int nMSize, unsigned int nNSize) {
  SetDimensions(nMSize, nNSize);
}

inline BaseMatrix::BaseMatrix(const BaseMatrix& M) {
  SetDimensions(M.GetM(), M.GetN());
}

}  // namespace QLib

#endif  //__QBaseMatrix_h__
