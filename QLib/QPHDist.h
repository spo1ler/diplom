/********************************************************************
        created:	8:11:2003   17:19
        filename: QPHDist.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QPHDist_h__
#define __QPHDist_h__

#include <string>

#include "QLib/QMatrix.h"
#include "QLib/QCfgObject.h"

namespace QLib {

class PHDist : public ICfgObject {
 public:
  PHDist();
  PHDist(const DMatrix& mS, const DMatrix& vBeta);

  void Init(const DMatrix& mS, const DMatrix& vBeta);

  inline unsigned int GetM() const { return m_vBeta.GetN(); }

  inline const DMatrix& GetS() const { return m_mS; }

  const DMatrix GetS0() const;

  inline const DMatrix& GetBeta() const { return m_vBeta; }

  void SetIntensity(double intensity);

  double GetIntensity() const;
  double GetVariation() const;

  // ICfgObject interface
  virtual void CfgGet(const ConfigFile& config, const std::string& section);
  virtual void CfgSet(ConfigFile& config, const std::string& section) const;
  virtual void CfgSetInfo(ConfigFile& config, const std::string& section) const;

  virtual void Dump(ELib::ILogger& logger) const;
  virtual void DumpInfo(ELib::ILogger& logger) const;

 protected:
  static void Validate(const DMatrix& mS, const DMatrix& vBeta);

 private:
  DMatrix m_mS;     // Matrix of PH transitions
  DMatrix m_vBeta;  // Initial PH state probabilities vector
};

}  // namespace QLib

#endif  //__QPHDist_h__
