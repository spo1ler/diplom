/********************************************************************
        created:	17:2:2006   18:18
        filename: QMatrixArithm.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QMatrixArithm_h__
#define __QMatrixArithm_h__

#include <cmath>

#include "QLib/QMatrix.h"
#include "QLib/QMatrixViews.h"
#include "QLib/QGenericOps.h"

namespace QLib {
typedef MViewDiag<DMatrix> DMViewDiag;
typedef MViewRow<DMatrix> DMViewRow;
typedef MViewColumn<DMatrix> DMViewColumn;
typedef MViewBlock<DMatrix> DMViewBlock;
typedef MViewDiagConst<DMatrix> DMViewDiagConst;
typedef MViewRowConst<DMatrix> DMViewRowConst;
typedef MViewColumnConst<DMatrix> DMViewColumnConst;
typedef MViewBlockConst<DMatrix> DMViewBlockConst;

//========================= DMatrix ===========================================
double Sum(const DMatrix& m);
double Norm(const DMatrix& m);
double NormDiff(const DMatrix& m1, const DMatrix& m2);

double ScalarMul(const DMatrix& m1, const DMatrix& m2);

DMatrix operator+(const DMatrix& m1, const DMatrix& m2);
DMatrix operator+(const DMatrix& m1, const DMatrixDiag& md2);
DMatrix operator+(const DMatrix& m1, const DMatrixConst& mc2);
DMatrix operator+(const DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix& operator+=(DMatrix& m1, const DMatrix& m2);
DMatrix& operator+=(DMatrix& m1, const DMatrixDiag& md2);
DMatrix& operator+=(DMatrix& m1, const DMatrixConst& mc2);
DMatrix& operator+=(DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix operator-(const DMatrix& m);
DMatrix& Neg(DMatrix& m);

DMatrix operator-(const DMatrix& m1, const DMatrix& m2);
DMatrix operator-(const DMatrix& m1, const DMatrixDiag& md2);
DMatrix operator-(const DMatrix& m1, const DMatrixConst& mc2);
DMatrix operator-(const DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix& operator-=(DMatrix& m1, const DMatrix& m2);
DMatrix& operator-=(DMatrix& m1, const DMatrixDiag& md2);
DMatrix& operator-=(DMatrix& m1, const DMatrixConst& mc2);
DMatrix& operator-=(DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix operator*(const DMatrix& m, const double r);
DMatrix operator*(const double r, const DMatrix& m);

DMatrix operator*(const DMatrix& m1, const DMatrix& m2);
DMatrix operator*(const DMatrix& m1, const DMatrixDiag& md2);
DMatrix operator*(const DMatrix& m1, const DMatrixConst& mc2);
DMatrix operator*(const DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix& operator*=(DMatrix& m, const double r);

DMatrix& operator*=(DMatrix& m1, const DMatrix& m2);
DMatrix& operator*=(DMatrix& m1, const DMatrixDiag& md2);
DMatrix& operator*=(DMatrix& m1, const DMatrixConst& mc2);
DMatrix& operator*=(DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix operator/(const DMatrix& m, const double r);
DMatrix& operator/=(DMatrix& m, const double r);

DMatrix operator^(const DMatrix& m1, const DMatrix& m2);
DMatrix operator^(const DMatrix& m1, const DMatrixDiag& md2);
DMatrix operator^(const DMatrix& m1, const DMatrixConst& mc2);
DMatrix operator^(const DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix operator|(const DMatrix& m1, const DMatrix& m2);
DMatrix operator|(const DMatrix& m1, const DMatrixDiag& md2);
DMatrix operator|(const DMatrix& m1, const DMatrixConst& mc2);
DMatrix operator|(const DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix& operator^=(DMatrix& m1, const DMatrix& m2);
DMatrix& operator^=(DMatrix& m1, const DMatrixDiag& md2);
DMatrix& operator^=(DMatrix& m1, const DMatrixConst& mc2);
DMatrix& operator^=(DMatrix& m1, const DMatrixCDiag& mcd2);

DMatrix& operator|=(DMatrix& m1, const DMatrix& m2);
DMatrix& operator|=(DMatrix& m1, const DMatrixDiag& md2);
DMatrix& operator|=(DMatrix& m1, const DMatrixConst& mc2);
DMatrix& operator|=(DMatrix& m1, const DMatrixCDiag& mcd2);

//!  Performs t += a*x operation and returns reference to t.
//! Works correctly even if some of matrices t, a, x are the same or share
// buffers
DMatrix& tpax(DMatrix& t, const DMatrix& a, const DMatrix& x);

//!  Performs dest = b + a*x operation and returns reference to dest.
//! Works correctly even if some of matrices dest, b, a, x are the same or share
// buffers
DMatrix& bpax(DMatrix& dest, const DMatrix& b, const DMatrix& a,
              const DMatrix& x);

//! Returns matrix multiplied by unit column vector
DMatrix t1(const DMatrix& m);

template <class UnaryOperator>
void BlkGenerate(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                 const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
                 unsigned int mSize, unsigned int nSize, UnaryOperator op) {
  dest.Clip();

  DMViewBlock destView(dest, mTo, nTo, mSize, nSize);
  DMViewBlockConst srcView(src, mFrom, nFrom, mSize, nSize);

  GenerateUnOp(destView.Begin(), destView.End(), srcView.Begin(), op);
}

template <class UnaryOperator>
void BlkGenerate(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                 const DMatrix& src, UnaryOperator op) {
  dest.Clip();

  DMViewBlock destView(dest, mTo, nTo, src.GetM(), src.GetN());

  GenerateUnOp(destView.Begin(), destView.End(), src.Begin(), op);
}

template <class UnaryOperator>
void BlkGenerate(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
                 unsigned int nFrom, UnaryOperator op) {
  dest.Clip();

  DMViewBlockConst srcView(src, mFrom, nFrom, dest.GetM(), dest.GetN());

  GenerateUnOp(dest.Begin(), dest.End(), srcView.Begin(), op);
}

//
template <class BinaryModOperator>
void BlkApply(DMatrix& dest, unsigned int mTo, unsigned int nTo,
              const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
              unsigned int mSize, unsigned int nSize, BinaryModOperator op) {
  dest.Clip();

  DMViewBlock destView(dest, mTo, nTo, mSize, nSize);
  DMViewBlockConst srcView(src, mFrom, nFrom, mSize, nSize);

  ApplyBinOp(destView.Begin(), destView.End(), srcView.Begin(), op);
}

template <class BinaryModOperator>
void BlkApply(DMatrix& dest, unsigned int mTo, unsigned int nTo,
              const DMatrix& src, BinaryModOperator op) {
  dest.Clip();

  DMViewBlock destView(dest, mTo, nTo, src.GetM(), src.GetN());

  ApplyBinOp(destView.Begin(), destView.End(), src.Begin(), op);
}

template <class BinaryModOperator>
void BlkApply(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
              unsigned int nFrom, BinaryModOperator op) {
  dest.Clip();

  DMViewBlockConst srcView(src, mFrom, nFrom, dest.GetM(), dest.GetN());

  ApplyBinOp(dest.Begin(), dest.End(), srcView.Begin(), op);
}

// Block operations specializations
void CopyBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
             unsigned int mSize, unsigned int nSize);

void CopyBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             const DMatrix& src);
void CopyBlk(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
             unsigned int nFrom);

void AddBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
            const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
            unsigned int mSize, unsigned int nSize);

void AddBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
            const DMatrix& src);
void AddBlk(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
            unsigned int nFrom);

void CopyBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
                unsigned int mSize, unsigned int nSize, double mul);

void CopyBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                const DMatrix& src, double mul);
void CopyBlkMul(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
                unsigned int nFrom, double mul);

void AddBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
               const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
               unsigned int mSize, unsigned int nSize, double mul);

void AddBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
               const DMatrix& src, double mul);
void AddBlkMul(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
               unsigned int nFrom, double mul);

void FillBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             unsigned int mSize, unsigned int nSize, const double r);

inline void SetBlkZero(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                       unsigned int mSize, unsigned int nSize) {
  FillBlk(dest, mTo, nTo, mSize, nSize, 0.);
}

//========================= DMatrixDiag ==================================
double Sum(const DMatrixDiag& m);
double Norm(const DMatrixDiag& m);
double NormDiff(const DMatrixDiag& m1, const DMatrixDiag& m2);

double ScalarMul(const DMatrixDiag& m1, const DMatrixDiag& m2);

DMatrixDiag Inversion(const DMatrixDiag& m);  // Returns inversed matrix
DMatrixDiag& Inverse(DMatrixDiag& m);         // Inverses the matrix

DMatrix operator+(const DMatrixDiag& m1, const DMatrix& m2);
DMatrixDiag operator+(const DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrix operator+(const DMatrixDiag& m1, const DMatrixConst& m2);
DMatrixDiag operator+(const DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag& operator+=(DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrixDiag& operator+=(DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag operator-(const DMatrixDiag& m);
DMatrixDiag& Neg(DMatrixDiag& m);

DMatrix operator-(const DMatrixDiag& m1, const DMatrix& m2);
DMatrixDiag operator-(const DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrix operator-(const DMatrixDiag& m1, const DMatrixConst& m2);
DMatrixDiag operator-(const DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag& operator-=(DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrixDiag& operator-=(DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrix operator*(const DMatrixDiag& m1, const DMatrix& m2);
DMatrixDiag operator*(const DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrix operator*(const DMatrixDiag& m1, const DMatrixConst& m2);
DMatrixDiag operator*(const DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag& operator*=(DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrixDiag& operator*=(DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag operator*(const double r, const DMatrixDiag& m);
DMatrixDiag operator*(const DMatrixDiag& m, const double r);

DMatrixDiag& operator*=(DMatrixDiag& m, const double r);

DMatrixDiag operator/(DMatrixDiag& m, const double r);
DMatrixDiag& operator/=(DMatrixDiag& m, const double r);

DMatrix operator^(const DMatrixDiag& m1, const DMatrix& m2);
DMatrixDiag operator^(const DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrix operator^(const DMatrixDiag& m1, const DMatrixConst& m2);
DMatrixDiag operator^(const DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag& operator^=(DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrixDiag& operator^=(DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrix operator|(const DMatrixDiag& m1, const DMatrix& m2);
DMatrixDiag operator|(const DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrix operator|(const DMatrixDiag& m1, const DMatrixConst& m2);
DMatrixDiag operator|(const DMatrixDiag& m1, const DMatrixCDiag& m2);

DMatrixDiag& operator|=(DMatrixDiag& m1, const DMatrixDiag& m2);
DMatrixDiag& operator|=(DMatrixDiag& m1, const DMatrixCDiag& m2);

//========================= DMatrixConst ==================================
double Norm(const DMatrixConst& m);

DMatrix operator+(const DMatrixConst& m1, const DMatrix& m2);
DMatrix operator+(const DMatrixConst& m1, const DMatrixDiag& m2);
DMatrixConst operator+(const DMatrixConst& m1, const DMatrixConst& m2);
DMatrix operator+(const DMatrixConst& m1, const DMatrixCDiag& m2);

DMatrix operator-(const DMatrixConst& m1, const DMatrix& m2);
DMatrix operator-(const DMatrixConst& m1, const DMatrixDiag& m2);
DMatrixConst operator-(const DMatrixConst& m1, const DMatrixConst& m2);
DMatrix operator-(const DMatrixConst& m1, const DMatrixCDiag& m2);

DMatrixConst operator-(const DMatrixConst& m);
DMatrixConst& Neg(DMatrixConst& m);

DMatrixConst& operator+=(DMatrixConst& m1, const DMatrixConst& m2);
DMatrixConst& operator-=(DMatrixConst& m1, const DMatrixConst& m2);

DMatrix operator*(const DMatrixConst& m1, const DMatrix& m2);
DMatrix operator*(const DMatrixConst& m1, const DMatrixDiag& m2);
DMatrixConst operator*(const DMatrixConst& m1, const DMatrixConst& m2);
DMatrixConst operator*(const DMatrixConst& m1, const DMatrixCDiag& m2);

DMatrixConst& operator*=(DMatrixConst& m1, const DMatrixConst& m2);
DMatrixConst& operator*=(DMatrixConst& m1, const DMatrixCDiag& m2);

DMatrixConst operator*(const DMatrixConst& m, const double r);
DMatrixConst operator*(const double r, const DMatrixConst& m);

DMatrixConst& operator*=(DMatrixConst& m, const double r);

DMatrixConst operator/(const DMatrixConst& m, const double r);
DMatrixConst& operator/=(DMatrixConst& m, const double r);

DMatrix operator^(const DMatrixConst& m1, const DMatrix& m2);
DMatrix operator^(const DMatrixConst& m1, const DMatrixDiag& m2);
DMatrixConst operator^(const DMatrixConst& m1, const DMatrixConst& m2);
DMatrix operator^(const DMatrixConst& m1, const DMatrixCDiag& m2);

DMatrixConst& operator^=(DMatrixConst& m1, const DMatrixConst& m2);

DMatrix operator|(const DMatrixConst& m1, const DMatrix& m2);
DMatrix operator|(const DMatrixConst& m1, const DMatrixDiag& m2);
DMatrix operator|(const DMatrixConst& m1, const DMatrixConst& m2);
DMatrix operator|(const DMatrixConst& m1, const DMatrixCDiag& m2);

//========================= DMatrixCDiag =================================
double Norm();

DMatrix operator+(const DMatrixCDiag& m1, const DMatrix& m2);
DMatrixDiag operator+(const DMatrixCDiag& m1, const DMatrixDiag& m2);
DMatrix operator+(const DMatrixCDiag& m1, const DMatrixConst& m2);
DMatrixCDiag operator+(const DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag& operator+=(DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag operator-(const DMatrixCDiag& m);
DMatrixCDiag& Neg(const DMatrixCDiag& m);

DMatrix operator-(const DMatrixCDiag& m1, const DMatrix& m2);
DMatrixDiag operator-(const DMatrixCDiag& m1, const DMatrixDiag& m2);
DMatrix operator-(const DMatrixCDiag& m1, const DMatrixConst& m2);
DMatrixCDiag operator-(const DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag& operator-=(DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag operator*(const DMatrixCDiag& m, const double r);
DMatrixCDiag operator*(const double r, const DMatrixCDiag& m);

DMatrix operator*(const DMatrixCDiag& m1, const DMatrix& m2);
DMatrixDiag operator*(const DMatrixCDiag& m1, const DMatrixDiag& m2);
DMatrixConst operator*(const DMatrixCDiag& m1, const DMatrixConst& m2);
DMatrixCDiag operator*(const DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag& operator*=(const DMatrixCDiag& m, const double r);
DMatrixCDiag& operator*=(DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag operator/(const DMatrixCDiag& m, const double r);
DMatrixCDiag& operator/=(DMatrixCDiag& m, const double r);

DMatrix operator^(const DMatrixCDiag& m1, const DMatrix& m2);
DMatrixDiag operator^(const DMatrixCDiag& m1, const DMatrixDiag& m2);
DMatrix operator^(const DMatrixCDiag& m1, const DMatrixConst& m2);
DMatrixCDiag operator^(const DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag& operator^=(DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrix operator|(const DMatrixCDiag& m1, const DMatrix& m2);
DMatrixDiag operator|(const DMatrixCDiag& m1, const DMatrixDiag& m2);
DMatrix operator|(const DMatrixCDiag& m1, const DMatrixConst& m2);
DMatrixCDiag operator|(const DMatrixCDiag& m1, const DMatrixCDiag& m2);

DMatrixCDiag& operator|=(const DMatrixCDiag& m, const DMatrixCDiag& m2);

}  // namespace QLib

#endif  //__QMatrixArithm_h__
