/********************************************************************
        created:	2:2:2006   13:33
*********************************************************************/

#ifndef __QCalcModule_h__
#define __QCalcModule_h__

#include "ELib/ECopyProtected.h"
#include "ELib/EXception.h"

#include <string>

namespace ELib {
class ILogger;
class IProgressor;
}

namespace QLib {
class ConfigFile;

class ICalcModule {
 public:
  virtual ~ICalcModule() = default;

  virtual void Calc(ConfigFile& config, ELib::ILogger& logger,
                    ELib::IProgressor& progressor) = 0;
};

}  // namespace QLib

#endif  //__QCalcModule_h__
