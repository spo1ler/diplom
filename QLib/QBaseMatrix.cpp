/********************************************************************
        created:	17:2:2006   19:59
        filename: QBaseMatrix.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QBaseMatrix.h"

#include "ELib/EXception.h"

namespace QLib {

void CheckAllowSum(const BaseMatrix& m1, const BaseMatrix& m2) {
  if (m1.GetM() != m2.GetM() || m1.GetN() != m2.GetN()) {
    throw ELib::Exception(
        "QLib::CheckAllowSum(const BaseMatrix& m1, const BaseMatrix& m2) "
        "failed. "
        "A:%dx%d  B:%dx%d",
        m1.GetM(), m1.GetN(), m2.GetM(), m2.GetN());
  }
}

void CheckAllowMul(const BaseMatrix& m1, const BaseMatrix& m2) {
  if (m1.GetN() != m2.GetM()) {
    throw ELib::Exception(
        "QLib::CheckAllowMul(const BaseMatrix& m1, const BaseMatrix& m2) "
        "failed. "
        "A:%dx%d  B:%dx%d",
        m1.GetM(), m1.GetN(), m2.GetM(), m2.GetN());
  }
}

void CheckAllowKSum(const BaseMatrix& m1, const BaseMatrix& m2) {
  if (m1.GetM() != m1.GetN() && m2.GetM() != m2.GetN()) {
    throw ELib::Exception(
        "QLib::CheckAllowKSum(const BaseMatrix& m1, const BaseMatrix& m2) "
        "failed. "
        "A:%dx%d  B:%dx%d",
        m1.GetM(), m1.GetN(), m2.GetM(), m2.GetN());
  }
}

void BaseMatrix::CheckMSizeIn(unsigned int m) const {
  if (m >= GetM()) {
    throw ELib::Exception(
        "QLib::BaseMatrix::CheckMSizeIn(int m[%d]) failed: "
        "(m < GetM()[%d])",
        m, GetM());
  }
}

void BaseMatrix::CheckNSizeIn(unsigned int n) const {
  if (n >= GetN()) {
    throw ELib::Exception(
        "QLib::BaseMatrix::CheckNSizeIn(int n[%d]) failed: "
        "(n < GetN()[%d])",
        n, GetN());
  }
}

void BaseMatrix::CheckMNSizeIn(unsigned int m, unsigned int n) const {
  if (!(m < GetM() && n < GetN())) {
    throw ELib::Exception(
        "QLib::BaseMatrix::CheckMNSizeIn("
        "unsigned int m[%d], unsigned int n[%d]) failed: (m < GetM()[%d] && n "
        "< GetN()[%d])",
        m, n, GetM(), GetN());
  }
}

}  // namespace QLib
