/********************************************************************
  created:  28.11.06    09:30
  filename: QKroneckerPowers.h
  author:   Dmitry Orlovsky

  purpose:  Structures for calculating, storing and accessing Kronecker powers
*********************************************************************/

#ifndef __QKroneckerPowers_h__
#define __QKroneckerPowers_h__

#include "QLib/QMatrix.h"

namespace QLib {

enum KroneckerOperation { KO_OTIMES, KO_OPLUS, KO_NOT_SPECIFIED };

class KroneckerPowers {
 public:
  KroneckerPowers();

  //! Initializes the internal array with [0..initialMaxPower] Kronecker powers
  // of mBase matrix
  /*!
   If initialMaxPower is equal to 0, its value is assumed to be 1 automatically
   The Kronecker power for OPLUS operation is defined for square matrices and
   vectors only.
   The growIfNeeded flag determines the behavior in case when power i greater
   than MaxPower()
    is requested by Get() operation. If the flag is set to 'true', powers
   [MaxPower() + 1, i]
    are generated and stored, and then the power i is returned. Otherwise an
   exception is thrown.
  */
  KroneckerPowers(const DMatrix& mBase, KroneckerOperation operation,
                  unsigned int initialMaxPower, bool growIfNeeded = true);

  // Reinitializes (or initializes, if object was cleared or created with
  // default constructor) the object.
  /*!
    Parameters are the same as in constructor.
    \sa KroneckerPowers::KroneckerPowers
  */
  void Init(const DMatrix& mBase, KroneckerOperation operation,
            unsigned int initialMaxPower, bool growIfNeeded = true);

  void Clear();

  //! Returns maximum stored power.
  /*!
    If object is not initialized, returns (unsigned int)(-1)
  */
  inline unsigned int MaxPower() const;

  //! Returns maximum stored power.
  const DMatrix& Get(unsigned int power);

 protected:
  //! Calculates powers [MaxPower() + 1, power]
  void CalculatePowersUpTo(unsigned int power);

 private:
  bool m_growIfNeeded;
  KroneckerOperation m_operation;
  DMatrixCDiag m_dmOplusOperand;

  VectorDM m_amPowers;
};

// Returns Kronecker powers of matrices sequentially, and stores only the last
// calculated power
// TODO: implement
/*
class SequentialPowersCalculator
{
public:
  SequentialPowersCalculator();
  SequentialPowersCalculator(const DMatrix& mBase, KroneckerOperation
operation);

  void Init(const DMatrix& mBase, KroneckerOperation operation);

  // Each time we call this function, the power mus be not less than previous
time
  //  If object is just initialized, any power can be ordered
  DMatrix Get(unsigned int power);

  // Returns next power. If object is just initialized, next power is 0
  DMatrix GetNextPower(unsigned int* pReturnedPower);

private:
  KroneckerOperation m_operation;
  DMatrixCDiag m_dmOplusOperand;

  DMatrix m_mBase;
  DMatrix m_mCurrentPower;

  bool m_isJustInitialized;
  unsigned int m_nLastReturnedPower;
};
*/

// KroneckerPowers implementation
inline unsigned int KroneckerPowers::MaxPower() const {
  return m_amPowers.size() - 1;
}

}  // namespace QLib

#endif  //__QKroneckerPowers_h__
