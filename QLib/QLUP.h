/********************************************************************
        created:	8:11:2005   18:04
        filename: QLUP.h
        author:		Dmitry Orlovsky

        purpose:	LUP-decomposition class. Used in LAES solving
*********************************************************************/

#ifndef __QLUP_h__
#define __QLUP_h__

#include "ELib/EXception.h"
#include "QLib/QMatrix.h"

namespace QLib {

//! Calculates determinant of square matrix
double Det(const DMatrix &m);

//! Inverts M and returns result
DMatrix Inversion(const DMatrix &m);

//! Solves linear system M*x = b and returns x
DMatrix LSolve(const DMatrix &mM, const DMatrix &b);

//! Solves linear system x*mM == 0 && x*vf == 1 and returns x
DMatrix LSolveRowNorm(const DMatrix &mM, const DMatrix &vf);

//! Solves linear system x*mM == 0 && x*e == 1 and returns x
DMatrix LSolveRowNorm(const DMatrix &mM);

}  // namespace QLib

#endif  //__QLUP_h__
