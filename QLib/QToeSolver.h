/********************************************************************
created:	18:11:2005   11:46
filename: QToeSolver.h
author:		Dmitry Orlovsky

purpose:	Quasi-Toeplits Markov chain solver
*********************************************************************/

#ifndef __QToeSolver_h__
#define __QToeSolver_h__

#include "ELib/ELogProg.h"
#include "QLib/QBlockDMatrix.h"
#include "QLib/QEngineStructures.h"

namespace QLib {

//! Calculates stationary probabilities of Markov chains with finite number
//! of states and of quasi-Toeplitz Markov chains
class ToeSolver : protected ELib::LogProg {
 public:
  //! Calculation algorithm parameters
  struct Parameters {
    double precisionG;  //! Error norm to be achieved when calculating G
    unsigned int
        maxIterationsG;  //! Maximum number of iterations when calculating G
    double precisionPi;  //! Estimate of norm of last calculated Pi entry
    unsigned int maxPi;  //! Maximum number Pi vectors to be calculated

    //! Retrieves the default processing settings for ToeSolver
    static const Parameters& GetDefault();
  };

  //! Constructs the ToeSolver object with given logger and progressor
  /*!
    \sa ELib::ILogger, ELib::IProgressor
  */
  ToeSolver(ELib::ILogger& logger, ELib::IProgressor& progressor);

  //! Destroys an object
  virtual ~ToeSolver() = default;

  //! Finds the stationary probability vectors of the Markov chain with finite
  // infinitesimal generator
  /*!
    \param q infinitesimal generator of the Markov chain
    \return array of stationary probability vectors

    \sa PiVector, IBlockDMatrix
  */
  PiVector Solve(const IBlockDMatrix& q);

  //! Finds the stationary probability vectors of the Markov chain with infinite
  // infinitesimal generator
  /*!
    \param q          infinitesimal generator of the Markov chain
    \param uniqueRows number of unique rows of the generator:
                        given i >= uniqueRows - 1  we must have Q[i,l] = Q_{l-i}
    \param params     calculation parameters, see ToeSolver::Parameters for
    details
    \return           array of stationary probability vectors

    \sa PiVector, IBlockDMatrix, ToeSolver::Parameters
  */
  PiVector Solve(const IBlockDMatrix& q, unsigned int uniqueRows,
                 const Parameters& params = Parameters::GetDefault());

  //! Returns the number of calculated F_l matrices
  unsigned int GetFlNumber() const { return m_aFl.size(); }

 protected:
  inline bool IsFinite() const { return m_bIsFinite; }

  inline unsigned int URows() const { return m_nOfUniqueRows; }

  inline unsigned int MaxState() const { return m_maxState; }

  inline const Parameters& GetParameters() const { return m_params; }

  inline const IBlockDMatrix& Q() const { return *m_pQ; }

  inline const DMatrix& Fl(unsigned int l) const { return m_aFl.at(l); }

  //! Pre-validates the generator and determines the maximal state reachable
  // from unique rows
  virtual void ValidateQ(unsigned int& maxState);

  //! Calculates G matrix, such that matrices Gk[i] == G, i >= Unique().
  virtual void CalculateG(DMatrix& mG);

  //! Calculates Gk[0..Unique()] array, Gk[Unique()] must be set to G.
  virtual void CalculateGk(VectorDM& aGk);

  virtual const DMatrix& G(unsigned int i) const;

  //! Calculates Fl matrices array. \bar Q_{0,0} is stored to Fl[0], and F_l to
  // Fl[l], l >= 1.
  virtual void CalculateFl(VectorDM& aFl);

  //! Calculates Pi matrices array. First record in array is F_1
  virtual void CalculatePi(VectorDM& aPi);

 private:
  Parameters m_params;

  bool m_bIsFinite = false;
  unsigned int m_maxState = 0;
  unsigned int m_nOfUniqueRows = 0;

  const IBlockDMatrix* m_pQ = nullptr;
  VectorDM m_aG;
  VectorDM m_aFl;
  VectorDM m_aStoredQil;
};

}  // namespace QLib

#endif  //__QToeSolver_h__
