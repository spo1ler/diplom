/********************************************************************
        created:	8:11:2003   17:23
        filename: QPHDist.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QPHDist.h"

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QConfigFile.h"
#include "QLib/QMatrixArithm.h"
#include "QLib/QLUP.h"

namespace QLib {

PHDist::PHDist() {}

PHDist::PHDist(const DMatrix& mS, const DMatrix& vBeta) {
  Validate(mS, vBeta);

  m_mS = mS;
  m_vBeta = vBeta;
}

void PHDist::Validate(const DMatrix& mS, const DMatrix& vBeta) {
  ELib::FormatStr s;
  if (mS.GetM() != mS.GetN())
    s.Format("PHDist::Validate: matrix S(%d,%d) must be square", mS.GetM(),
             mS.GetN());
  else if (vBeta.GetM() != 1)
    s.Format("PHDist::Validate: Beta(%d,%d) must be a row matrix", vBeta.GetM(),
             vBeta.GetN());
  else if (mS.GetM() != vBeta.GetN()) {
    s.Format(
        "PHDist::Validate: S(%d,%d) and Beta(%d,%d) dimensions must correspond",
        mS.GetM(), mS.GetN(), vBeta.GetM(), vBeta.GetN());
  } else if (mS.GetM() == 0)
    s.Format("PHDist::Validate: matrix dimensions can't be zero");

  double rSum = 0;
  for (unsigned int i = 0; i < vBeta.GetDLen() && s.empty(); ++i) {
    const double crAdd = vBeta.GetV(i);
    if (crAdd < 0)
      s.Format("PHDist::Validate: Beta has negative entry #%d = %g", i, crAdd);
    else
      rSum += crAdd;
  }
  if (s.empty() && fabs(rSum - 1.) > 1e-12)
    s.Format("PHDist::Validate: Beta is non-stochastic, Sum = %g", rSum);

  for (unsigned int i = 0; i < mS.GetM() && s.empty(); ++i) {
    rSum = 0;
    for (unsigned int j = 0; j < mS.GetN() && s.empty(); ++j) {
      if (i == j && mS.Get(i, j) >= 0)
        s.Format("PHDist::Validate: M(%d,%d)=%g, while must be negative", i, j,
                 mS.Get(i, j));
      else if (i != j && mS.Get(i, j) < 0)
        s.Format("PHDist::Validate: M(%d,%d)=%g, while must be non-negative", i,
                 j, mS.Get(i, j));
      else
        rSum += mS.Get(i, j);
    }

    if (s.empty() && rSum > 1e-12)
      s.Format(
          "PHDist::Validate: sum of %d-th Ms row =%g, while must be "
          "non-positive",
          i, rSum);
  }

  if (!s.empty()) throw ELib::Exception(s);
}

void PHDist::Init(const DMatrix& mS, const DMatrix& vBeta) {
  Validate(mS, vBeta);

  m_mS = mS;
  m_vBeta = vBeta;
}

const DMatrix PHDist::GetS0() const { return -t1(m_mS); }

void PHDist::SetIntensity(double intensity) {
  if (intensity <= 0)
    throw ELib::Exception(
        "PHDist::::SetIntensity(intensity[%g]) failed: intensity must be "
        "positive",
        intensity);

  m_mS *= intensity / GetIntensity();
}

double PHDist::GetIntensity() const {
  return -1. / Sum(m_vBeta * Inversion(m_mS));
}

double PHDist::GetVariation() const {
  const double intensity = GetIntensity();
  const DMatrix mSInv = Inversion(m_mS);

  return 2. * Sum(m_vBeta * mSInv * mSInv) * intensity * intensity - 1.;
}

// ICfgObject interface
void PHDist::CfgGet(const ConfigFile& config, const std::string& section) {
  if (const ConfigSection* pSection = config.SectionByName(section)) {
    DMatrix mS, vBeta;

    QLib::CfgGet(mS, pSection, "S");
    QLib::CfgGet(vBeta, pSection, "beta");

    Init(mS, vBeta);
  } else
    throw ELib::Exception("ReadFromConfig(PHDist& ...): can`t find section %s",
                          section.c_str());
}

void PHDist::CfgSet(ConfigFile& config, const std::string& section) const {
  QLib::CfgSet(config, section, "S", GetS());
  QLib::CfgSet(config, section, "beta", GetBeta());
}

void PHDist::Dump(ELib::ILogger& logger) const {
  logger << "Dumping PH:\n";
  logger << "S = { " << GetS() << "\n}\n";
  logger << "beta = { " << GetBeta() << "}\n";
}

void PHDist::CfgSetInfo(ConfigFile& config, const std::string& section) const {
  CfgSetDbl(config, section, "PH_Intensity", GetIntensity());
  CfgSetDbl(config, section, "PH_Variation", GetVariation());
}

void PHDist::DumpInfo(ELib::ILogger& logger) const {
  logger << "Dumping PH info:\n";
  logger << "PH_Intensity" << GetIntensity() << '\n';
  logger << "PH_Variation" << GetVariation() << '\n';
}

}  // namespace QLib
