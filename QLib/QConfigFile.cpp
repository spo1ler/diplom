/********************************************************************
        created:	8:11:2000   18:02
        filename: QConfig.cpp
        author:		Gennady Tsarenkov

        purpose:
*********************************************************************/

#include "QLib/QConfigFile.h"

#ifdef _MSC_VER
#include <io.h>
#define ftruncate chsize
#else
#include <unistd.h>
#endif

#include <string.h>

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

namespace QLib {
using namespace ELib;

//!  Fills the STL string with part of C-style string
//
// Arguments:
//  s - destination string
//  str - source string
//  pos - zero-based start position index of fragment being copied
//  len - lenght of fragment
static void SetByFragment(std::string& s, const char* str, int pos, int len) {
  s.assign(str + pos, len);
}

//!  Unescapes the part of string and puts the result into STL string
//
// Arguments:
//  s - destination string
//  str - source string
//  pos - zero-based start position index of fragment being unescaped
//  len - lenght of fragment
static void SetEscape(std::string& s, const std::string& str, int pos,
                      int len) {
  int isrc, idst, framing_quotas;

  std::string text(str.substr(pos, len));

  for (framing_quotas = idst = isrc = 0;
       isrc < len && isrc < (int)text.length(); ++isrc) {
    if (isrc == 0 && text[isrc] == '"') {
      framing_quotas = 1;
      continue;
    }

    if (text[isrc] == '\\') {
      switch (text[++isrc]) {
        case 'a':
          text[idst++] = '\a';
          break;
        case 'n':
          text[idst++] = '\n';
          break;
        case 'r':
          text[idst++] = '\r';
          break;
        case 't':
          text[idst++] = '\t';
          break;
        case '"':
          text[idst++] = '"';
          break;
        case '\\':
        default:
          // Do nothing.
          break;
      }
    } else
      text[idst++] = text[isrc];
  }
  if (text[isrc - 1] == '"' && framing_quotas) --idst;

  s = text.substr(0, idst);
}

// **********************************
// *  ConfigRecord implementation
// **********************************
ConfigRecord::ConfigRecord(ConfigRecordType t) {
  pos_start = pos_end = 0;

  switch (type = t) {
    case cfgString:
      value.str = new std::string();
      break;
    case cfgArray:
      value.array = new StringArray();
      break;
    default:
      type = cfgUnknown;
      memset(&value, 0, sizeof(value));
  }
}

//---------------------------------------------------------
ConfigRecord::ConfigRecord(const ConfigRecord& crec) {
  name = crec.name;
  SetStart(crec.GetStart());
  SetEnd(crec.Ind());

  switch (type = crec.GetType()) {
    case cfgString:
      value.str = new std::string(*crec.value.str);
      break;
    case cfgArray:
      value.array = new StringArray(*crec.value.array);
      break;
    default:
      type = cfgUnknown;
      memset(&value, 0, sizeof(value));
  }
}

//---------------------------------------------------------
ConfigRecord::~ConfigRecord() {
  switch (type) {
    case cfgString:
      if (value.str) delete value.str;
      break;
    case cfgArray:
      if (value.array) delete value.array;
      break;
    default:
      break;
  }
}

//---------------------------------------------------------
ConfigRecord& ConfigRecord::operator=(const ConfigRecord& crec) {
  if (this == &crec) return *this;

  switch (type) {
    case cfgString:
      delete value.str;
      break;
    case cfgArray:
      delete value.array;
      break;
    default:
      throw Exception("Unknown configuration value type");
  }
  type = crec.type;
  name = crec.name;
  pos_start = crec.pos_start;
  pos_end = crec.pos_end;

  switch (type) {
    case cfgString:
      value.str = new std::string(*crec.value.str);
      break;
    case cfgArray:
      value.array = new StringArray(*crec.value.array);
      break;
    default:
      throw Exception("Unknown configuration value type");
  }
  return *this;
}

//---------------------------------------------------------
void ConfigRecord::SetValue(const std::string& val) {
  switch (type) {
    case cfgArray:
      if (value.array) delete value.array;
      value.str = new std::string(val);
      type = cfgString;
      break;
    case cfgString:
      *value.str = val;
      break;
    default:
      throw Exception("Unknown configuration value type");
  }
}

//---------------------------------------------------------
void ConfigRecord::SetValue(const StringArray& val) {
  switch (type) {
    case cfgString:
      if (value.str) delete value.str;
      value.array = new StringArray(val);
      type = cfgArray;
      break;
    case cfgArray:
      *(value.array) = val;
      break;
    default:
      throw Exception("Unknown configuration value type");
  }
}

// ***********************************
// *  ConfigSection implementation  *
// ***********************************
ConfigSection::ConfigSection() {
  pos_start = pos_end = 0;
  pos_contentstart = pos_contentend = 0;
}

ConfigSection::ConfigSection(const std::string& nstr, int npos,
                             unsigned int nlen) {
  SetEscape(name, nstr, npos, nlen);

  pos_start = pos_end = 0;
  pos_contentstart = pos_contentend = 0;
}

ConfigSection::ConfigSection(const ConfigSection& csec)
    : ConfigSectionBase(csec) {
  pos_start = csec.pos_start;
  pos_end = csec.pos_end;
  pos_contentstart = csec.pos_contentstart;
  pos_contentend = csec.pos_contentend;
  name = csec.name;
}

ConfigSection& ConfigSection::operator=(const ConfigSection& csec) {
  if (this != &csec) {
    pos_start = csec.pos_start;
    pos_end = csec.pos_end;
    pos_contentstart = csec.pos_contentstart;
    pos_contentend = csec.pos_contentend;

    name = csec.name;
    ConfigSectionBase::operator=(csec);
  }
  return *this;
}

void ConfigSection::AddValue(const std::string& n, const std::string& value,
                             unsigned int start, unsigned int end) {
  ConfigRecord rec(ConfigRecord::cfgString);

  rec.name = n;
  rec.SetValue(value);
  rec.SetStart(start);
  rec.SetEnd(end);
  push_back(rec);
}

void ConfigSection::AddValueArray(const std::string& n,
                                  const StringArray& value, unsigned int start,
                                  unsigned int end) {
  ConfigRecord rec;

  rec.name = n;
  rec.SetValue(value);
  rec.SetStart(start);
  rec.SetEnd(end);
  push_back(rec);
}

ConfigRecord* ConfigSection::RecordByName(const std::string& sName) {
  for (iterator it = begin(); it != end(); ++it) {
    if (it->name == sName) return &(*it);
  }
  return NULL;
}

const ConfigRecord* ConfigSection::RecordByName(
    const std::string& sName) const {
  for (const_iterator it = begin(); it != end(); ++it) {
    if (it->name == sName) return &(*it);
  }
  return NULL;
}

ConfigFile::ConfigFile(const std::string& fn)
    : state(CONF_FNSET), filename(fn), fp(NULL) {}

ConfigFile::~ConfigFile() {
  if (fp && !(state | CONF_FSET)) fclose(fp);
}

int ConfigFile::SkipSpaces(const char* text, unsigned int len, int i) {
  for (; (unsigned int)i < len; i++) {
    switch (text[i]) {
      case '\n':
        ++lineno;
      // Fallthrough.
      case '\r':
      case ' ':
      case '\t':
        break;
      case '/':
        switch (text[i + 1]) {
          case '*':
            for (i += 2; (unsigned int)i < len - 1 &&
                             (text[i] != '*' || text[i + 1] != '/');
                 i++)
              ;

            if (text[i] == '*' && text[i + 1] == '/')
              i++;
            else {
              i = len;
              goto SkipSpaceExit;
            }
            break;
          case '/':
            // !!! \r symbol is skipped (if necessary
            // it will proceeded next iteration)
            for (i += 2; (unsigned int)i < len && text[i] == '\n'; i++)
              ;
            if ((unsigned int)i == len) {
              i = len;
              goto SkipSpaceExit;
            }
            break;
          default:
            goto SkipSpaceExit;
        }
        break;

      default:
        goto SkipSpaceExit;
    }
  }
SkipSpaceExit:
  return i;
}

int ConfigFile::GetToken(const char* s, unsigned int len, int pos, int* tpos,
                         unsigned int* tlen) {
  *tlen = 0;
  if ((unsigned)(*tpos = SkipSpaces(s, len, pos)) >= len) goto GetTokenExit;

  *tlen = 0;
  if (s[*tpos + *tlen] == '"') {
    for ((*tlen)++; *tpos + *tlen <= len && s[*tpos + *tlen] != '\0';
         (*tlen)++) {
      if (s[*tpos + *tlen] == '"' && *tpos + *tlen > 0 &&
          s[*tpos + *tlen - 1] != '\\') {
        ++(*tlen);
        break;
      }
    }
  } else {
    while (*tpos + *tlen <= len && !isspace(s[*tpos + *tlen]) &&
           s[*tpos + *tlen] != '\0')
      (*tlen)++;
  }

GetTokenExit:
  return *tpos + *tlen;
}

int ConfigFile::ParseConfigText(const char* text, unsigned int lsize) {
  unsigned int tlen;
  int i, tpos, ipos, vstart, vend;
  std::string s1, s2, valname, tok, val;

  lineno = 1;
  for (i = 0; (unsigned)i < lsize; i++) {
    i = GetToken(text, lsize, i, &tpos, &tlen);
    SetByFragment(s2, text, tpos, tlen);
    if (s2 == "section") {
      ipos = tpos;
      i = GetToken(text, lsize, i, &tpos, &tlen);

      ConfigSection sec(text, tpos, tlen);
      sec.SetStart(ipos);

      i = GetToken(text, lsize, i, &tpos, &tlen);

      if (text[tpos] == '{' && tlen == 1) {
        sec.SetContentStart(tpos);
        for (i = GetToken(text, lsize, i, &tpos, &tlen);
             (text[tpos] != '}' || i - tpos != 1) && (unsigned)tpos < lsize;
             i = GetToken(text, lsize, i, &tpos, &tlen)) {
          vstart = tpos - sec.GetContentStart();
          SetByFragment(valname, text, tpos, tlen);

          i = GetToken(text, lsize, i, &tpos, &tlen);
          SetByFragment(tok, text, tpos, tlen);

          if (tok != "=") break;

          i = GetToken(text, lsize, i, &tpos, &tlen);
          SetByFragment(val, text, tpos, tlen);

          if (tlen == 1 && text[tpos] == '{') {
            // parse and set array of values
            StringArray vals;

            for (i = GetToken(text, lsize, i, &tpos, &tlen);
                 (text[tpos] != '}' || i - tpos != 1) && (unsigned)tpos < lsize;
                 i = GetToken(text, lsize, i, &tpos, &tlen)) {
              SetByFragment(val, text, tpos, tlen);
              vals.push_back(val);
            }
            vend = tpos + tlen - sec.GetContentStart() - 1;
            sec.AddValueArray(valname.c_str(), vals, vstart, vend);
          } else {
            // parse and set value
            vend = tpos + tlen - sec.GetContentStart() - 1;
            sec.AddValue(valname.c_str(), val.c_str(), vstart, vend);
          }
        }
        if ((unsigned)tpos >= lsize && (text[tpos] != '}' || i - tpos != 1)) {
        } else {
          sec.SetContentEnd(tpos);
          sec.SetEnd(tpos);
          push_back(sec);
        }
      }
    }
  }
  return 0;
}

FILE* ConfigFile::BindQConfigFile() {
  if (state & CONF_FNSET) {
    if (!(fp = fopen(filename.c_str(), "r+b")))
      fp = fopen(filename.c_str(), "w+b");
  } else {
    if (!state & CONF_FSET) fp = NULL;
  }

  return fp;
}

void ConfigFile::UnbindQConfigFile() {
  if ((state & CONF_FNSET) && fp) fclose(fp);
}

void ConfigFile::Read() {
  if (FILE* fa = BindQConfigFile()) {
    unsigned int lstart, lsize;

    startat = lstart = ftell(fa);
    fseek(fa, 0, SEEK_END);
    lsize = ftell(fa);
    fseek(fa, lstart, SEEK_SET);

    if (lsize > lstart) {
      std::string text;
      text.resize(lsize - lstart, '\0');
      fread(&text[0], lsize - lstart, 1, fa);

      ParseConfigText(text.c_str(), text.length());
    }

    UnbindQConfigFile();
  }
}

ConfigSection* ConfigFile::SectionByName(const std::string& sName) {
  std::string nm;
  SetEscape(nm, sName, 0, sName.length());

  for (iterator it = begin(); it != end(); ++it) {
    if (it->name == nm) return &(*it);
  }
  return NULL;
}

const ConfigSection* ConfigFile::SectionByName(const std::string& sName) const {
  std::string nm;
  SetEscape(nm, sName, 0, sName.length());

  for (const_iterator it = begin(); it != end(); ++it) {
    if (it->name == nm) return &(*it);
  }
  return NULL;
}

void ConfigFile::SectionCleanup(const std::string& sName) {
  FILE* fa;
  unsigned int lstart, lsize, deltalen;
  int i, spos;
  std::string s1, s2, valname, tok, val;
  ConfigSection* sec, *tsec;

  if (!(sec = SectionByName(sName)) || !(fa = BindQConfigFile())) return;

  lstart = startat;
  fseek(fa, 0, SEEK_END);
  lsize = ftell(fa);
  fseek(fa, lstart, SEEK_SET);

  std::string text;
  text.resize(lsize - lstart, '\0');
  fread(&text[0], lsize - lstart, 1, fa);
  lsize = text.length();

  deltalen = sec->GetContentEnd() - sec->GetContentStart() - 2;
  text[sec->GetContentStart() + 1] = ' ';
  for (spos = i = sec->GetContentEnd(); (unsigned int)i < lsize; i++)
    text[i - deltalen] = text[i];

  for (i = 0; i < static_cast<int>(size()); i++) {
    tsec = &at(i);
    if (tsec == sec) continue;

    if (tsec->GetStart() > spos) {
      tsec->SetStart(tsec->GetStart() - deltalen);
      tsec->SetEnd(tsec->Ind() - deltalen);
      tsec->SetContentStart(tsec->GetContentStart() - deltalen);
      tsec->SetContentEnd(tsec->GetContentEnd() - deltalen);
    }
  }
  sec->SetContentEnd(sec->GetContentStart() + 2);
  sec->SetEnd(sec->GetContentEnd());
  sec->clear();
  fseek(fa, lstart, SEEK_SET);
  fwrite(text.c_str(), lsize - deltalen, 1, fa);
  ftruncate(fileno(fa), ftell(fa));

  UnbindQConfigFile();
}

void ConfigFile::SectionDelete(const std::string& sName) {
  FILE* fa;
  unsigned int lstart, lsize, deltalen;
  int i, j, spos;
  std::string s1, s2, valname, tok, val;
  ConfigSection* sec, *tsec;

  if (!(sec = SectionByName(sName)) || !(fa = BindQConfigFile())) return;

  lstart = startat;
  fseek(fa, 0, SEEK_END);
  lsize = ftell(fa);
  fseek(fa, lstart, SEEK_SET);

  std::string text;
  text.resize(lsize - lstart, '\0');
  fread(&text[0], lsize - lstart, 1, fa);
  lsize = text.length();

  deltalen = sec->Ind() - sec->GetStart() + 1;
  for (spos = i = sec->Ind() + 1; (unsigned int)i < lsize; i++)
    text[i - deltalen] = text[i];

  for (j = -1, i = 0; i < static_cast<int>(size()); i++) {
    tsec = &at(i);
    if (tsec == sec) {
      j = i;
      continue;
    }

    if (tsec->GetStart() > spos) {
      tsec->SetStart(tsec->GetStart() - deltalen);
      tsec->SetEnd(tsec->Ind() - deltalen);
      tsec->SetContentStart(tsec->GetContentStart() - deltalen);
      tsec->SetContentEnd(tsec->GetContentEnd() - deltalen);
    }
  }
  if (j >= 0) erase(begin() + j);
  fseek(fa, lstart, SEEK_SET);
  fwrite(text.c_str(), lsize - deltalen, 1, fa);
  ftruncate(fileno(fa), ftell(fa));

  UnbindQConfigFile();
}

void ConfigFile::RecordDelete(const std::string& sSectionName,
                              const std::string& sName) {
  FILE* fa;
  unsigned int lstart, lsize, deltalen;
  int i, j, spos;
  std::string s1, s2, valname, tok;
  ConfigSection* sec, *tsec;
  ConfigRecord* val, *vl, *pr, *nx;

  if (!(sec = SectionByName(sSectionName)) || !(val = sec->RecordByName(sName)))
    return;

  for (j = -1, pr = nx = NULL, i = 0; i < static_cast<int>(sec->size()); i++) {
    if ((vl = &(sec->at(i))) == val) {
      j = i;
      continue;
    }

    if ((!pr && vl->Ind() < val->GetStart()) ||
        (pr && pr->Ind() < vl->Ind() && vl->Ind() < val->GetStart())) {
      pr = vl;
    }
    if ((!nx && vl->GetStart() > val->Ind()) ||
        (nx && nx->GetStart() > vl->GetStart() &&
         vl->GetStart() > val->Ind())) {
      nx = vl;
    }
  }
  if (j < 0) return;

  spos = 0;
  deltalen = 0;
  if (pr && nx) {
    deltalen = val->Ind() - pr->Ind();
    spos = val->Ind() + 1;
  } else if (!pr && !nx) {
    SectionCleanup(sSectionName);
  } else if (!pr) {
    deltalen = nx->GetStart() - val->GetStart();
    spos = nx->GetStart();
  } else if (!nx) {
    deltalen = val->Ind() - pr->Ind();
    spos = val->Ind() + 1;
  }

  if (!deltalen || !(fa = BindQConfigFile())) return;

  lstart = startat;
  fseek(fa, 0, SEEK_END);
  lsize = ftell(fa);
  fseek(fa, lstart, SEEK_SET);

  std::string text;
  text.resize(lsize - lstart, '\0');
  fread(&text[0], lsize - lstart, 1, fa);
  lsize = text.length();

  for (i = spos + sec->GetContentStart(); (unsigned int)i < lsize; i++)
    text[i - deltalen] = text[i];

  sec->erase(sec->begin() + j);
  for (i = 0; i < static_cast<int>(size()); i++) {
    tsec = &at(i);
    if (tsec == sec) {
      sec->SetContentEnd(sec->GetContentEnd() - deltalen);
      sec->SetEnd(sec->Ind() - deltalen);
      for (j = 0; j < static_cast<int>(sec->size()); j++) {
        vl = &(sec->at(j));
        if (static_cast<int>(vl->GetStart()) > spos) {
          vl->SetStart(vl->GetStart() - deltalen);
          vl->SetEnd(vl->Ind() - deltalen);
        }
      }
      continue;
    }

    if (tsec->GetStart() > spos) {
      tsec->SetStart(tsec->GetStart() - deltalen);
      tsec->SetEnd(tsec->Ind() - deltalen);
      tsec->SetContentStart(tsec->GetContentStart() - deltalen);
      tsec->SetContentEnd(tsec->GetContentEnd() - deltalen);
    }
  }
  fseek(fa, lstart, SEEK_SET);
  fwrite(text.c_str(), lsize - deltalen, 1, fa);
  ftruncate(fileno(fa), ftell(fa));

  UnbindQConfigFile();
}

void ConfigFile::Write(const std::string& sSectionName,
                       const std::string& sName, const std::string& value) {
  int blen;
  FILE* fa;
  unsigned int lstart, lsize;

  blen = sName.length() + value.length() + 100;
  ConfigSection* sec = SectionByName(sSectionName);
  if (sec)
    RecordDelete(sSectionName, sName);
  else
    blen += sSectionName.length() + 100;

  if (!(fa = BindQConfigFile())) return;

  if (!sec) {
    FormatStr buff("\nsection %s {\n    %s = %s\n}", sSectionName.c_str(),
                   sName.c_str(), value.c_str());

    fseek(fa, 0, SEEK_END);
    fwrite(buff.c_str(), buff.length(), 1, fa);
  } else {
    lstart = startat;
    fseek(fa, 0, SEEK_END);
    lsize = ftell(fa);
    fseek(fa, lstart, SEEK_SET);

    std::string text;
    text.resize(lsize - lstart, '\0');
    fread(&text[0], lsize - lstart, 1, fa);
    lsize = text.length();

    fseek(fa, lstart, SEEK_SET);

    if (sec->size() == 0)
      fwrite(text.c_str(), sec->GetContentStart() + 1, 1, fa);
    else
      fwrite(text.c_str(), sec->GetContentEnd(), 1, fa);

    FormatStr buff;
    if (sec->size() == 0)
      buff.Format("\n    %s = %s\n", sName.c_str(), value.c_str());
    else
      buff.Format("    %s = %s\n", sName.c_str(), value.c_str());

    fwrite(buff.c_str(), buff.length(), 1, fa);
    fwrite(text.c_str() + sec->GetContentEnd(),
           text.length() - sec->GetContentEnd(), 1, fa);
  }

  lstart = startat;
  fseek(fa, 0, SEEK_END);
  lsize = ftell(fa);
  fseek(fa, lstart, SEEK_SET);

  std::string text;
  text.resize(lsize - lstart, '\0');
  lsize = fread(&text[0], lsize - lstart, 1, fa);

  clear();
  ParseConfigText(text.c_str(), text.length());

  UnbindQConfigFile();
}

//========================= basic IO routines =============================
void ThrowCfgGet(const std::string& sInfo, const ConfigSection* pSection,
                 const std::string& sRecName) {
  const char szErrPatt[] =
      "CfgGet%sconst ConfigSection* pSection[%s], "
      "const std::string& sRecName[%s]) failed";

  throw Exception(szErrPatt, sInfo.c_str(), pSection->name.c_str(),
                  sRecName.c_str());
}

void CfgSetStr(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, const std::string& sVal) {
  config.Write(sSectionName, sRecordName, '"' + sVal + '"');
}

void CfgSetDbl(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, double rVal) {
  config.Write(sSectionName, sRecordName, FormatStr("%.16g", rVal));
}

void CfgSetInt(ConfigFile& config, const std::string& sSectionName,
               const std::string& sRecordName, int iVal) {
  config.Write(sSectionName, sRecordName, FormatStr("%d", iVal));
}

void CfgSetUInt(ConfigFile& config, const std::string& sSectionName,
                const std::string& sRecordName, unsigned int iVal) {
  config.Write(sSectionName, sRecordName, FormatStr("%d", iVal));
}

void CfgSetBool(ConfigFile& config, const std::string& sSectionName,
                const std::string& sRecordName, bool bVal) {
  config.Write(sSectionName, sRecordName, bVal ? "true" : "false");
}

std::string CfgGetStr(const ConfigSection* pSection,
                      const std::string& sRecordName) {
  std::string sVal;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName)) {
    sVal = *pRec->GetValue().str;
    const std::string::size_type cnBeg = sVal.find_first_not_of('"');
    const std::string::size_type cnEnd = sVal.find_last_not_of('"');

    sVal = sVal.substr(cnBeg, cnEnd - cnBeg + 1);
  } else
    ThrowCfgGet("Str(", pSection, sRecordName);

  return sVal;
}

std::string CfgGetStr(const ConfigSection* pSection,
                      const std::string& sRecordName,
                      const std::string& defaultValue) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName)) {
    std::string sVal = *pRec->GetValue().str;
    const std::string::size_type cnBeg = sVal.find_first_not_of('"');
    const std::string::size_type cnEnd = sVal.find_last_not_of('"');

    return sVal.substr(cnBeg, cnEnd - cnBeg + 1);
  } else
    return defaultValue;
}

double CfgGetDbl(const ConfigSection* pSection,
                 const std::string& sRecordName) {
  double rVal;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    rVal = atof(pRec->GetValue().str->c_str());
  else
    ThrowCfgGet("Dbl(", pSection, sRecordName);

  return rVal;
}

double CfgGetDbl(const ConfigSection* pSection, const std::string& sRecordName,
                 double defaultValue) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    return atof(pRec->GetValue().str->c_str());
  else
    return defaultValue;
}

int CfgGetInt(const ConfigSection* pSection, const std::string& sRecordName) {
  int iVal;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    iVal = atoi(pRec->GetValue().str->c_str());
  else
    ThrowCfgGet("Int(", pSection, sRecordName);

  return iVal;
}

int CfgGetInt(const ConfigSection* pSection, const std::string& sRecordName,
              int defaultValue) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    return atoi(pRec->GetValue().str->c_str());
  else
    return defaultValue;
}

unsigned int CfgGetUInt(const ConfigSection* pSection,
                        const std::string& sRecordName) {
  unsigned int iVal;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    iVal = static_cast<unsigned int>(atoi(pRec->GetValue().str->c_str()));
  else
    ThrowCfgGet("UInt(", pSection, sRecordName);

  return iVal;
}

unsigned int CfgGetUInt(const ConfigSection* pSection,
                        const std::string& sRecordName,
                        unsigned int defaultValue) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName))
    return static_cast<unsigned int>(atoi(pRec->GetValue().str->c_str()));
  else
    return defaultValue;
}

bool CfgGetBool(const ConfigSection* pSection, const std::string& sRecordName) {
  bool bVal;

  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName)) {
    const std::string& s = *(pRec->GetValue().str);

    if (s == "true" || s == "True" || s == "TRUE" || s == "1")
      bVal = true;
    else if (s == "false" || s == "False" || s == "FALSE" || s == "0")
      bVal = false;
    else
      ThrowCfgGet("Bool(", pSection, sRecordName);
  } else
    ThrowCfgGet("Bool(", pSection, sRecordName);

  return bVal;
}

bool CfgGetBool(const ConfigSection* pSection, const std::string& sRecordName,
                bool defaultValue) {
  if (const ConfigRecord* pRec = pSection->RecordByName(sRecordName)) {
    bool bVal;

    const std::string& s = *(pRec->GetValue().str);

    if (s == "true" || s == "True" || s == "TRUE" || s == "1")
      bVal = true;
    else if (s == "false" || s == "False" || s == "FALSE" || s == "0")
      bVal = false;
    else
      ThrowCfgGet("Bool(", pSection, sRecordName);

    return bVal;
  } else
    return defaultValue;
}

}  // namespace QLib
