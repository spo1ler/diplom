/********************************************************************
  created:  2004/05/02
  created:  2:5:2004   17:50
  filename: QRamaswamiPH.h
  author:   Dmitry Orlovsky

  purpose:  Routines for Ramaswami approach to PH-service support
*********************************************************************/

#ifndef __QRamamswamiPH_H__
#define __QRamamswamiPH_H__

#include "QLib/QLUP.h"

#include "ELib/ELogger.h"
#include "QLib/QMatrix.h"

namespace QLib {

class RamaswamiPHCalculator : public ELib::Logged {
 public:
  explicit RamaswamiPHCalculator(ELib::ILogger& logger);
  virtual ~RamaswamiPHCalculator() = default;

  //! Creates the generator with adsorbing state from phase-type distribution's
  // S matrix
  DMatrix CreateSTilde(const DMatrix& mS) const;

  void CreateABlocks(VectorDM& dest, unsigned int nC, const DMatrix& mS) const;
  void CreateUBlocks(VectorDM& dest, unsigned int nC, const DMatrix& mT) const;
  void CreateLBlocks(VectorDM& dest, unsigned int nC, const DMatrix& mT) const;
  void CreatePBlocks(VectorDM& dest, unsigned int nC,
                     const DMatrix& vBeta) const;

  DMatrix GetA(unsigned int nC, const DMatrix& mS) const;
  DMatrix CalcDiagEntries(unsigned int nC, const DMatrix& mS) const;
};

}  // namespace QLib

#endif  //__QRamamswamiPH_H__
