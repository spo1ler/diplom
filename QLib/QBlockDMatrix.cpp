/********************************************************************
        created:	1:3:2006   16:53
        filename: QBlockDMatrix.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/
#include "QLib/QBlockDMatrix.h"

#include "ELib/EXception.h"

namespace QLib {

IBlockDMatrix::IBlockDMatrix(unsigned int dim) : m_dim(dim) {}

IBlockDMatrix::~IBlockDMatrix() {}

void IBlockDMatrix::CheckRange(unsigned int row, unsigned int col) const {
  if (row >= GetDim() || col >= GetDim()) {
    throw ELib::Exception(
        "IBlockDMatrix::CheckRange(row, col) failed:"
        " [%d],j[%d] < GetDim()[%d]",
        row, col, GetDim());
  }
}

// ==== MatrixStructInfoStoring ====
MatrixStructInfoStoring::MatrixStructInfoStoring() : m_isFinite(true) {}

void MatrixStructInfoStoring::PushDim(unsigned int dim) {
  m_blocks.push_back(MatrixBlockInfo(dim));
}

void MatrixStructInfoStoring::Finalize(bool isFinite) {
  if (m_blocks.empty())
    throw ELib::Exception(
        "MatrixStructInfoStoring::Finalize(.) failed: trying to finalize an "
        "empty set");

  m_isFinite = isFinite;
  unsigned int off = 0;

  for (Blocks::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it) {
    it->off = off;
    off += it->dim;
  }
}

void MatrixStructInfoStoring::Clear() {
  Blocks empty;
  m_blocks.swap(empty);
  m_isFinite = true;
}

unsigned int MatrixStructInfoStoring::TotalDim() const {
  if (m_isFinite)
    return m_blocks.empty() ? 0 : m_blocks.back().EndOffset();
  else
    return IBlockDMatrix::INF;
}

MatrixBlockInfo MatrixStructInfoStoring::At(unsigned int i) const {
  if (m_isFinite) {
    if (i >= m_blocks.size())
      throw ELib::Exception(
          "MatrixStructInfoStoring::At(i[%d]) failed: index out of range", i);

    return m_blocks[i];
  } else if (i < m_blocks.size())
    return m_blocks[i];
  else {
    MatrixBlockInfo ret = m_blocks.back();
    ret.off += (i + 1 - m_blocks.size()) * ret.dim;

    return ret;
  }
}

// ==== MatrixStructInfoConst ====
MatrixStructInfoConst::MatrixStructInfoConst() : m_dim(0), m_blockSize(0) {}

MatrixStructInfoConst::MatrixStructInfoConst(unsigned int dim,
                                             unsigned int blockSize)
    : m_dim(dim), m_blockSize(blockSize) {}

void MatrixStructInfoConst::Init(unsigned int dim, unsigned int blockSize) {
  m_dim = dim;
  m_blockSize = blockSize;
}

MatrixBlockInfo MatrixStructInfoConst::At(unsigned int i) const {
  if (i >= m_dim)
    throw ELib::Exception(
        "MatrixStructInfoStoring::At(i[%d]) failed: index out of range", i);

  MatrixBlockInfo ret(m_blockSize, i * m_blockSize);

  return ret;
}

// ==== BlockDMatrixStoring ====
BlockDMatrixStoring::BlockDMatrixStoring() {}

void BlockDMatrixStoring::InitArrays(unsigned int dim) {
  Clear();
  SetDimension(dim);

  m_storage.resize(GetDim() * GetDim());
  m_zeroes.resize(GetDim() * GetDim(), true);

  m_aColsInfo.resize(GetDim());
  m_aRowsInfo.resize(GetDim());
}

void BlockDMatrixStoring::Init(const std::vector<unsigned int>& blkSizes) {
  InitArrays(blkSizes.size());

  for (std::vector<unsigned int>::const_iterator it = blkSizes.begin();
       it != blkSizes.end(); ++it)
    m_info.PushDim(*it);
  m_info.Finalize();
}

void BlockDMatrixStoring::InitRegular(unsigned int dim,
                                      unsigned int blockSize) {
  InitArrays(dim);

  for (unsigned int i = 0; i < dim; ++i) m_info.PushDim(blockSize);
  m_info.Finalize();
}

void BlockDMatrixStoring::Clear() {
  SetDimension(0);
  VectorDM empty;
  m_storage.swap(empty);
  m_zeroes.clear();

  m_aColsInfo.clear();
  m_aRowsInfo.clear();

  m_info.Clear();
}

unsigned int BlockDMatrixStoring::OffsetByIJ(unsigned int i,
                                             unsigned int j) const {
  return i + GetDim() * j;
}

void BlockDMatrixStoring::UpdateStructInfoAfterStoring(unsigned int i,
                                                       unsigned int j) {
  FirstLastInfo& currColInfo = m_aColsInfo[j];
  FirstLastInfo& currRowInfo = m_aRowsInfo[i];

  if (i < currColInfo.iFirst || currColInfo.iLast == 0) currColInfo.iFirst = i;
  if (i >= currColInfo.iLast) currColInfo.iLast = i + 1;

  if (j < currRowInfo.iFirst || currRowInfo.iLast == 0) currRowInfo.iFirst = j;
  if (j >= currRowInfo.iLast) currRowInfo.iLast = j + 1;
}

void BlockDMatrixStoring::Set(unsigned int i, unsigned int j,
                              const DMatrix& m) {
  CheckRange(i, j);

  if (m.GetM() != InfoAt(i).dim || m.GetN() != InfoAt(j).dim) {
    throw ELib::Exception(
        "BlockDMatrixStoring::Set(i[%d], j[%d], m[%dx%d]) failed: matrix must "
        "have size %dx%d",
        i, j, m.GetM(), m.GetN(), InfoAt(i).dim, InfoAt(j).dim);
  }

  UpdateStructInfoAfterStoring(i, j);

  const unsigned int off = OffsetByIJ(i, j);
  m_storage[off] = m;
  m_zeroes[off] = false;
}

unsigned int BlockDMatrixStoring::GetRCInfo(
    const std::vector<FirstLastInfo>& firstlast, unsigned int idx,
    unsigned int* piFirst) const {
  unsigned int iFirst, iLast;

  if (idx < firstlast.size()) {
    iFirst = firstlast[idx].iFirst;
    iLast = firstlast[idx].iLast;
  } else
    iFirst = iLast = 0;

  if (piFirst) *piFirst = iFirst;

  return iLast;
}

// IBlockDMatrix interface
unsigned int BlockDMatrixStoring::GetRowInfo(
    unsigned int iRow, unsigned int* piFirst /*= NULL*/) const {
  return GetRCInfo(m_aRowsInfo, iRow, piFirst);
}

unsigned int BlockDMatrixStoring::GetColInfo(
    unsigned int iCol, unsigned int* piFirst /*= NULL*/) const {
  return GetRCInfo(m_aColsInfo, iCol, piFirst);
}

MatrixBlockInfo BlockDMatrixStoring::InfoAt(unsigned int i) const {
  return m_info.At(i);
}

// Returns the matrix stored at position (i, j).
//  Throws an exception if the position doesn't fit the RowInfo and ColInfo
DMatrix BlockDMatrixStoring::Get(unsigned int i, unsigned int j) const {
  CheckRange(i, j);
  const unsigned int off = OffsetByIJ(i, j);

  if (m_zeroes[OffsetByIJ(i, j)])
    return DMatrix(InfoAt(i).dim, InfoAt(j).dim, 0.);
  else
    return m_storage[off];
}

// Checks whether the matrix stored at position (i, j) is zero.
//  Throws an exception if the position doesn't fit the RowInfo and ColInfo
bool BlockDMatrixStoring::IsZeroAt(unsigned int i, unsigned int j) const {
  CheckRange(i, j);

  return m_zeroes[OffsetByIJ(i, j)];
}

// IDQBDGenerator
IDQBDGenerator::IDQBDGenerator() : m_maxMinus(0), m_maxPlus(0) {}

IDQBDGenerator::IDQBDGenerator(unsigned int maxMinus, unsigned int maxPlus,
                               unsigned int blockDim) {
  Init(maxMinus, maxPlus, blockDim);
}

void IDQBDGenerator::Init(unsigned int maxMinus, unsigned int maxPlus,
                          unsigned int blockDim) {
  m_maxMinus = maxMinus;
  m_maxPlus = maxPlus;
  SetDimension(blockDim);
}

unsigned int IDQBDGenerator::GetRowInfo(unsigned int iRow,
                                        unsigned int* piFirst) const {
  if (iRow < GetDim()) {
    if (piFirst) *piFirst = (iRow > m_maxMinus) ? iRow - m_maxMinus : 0;

    return std::min(iRow + m_maxPlus + 1, GetDim());
  } else {
    if (piFirst) *piFirst = 0;

    return 0;
  }
}

unsigned int IDQBDGenerator::GetColInfo(unsigned int iCol,
                                        unsigned int* piFirst) const {
  if (iCol < GetDim()) {
    if (piFirst) *piFirst = (iCol > m_maxPlus) ? iCol - m_maxPlus : 0;

    return std::min(iCol + m_maxMinus + 1, GetDim());
  } else {
    if (piFirst) *piFirst = 0;

    return 0;
  }
}

bool IDQBDGenerator::IsZeroAt(unsigned int i, unsigned int j) const {
  CheckRange(i, j);

  return j > i + m_maxPlus || (i > m_maxMinus && j < i - m_maxMinus);
}

}  // namespace QLib
