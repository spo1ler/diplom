/********************************************************************
  created:  20:4:2006   12:15
  filename: QPolynomial.h
  author:   Dmitry Orlovsky

  purpose:
*********************************************************************/

#ifndef __QPolynomial_h__
#define __QPolynomial_h__

#include <limits>
#include <vector>

#include "ELib/EXception.h"

namespace QLib {
template <class Type>
class Polynomial : public std::vector<Type> {
 public:
  Polynomial() = default;
  explicit Polynomial(const std::vector<Type>& vec);

  Polynomial& operator=(const std::vector<Type>& vec);

  //! If the sum is non-zero, stores it to result and returns true, otherwise
  //!   doesn't affect the result, and returns false
  bool Sum(Type& dest, int first = 0,
           int last = std::numeric_limits<int>::max()) const;

  //! Returns sum. Throws exception if the result is an empty sum
  Type GetSum(int first = 0, int last = std::numeric_limits<int>::max()) const;

  //! If the sum is non-zero, stores it to result and returns true, otherwise
  // doesn't
  //!   affect the result, and returns false
  void AddTo(Type& dest, int first = 0,
             int last = std::numeric_limits<int>::max()) const;

  //! Returns derivative
  Polynomial Deriv() const;

  //! Replaces polynomial with its derivative
  void Diff();

  //!
  template <class PtType>
  bool AtPoint(Type& dest, const PtType& pt) const;

  //! Throws exception if the result is an empty sum
  template <class PtType>
  Type GetAtPoint(const PtType& pt) const;

  //! Stores to dest polynomial derivation at given point
  template <class PtType>
  bool DerivAtPoint(Type& dest, const PtType& pt) const;

  //! Throws exception if the result is an empty sum
  template <class PtType>
  Type GetDerivAtPoint(const PtType& pt) const;

  // See https://gcc.gnu.org/onlinedocs/gcc/Name-lookup.html.
  using std::vector<Type>::back;
  using std::vector<Type>::size;

 protected:
  using typename std::vector<Type>::const_iterator;
  using typename std::vector<Type>::iterator;
  using std::vector<Type>::begin;
  using std::vector<Type>::end;
};

//=============== implementation =================
template <class Type>
Polynomial<Type>::Polynomial(const std::vector<Type>& vec)
    : std::vector<Type>(vec) {}

template <class Type>
Polynomial<Type>& Polynomial<Type>::operator=(const std::vector<Type>& vec) {
  std::vector<Type>::operator=(vec);

  return *this;
}

template <class Type>
bool Polynomial<Type>::Sum(Type& result, int first, int last) const {
  if (first < static_cast<int>(size()) && first <= last) {
    auto it = begin() + first;
    auto itEnd = last < static_cast<int>(size()) ? begin() + (last + 1) : end();

    result = *it;
    for (++it; it != itEnd; ++it) result += *it;

    return true;
  } else
    return false;
}

template <class Type>
Type Polynomial<Type>::GetSum(int first, int last) const {
  Type ret;

  if (!Sum(ret, first, last))
    throw ELib::Exception(
        "Polynomial::GetSum(.) failed: result is an empty sum");

  return ret;
}

template <class Type>
void Polynomial<Type>::AddTo(Type& dest, int first, int last) const {
  if (first < size() && first <= last) {
    const_iterator it = begin() + first;
    const_iterator itEnd = last < size() ? begin() + (last + 1) : end();

    for (const_iterator it = begin() + first; it != itEnd; ++it) dest += *it;
  }
}

//! Returns derivative
template <class Type>
Polynomial<Type> Polynomial<Type>::Deriv() const {
  Polynomial<Type> ret;

  if (size() > 1) {
    ret.reserve(size() - 1);

    int k = 2;
    const_iterator it = begin() + 1;

    ret.push_back(*it);
    for (++it; it != end(); ++it, ++k) ret.push_back(k * (*it));
  }

  return ret;
}

template <class Type>
void Polynomial<Type>::Diff() {
  if (size() > 1) {
    int k = 2;
    iterator itd = begin();
    const_iterator it = begin() + 1;

    *itd = *it;
    for (++itd, ++it; it != end(); ++itd, ++it, ++k) (*itd) = k * (*it);
  }

  if (size() > 0) resize(size() - 1);
}

template <class Type>
template <class PtType>
bool Polynomial<Type>::AtPoint(Type& dest, const PtType& pt) const {
  if (size() > 0) {
    dest = back();

    const_iterator it = end() - 2;
    for (int i = size() - 1; i > 0; --i, --it) {
      Type tmp = *it;
      tmp += dest * pt;

      dest = tmp;
    }

    return true;
  } else
    return false;
}

template <class Type>
template <class PtType>
Type Polynomial<Type>::GetAtPoint(const PtType& pt) const {
  Type ret;

  if (!AtPoint(ret, pt))
    throw ELib::Exception(
        "Polynomial::GetAtPoint(.) failed: result is an empty sum");

  return ret;
}

template <class Type>
template <class PtType>
bool Polynomial<Type>::DerivAtPoint(Type& dest, const PtType& pt) const {
  if (size() > 1) {
    const_iterator it = end() - 1;
    int i = size() - 1;

    dest = (*it) * i;

    for (--i, --it; i > 0; --i, --it) {
      Type tmp = (*it) * i;
      tmp += dest * pt;

      dest = tmp;
    }

    return true;
  } else
    return false;
}

template <class Type>
template <class PtType>
Type Polynomial<Type>::GetDerivAtPoint(const PtType& pt) const {
  Type ret;

  if (!DerivAtPoint(ret, pt))
    throw ELib::Exception(
        "Polynomial::GetDerivAtPoint(.) failed: result is an empty sum");

  return ret;
}

}  // namespace QLib

#endif  //__QPolynomial_h__
