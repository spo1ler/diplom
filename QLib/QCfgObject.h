/********************************************************************
  created:	2006/04/30   13:28
  filename: QCfgObject.h
  author:	Dmitry Orlovsky

  purpose:
*********************************************************************/

#ifndef __QCfgObject_h__
#define __QCfgObject_h__

#include <string>

#include "ELib/ELogger.h"

namespace QLib {
class ConfigFile;

class ICfgObject {
 public:
  virtual ~ICfgObject() = default;
  virtual void CfgGet(const ConfigFile& config, const std::string& section) {};

  virtual void Dump(ELib::ILogger& logger) const {};
  virtual void CfgSet(ConfigFile& config, const std::string& section) const {};

  virtual void CfgSetInfo(ConfigFile& config,
                          const std::string& section) const {};
  virtual void DumpInfo(ELib::ILogger& logger) const {};
};

}  // namespace QLib

#endif  //__QCfgObject_h__
