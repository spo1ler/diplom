/********************************************************************
        created:	15:2:2006   14:48
        filename: QEngineStructures.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QEngineStructures_h__
#define __QEngineStructures_h__

#include <string>

#include "QLib/QMatrix.h"
#include "QLib/QBlockDMatrix.h"
#include "QLib/QCfgObject.h"

namespace ELib {
class ILogger;
}

namespace QLib {
//! PiVector is a polynomial containing the vector of probabilities
//
// Warning! "QLib/QCfgObject.h" must be included to .cpp file with
//  implementation of structures using PiVector
class PiVector : public PolyDM, public ICfgObject {
 public:
  PiVector();
  PiVector(const PolyDM& poly);
  PiVector& operator=(const PolyDM& poly);

  virtual ~PiVector() = default;

  using PolyDM::begin;
  using PolyDM::end;
  using PolyDM::size;
  using PolyDM::back;

  void EnableIO(bool isIOenabled);
  void ToggleReportOnlyDist(bool reportOnlyDist);

  //! Returns vector of marginal distribution {Sum(pi[i]), i = 0..i_max}
  DMatrix MarginalDist() const;

  virtual void CfgSet(ConfigFile& config, const std::string& section) const;
  virtual void Dump(ELib::ILogger& logger) const;

 private:
  bool m_isIOenabled;
  bool m_reportOnlyDist;
};

// StringVector
// -----------------------------------------------------------------------
class StringVector : public std::vector<std::string>, public ICfgObject {
 public:
  StringVector() {};
  StringVector(const std::vector<std::string>& s)
      : std::vector<std::string>(s) {};
  StringVector& operator=(const std::vector<std::string>& s);

  virtual ~StringVector() {}

  virtual void CfgSet(ConfigFile& config, const std::string& section) const;
  virtual void Dump(ELib::ILogger& logger) const;
};

// GenAndPiManagementParams
// -----------------------------------------------------------
struct GenAndPiManagementParams {
 public:
  unsigned int nDumpRows;  //! Number of rows to dump, 0 if none
  unsigned int nValiRows;  //! Number of rows to validate, 0 if none

  bool verifyPi;        //! Whether to verify the solution
  bool reportPi;        //! Selects whether to report pi vectors
  bool reportOnlyDist;  //! Selects whether to report only distribution during
  // pi report

 protected:
  void CfgGetGPM(const QLib::ConfigSection* pSection);
  void CfgSetGPM(QLib::ConfigFile& config, const std::string& section) const;
  void DumpGPM(ELib::ILogger& logger) const;
};

// ParameterHolder
// -------------------------------------------------------------------
template <class EngineParametersType>
class ParameterHolder {
  EngineParametersType m_parameters;

 public:
  ParameterHolder() {}

  explicit ParameterHolder(const EngineParametersType& parameters)
      : m_parameters(parameters) {}

  void SetParameters(const EngineParametersType& parameters) {
    m_parameters = parameters;
  }

  const EngineParametersType& P() const { return m_parameters; }
};

//! Dumps necessary number of generator lines or the whole generator
void DumpGenerator(ELib::ILogger& logger, unsigned int logAttribute,
                   const QLib::IBlockDMatrix& q,
                   unsigned int maxI = QLib::IBlockDMatrix::INF);

//! Checks whether the row sums are equal to zero
void ValidateGenerator(ELib::ILogger& logger, unsigned int logAttribute,
                       const QLib::IBlockDMatrix& q,
                       unsigned int maxI = QLib::IBlockDMatrix::INF);

//! Validates the solution by substituting it to the generator
void VerifySolution(ELib::ILogger& logger, unsigned int logAttribute,
                    const IBlockDMatrix& q, const VectorDM& avPi);

}  // namespace QLib

#endif  //__QEngineParameters_h__
