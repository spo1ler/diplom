cmake_minimum_required(VERSION 2.8)
project(sirius)

add_library(
    qlib_all
    QAQTMCSolver.cpp
    QAQTMCSolver.h
    QBaseMatrix.cpp
    QBaseMatrix.h
    QBlockDMatrix.cpp
    QBlockDMatrix.h
    QBMAP.cpp
    QBMAP.h
    QCalcModule.h
    QCfgObject.h
    QConfigFile.cpp
    QConfigFile.h
    QEngineStructures.cpp
    QEngineStructures.h
    QGenericOps.h
    QKroneckerPowers.cpp
    QKroneckerPowers.h
    QLUP.cpp
    QLUP.h
    QMatrixArithm.cpp
    QMatrixArithm.h
    QMatrix.cpp
    QMatrix.h
    QMatrixViews.h
    QPHDist.cpp
    QPHDist.h
    QPolynomial.h
    QRamaswamiPH.cpp
    QRamaswamiPH.h
    QSharedVectorWrapper.h
    QToeSolver.cpp
    QToeSolver.h
    QUtils.cpp
    QUtils.h)

target_link_libraries(
    qlib_all
    elib_all)

if (WIN32)
    find_library(
        BLAS_LIBRARY openblas "../ext/openblas-win32"
        NO_DEFAULT_PATH)
    if (BLAS_LIBRARY)
        add_definitions(-DHAVE_LAPACK_CONFIG_H)
        add_definitions(-DLAPACK_COMPLEX_CPP)
        add_definitions(-DUSE_BLAS)
        target_link_libraries(
            qlib_all
            ${BLAS_LIBRARY})
        target_include_directories(
            qlib_all PRIVATE "../ext/openblas-win32/include")
        set(OPENBLAS_BIN_DIR "${PROJECT_SOURCE_DIR}/../ext/openblas-win32/bin"
            PARENT_SCOPE)
    endif()
else()
    find_library(BLAS_LIBRARY openblas "/opt/OpenBLAS/lib")
    if (BLAS_LIBRARY)
        target_link_libraries(
            qlib_all
            ${BLAS_LIBRARY})
        target_include_directories(
            qlib_all PRIVATE "/opt/OpenBLAS/include")
        add_definitions(-DUSE_BLAS)
    endif()
endif()
