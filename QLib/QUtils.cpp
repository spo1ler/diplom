/********************************************************************
        created:	8:11:2005   18:02
        filename: QUtils.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QUtils.h"

namespace QLib {

unsigned long QUtils::Fact(unsigned int i) {
  unsigned long ret = 1;

  while (i > 0) ret *= (i--);

  return ret;
}

}  // namespace QLib
