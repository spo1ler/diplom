/********************************************************************
        created:	8:11:2005   18:06
        filename: QUtils.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QUtils_h__
#define __QUtils_h__

#include <assert.h>

namespace QLib {

class QUtils {
 public:
  //! Returns i^p
  template <class T>
  static void Pow(T& subj, int p) {
    assert(p > 0);
    T i = subj;
    --p;

    while (p > 0) {
      if (p & 1) subj *= i;
      i *= i;
      p >>= 1;
    }
  }

  template <class T>
  static inline T GetPow(const T& subj, int p) {
    T retVal = subj;
    Pow(retVal, p);

    return retVal;
  }

  unsigned long Fact(unsigned int i);

  template <class T>
  static inline T Sq(const T& x) {
    return x * x;
  }
};

}  // namespace QLib

#endif  //__QUtils_h__
