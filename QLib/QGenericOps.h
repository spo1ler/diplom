/********************************************************************
        created:	2006/02/20   11:19
        filename: QGenericOps.h
        author:		Dmitry Orlovsky

        purpose:	Some generic algorithms used in matrix calculations
*********************************************************************/

#ifndef __QGenericOps_h__
#define __QGenericOps_h__

#include <functional>

namespace QLib {

template <class IteratorD, class Iterator, class UnaryOperator>
void GenerateUnOp(IteratorD itDestBegin, IteratorD itDestEnd,
                  Iterator itArgBegin, UnaryOperator op) {
  Iterator it = itArgBegin;

  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd, ++it)
    *itd = op(*it);
}

template <class IteratorD, class UnaryModOperator>
void ApplyUnOp(IteratorD itDestBegin, IteratorD itDestEnd,
               UnaryModOperator op) {
  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd) op(*itd);
}

// returns (first -= second)
template <class Type>
struct NegMod : public std::unary_function<Type, Type> {
  Type operator()(Type& arg) const { return (arg = -arg); }
};

template <class IteratorD, class Iterator1, class Iterator2,
          class BinaryOperator>
void GenerateBinOp(IteratorD itDestBegin, IteratorD itDestEnd,
                   Iterator1 itArg1Begin, Iterator2 itArg2Begin,
                   BinaryOperator op) {
  Iterator1 it1 = itArg1Begin;
  Iterator2 it2 = itArg2Begin;

  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd, ++it1, ++it2)
    *itd = op(*it1, *it2);
}

template <class IteratorD, class Iterator, class Type, class BinaryOperator>
void GenerateBinOp1stConst(IteratorD itDestBegin, IteratorD itDestEnd,
                           const Type& val, Iterator itArg2Begin,
                           BinaryOperator op) {
  Iterator it = itArg2Begin;
  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd, ++it)
    *itd = op(val, *it);
}

template <class IteratorD, class Iterator, class Type, class BinaryOperator>
void GenerateBinOp2ndConst(IteratorD itDestBegin, IteratorD itDestEnd,
                           Iterator itArg1Begin, const Type& val,
                           BinaryOperator op) {
  Iterator it = itArg1Begin;
  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd, ++it)
    *itd = op(*it, val);
}

template <class IteratorD, class Iterator, class BinaryModOperator>
void ApplyBinOp(IteratorD itDestBegin, IteratorD itDestEnd, Iterator itArgBegin,
                BinaryModOperator op) {
  Iterator it = itArgBegin;
  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd, ++it)
    op(*itd, *it);
}

template <class IteratorD, class Type, class BinaryModOperator>
void ApplyBinOp2ndConst(IteratorD itDestBegin, IteratorD itDestEnd,
                        const Type& val, BinaryModOperator op) {
  for (IteratorD itd = itDestBegin; itd != itDestEnd; ++itd) op(*itd, val);
}

template <class Type>
struct OpIdentity : public std::unary_function<Type, Type> {
  Type operator()(const Type& operand) const { return operand; }
};

template <class MultiplierType, class OperandType,
          class ResultType = OperandType>
struct OpMultiplyByConst : public std::unary_function<OperandType, ResultType> {
  OpMultiplyByConst(const MultiplierType& multiplier)
      : m_multiplier(multiplier) {}

  ResultType operator()(const OperandType& operand) const {
    return m_multiplier * operand;
  }

 private:
  MultiplierType m_multiplier;
};

//! Applies left = right and returns left
template <class Type>
struct BinOpAssign : public std::binary_function<Type, Type, Type> {
  Type operator()(Type& left, const Type& right) const {
    return (left = right);
  }
};

//! Applies left += right and returns left
template <class Type>
struct PlusEq : public std::binary_function<Type, Type, Type> {
  Type operator()(Type& left, const Type& right) const {
    return (left += right);
  }
};

template <class MultiplierType, class Type>
struct PlusEqMultiplied : public std::binary_function<Type, Type, Type> {
  PlusEqMultiplied(const MultiplierType& multiplier)
      : m_multiplier(multiplier) {}

  Type operator()(Type& left, const Type& right) const {
    return (left += right * m_multiplier);
  }

 private:
  MultiplierType m_multiplier;
};

//! Applies left -= right and returns left
template <class Type>
struct OpMinusEq : public std::binary_function<Type, Type, Type> {
  Type operator()(Type& left, const Type& right) const {
    return (left -= right);
  }
};

//! Applies left *= right and returns left
template <class Type>
struct OpMulEq : public std::binary_function<Type, Type, Type> {
  Type operator()(Type& left, const Type& right) const {
    return (left *= right);
  }
};

}  // namespace QLib

#endif  //__QGenericOps_h__