/********************************************************************
        created:	15:3:2006   13:58
        filename: QEngineStructures.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QEngineStructures.h"

#include "ELib/EStrUtils.h"

#include "QLib/QConfigFile.h"
#include "QLib/QMatrixArithm.h"

namespace QLib {
using namespace ELib;

// PiVector implementation -------------------------------------------
PiVector::PiVector() : m_isIOenabled(true), m_reportOnlyDist(false) {}

PiVector::PiVector(const PolyDM& poly)
    : PolyDM(poly), m_isIOenabled(true), m_reportOnlyDist(false) {}

PiVector& PiVector::operator=(const PolyDM& poly) {
  PolyDM::operator=(poly);
  m_isIOenabled = true;
  m_reportOnlyDist = false;

  return *this;
}

void PiVector::EnableIO(bool isIOenabled) { m_isIOenabled = isIOenabled; }

void PiVector::ToggleReportOnlyDist(bool reportOnlyDist) {
  m_reportOnlyDist = reportOnlyDist;
}

DMatrix PiVector::MarginalDist() const {
  DMatrix vMarginal(1, size());
  DMatrix::pointer itMarginal = vMarginal.Data();

  for (const_iterator it = begin(); it != end(); ++it, ++itMarginal)
    *itMarginal = QLib::Sum(*it);

  return vMarginal;
}

void PiVector::CfgSet(ConfigFile& config, const std::string& section) const {
  if (m_isIOenabled) {
    unsigned int i = 0;

    if (!m_reportOnlyDist) {
      for (const_iterator it = begin(); it != end(); ++it, ++i)
        QLib::CfgSet(config, section, FormatStr("pi[%i]", i), *it);
    }

    QLib::CfgSet(config, section, "dist", MarginalDist());
  }
}

void PiVector::Dump(ILogger& logger) const {
  if (m_isIOenabled) {
    unsigned int i = 0;

    if (m_reportOnlyDist)
      logger
          << "Detailed dump skipped, use ToggleReportOnlyDist to enable it\n";
    else {
      for (const_iterator it = begin(); it != end(); ++it, ++i)
        logger << "pi[" << i << "] = {" << *it << "}\n";
    }

    logger << '\n';
    logger << "dist = {" << MarginalDist() << "}\n";
  }
}

// StringVector implementation --------------------------------
StringVector& StringVector::operator=(const std::vector<std::string>& s) {
  std::vector<std::string>::operator=(s);

  return *this;
}

void StringVector::CfgSet(ConfigFile& config,
                          const std::string& section) const {
  unsigned int i = 0;
  for (const_iterator it = begin(); it != end(); ++it, ++i)
    CfgSetStr(config, section, FormatStr("str[%d]", i), *it);
}

void StringVector::Dump(ELib::ILogger& logger) const {
  logger << "dumping StringVector:\n";

  unsigned int i = 0;
  for (const_iterator it = begin(); it != end(); ++it, ++i)
    logger << "str[" << i << "] = " << it->c_str() << '\n';
}

// GenAndPiManagementParams
// -----------------------------------------------------------
void GenAndPiManagementParams::CfgGetGPM(const ConfigSection* pSection) {
  nDumpRows = CfgGetUInt(pSection, "DumpRows", 0);
  nValiRows = CfgGetUInt(pSection, "ValiRows", 0);

  verifyPi = CfgGetBool(pSection, "VerifyPi", false);
  reportPi = CfgGetBool(pSection, "ReportPi", true);
  reportOnlyDist = CfgGetBool(pSection, "ReportOnlyDist", false);
}

void GenAndPiManagementParams::CfgSetGPM(ConfigFile& config,
                                         const std::string& section) const {
  if (nDumpRows) CfgSetUInt(config, section, "DumpRows", nDumpRows);
  if (nValiRows) CfgSetUInt(config, section, "ValiRows", nValiRows);

  if (verifyPi) CfgSetStr(config, section, "VerifyPi", "true");
  if (!reportPi) CfgSetStr(config, section, "ReportPi", "false");
}

void GenAndPiManagementParams::DumpGPM(ILogger& logger) const {
  if (nDumpRows) logger << "  DumpRows = " << nDumpRows << '\n';
  if (nValiRows) logger << "  ValiRows = " << nValiRows << '\n';
  if (verifyPi) logger << "  VerifyPi = true\n";
  if (!reportPi) logger << "  ReportPi = false\n";
}

// Validation functions ------------------------------------------
void DumpGenerator(ILogger& logger, unsigned int logAttribute,
                   const IBlockDMatrix& q, unsigned int maxI) {
  if (!logger.IsActive()) return;

  LogAutoPusher logPusher(logger, logAttribute);

  for (unsigned int i = 0; i < std::min(q.GetDim(), maxI); ++i) {
    unsigned int njMin, njMax;
    njMax = q.GetRowInfo(i, &njMin);

    for (unsigned int j = njMin; j < njMax; ++j) {
      if (q.IsZeroAt(i, j)) {
        const unsigned int cnMdim = q.InfoAt(i).dim;
        const unsigned int cnNdim = q.InfoAt(j).dim;
        logger << "q[" << i << ',' << j << "] = Zero(" << cnMdim << 'x'
               << cnNdim << ")\n";
      } else
        logger << "q[" << i << ',' << j << "] = {\n" << q.Get(i, j) << "\n}\n";
    }
  }
}

void ValidateGenerator(ILogger& logger, unsigned int logAttribute,
                       const IBlockDMatrix& q, unsigned int maxI) {
  LogAutoPusher logPusher(logger, logAttribute);
  logger << "Validating generator\n";

  if (maxI == IBlockDMatrix::INF && q.GetDim() == IBlockDMatrix::INF)
    throw Exception(
        "Full-size validation of infinite BlockDMatrix detected, use finite "
        "maxI");

  const double epsilonRowsum = 1e-10;
  std::string sErr;

  for (unsigned int i = 0; i < std::min(q.GetDim(), maxI); ++i) {
    DMatrix vChk(q.InfoAt(i).dim, 1, 0.);
    unsigned int njMin, njMax;
    njMax = q.GetRowInfo(i, &njMin);

    for (unsigned int j = njMin; j < njMax; ++j) vChk += t1(q.Get(i, j));

    for (unsigned int k = 0; k < vChk.GetDLen(); ++k) {
      if (fabs(vChk.GetV(k)) > epsilonRowsum)
        sErr += FormatStr("%d:%d(%g) ", i, k, vChk.GetV(k));
    }
  }

  if (sErr.empty())
    logger << "Generator validation passed\n";
  else {
    sErr = std::string(
               "Generator validation failed: non-zero rowsums found at rows ") +
           sErr;
    logger << sErr << '\n';
    throw Exception(sErr);
  }

  logger << "Done.\n";
}

void VerifySolution(ILogger& logger, unsigned int logAttribute,
                    const IBlockDMatrix& q, const VectorDM& avPi) {
  LogAutoPusher logPusher(logger, logAttribute);
  logger << "Validating solution\n";

  if (avPi.size() > q.GetDim())
    throw Exception("Too many components in solution vector");

  std::string sErr;
  const double epsilonErr = 1e-10;

  for (unsigned int j = 0; j < avPi.size(); ++j) {
    DMatrix vChk(1, avPi[j].GetN(), 0.);
    unsigned int iMin, iMax;
    iMax = std::min<unsigned int>(q.GetColInfo(j, &iMin), avPi.size());

    for (unsigned int i = iMin; i < iMax; ++i)
      tpax(vChk, avPi.at(i), q.Get(i, j));

    for (unsigned int i = 0; i < vChk.GetDLen(); ++i) {
      if (fabs(vChk.GetV(i)) > epsilonErr)
        sErr += FormatStr("%d:%d(%g) ", j, i, vChk.GetV(i));
    }
  }

  if (!sErr.empty()) {
    sErr = std::string("Non-zero norms found at columns: ") + sErr;
    logger << sErr << '\n';
    throw Exception(sErr);
  } else
    logger << "Solution validation passed\n";

  logger << "Done.\n";
}

}  // namespace QLib
