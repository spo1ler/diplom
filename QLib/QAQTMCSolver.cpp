/********************************************************************
created:	16:11:2006   22:34
filename: QAQTMCSolver.cpp
author:		Dmitry Orlovsky

purpose:	Asymptotically Quasi-Toeplitz Markov chain solver

based on: Klimenok, Dudin, Continuous-time Asymptotially Quasi-Toeplitz
          Markov Chains // Queues: Flows, systems, Networks, 22-24
          February, 2005, Minsk, V.18, P.76-84 (Yellow book)
*********************************************************************/

#include "QLib/QAQTMCSolver.h"

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QConfigFile.h"
#include "QLib/QMatrixArithm.h"
#include "QLib/QBlockDMatrix.h"
#include "QLib/QLUP.h"

#define _ITERATIONAL_GI_REFINEMENT_

#include <cfloat>

namespace QLib {
using namespace ELib;

// AQTMCSolver::Parameters implementation
// ---------------------------------------
const AQTMCSolver::Parameters& AQTMCSolver::Parameters::GetDefault() {
  static Parameters params;

  params.precisionGi = 1e-5;
  params.maxTildeI = 100000;
  params.errorGi = 1e-30;
  params.maxGiRefIter = 100;
  params.precisionPi = 1e-12;
  params.maxPi = 1000;
  params.lStep = 10;

  return params;
}

void AQTMCSolver::Parameters::CfgGet(const ConfigFile& config,
                                     const std::string& section) {
  (*this) = GetDefault();

  if (const ConfigSection* pSec = config.SectionByName(section)) {
    precisionGi = CfgGetDbl(pSec, "PrecisionGi", precisionGi);
    maxTildeI = CfgGetUInt(pSec, "MaxTildeI", maxTildeI);
    errorGi = CfgGetDbl(pSec, "errorGi", errorGi);
    maxGiRefIter = CfgGetUInt(pSec, "maxGiRefIter", maxGiRefIter);
    lStep = CfgGetUInt(pSec, "LStep", lStep);
    precisionPi = CfgGetDbl(pSec, "PrecisionPi", precisionPi);
    maxPi = CfgGetUInt(pSec, "MaxPi", maxPi);
  }
}

void AQTMCSolver::Parameters::CfgSet(ConfigFile& config,
                                     const std::string& section) const {
  CfgSetDbl(config, section, "PrecisionGi", precisionGi);
  CfgSetUInt(config, section, "MaxTildeI", maxTildeI);
  CfgSetDbl(config, section, "errorGi", errorGi);
  CfgSetUInt(config, section, "maxGiRefIter", maxGiRefIter);
  CfgSetUInt(config, section, "LStep", lStep);
  CfgSetDbl(config, section, "PrecisionPi", precisionPi);
  CfgSetUInt(config, section, "MaxPi", maxPi);
}

void AQTMCSolver::Parameters::Dump(ILogger& logger) const {
  logger << "Dumping AQTMCSolver::Parameters:\n";
  logger << "PrecisionGi = " << precisionGi << '\n';
  logger << "MaxTildeI   = " << maxTildeI << '\n';
  logger << "errorGi     = " << errorGi << '\n';
  logger << "maxGiRefIter= " << maxGiRefIter << '\n';
  logger << "LStep       = " << lStep << '\n';
  logger << "PrecisionPi = " << precisionPi << '\n';
  logger << "MaxPi       = " << maxPi << '\n';
}

// AQTMCSolver implementation
// ---------------------------------------------------------
AQTMCSolver::AQTMCSolver(ILogger& logger, IProgressor& progressor)
    : LogProg(logger, progressor) {}

AQTMCSolver::Result AQTMCSolver::Solve(
    const IBlockDMatrix& bmA,  //! Infinitesimal generator
    const DMatrix& mG,         //! Neuts's matrix
    unsigned int i0,  //! i_0 from AQTMC definition (definition 1.3, P.78)
    unsigned int k0,  //! k_0 from AQTMC definition (definition 1.3, P.78)
    const Parameters& params) {
  m_pA = &bmA;
  m_mG = mG;
  m_i0 = i0;
  m_k0 = k0;
  m_params = params;

  m_ahPowersOfG.push_back(mG);

  Validate();
  PreCalc();

  Result res;

  CalculateGi(m_ahGi, res.messages);
  CalculateFl(res.messages);
  CalculatePi(res.avPi);

  Cleanup();

  return res;
}

void AQTMCSolver::Cleanup() {
  m_aIMin.clear();
  m_ahPowersOfG.clear();
  m_ahGi.clear();
  m_ahFl.clear();
  m_vSumFie.Init();
  m_barA00.Init();
}

void AQTMCSolver::Validate() {
  BNDLogger flogger(GetLogger(), "AQTMCSolver::Validate()",
                    ILogger::MA_LIB_WORKFLOW_MINOR);

  // Generator validation, see 'generator.nb' in theory section
  const char szErrHeader[] = "AQTMCSolver::Validate() failed:";

  if (A().GetDim() != IBlockDMatrix::INF || A().GetDim() != IBlockDMatrix::INF)
    throw Exception("%s generator must be infinite in both dimensions",
                    szErrHeader);

  unsigned int reachable = 0;
  for (unsigned int i = 0; i <= I0(); ++i) {
    // Checking whether A_{i,i-k} == 0, k > 1
    unsigned int first;
    unsigned int last = A().GetRowInfo(i, &first);

    if (last == IBlockDMatrix::INF)
      throw Exception("%s generator row %d must be finite", szErrHeader, i);
    if (last <= i)
      throw Exception("%s zero diagonal entry detected at row %d ", szErrHeader,
                      i);
    if (first + 1 < i)
      throw Exception("%s non-zero block detected below subdiagonal at row %d",
                      szErrHeader, i);

    reachable = std::max(reachable, last - 1);
  }

  if (reachable < I0()) {
    throw Exception(
        "%s state i_0[%d] can't be reached: maximum reached state %d",
        szErrHeader, I0(), reachable);
  }

  // checking whether A_{i_0, i_0 + k} == A_{i_0 + 1, i_0 + 1 + k}, k =
  // \overline {k_0, k_{maxReg}}
  for (unsigned int k = K0(); I0() + k < A().GetRowInfo(I0()); ++k) {
    if (NormDiff(A().Get(I0(), I0() + k), A().Get(I0() + 1, I0() + k + 1)) !=
        0.)
      throw Exception("A_{i_0, i_0 + k} != A_{i_0 + 1, i_0 + 1 + k} for k = %d",
                      k);
  }
}

void AQTMCSolver::PreCalc() {
  BNDLogger flogger(GetLogger(), "AQTMCSolver::PreCalc()",
                    ILogger::MA_LIB_WORKFLOW_MINOR);

  // Calculating k_{maxReg}
  m_kMaxReg = A().GetRowInfo(I0()) - I0() - 1;

  // Calculating l_maxIrr and m_aIMin[l], l = \overline{0,m_lMaxIrr}
  m_lMaxIrr = 0;

  for (unsigned int i = 0; i < I0(); ++i) {
    const unsigned int last = A().GetRowInfo(i) - 1;

    if (last > m_lMaxIrr) {
      m_lMaxIrr = last;

      while (m_aIMin.size() <= m_lMaxIrr) m_aIMin.push_back(i);
    }
  }

  if (GetLogger().IsActive()) {
    LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_DUMP);
    GetLogger() << "AQTMCSolver::PreCalc() resume:\n";
    GetLogger() << "  k_{maxReg} = " << m_kMaxReg << '\n';
    GetLogger() << "  l_{maxIrr} = " << m_lMaxIrr << '\n';
    GetLogger() << "  i_{min}[0:l_{maxIrr}] = " << m_aIMin << '\n';
  }
}

DMatrix AQTMCSolver::PowerOfG(unsigned int pow) {
  if (pow == 0)
    throw Exception("AQTMCSolver::PowerOfG(pow) failed: pow must be positive");

  if (m_ahPowersOfG.size() < pow) {
    DMatrix mTmp;
    while (m_ahPowersOfG.size() < pow) {
      mTmp = m_ahPowersOfG.back() * G();
    }
    return mTmp;
  } else
    return m_ahPowersOfG.at(pow - 1);
}

void AQTMCSolver::CalculateGi(std::vector<DMatrix>& ahGi,
                              std::vector<std::string>& messages) {
  BNDLogger flogger(GetLogger(), "AQTMCSolver::CalculateGi()",
                    ILogger::MA_LIB_WORKFLOW_MINOR);
  SubProgressor sp(GetProgressor(), "CalculateGi: ", 0, 50);

  const char szWarning[] = "AQTMCSolver::CalculateGi(.): warning!";

  // calculating the sum tail: mGtail = \sum_{k=k_0}^{k_{maxReg}} A_{i+1,i+1+k}
  // G^k
  DMatrix mGtail = (K0() == 0) ? A().Get(I0(), I0())
                               : A().Get(I0(), I0() + K0()) * PowerOfG(K0());
  for (unsigned int k = K0() + 1; k <= KmaxReg(); ++k)
    tpax(mGtail, A().Get(I0(), I0() + k), PowerOfG(k));

  // determining the \tilde i: G_i = G, i \ge \tilde i
  DMatrix mSumInv;
  if (K0() == 0) mSumInv = Inversion(mGtail);

  bool fwd = true;  // forward direction flag
  unsigned int tildeImin = std::max(I0(), 2u);
  unsigned int tildeImax = tildeImin * 2;

  DMatrix mGlast;
  double err;

  while (tildeImax - tildeImin > 1) {
    sp.SetProgressMsg(
        FormatStr("Determining \\tilde i: lower bound %d, upper bound %d",
                  tildeImin, tildeImax));

    unsigned int tildeI;
    if (tildeImin >= Par().maxTildeI)
      tildeI = Par().maxTildeI;
    else
      tildeI = fwd ? tildeImax : (tildeImin + tildeImax) / 2;

    if (K0() > 0) {
      DMatrix mSum = mGtail + A().Get(tildeI, tildeI);

      for (unsigned int k = 1; k < K0(); ++k)
        tpax(mSum, A().Get(tildeI, tildeI + k), PowerOfG(k));

      mSumInv = Inversion(Neg(mSum));
    }
    mGlast = mSumInv * A().Get(tildeI, tildeI - 1);

    err = NormDiff(mGlast, G());

    if (tildeI == Par().maxTildeI) {
      if (err > Par().precisionGi) {
        FormatStr msg(
            " %s Maximum allowed value of \\tilde i (%d) reached "
            " but error value still too high: %g while needed at least %g. "
            " Try to raise the maxTildeI or precisionGi value.",
            szWarning, tildeI, err, Par().precisionGi);

        messages.push_back(msg);

        LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_WORKFLOW);
        GetLogger() << msg << '\n';
      }

      tildeImin = tildeImax = tildeI;
    } else if (fwd) {
      if (err > Par().precisionGi) {
        tildeImin = tildeImax;
        tildeImax *= 2;
      } else
        fwd = false;
    } else {
      if (err > Par().precisionGi)
        tildeImin = tildeI;
      else
        tildeImax = tildeI;
    }
  }

  // Calculating G_i matrices
  ahGi.resize(tildeImax);
  ahGi.back() = mGlast;

  // We know that tildeImax >= 2
  for (unsigned int i = tildeImax - 2; i + 1 > 0; --i) {
    sp.SetProgressMsg(
        FormatStr("calculating matrix %d of %d", tildeImax - i, tildeImax));

    const unsigned int dim = A().InfoAt(i + 1).dim;
    DMatrix mSum(dim, dim, 0.);
    DMatrix mGProd;

    const unsigned int lMax = A().GetRowInfo(i + 1);
    for (unsigned int l = i + 2; l < lMax; ++l) {
      const DMatrix cmGlm1 =
          (l - 1 < ahGi.size()) ? ahGi.at(l - 1) : G();  // G_{l-1}
      mGProd = (l == i + 2) ? cmGlm1 : cmGlm1 * mGProd;

      tpax(mSum, A().Get(i + 1, l), mGProd);
    }
    mSum += A().Get(i + 1, i + 1);

    DMatrix mB = A().Get(i + 1, i);

#ifdef _LOG_GI_REFINEMENT_
    GetLogger() << "A = " << -mSum << '\n';
    GetLogger() << "B = " << mB << '\n';
#endif  //_LOG_GI_REFINEMENT_

    // Correcting possible error on diagonal
    const double zeroAppr = 1e-16;
    DMatrix vBe = t1(mB);
    DMatrix vErrAB = t1(mSum) + vBe;  // Be == Ae + cvErrAB

#ifdef _LOG_GI_REFINEMENT_
    GetLogger() << "Be - Ae = " << vErrAB << '\n';
#endif  //_LOG_GI_REFINEMENT_

    DMatrixDiag dmErrAB(vErrAB.GetDLen());
    std::copy_n(vErrAB.Data(), vErrAB.GetDLen(), dmErrAB.Data());

    mSum -= dmErrAB;
    DMatrix mGi = Inversion(-mSum) * mB;

#ifdef _ITERATIONAL_GI_REFINEMENT_
    DMatrix vAe = -t1(mSum);

#ifdef _LOG_GI_REFINEMENT_
    GetLogger() << "Be - A_m e = " << t1(mB) - vAe << '\n';
#endif  //_LOG_GI_REFINEMENT_

    // Rearrange the system to make it strictly diagonal-dominant
    for (unsigned int row = 0; row < mSum.GetM(); ++row) {
      if (vAe.AtV(row) < zeroAppr) {
        // find j!=row such that element Aij has maximum module and (A)row e !=
        // 0
        DMViewRow viewRowI(mSum, row);

        double maxAbs = 0.;
        unsigned int foundJ = row;

        DMViewRow::const_iterator citRow = viewRowI.Begin();
        for (unsigned int j = 0; j < mSum.GetM(); ++j, ++citRow) {
          if (j != row && vAe.AtV(j) > zeroAppr && (*citRow) > maxAbs) {
            maxAbs = (*citRow);
            foundJ = j;
          }
        }

        if (foundJ != row) {
          const double multiplier = -maxAbs / mSum.At(foundJ, foundJ);

#ifdef _LOG_GI_REFINEMENT_
          GetLogger() << "Adding to row " << row << " row " << foundJ
                      << " multiplied by " << multiplier << '\n';
#endif  //_LOG_GI_REFINEMENT_

          PlusEqMultiplied<double, double> opPlusEqMul(multiplier);

          DMViewRowConst viewRowJ(mSum, foundJ);
          ApplyBinOp(viewRowI.Begin(), viewRowI.End(), viewRowJ.Begin(),
                     opPlusEqMul);

          DMViewRow viewBRowI(mB, row);
          DMViewRowConst viewBRowJ(mB, foundJ);
          ApplyBinOp(viewBRowI.Begin(), viewBRowI.End(), viewBRowJ.Begin(),
                     opPlusEqMul);

          vAe.AtV(row) += multiplier * vAe.AtV(foundJ);
          vBe.AtV(row) += multiplier * vBe.AtV(foundJ);
        }
      }
    }

    // Second diagonal refinement
    vErrAB = t1(mSum) + t1(mB);  // Be == Ae + cvErrAB
    std::copy_n(vErrAB.Data(), vErrAB.GetDLen(), dmErrAB.Data());
    mSum -= dmErrAB;

#ifdef _LOG_GI_REFINEMENT_
    GetLogger() << "A' = " << -mSum << '\n';
    GetLogger() << "B' = " << mB << '\n';
    GetLogger() << "B'e - A'e = " << t1(mSum) + t1(mB) << '\n';
#endif  //_LOG_GI_REFINEMENT_

    // Gi is solution X to the system AX = B, where A = -mSum, B = A().Get(i +
    // 1, i)
    // Let A = D - V, where D represents the diagonal of A, and V stays for the
    // rest of entries,
    //  then (D - V)X = B  =>  X = D^{-1}(VX + B).

    DMatrixDiag dmD(mSum.GetM());
    double* itDest = dmD.Data();

    DMViewDiag diagView(mSum);
    for (DMViewDiag::iterator it = diagView.Begin(); it != diagView.End();
         ++it, ++itDest) {
      *itDest = -1. / (*it);
      *it = 0.;
    }

    DMatrix mGnext;
    double epsG = Par().errorGi + 1;
    const DMatrix cmE(mGi.GetM(), 1, 1.);

    for (unsigned int iteration = 0;
         iteration < Par().maxGiRefIter && epsG > Par().errorGi; ++iteration) {
      DMatrix mGnextWOD;
      bpax(mGnextWOD, mB, mSum, mGi);
      mGnext = dmD * mGnextWOD;

      epsG = NormDiff(mGnext, mGi);

#ifdef _LOG_GI_REFINEMENT_
      GetLogger() << epsG << ' ' << NormDiff(t1(mGi), cmE) << "; ";
#endif  //_LOG_GI_REFINEMENT_

      mGi = mGnext;
    }

#ifdef _LOG_GI_REFINEMENT_
    GetLogger() << '\n';

    GetLogger() << "G(" << i << ") = " << mGi << '\n';
    GetLogger() << "G(" << i << ")e = " << t1(mGi) << '\n';
#endif  //_LOG_GI_REFINEMENT_

    if (epsG > Par().errorGi) {
      FormatStr msg(
          "%s Iteration limit (%u) reached while refining G_{%u}"
          " but epsG still too high: %g while needed at least %g. "
          "(epsE = %g, convRate = %g"
          " Try to raise the maxGiRefIter or use lower errorGi value.",
          szWarning, Par().maxGiRefIter, i, epsG, Par().errorGi,
          NormDiff(t1(mGi), cmE), Norm(dmD * mSum));

      if (messages.size() < 3) messages.push_back(msg);

      LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_WORKFLOW);
      GetLogger() << msg << '\n';
    }

#endif  // _ITERATIONAL_GI_REFINEMENT_

    ahGi.at(i) = mGi;
  }

  if (GetLogger().IsActive()) {
    LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_DUMP);
    GetLogger() << "AQTMCSolver::CalculateGi(.) resume:\n";
    GetLogger() << "  \\tilde i value: " << ahGi.size() << '\n';
    GetLogger() << "  Error value achieved: " << err << '\n';
  }
}

void AQTMCSolver::CalculateFl(std::vector<std::string>& messages) {
  BNDLogger flogger(GetLogger(), "AQTMCSolver::CalculateFl(.)",
                    ILogger::MA_LIB_WORKFLOW_MINOR);
  SubProgressor sp(GetProgressor(), "CalculateFl: ", 50, 95);

  // Calculating F_l matrices, until |F_l| >= Prec()

  // Stores \bar A_{*l} columns from lPrev + 1 (position 0) to lCurr (position
  // lCurr - lPrev - 1)
  std::vector<Adotl> barAil;

  // \sum_{i=1}^l F_i \bf e
  m_vSumFie.Init(A().InfoAt(0).dim, 1, 1.);

  // fCrit is value such that (fCrit < eps) => (|\pi_l| < eps)
  // Currently, fCrit = (|F_l|*|1|) / |\sum_{i=0}^l F_l \bf e|
  const double normOne = Norm(DMatrix(1, 1, 1.));
  double fCrit = Par().precisionPi + 1;

  for (unsigned int lPrev = 0, lCurr = Par().lStep;
       fCrit > Par().precisionPi && lPrev < Par().maxPi;
       lPrev = lCurr, lCurr += Par().lStep) {
    sp.SetStatus(FormatStr("matrix %u of max %u, error %g vs %g", lPrev,
                           Par().maxPi, fCrit, Par().precisionPi),
                 lPrev * 100 / Par().maxPi);

    // First stage, calculating \bar A_{i, lCurr}
    {
      const unsigned int l = lCurr;

      barAil.clear();
      barAil.resize(lCurr - lPrev);

      // barAil[l - lPrev - 1] stores IMin(l) as iMin, and \bar Ail with i
      // indexes from Adotl.iMin to l
      Adotl& currAdotl = barAil.back();
      currAdotl.iMin = IMin(l);
      currAdotl.amByI.resize(l - currAdotl.iMin + 1);

      for (unsigned int i = currAdotl.iMin; i <= l; ++i)
        currAdotl.amByI[i - currAdotl.iMin] = A().Get(i, l);

      DMatrix mGprod = Gi(l);

      // determining k_max
      unsigned int kMax = 0;
      if (currAdotl.iMin < I0()) {
        for (unsigned int i = currAdotl.iMin; i <= l; ++i)
          kMax = std::max(kMax, A().GetRowInfo(i) - i - 1);
      } else
        kMax = KmaxReg();

      // calculating \bar A_{i, lCurr}
      for (unsigned int k = 1; k <= kMax; ++k) {
        if (k > 1) {
          if (l < m_ahGi.size())
            mGprod = Gi(l + k - 1) * mGprod;
          else
            mGprod = PowerOfG(k);
        }

        for (unsigned int i = IMin(l + k); i <= l; ++i) {
          if (!A().IsZeroAt(i, l + k)) {
            DMatrix& barAil = currAdotl.amByI.at(i - currAdotl.iMin);
            tpax(barAil, A().Get(i, l + k), mGprod);
          }
        }
      }
    }

    // Second stage, calculating \bar A_{i,l}, l = \overline{lPrev + 1, lCurr -
    // 1}
    for (unsigned int l = lCurr - 1; l > lPrev; --l) {
      Adotl& prevAdotl = barAil.at(l - lPrev);
      Adotl& currAdotl = barAil.at(l - lPrev - 1);

      currAdotl.iMin = IMin(l);
      currAdotl.amByI.resize(l - currAdotl.iMin + 1);

      for (unsigned int i = currAdotl.iMin; i < prevAdotl.iMin; ++i)
        currAdotl.amByI[i - currAdotl.iMin] = A().Get(i, l);

      for (unsigned int i = prevAdotl.iMin; i <= l; ++i) {
        bpax(currAdotl.amByI[i - currAdotl.iMin], A().Get(i, l),
             prevAdotl.amByI[i - prevAdotl.iMin], Gi(l));
      }
    }

    if (lPrev == 0) bpax(m_barA00, A().Get(0, 0), barAil.at(0).amByI[0], Gi(0));

    // Third stage, calculating F_l, l = \overline{lPrev + 1, lCurr}
    {
      unsigned int l;

      for (l = lPrev + 1;
           fCrit > Par().precisionPi && l <= Par().maxPi && l <= lCurr; ++l) {
        sp.SetStatus(FormatStr("matrix %u of max %u, error %g vs %g", l,
                               Par().maxPi, fCrit, Par().precisionPi),
                     l * 100 / Par().maxPi);

        Adotl& currAdotl = barAil.at(l - lPrev - 1);
        DMatrix mFl;

        auto itFi = m_ahFl.rbegin();
        for (unsigned int i = l - 1; i + 1 > currAdotl.iMin; --i, ++itFi) {
          const DMatrix& barAil = currAdotl.amByI.at(i - currAdotl.iMin);
          //            GetLogger() << "barAil = " << barAil << '\n';

          if (i == 0) {
            if (i == l - 1)
              mFl = barAil;
            else
              mFl += barAil;

            //              GetLogger() << "mFl(i == 0)[" << l << "] = " << mFl
            //              << '\n';
          } else {
            if (i == l - 1)
              mFl = (*itFi) * barAil;
            else
              tpax(mFl, *itFi, barAil);

            //              GetLogger() << "mFl(i != 0)[" << l << "] = " << mFl
            //              << '\n';
          }
        }

        mFl *= Inversion(Neg(currAdotl.amByI.at(l - currAdotl.iMin)));

        m_ahFl.push_back(mFl);

        // Checking the exit condition
        m_vSumFie += t1(mFl);

#ifdef OS_WINDOWS
        const double norm1 = Norm(mFl);
        const double norm2 = Norm(m_vSumFie);

        if (!_finite(norm1) || !_finite(norm2))
          throw Exception(
              "AQTMCSolver::CalculateFl(.) failed: infinite matrix norms "
              "detected");
#endif  // OS_WINDOWS

        fCrit = (normOne * Norm(mFl)) / Norm(m_vSumFie);
      }
    }
  }

  if (fCrit > Par().precisionPi) {
    FormatStr msg(
        "AQTMCSolver::CalculateFl(.): warning! Maximum allowed number (%d) of "
        "F_l matrices reached "
        " but criterion value still too high: %g while needed at least %g. "
        " Try to raise the maxPi or precisionPi value.",
        Par().maxPi, fCrit, Par().precisionPi);

    messages.push_back(msg);

    LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_WORKFLOW);
    GetLogger() << msg << '\n';
  }

  if (GetLogger().IsActive()) {
    LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_DUMP);
    GetLogger() << "AQTMCSolver::CalculateFl(.) resume:\n";
    GetLogger() << "  F_l matrices calculated: " << m_ahFl.size() + 1 << '\n';
    GetLogger() << "  Criterion value achieved: " << fCrit << '\n';
  }
}

void AQTMCSolver::CalculatePi(PiVector& avPi) {
  GetProgressor().SetProgressMsg("AQTMCSolver::CalculatePi");

  avPi.resize(m_ahFl.size() + 1);
  avPi.at(0) = LSolveRowNorm(Neg(m_barA00), m_vSumFie);

  GetLogger() << "m_barA00 = " << m_barA00 << '\n';
  GetLogger() << "m_vSumFie = " << m_vSumFie << '\n';
  GetLogger() << "avPi.at(0) = " << avPi.at(0) << '\n';

  auto itFl = m_ahFl.begin();

  for (auto it = avPi.begin() + 1; it != avPi.end(); ++it, ++itFl)
    it->Assign(avPi.front() * (*itFl));

  if (GetLogger().IsActive()) {
    LogAutoPusher autoPusher(GetLogger(), ILogger::MA_LIB_DUMP);
    GetLogger() << "AQTMCSolver::CalculatePi(.) resume:\n";
    GetLogger() << "  pi vectors calculated: " << avPi.size() << '\n';
    GetLogger() << "  last pi norm: " << Norm(avPi.back()) << '\n';
    GetLogger() << "  pi: ";
    avPi.Dump(GetLogger());
  }
}

DMatrix AQTMCSolver::Gi(unsigned int i) {
  return (i < m_ahGi.size()) ? m_ahGi.at(i) : G();
}

// AQTMCLimitingChain implementation
// -------------------------------------------------------------
AQTMCLimitingChain::AQTMCLimitingChain(ILogger& logger) : Logged(logger) {}

void AQTMCLimitingChain::Init(const VectorDM& amYk) {
  if (amYk.size() <= 2) {
    throw Exception(
        "AQTMCLimitingChain::Init(const VectorDM& amYk) failed: "
        "amYk must contain at least two matrices");
  }

  m_amYk = amYk;
}

void AQTMCLimitingChain::Validate() const {
  const char szHead[] = "AQTMCLimitingChain::Validate() failed:";
  double r = 100;

  try {
    r = Norm(t1(m_amYk.GetSum()) - DMatrixConst(m_amYk[0].GetM(), 1, 1.));
  }
  catch (Exception&) {
    throw Exception("%s can't calculate Y_k matrices sum, check dimensions",
                    szHead);
  }

  if (fabs(r) > 1e-14)
    throw Exception("%s one or more rows of chain generator are not stochastic",
                    szHead);
}

//   void AQTMCLimitingChain::InitViaGeneratorRow(IBlockDMatrix& bmA, unsigned
//   int row)
//   {
//     throw Exception("AQTMCLimitingChain::InitViaGeneratorRow(): not
//     implemented yet!");
//   }

bool AQTMCLimitingChain::CheckStability() const {
  BNDLogger flogger(GetLogger(), "AQTMCLimitingChain::CheckStability()",
                    ILogger::MA_LIB_WORKFLOW_MINOR);
  LogAutoPusher logPusher(GetLogger(), ILogger::MA_LIB_DUMP_MINOR);

  // Stability condition:
  //  x Y'(1) \bf 1 < 1, where x is the solution of the system
  //  x(Y(1) - I) = 0
  //  x \bf 1 = 1.

  // Creating the Y(1) - I matrix
  DMatrix mSys;
  m_amYk.AtPoint(mSys, 1.);
  mSys -= DMatrixCDiag::I(mSys.GetM());

  const DMatrix vX = LSolveRowNorm(mSys);

  // Creating the Y'(1) matrix
  m_amYk.DerivAtPoint(mSys, 1.);

  double rCriterion = ScalarMul(vX, t1(mSys));

  GetLogger() << "Criterion = " << rCriterion << '\n';

  return rCriterion < 1;
}

//-----------------------------------------------------------------------------
DMatrix AQTMCLimitingChain::CalculateG(const DMatrix& mInitialG,
                                       double rPrecision,
                                       unsigned int iMaxIterations,
                                       IProgressor& progressor) const {
  BNDLogger flogger(GetLogger(), "AQTMCLimitingChain::CalculateG(...)",
                    ILogger::MA_LIB_WORKFLOW_MINOR);
  LogAutoPusher logPusher(GetLogger(), ILogger::MA_LIB_DUMP_MINOR);

  unsigned int progress = 0;

  // calculating mA = I - Y_1
  // if (det(mA)!=0)
  //    G_{[k+1]} = mA^{-1} (Y_0 + sum_i=2^{\infty} Y_i G^i_{[k]});
  // else
  //    G_{[k+1]} = sum_i=0^{\infty} Y_i G^i_{[k]});

  const unsigned int dim = m_amYk[1].GetM();
  DMatrix mA = DMatrixCDiag::I(dim) - m_amYk[1];
  const bool isDetZero = (Det(mA) <= 1e-30);

  if (isDetZero)
    mA.Init();
  else
    mA = Inversion(mA);

  DMatrix mG, mG1;

  if (mInitialG.GetM() == 0)
    mG.InitI(dim);
  else {
    if (mInitialG.GetM() != dim || mInitialG.GetN() != dim)
      throw Exception("Wrong initial G dimension");
    mG = mInitialG;
  }

  double precision = rPrecision + 1;
  unsigned int iter = 0;

  for (; precision > rPrecision && iter < iMaxIterations; ++iter) {
    mG1 = mG;
    mG.Clone(m_amYk.back());

    if (isDetZero) {
      for (unsigned int i = m_amYk.size() - 1; i > 0; --i)
        bpax(mG, m_amYk[i - 1], mG, mG1);
    } else {
      for (unsigned int i = m_amYk.size() - 1; i > 0; --i) {
        if (i == 2)
          mG *= mG1;
        else
          bpax(mG, m_amYk[i - 1], mG, mG1);
      }

      mG = mA * mG;
    }

    if (GetLogger().IsActive())
      GetLogger() << "G^{(" << iter << ")} = {\n" << mG << "\n}\n";

    precision = NormDiff(mG1, mG);

    progress = (100 + 4 * progress) / 5;

    progressor.SetStatus(
        FormatStr("Calculating G matrix (#%d, e=%g)", iter, precision),
        progress);
  }

  if (precision > rPrecision)
    throw Exception("Matrix G calculation doesn't converge in %d iterations",
                    iMaxIterations);

  if (GetLogger().IsActive()) {
    GetLogger().Push(ILogger::MA_LIB_DIAG) << "Matrix G calculated in " << iter
                                           << " iterations\n" << LoggerPop();

    GetLogger().Push(ILogger::MA_LIB_DUMP) << "G = {\n" << mG << "\n}\n"
                                           << LoggerPop();
  }

  return mG;
}

}  // namespace QLib
