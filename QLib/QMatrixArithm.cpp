/********************************************************************
        created:	17:2:2006   18:18
        filename: QMatrixArithm.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QMatrixArithm.h"

#include <functional>

#include "ELib/EXception.h"
#include "QLib/QGenericOps.h"

#ifdef USE_BLAS
#include "cblas.h"
#endif

namespace QLib {

//================= DMatrix ===================
double Sum(const DMatrix& m) {
  double rRet = 0;

  for (DMatrix::const_iterator it = m.Begin(); it != m.End(); ++it) rRet += *it;

  return rRet;
}

double ScalarMul(const DMatrix& m1, const DMatrix& m2) {
  if (m1.GetDLen() != m2.GetDLen()) {
    throw ELib::Exception(
        "DMatrix::ScalarMul(const DMatrix& m1, const DMatrix& m2) failed: "
        "(m1.GetDLen()[%d] == m2.GetDLen()[%d])",
        m1.GetDLen(), m2.GetDLen());
  }

  double rRet = 0;
  for (DMatrix::const_iterator it1 = m1.Begin(), it2 = m2.Begin();
       it1 != m1.End(); ++it1, ++it2)
    rRet += (*it1) * (*it2);

  return rRet;
}

double Norm(const DMatrix& m) {
  double norm = 0.;
  DMatrix::const_iterator it = m.Begin();

  for (unsigned int i = 0; i < m.GetM(); ++i) {
    double sum = 0.;
    for (unsigned int j = 0; j < m.GetN(); ++j, ++it) sum += fabs(*it);

    if (norm < sum) norm = sum;
  }

  return norm;
}

double NormDiff(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);

  double norm = 0.;
  DMatrix::const_iterator it1 = m1.Begin();
  DMatrix::const_iterator it2 = m2.Begin();

  for (unsigned int i = 0; i < m1.GetM(); ++i) {
    double sum = 0.;
    for (unsigned int j = 0; j < m1.GetN(); ++j, ++it1, ++it2)
      sum += fabs(*it1 - *it2);

    if (norm < sum) norm = sum;
  }

  return norm;
}

// binary + operators group
DMatrix operator+(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m1.GetM(), m1.GetN());
  GenerateBinOp(temp.Begin(), temp.End(), m1.Begin(), m2.Begin(),
                std::plus<double>());

  return temp;
}

DMatrix operator+(const DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp;
  temp.Clone(m1);

  DMViewDiag viewDiagDest(temp);
  ApplyBinOp(viewDiagDest.Begin(), viewDiagDest.End(), m2.Begin(),
             PlusEq<double>());

  return temp;
}

DMatrix operator+(const DMatrix& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m1.GetM(), m1.GetN());
  ApplyBinOp2ndConst(temp.Begin(), temp.End(), m2.GetValue(), PlusEq<double>());

  return temp;
}

DMatrix operator+(const DMatrix& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp;
  temp.Clone(m1);

  DMViewDiag viewDiagDest(temp);
  ApplyBinOp2ndConst(viewDiagDest.Begin(), viewDiagDest.End(), m2.GetValue(),
                     PlusEq<double>());

  return temp;
}

// binary - operator group
DMatrix operator-(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m1.GetM(), m1.GetN());
  GenerateBinOp(temp.Begin(), temp.End(), m1.Begin(), m2.Begin(),
                std::minus<double>());

  return temp;
}

DMatrix operator-(const DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp;
  temp.Clone(m1);

  DMViewDiag viewDiagDest(temp);
  ApplyBinOp(viewDiagDest.Begin(), viewDiagDest.End(), m2.Begin(),
             OpMinusEq<double>());

  return temp;
}

DMatrix operator-(const DMatrix& m1, const DMatrixConst& m2) {
  return m1 + (-m2);
}

DMatrix operator-(const DMatrix& m1, const DMatrixCDiag& m2) {
  return m1 + (-m2);
}

// unary - operator
DMatrix operator-(const DMatrix& m) {
  DMatrix temp(m.GetM(), m.GetN());

  GenerateUnOp(temp.Begin(), temp.End(), m.Begin(), std::negate<double>());

  return temp;
}

DMatrix& Neg(DMatrix& m) {
  m.Clip();

  ApplyUnOp(m.Begin(), m.End(), NegMod<double>());

  return m;
}

// binary += operators group
DMatrix& operator+=(DMatrix& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp(m1.Begin(), m1.End(), m2.Begin(), PlusEq<double>());

  return m1;
}

DMatrix& operator+=(DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  DMViewDiag viewDiagM1(m1);
  ApplyBinOp(viewDiagM1.Begin(), viewDiagM1.End(), m2.Begin(),
             PlusEq<double>());

  return m1;
}

DMatrix& operator+=(DMatrix& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp2ndConst(m1.Begin(), m1.End(), m2.GetValue(), PlusEq<double>());

  return m1;
}

DMatrix& operator+=(DMatrix& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  DMViewDiag viewDiag1(m1);
  ApplyBinOp2ndConst(viewDiag1.Begin(), viewDiag1.End(), m2.GetValue(),
                     PlusEq<double>());

  return m1;
}

// binary -= operator group
DMatrix& operator-=(DMatrix& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp(m1.Begin(), m1.End(), m2.Begin(), OpMinusEq<double>());

  return m1;
}

DMatrix& operator-=(DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  DMViewDiag viewDiagM1(m1);
  ApplyBinOp(viewDiagM1.Begin(), viewDiagM1.End(), m2.Begin(),
             OpMinusEq<double>());

  return m1;
}

DMatrix& operator-=(DMatrix& m1, const DMatrixConst& m2) { return m1 += (-m2); }

DMatrix& operator-=(DMatrix& m1, const DMatrixCDiag& m2) { return m1 += (-m2); }

// dot product operator group
#ifdef USE_BLAS
DMatrix operator*(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowMul(m1, m2);
  DMatrix temp(m1.GetM(), m2.GetN());

  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m1.GetM(), m2.GetN(),
              m1.GetN(), 1., &(*m1.Begin()), m1.GetN(), &(*m2.Begin()),
              m2.GetN(), 0., &(*temp.Begin()), temp.GetN());

  return temp;
}
#else   // own multiplication is used
DMatrix operator*(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowMul(m1, m2);
  DMatrix temp(m1.GetM(), m2.GetN());

  DMatrix::const_iterator itBrEnd = m2.Begin() + m2.GetN();
  DMatrix::iterator itd = temp.Begin();

  const unsigned int incB = m2.GetN();

  for (DMatrix::const_iterator itArowBegin = m1.Begin();
       itArowBegin != m1.End();) {
    const DMatrix::const_iterator itArowEnd = itArowBegin + m1.GetN();

    for (DMatrix::const_iterator itBcolBegin = m2.Begin();
         itBcolBegin != itBrEnd; ++itBcolBegin, ++itd) {
      double r = 0;
      for (DMatrix::const_iterator itA = itArowBegin, itB = itBcolBegin;
           itA != itArowEnd; ++itA, itB += incB)
        r += (*itA) * (*itB);
      *itd = r;
    }

    itArowBegin = itArowEnd;
  }
  return temp;
}
#endif  // USE_BLAS

DMatrix operator*(const DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);
  DMatrix temp(m1.GetM(), m2.GetN());

  DMatrix::const_iterator it1 = m1.Begin();
  DMatrixDiag::const_iterator it2 = m2.Begin();
  for (DMatrix::iterator itd = temp.Begin(); itd != temp.End();
       ++itd, ++it1, ++it2) {
    if (it2 == m2.End()) it2 = m2.Begin();
    *itd = (*it1) * (*it2);
  }

  return temp;
}

DMatrix operator*(const DMatrix& m1, const DMatrixConst& m2) {
  CheckAllowMul(m1, m2);

  DMatrix temp(m1.GetM(), m2.GetN());
  DMatrix::iterator itd = temp.Begin();
  DMatrix::const_iterator it1 = m1.Begin();

  for (unsigned int i = 0; i < m1.GetM(); ++i) {
    double d = 0;
    for (unsigned int j = 0; j < m1.GetN(); ++j, ++it1) d += *it1;

    d *= m2.GetValue();
    for (unsigned int j = 0; j < m2.GetN(); ++j, ++itd) *itd = d;
  }

  return temp;
}

DMatrix operator*(const DMatrix& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);
  return m1 * m2.GetValue();
}

// operator *= group
DMatrix& operator*=(DMatrix& m1, const DMatrix& m2) {
  m1 = m1 * m2;
  return m1;
}

DMatrix& operator*=(DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);
  m1.Clip();

  DMatrixDiag::const_iterator it2 = m2.Begin();
  for (DMatrix::iterator it1 = m1.Begin(); it1 != m1.End(); ++it1, ++it2) {
    if (it2 == m2.End()) it2 = m2.Begin();
    (*it1) *= (*it2);
  }

  return m1;
}

DMatrix& operator*=(DMatrix& m1, const DMatrixConst& m2) {
  m1 = m1 * m2;
  return m1;
}

DMatrix& operator*=(DMatrix& m1, const DMatrixCDiag& m2) {
  m1.Clip();
  return m1 *= m2.GetValue();
}

// product with scalar element
DMatrix operator*(const DMatrix& m, const double r) { return r * m; }

DMatrix operator*(const double r, const DMatrix& m) {
  if (r == 0)
    return DMatrix(m.GetM(), m.GetN(), 0);
  else if (r == 1) {
    DMatrix temp;
    return temp.Clone(m);
  } else {
    DMatrix temp(m.GetM(), m.GetN());

    GenerateBinOp2ndConst(temp.Begin(), temp.End(), m.Begin(), r,
                          std::multiplies<double>());

    return temp;
  }
}

// self-product with scalar element
DMatrix& operator*=(DMatrix& m, const double r) {
  m.Clip();

  ApplyBinOp2ndConst(m.Begin(), m.End(), r, OpMulEq<double>());

  return m;
}

DMatrix operator/(const DMatrix& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(DMatrix& m, const double r): division by zero");

  return (1. / r) * m;
}

DMatrix& operator/=(DMatrix& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(DMatrix& m, const double r): division by zero");

  return m *= (1. / r);
}

// Kronecker product group
DMatrix operator^(const DMatrix& m1, const DMatrix& m2) {
  if (m1.GetM() == 1 && m1.GetN() == 1)
    return m1.GetV(0) * m2;
  else if (m2.GetM() == 1 && m2.GetN() == 1)
    return m2.GetV(0) * m1;

  //#ifdef USE_BLAS
  //  DMatrix result(m1.GetM() * m2.GetM(), m1.GetN() * m2.GetN());
  //  return result;
  //#else  // USE_BLAS
  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int olpr = nd - m2.GetN();
  const unsigned int ou_ = m2.GetM() * nd;
  const unsigned int our_ = ou_ - m2.GetN();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(md, nd);
  DMatrix::iterator itd = temp.Begin();
  DMatrix::const_iterator it1 = m1.Begin();

  for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr) {
    for (unsigned int j1 = 0; j1 < m1.GetN(); ++it1, ++j1, itd -= our_) {
      if (*it1 == 0) {  // Result block is zero
        DMatrix::const_iterator it2 = m2.Begin();
        for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
          for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2, ++itd)
            *itd = 0.;
      } else if (*it1 == 1) {  // Result block is the copy of M2
        DMatrix::const_iterator it2 = m2.Begin();
        for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
          for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2, ++itd)
            *itd = *it2;
      } else {  // Multiplication
        DMatrix::const_iterator it2 = m2.Begin();
        for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
          for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2, ++itd)
            *itd = (*it1) * (*it2);
      }
    }
  }
  return temp;
  //#endif // USE_BLAS
}

DMatrix operator^(const DMatrix& m1, const DMatrixDiag& m2) {
  if (m1.GetM() == 1 && m1.GetN() == 1)
    return (m1.GetV(0) * m2).GetAsOrdinary();
  else if (m2.GetM() == 1 && m2.GetN() == 1)
    return m2.Get(0) * m1;

  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int oprpc = nd + 1;
  const unsigned int ou_ = m2.GetM() * nd;
  const unsigned int odr = m2.GetN() + ou_;
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(md, nd, 0.);
  DMatrix::iterator itd = temp.Begin();
  DMatrix::const_iterator it1 = m1.Begin();

  for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr) {
    for (unsigned int j1 = 0; j1 < m1.GetN(); ++it1, ++j1, itd -= ou_) {
      if (*it1 == 0)
        itd += odr;
      else if (*it1 == 1.) {
        for (DMatrixDiag::const_iterator it2 = m2.Begin(); it2 != m2.End();
             ++it2, itd += oprpc)
          *itd = *it2;
      } else {
        for (DMatrixDiag::const_iterator it2 = m2.Begin(); it2 != m2.End();
             ++it2, itd += oprpc)
          *itd = (*it1) * (*it2);
      }
    }
  }
  return temp;
}

DMatrix operator^(const DMatrix& m1, const DMatrixConst& m2) {
  if (m1.GetM() == 1 && m1.GetN() == 1)
    return DMatrix(m2.GetM(), m2.GetN(), m1.GetV(0) * m2.GetValue());
  else if (m2.GetM() == 1 && m2.GetN() == 1)
    return m2.GetValue() * m1;

  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int olpr = nd - m2.GetN();
  const unsigned int ou_ = m2.GetM() * nd;
  const unsigned int our_ = ou_ - m2.GetN();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(md, nd);

  if (m2.GetValue() == 0)
    temp.FillZero();
  else {
    DMatrix::iterator itd = temp.Begin();
    DMatrix::const_iterator it1 = m1.Begin();

    for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr) {
      for (unsigned int j1 = 0; j1 < m1.GetN(); ++it1, ++j1, itd -= our_) {
        if (*it1 == 0) {
          for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr) {
            for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++itd) *itd = 0.;
          }
        } else {
          const double r1 = (*it1) * m2.GetValue();
          for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr) {
            for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++itd) *itd = r1;
          }
        }
      }
    }
  }
  return temp;
}

DMatrix operator^(const DMatrix& m1, const DMatrixCDiag& m2) {
  if (m1.GetM() == 1 && m1.GetN() == 1)
    return (m1.GetV(0) * m2).GetAsOrdinary();
  else if (m2.GetM() == 1 && m2.GetN() == 1)
    return m2.GetValue() * m1;

  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int oprpc = nd + 1;
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odr = ou_ + m2.GetN();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(md, nd, 0);

  if (m2.GetValue() != 0) {
    DMatrix::iterator itd = temp.Begin();
    DMatrix::const_iterator it1 = m1.Begin();

    for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr) {
      for (unsigned int j1 = 0; j1 < m1.GetN(); ++it1, ++j1, itd -= ou_) {
        if (*it1 == 0)
          itd += odr;
        else {
          const double r = (*it1) * m2.GetValue();
          for (unsigned int ps2 = 0; ps2 < m2.GetM(); ++ps2, itd += oprpc)
            *itd = r;
        }
      }
    }
  }
  return temp;
}

// Kronecker sum: M1^E(M2.m) + E(M1.m)^M2 group
DMatrix operator|(const DMatrix& m1, const DMatrix& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrix& m1, const DMatrixConst& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrix& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

// Kronecker self-product group
DMatrix& operator^=(DMatrix& m1, const DMatrix& m2) { return m1 = m1 ^ m2; }

DMatrix& operator^=(DMatrix& m1, const DMatrixDiag& m2) { return m1 = m1 ^ m2; }

DMatrix& operator^=(DMatrix& m1, const DMatrixConst& m2) {
  return m1 = m1 ^ m2;
}

DMatrix& operator^=(DMatrix& m1, const DMatrixCDiag& m2) {
  return m1 = m1 ^ m2;
}

// Kronecker self-sum group
DMatrix& operator|=(DMatrix& m1, const DMatrix& m2) {
  CheckAllowKSum(m1, m2);
  return m1 = m1 | m2;
}

DMatrix& operator|=(DMatrix& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  return m1 = m1 | m2;
}

DMatrix& operator|=(DMatrix& m1, const DMatrixConst& m2) {
  CheckAllowKSum(m1, m2);
  return m1 = m1 | m2;
}

DMatrix& operator|=(DMatrix& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  return m1 = m1 | m2;
}

//  return t += a*x;
DMatrix& tpax(DMatrix& t, const DMatrix& a, const DMatrix& x) {
  if (&t == &a || &t == &x) {
    DMatrix dest;
    bpax(dest, t, a, x);

    return t = dest;
  }

  CheckAllowMul(a, x);
  CheckAllowSum(t, DMatrixConst(a.GetM(), x.GetN()));
  t.Clip();

#ifdef USE_BLAS

  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, a.GetM(), x.GetN(),
              a.GetN(), 1., &(*a.Begin()), a.GetN(), &(*x.Begin()), x.GetN(),
              1., &(*t.Begin()), t.GetN());

#else   // #ifdef USE_BLAS

  DMatrix::const_iterator itBrEnd = x.Begin() + x.GetN();
  DMatrix::iterator itd = t.Begin();

  const unsigned int incB = x.GetN();

  for (DMatrix::const_iterator itArowBegin = a.Begin();
       itArowBegin != a.End();) {
    const DMatrix::const_iterator itArowEnd = itArowBegin + a.GetN();

    for (DMatrix::const_iterator itBcolBegin = x.Begin();
         itBcolBegin != itBrEnd; ++itBcolBegin, ++itd) {
      double r = 0;
      for (DMatrix::const_iterator itA = itArowBegin, itB = itBcolBegin;
           itA != itArowEnd; ++itA, itB += incB)
        r += (*itA) * (*itB);
      *itd += r;
    }

    itArowBegin = itArowEnd;
  }
#endif  // #ifdef USE_BLAS

  return t;
}

//  return dest = b + a*x;
DMatrix& bpax(DMatrix& dest, const DMatrix& b, const DMatrix& a,
              const DMatrix& x) {
  if (&dest == &b) return tpax(dest, a, x);

  DMatrix mLocalDest;
  DMatrix& mDest = (&dest == &a || &dest == &x) ? mLocalDest : dest;

  CheckAllowMul(a, x);

#ifdef USE_BLAS
  mDest.Clone(b);
  CheckAllowSum(mDest, b);

  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, a.GetM(), x.GetN(),
              a.GetN(), 1., &(*a.Begin()), a.GetM(), &(*x.Begin()), x.GetM(),
              1., &(*mDest.Begin()), mDest.GetM());

#else   // #ifdef USE_BLAS
  mDest.Init(a.GetM(), x.GetN());
  CheckAllowSum(mDest, b);

  DMatrix::const_iterator itBrEnd = x.Begin() + x.GetN();
  DMatrix::const_iterator itb = b.Begin();
  DMatrix::iterator itd = mDest.Begin();

  const unsigned int incB = x.GetN();

  for (DMatrix::const_iterator itArowBegin = a.Begin();
       itArowBegin != a.End();) {
    const DMatrix::const_iterator itArowEnd = itArowBegin + a.GetN();

    for (DMatrix::const_iterator itBcolBegin = x.Begin();
         itBcolBegin != itBrEnd; ++itBcolBegin, ++itd, ++itb) {
      double r = 0.;
      for (DMatrix::const_iterator itA = itArowBegin, itB = itBcolBegin;
           itA != itArowEnd; ++itA, itB += incB)
        r += (*itA) * (*itB);
      *itd = *itb + r;
    }

    itArowBegin = itArowEnd;
  }
#endif  // #ifdef USE_BLAS

  if (&dest != &mDest) dest = mDest;

  return dest;
}

DMatrix t1(const DMatrix& m) { return m * DMatrixConst(m.GetN(), 1, 1.); }

// block-management group
void CopyBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
             unsigned int mSize, unsigned int nSize) {
  BlkGenerate(dest, mTo, nTo, src, mFrom, nFrom, mSize, nSize,
              OpIdentity<double>());
}

void CopyBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             const DMatrix& src) {
  BlkGenerate(dest, mTo, nTo, src, OpIdentity<double>());
}

void CopyBlk(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
             unsigned int nFrom) {
  BlkGenerate(dest, src, mFrom, nFrom, OpIdentity<double>());
}

void AddBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
            const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
            unsigned int mSize, unsigned int nSize) {
  BlkApply(dest, mTo, nTo, src, mFrom, nFrom, mSize, nSize, PlusEq<double>());
}

void AddBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
            const DMatrix& src) {
  BlkApply(dest, mTo, nTo, src, PlusEq<double>());
}

void AddBlk(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
            unsigned int nFrom) {
  BlkApply(dest, src, mFrom, nFrom, PlusEq<double>());
}

void CopyBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
                unsigned int mSize, unsigned int nSize, double mul) {
  BlkGenerate(dest, mTo, nTo, src, mFrom, nFrom, mSize, nSize,
              OpMultiplyByConst<double, double>(mul));
}

void CopyBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
                const DMatrix& src, double mul) {
  BlkGenerate(dest, mTo, nTo, src, OpMultiplyByConst<double, double>(mul));
}

void CopyBlkMul(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
                unsigned int nFrom, double mul) {
  BlkGenerate(dest, src, mFrom, nFrom, OpMultiplyByConst<double, double>(mul));
}

void AddBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
               const DMatrix& src, unsigned int mFrom, unsigned int nFrom,
               unsigned int mSize, unsigned int nSize, double mul) {
  BlkApply(dest, mTo, nTo, src, mFrom, nFrom, mSize, nSize,
           PlusEqMultiplied<double, double>(mul));
}

void AddBlkMul(DMatrix& dest, unsigned int mTo, unsigned int nTo,
               const DMatrix& src, double mul) {
  BlkApply(dest, mTo, nTo, src, PlusEqMultiplied<double, double>(mul));
}

void AddBlkMul(DMatrix& dest, const DMatrix& src, unsigned int mFrom,
               unsigned int nFrom, double mul) {
  BlkApply(dest, src, mFrom, nFrom, PlusEqMultiplied<double, double>(mul));
}

void FillBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             unsigned int mSize, unsigned int nSize, const double r);

void FillBlk(DMatrix& dest, unsigned int mTo, unsigned int nTo,
             unsigned int mSize, unsigned int nSize, const double r) {
  DMViewBlock destView(dest, mTo, nTo, mSize, nSize);

  dest.Clip();

  std::fill(destView.Begin(), destView.End(), r);
}

//================= DMatrixDiag ===================

double Sum(const DMatrixDiag& m) {
  double rRet = 0;

  for (DMatrixDiag::const_iterator it = m.Begin(); it != m.End(); ++it)
    rRet += *it;

  return rRet;
}

double ScalarMul(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  if (m1.GetDLen() != m2.GetDLen()) {
    throw ELib::Exception(
        "DMatrix::ScalarMul(const DMatrixDiag& m1, const DMatrixDiag& m2) "
        "failed: "
        "(m1.GetDLen()[%d] == m2.GetDLen()[%d])",
        m1.GetDLen(), m2.GetDLen());
  }

  double rRet = 0;
  for (DMatrix::const_iterator it1 = m1.Begin(), it2 = m2.Begin();
       it1 != m1.End(); ++it1, ++it2)
    rRet += (*it1) * (*it2);

  return rRet;
}

double Norm(const DMatrixDiag& m) {
  double norm = 0.;

  for (DMatrixDiag::const_iterator it = m.Begin(); it != m.End(); ++it) {
    if (norm < fabs(*it)) norm = fabs(*it);
  }

  return norm;
}

double NormDiff(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  double norm = 0.;

  for (DMatrixDiag::const_iterator it1 = m1.Begin(), it2 = m2.Begin();
       it1 != m1.End(); ++it1, ++it2) {
    const double curr = fabs(*it1 - *it2);
    if (norm < curr) norm = curr;
  }

  return norm;
}

DMatrixDiag Inversion(const DMatrixDiag& m) {
  DMatrixDiag dmRet(m.GetM());
  DMatrixDiag::iterator itd = dmRet.Begin();

  for (DMatrixDiag::const_iterator it = m.Begin(); it != m.End(); ++it) {
    if (*it == 0)
      throw ELib::Exception(
          "DMatrixDiag::Inversion() failed: (Get(i[%d]) != 0)",
          std::distance(m.Begin(), it));

    *itd = 1. / (*it);
  }

  return dmRet;
}

// replaces matrix with inversed
DMatrixDiag& Inverse(DMatrixDiag& m) {
  m.Clip();

  for (DMatrixDiag::iterator it = m.Begin(); it != m.End(); ++it) {
    if (*it == 0)
      throw ELib::Exception("DMatrixDiag::Inverse() failed: (Get(i[%d]) != 0)",
                            std::distance(m.Begin(), it));

    *it = 1. / (*it);
  }

  return m;
}

// binary + operator group
DMatrix operator+(const DMatrixDiag& m1, const DMatrix& m2) { return m2 + m1; }

DMatrixDiag operator+(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrixDiag temp(m1.GetM());
  GenerateBinOp(temp.Begin(), temp.End(), m1.Begin(), m2.Begin(),
                std::plus<double>());

  return temp;
}

DMatrix operator+(const DMatrixDiag& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m2.GetM(), m2.GetN(), m2.GetValue());
  DMViewDiag viewDiagDest(temp);
  ApplyBinOp(viewDiagDest.Begin(), viewDiagDest.End(), m1.Begin(),
             std::plus<double>());

  return temp;
}

DMatrixDiag operator+(const DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrixDiag temp(m1.GetM());
  GenerateBinOp2ndConst(temp.Begin(), temp.End(), m1.Begin(), m2.GetValue(),
                        std::plus<double>());

  return temp;
}

// binary - operator group
DMatrix operator-(const DMatrixDiag& m1, const DMatrix& m2) {
  DMatrix temp(-m2);
  temp += m1;

  return temp;
}

DMatrixDiag operator-(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrixDiag temp(m1.GetM());
  GenerateBinOp(temp.Begin(), temp.End(), m1.Begin(), m2.Begin(),
                std::minus<double>());

  return temp;
}

DMatrix operator-(const DMatrixDiag& m1, const DMatrixConst& m2) {
  return m1 + (-m2);
}

DMatrixDiag operator-(const DMatrixDiag& m1, const DMatrixCDiag& m2) {
  return m1 + (-m2);
}

// unary - operator
DMatrixDiag operator-(const DMatrixDiag& m) {
  DMatrixDiag temp(m.GetM());

  GenerateUnOp(temp.Begin(), temp.End(), m.Begin(), std::negate<double>());

  return temp;
}

DMatrixDiag& Neg(DMatrixDiag& m) {
  m.Clip();

  ApplyUnOp(m.Begin(), m.End(), NegMod<double>());

  return m;
}

// += operator group
DMatrixDiag& operator+=(DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp(m1.Begin(), m1.End(), m2.Begin(), PlusEq<double>());

  return m1;
}

DMatrixDiag& operator+=(DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp2ndConst(m1.Begin(), m1.End(), m2.GetValue(), PlusEq<double>());

  return m1;
}

// -= operator group
DMatrixDiag& operator-=(DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);
  m1.Clip();

  ApplyBinOp(m1.Begin(), m1.End(), m2.Begin(), OpMinusEq<double>());

  return m1;
}

DMatrixDiag& operator-=(DMatrixDiag& m1, const DMatrixCDiag& m2) {
  return m1 += (-m2);
}

// cross product group
DMatrix operator*(const DMatrixDiag& m1, const DMatrix& m2) {
  CheckAllowMul(m1, m2);
  DMatrix temp(m1.GetM(), m2.GetN());

  DMatrix::iterator itd = temp.Begin();
  DMatrix::const_iterator it2 = m2.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    for (unsigned int j = 0; j < m2.GetN(); ++j, ++itd, ++it2)
      *itd = (*it1) * (*it2);
  }
  return temp;
}

DMatrixDiag operator*(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);

  DMatrixDiag temp(m1.GetM());
  GenerateBinOp(temp.Begin(), temp.End(), m1.Begin(), m2.Begin(),
                std::multiplies<double>());

  return temp;
}

DMatrix operator*(const DMatrixDiag& m1, const DMatrixConst& m2) {
  CheckAllowMul(m1, m2);

  DMatrix temp(m1.GetM(), m2.GetN());
  DMatrix::iterator itd = temp.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    for (unsigned int j = 0; j < m2.GetN(); ++j, ++itd)
      *itd = (*it1) * m2.GetValue();
  }

  return temp;
}

DMatrixDiag operator*(const DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);

  return m1 * m2.GetValue();
}

// operator *= group
DMatrixDiag& operator*=(DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);
  m1.Clip();

  ApplyBinOp(m1.Begin(), m1.End(), m2.Begin(), std::multiplies<double>());

  return m1;
}

DMatrixDiag& operator*=(DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);

  return m1 *= m2.GetValue();
}

// product with scalar element
DMatrixDiag operator*(const DMatrixDiag& m, const double r) {
  if (r == 0)
    return DMatrixDiag(m.GetM(), 0);
  else if (r == 1) {
    DMatrixDiag temp;
    return temp.Clone(m);
  } else {
    DMatrixDiag temp(m.GetM());

    GenerateBinOp2ndConst(temp.Begin(), temp.End(), m.Begin(), r,
                          std::multiplies<double>());

    return temp;
  }
}

DMatrixDiag operator*(const double r, const DMatrixDiag& m) { return m * r; }

// self-product with scalar element
DMatrixDiag& operator*=(DMatrixDiag& m, const double r) {
  m.Clip();

  ApplyBinOp2ndConst(m.Begin(), m.End(), r, std::multiplies<double>());

  return m;
}

// Division
DMatrixDiag operator/(DMatrixDiag& m, const double r) { return (1. / r) * m; }

DMatrixDiag& operator/=(DMatrixDiag& m, const double r) {
  return m *= (1. / r);
}

// Kronecker product group
DMatrix operator^(const DMatrixDiag& m1, const DMatrix& m2) {
  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int olpr = nd - m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odr = ou_ + m2.GetN();

  DMatrix temp(md, nd, 0.);
  DMatrix::iterator itd = temp.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    if (*it1 == 0.)
      itd += odr;
    else if (*it1 == 1.) {
      DMatrix::const_iterator it2 = m2.Begin();

      for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
        for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2, ++itd)
          *itd = *it2;
      itd += m2.GetN();
    } else {
      DMatrix::const_iterator it2 = m2.Begin();
      for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
        for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2, ++itd)
          *itd = (*it1) * (*it2);

      itd += m2.GetN();
    }
  }
  return temp;
}

DMatrixDiag operator^(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  const unsigned int md = m1.GetM() * m2.GetM();

  DMatrixDiag temp(md);
  DMatrixDiag::iterator itd = temp.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    if (*it1 == 0.) {
      for (unsigned int ps2 = 0; ps2 < m2.GetM(); ++ps2, ++itd) *itd = 0.;
    } else if (*it1 == 1.) {
      for (DMatrixDiag::const_iterator it2 = m2.Begin(); it2 != m2.End();
           ++it2, ++itd)
        *itd = *it2;
    } else {
      for (DMatrixDiag::const_iterator it2 = m2.Begin(); it2 != m2.End();
           ++it2, ++itd)
        *itd = (*it1) * (*it2);
    }
  }
  return temp;
}

DMatrix operator^(const DMatrixDiag& m1, const DMatrixConst& m2) {
  unsigned int md = m1.GetM() * m2.GetM();
  unsigned int nd = m1.GetN() * m2.GetN();
  unsigned int olpr = nd - m2.GetN();
  unsigned int ou_ = nd * m2.GetM();
  unsigned int odr = ou_ + m2.GetN();

  DMatrix temp(md, nd, 0.);
  DMatrixDiag::iterator itd = temp.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    if (*it1 == 0.)
      itd += odr;
    else {
      const double r = (*it1) * m2.GetValue();
      for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, itd += olpr)
        for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++itd) *itd = r;
    }
  }

  return temp;
}

DMatrixDiag operator^(const DMatrixDiag& m1, const DMatrixCDiag& m2) {
  DMatrixDiag temp(m1.GetM() * m2.GetM());
  DMatrixDiag::iterator itd = temp.Begin();

  for (DMatrixDiag::const_iterator it1 = m1.Begin(); it1 != m1.End(); ++it1) {
    if (*it1 == 0.) {
      for (unsigned int i = 0; i < m2.GetM(); ++i, ++itd) *itd = 0;
    } else {
      for (unsigned int i = 0; i < m2.GetM(); ++i, ++itd) *itd = 0;
    }
  }
  return temp;
}

// ^= group
DMatrixDiag& operator^=(DMatrixDiag& m1, const DMatrixDiag& m2) {
  m1.Clip();
  return m1 = m1 ^ m2;
}

DMatrixDiag& operator^=(DMatrixDiag& m1, const DMatrixCDiag& m2) {
  m1.Clip();
  return m1 = m1 ^ m2;
}

// Kronecker sum: M1^E(M2.m) + E(M1.m)^M2
DMatrix operator|(const DMatrixDiag& m1, const DMatrix& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrixDiag operator|(const DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrixDiag& m1, const DMatrixConst& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrixDiag operator|(const DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

// |= group
DMatrixDiag& operator|=(DMatrixDiag& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  m1.Clip();
  return m1 = m1 | m2;
}

DMatrixDiag& operator|=(DMatrixDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  m1.Clip();
  return m1 = m1 | m2;
}

//========================= DMatrixDiag ==================================
double Norm(const DMatrixConst& m) { return fabs(m.GetValue()) * m.GetN(); }

// binary + operator group
DMatrix operator+(const DMatrixConst& m1, const DMatrix& m2) { return m2 + m1; }

DMatrix operator+(const DMatrixConst& m1, const DMatrixDiag& m2) {
  return m2 + m1;
}

DMatrixConst operator+(const DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  return DMatrixConst(m1.GetM(), m1.GetN(), m1.GetValue() + m2.GetValue());
}

DMatrix operator+(const DMatrixConst& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);
  DMatrix temp(m1.GetM(), m1.GetN(), m1.GetValue());

  DMViewDiag viewDiag(temp);
  ApplyBinOp2ndConst(viewDiag.Begin(), viewDiag.End(), m2.GetValue(),
                     PlusEq<double>());

  return temp;
}

// binary - operator group
DMatrix operator-(const DMatrixConst& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m1.GetM(), m1.GetN());
  GenerateBinOp1stConst(temp.Begin(), temp.End(), m1.GetValue(), m2.Begin(),
                        std::minus<double>());

  return temp;
}

DMatrix operator-(const DMatrixConst& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrix temp(m1.GetM(), m1.GetN(), m1.GetValue());
  DMViewDiag diagView(temp);
  ApplyBinOp(diagView.Begin(), diagView.End(), m2.Begin(), OpMinusEq<double>());

  return temp;
}

DMatrixConst operator-(const DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  return DMatrixConst(m1.GetM(), m1.GetN(), m1.GetValue() - m2.GetValue());
}

DMatrix operator-(const DMatrixConst& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);
  DMatrix temp(m1.GetM(), m1.GetN(), m1.GetValue());

  DMViewDiag viewDiag(temp);
  ApplyBinOp2ndConst(viewDiag.Begin(), viewDiag.End(), m2.GetValue(),
                     OpMinusEq<double>());

  return temp;
}

// unary - operator
DMatrixConst operator-(const DMatrixConst& m) {
  return DMatrixConst(m.GetM(), m.GetN(), -m.GetValue());
}

DMatrixConst& Neg(DMatrixConst& m) {
  m.Value() = -m.GetValue();
  return m;
}

// += operator
DMatrixConst& operator+=(DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  m1.Value() += m2.GetValue();

  return m1;
}

// -= operator
DMatrixConst& operator-=(DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  m1.Value() -= m2.GetValue();

  return m1;
}

// dot product group
DMatrix operator*(const DMatrixConst& m1, const DMatrix& m2) {
  CheckAllowMul(m1, m2);

  DMatrix temp(m1.GetM(), m2.GetN());
  const double r = m1.GetValue();

  for (unsigned int i = 0; i < m2.GetN(); ++i) {
    double r1 = 0;
    for (DMatrix::const_iterator it2 = m2.Begin() + i; it2 != m2.End() + i;
         it2 += m2.GetN())
      r1 += *it2;

    r1 *= r;
    for (DMatrix::iterator itd = temp.Begin() + i; itd != temp.End() + i;
         itd += m2.GetN())
      *itd = r1;
  }
  return temp;
}

DMatrix operator*(const DMatrixConst& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);

  DMatrix temp(m1.GetM(), m2.GetN());
  DMatrixDiag::const_iterator it2 = m2.Begin();

  for (unsigned int i = 0; i < m2.GetN(); ++i, ++it2) {
    const double r = m1.GetValue() * (*it2);
    for (DMatrix::iterator itd = temp.Begin() + i; itd != temp.End() + i;
         itd += m2.GetN())
      *itd = r;
  }
  return temp;
}

DMatrixConst operator*(const DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowMul(m1, m2);

  return DMatrixConst(m1.GetM(), m2.GetN(),
                      m1.GetValue() * m2.GetValue() * m1.GetN());
}

DMatrixConst operator*(const DMatrixConst& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);

  return DMatrixConst(m1.GetM(), m2.GetN(), m1.GetValue() * m2.GetValue());
}

// operator *=
DMatrixConst& operator*=(DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowMul(m1, m2);
  m1.Value() = m1.GetValue() * m2.GetValue() * m1.GetN();
  return m1;
}

DMatrixConst& operator*=(DMatrixConst& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);
  m1.Value() *= m2.GetValue();

  return m1;
}

// product with scalar element
DMatrixConst operator*(const DMatrixConst& m, const double r) { return r * m; }

DMatrixConst operator*(const double r, const DMatrixConst& m) {
  return DMatrixConst(m.GetM(), m.GetN(), m.GetValue() * r);
}

// self-product with scalar element
DMatrixConst& operator*=(DMatrixConst& m, const double r) {
  m.Value() *= r;

  return m;
}

DMatrixConst operator/(const DMatrixConst& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(const DMatrixConst& m, const double r): division by zero");

  return (1. / r) * m;
}

DMatrixConst& operator/=(DMatrixConst& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(const DMatrixConst& m, const double r): division by zero");

  return m *= (1. / r);
}

// Kronecker product group
DMatrix operator^(const DMatrixConst& m1, const DMatrix& m2) {
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(m1.GetM() * m2.GetM(), nd);

  const double r = m1.GetValue();
  DMatrix::const_iterator it2 = m2.Begin();

  if (r == 0)
    temp.FillZero();
  else if (r == 1.) {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2) {
      for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2) {
        DMatrix::iterator itd = temp.Begin() + i2 * nd + j2;

        for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr)
          for (unsigned int j1 = 0; j1 < m1.GetN(); ++j1, itd += m2.GetN())
            *itd = *it2;
      }
    }
  } else {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2) {
      for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2) {
        DMatrix::iterator itd = temp.Begin() + i2 * nd + j2;

        const double r1 = r * (*it2);
        for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr)
          for (unsigned int j1 = 0; j1 < m1.GetN(); ++j1, itd += m2.GetN())
            *itd = r1;
      }
    }
  }
  return temp;
}

DMatrix operator^(const DMatrixConst& m1, const DMatrixDiag& m2) {
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(m1.GetM() * m2.GetM(), nd, 0.);
  const double r = m1.GetValue();
  DMatrixDiag::const_iterator it2 = m2.Begin();

  if (r == 1.) {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, ++it2) {
      DMatrix::iterator itd = temp.Begin() + i2 * (nd + 1);
      for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr)
        for (unsigned int j1 = 0; j1 < m1.GetN(); ++j1, itd += m2.GetN())
          *itd = *it2;
    }
  } else if (r != 0.) {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2, ++it2) {
      DMatrix::iterator itd = temp.Begin() + i2 * (nd + 1);
      const double r1 = r * (*it2);

      for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr)
        for (unsigned int j1 = 0; j1 < m1.GetN(); ++j1, itd += m2.GetN())
          *itd = r1;
    }
  }
  return temp;
}

DMatrixConst operator^(const DMatrixConst& m1, const DMatrixConst& m2) {
  return DMatrixConst(m1.GetM() * m2.GetM(), m1.GetN() * m2.GetN(),
                      m2.GetValue() * m2.GetValue());
}

DMatrix operator^(const DMatrixConst& m1, const DMatrixCDiag& m2) {
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odmr = ou_ - nd;

  DMatrix temp(m1.GetM() * m2.GetM(), nd, 0.);

  if (const double r = m1.GetValue() * m2.GetValue()) {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2) {
      DMatrix::iterator itd = temp.Begin() + i2 * (nd + 1);
      for (unsigned int i1 = 0; i1 < m1.GetM(); ++i1, itd += odmr)
        for (unsigned int j1 = 0; j1 < m1.GetN(); ++j1, itd += m2.GetN())
          *itd = r;
    }
  }
  return temp;
}

// Kronecker sum: M1^E(M2.m) + E(M1.m)^M2 group
DMatrix operator|(const DMatrixConst& m1, const DMatrix& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrixConst& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrixConst& m1, const DMatrixConst& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrixConst& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrixConst& operator^=(DMatrixConst& m1, const DMatrixConst& m2) {
  return m1 = m1 ^ m2;
}

//================= DMatrixCDiag ===================
double Norm(const DMatrixCDiag& m) { return fabs(m.GetValue()); }

// binary + operator group
DMatrix operator+(const DMatrixCDiag& m1, const DMatrix& m2) { return m2 + m1; }

DMatrixDiag operator+(const DMatrixCDiag& m1, const DMatrixDiag& m2) {
  return m2 + m1;
}

DMatrix operator+(const DMatrixCDiag& m1, const DMatrixConst& m2) {
  return m2 + m1;
}

DMatrixCDiag operator+(const DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  return DMatrixCDiag(m1.GetM(), m1.GetValue() + m2.GetValue());
}

// += operator
DMatrixCDiag& operator+=(DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  m1.Value() += m2.GetValue();

  return m1;
}

// unary - operator
DMatrixCDiag operator-(const DMatrixCDiag& m) {
  return DMatrixCDiag(m.GetM(), -m.GetValue());
}

DMatrixCDiag& Neg(DMatrixCDiag& m) {
  m.Value() = -m.GetValue();
  return m;
}

// binary - operator
DMatrix operator-(const DMatrixCDiag& m1, const DMatrix& m2) {
  CheckAllowSum(m1, m2);

  return (-m2) + m1;
}

DMatrixDiag operator-(const DMatrixCDiag& m1, const DMatrixDiag& m2) {
  CheckAllowSum(m1, m2);

  DMatrixDiag temp(m1.GetM());
  GenerateBinOp1stConst(temp.Begin(), temp.End(), m1.GetValue(), m2.Begin(),
                        std::minus<double>());

  return temp;
}

DMatrix operator-(const DMatrixCDiag& m1, const DMatrixConst& m2) {
  CheckAllowSum(m1, m2);

  return (-m2) + m1;
}

DMatrixCDiag operator-(const DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  return DMatrixCDiag(m1.GetM(), m1.GetValue() - m2.GetValue());
}

// -= operator
DMatrixCDiag& operator-=(DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowSum(m1, m2);

  m1.Value() -= m2.GetValue();

  return m1;
}

// dot product group
DMatrix operator*(const DMatrixCDiag& m1, const DMatrix& m2) {
  CheckAllowMul(m1, m2);

  return m1.GetValue() * m2;
}

DMatrixDiag operator*(const DMatrixCDiag& m1, const DMatrixDiag& m2) {
  CheckAllowMul(m1, m2);

  return m1.GetValue() * m2;
}

DMatrixConst operator*(const DMatrixCDiag& m1, const DMatrixConst& m2) {
  CheckAllowMul(m1, m2);

  return DMatrixConst(m1.GetM(), m1.GetN(), m1.GetValue() * m2.GetValue());
}

DMatrixCDiag operator*(const DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);

  return DMatrixCDiag(m1.GetM(), m1.GetValue() * m2.GetValue());
}

// operator *=
DMatrixCDiag& operator*=(DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowMul(m1, m2);

  m1.Value() *= m2.GetValue();

  return m1;
}

// product with scalar element
DMatrixCDiag operator*(const double r, const DMatrixCDiag& m) {
  return DMatrixCDiag(m.GetM(), m.GetValue() * r);
}

DMatrixCDiag operator*(const DMatrixCDiag& m, const double r) {
  return DMatrixCDiag(m.GetM(), m.GetValue() * r);
}

// self-product with scalar element
DMatrixCDiag& operator*=(DMatrixCDiag& m, const double r) {
  m.Value() *= r;

  return m;
}

DMatrixCDiag operator/(const DMatrixCDiag& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(const DMatrixCDiag& m, const double r): division by zero");

  return (1. / r) * m;
}

DMatrixCDiag& operator/=(DMatrixCDiag& m, const double r) {
  if (r == 0)
    throw ELib::Exception(
        "operator /(const DMatrixCDiag& m, const double r): division by zero");

  return m *= (1. / r);
}

// Kronecker product group
DMatrix operator^(const DMatrixCDiag& m1, const DMatrix& m2) {
  if (m2.GetM() == 1 && m2.GetN() == 1)
    return (m2.GetV(0) * m1).GetAsOrdinary();
  else if (m1.GetM() == 1 && m1.GetN() == 1)
    return m1.GetValue() * m2;

  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odr = m2.GetN() + ou_;
  DMatrix temp(md, nd, 0.);

  if (m1.GetValue() != 0) {
    DMatrix::const_iterator it2 = m2.Begin();
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2) {
      for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2, ++it2) {
        const double r = m1.GetValue() * (*it2);

        const DMatrix::iterator itdBegin = temp.Begin() + i2 * nd + j2;
        for (DMatrix::iterator itd = itdBegin;
             itd != itdBegin + m1.GetM() * odr; itd += odr)
          *itd = r;
      }
    }
  }
  return temp;
}

DMatrixDiag operator^(const DMatrixCDiag& m1, const DMatrixDiag& m2) {
  DMatrixDiag temp(m1.GetM() * m2.GetM());

  if (m1.GetValue() == 0)
    temp.FillZero();
  else if (temp.GetM() > 0) {
    GenerateBinOp1stConst(temp.Begin(), temp.Begin() + m2.GetM(), m1.GetValue(),
                          m2.Begin(), std::multiplies<double>());

    for (unsigned int i = 1; i < m1.GetM(); ++i)
      std::copy_n(temp.Begin(), m2.GetM(), temp.Begin() + i * m2.GetM());
  }
  return temp;
}

DMatrix operator^(const DMatrixCDiag& m1, const DMatrixConst& m2) {
  const unsigned int md = m1.GetM() * m2.GetM();
  const unsigned int nd = m1.GetN() * m2.GetN();
  const unsigned int ou_ = nd * m2.GetM();
  const unsigned int odr = ou_ + m2.GetN();

  DMatrix temp(md, nd, 0);
  const double r = m1.GetValue() * m2.GetValue();

  if (r != 0) {
    for (unsigned int i2 = 0; i2 < m2.GetM(); ++i2) {
      for (unsigned int j2 = 0; j2 < m2.GetN(); ++j2) {
        const DMatrix::iterator itdBegin = temp.Begin() + i2 * nd + j2;
        for (DMatrix::iterator itd = itdBegin;
             itd != itdBegin + m1.GetM() * odr; itd += odr)
          *itd = r;
      }
    }
  }
  return temp;
}

DMatrixCDiag operator^(const DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  return DMatrixCDiag(m1.GetM() * m2.GetM(), m1.GetValue() * m2.GetValue());
}

// Kronecker sum group: M1^E(M2.m) + E(M1.m)^M2
DMatrix operator|(const DMatrixCDiag& m1, const DMatrix& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrixDiag operator|(const DMatrixCDiag& m1, const DMatrixDiag& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrix operator|(const DMatrixCDiag& m1, const DMatrixConst& m2) {
  CheckAllowKSum(m1, m2);
  return (m1 ^ DMatrixCDiag::I(m2.GetM())) + (DMatrixCDiag::I(m1.GetM()) ^ m2);
}

DMatrixCDiag operator|(const DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);
  return DMatrixCDiag(m1.GetM() * m2.GetM(), m1.GetValue() + m2.GetValue());
}

DMatrixCDiag& operator^=(DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  return m1 = m1 ^ m2;
}

DMatrixCDiag& operator|=(DMatrixCDiag& m1, const DMatrixCDiag& m2) {
  CheckAllowKSum(m1, m2);

  return m1 = m1 | m2;
}

}  // namespace QLib
