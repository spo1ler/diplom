/********************************************************************
  created:  2004/05/02
  created:  2:5:2004   18:58
  filename: QRamaswamiPH.cpp
  author:   Dmitry Orlovsky

  purpose:  Ramaswami approach to PH-service support implementation
*********************************************************************/

#include "QLib/QRamaswamiPH.h"

#include <assert.h>

#include "QLib/QMatrixArithm.h"

namespace QLib {

using namespace ELib;

//-----------------------------------------------------------------------------
RamaswamiPHCalculator::RamaswamiPHCalculator(ILogger& logger)
    : Logged(logger) {}

DMatrix RamaswamiPHCalculator::CreateSTilde(const DMatrix& mS) const {
  if (mS.GetM() != mS.GetN() || mS.GetM() == 0) {
    throw Exception(
        "RamaswamiPHCalculator::CreateSTilde(const DMatrix& mS) failed: "
        "condition (mS.GetM()[%d] == mS.GetN()[%d]) && (mS.GetM() != 0) "
        "violated",
        mS.GetM(), mS.GetN());
  }

  DMatrix mSTilde(mS.GetM() + 1, mS.GetM() + 1);
  mSTilde.Set(0, 0, 0);
  CopyBlk(mSTilde, 1, 1, mS);

  const DMatrix mS0 = -t1(mS);

  for (unsigned int l = 0; l < mS.GetM(); ++l) {
    mSTilde.Set(l + 1, 0, mS0.GetV(l));
    mSTilde.Set(0, l + 1, 0.);
  }

  return mSTilde;
}

//-----------------------------------------------------------------------------
void RamaswamiPHCalculator::CreateABlocks(VectorDM& dest, unsigned int nC,
                                          const DMatrix& mS) const {
  assert(mS.GetM() == mS.GetN() && mS.GetM() >= 2);

  const unsigned int cnM = mS.GetM();

  VectorDM& a_mAi = dest;  // A_i is stored at a_mAi[i-1]
  VectorDM a_mUi;          // U_i is stored at a_mUi[i-1]
  VectorDM a_mLi;          // L_i is stored at a_mLi[i]
  a_mAi.resize(nC);

  // Step 1. Initialization.

  const double cfSUEl = mS.Get(cnM - 2, cnM - 1);
  const double cfSLEl = mS.Get(cnM - 1, cnM - 2);
  for (unsigned int i = 1; i <= nC; ++i) {
    DMatrix& mAInit = a_mAi[i - 1];
    mAInit.InitZero(i + 1, i + 1);
    for (unsigned int j = 0; j < i; ++j) {
      mAInit.Set(j, j + 1, (i - j) * cfSUEl);
      mAInit.Set(j + 1, j, (j + 1) * cfSLEl);
    }
  }

  // Step 2-3, 5-7
  VectorDM a_mAiTmp(nC, DMatrix());
  for (unsigned int k = 1; k + 2 <= cnM; ++k) {
    // Step 4
    const int cnTDim = k + 2;
    DMatrix mT(cnTDim, cnTDim);

    CopyBlk(mT, 0, 0, mS, cnM - cnTDim, cnM - cnTDim, cnTDim, cnTDim);  // 4a
    CreateUBlocks(a_mUi, nC, mT);                                       // 4b
    CreateLBlocks(a_mLi, nC, mT);                                       // 4c

    // 4d defining the size of A_i
    const int cnOffsBase = a_mLi.back().GetN();
    int nADim = cnOffsBase;

    for (unsigned i = 1; i <= nC; ++i) {
      // filling in the new A_i
      DMatrix& mA = a_mAiTmp[i - 1];
      nADim += a_mUi[nC - i].GetN();
      mA.InitZero(nADim, nADim);

      // copying blocks
      int nAOffs = 0;
      double fNom = i;
      double fDenom = nC;

      VectorDM::const_iterator itA = a_mAi.begin();
      VectorDM::const_reverse_iterator itL =
          static_cast<const VectorDM&>(a_mLi).rbegin();
      VectorDM::const_reverse_iterator itU =
          static_cast<const VectorDM&>(a_mUi).rbegin();

      for (int nBOffs = cnOffsBase; nBOffs < nADim;
           ++itA, ++itL, ++itU, --fNom, --fDenom) {
        const int cnDBOfs = itA->GetM();
        CopyBlk(mA, nBOffs, nBOffs, *itA);
        CopyBlk(mA, nBOffs, nAOffs, *itL);
        CopyBlk(mA, nAOffs, nBOffs, (fNom / fDenom) * (*itU));

        nAOffs = nBOffs;
        nBOffs += cnDBOfs;
      }
    }
    a_mAi.swap(a_mAiTmp);
  }
}

//-----------------------------------------------------------------------------
void RamaswamiPHCalculator::CreateUBlocks(VectorDM& dest, unsigned int nC,
                                          const DMatrix& mT) const {
  assert(mT.GetM() == mT.GetN());

  // Step 1
  const unsigned int cnR = mT.GetM();
  VectorDM& a_mUi = dest;  // U_i is stored at a_mUi[i-1]
  a_mUi.resize(nC);

  // Step 2. Initialization.
  const double cfTEl = mT.Get(0, cnR - 1);
  for (VectorDM::reverse_iterator it1 = a_mUi.rbegin(); it1 != a_mUi.rend();
       ++it1)
    it1->Init(1, 1, cfTEl);

  // Step 3
  VectorDM a_mUiTmp(nC, DMatrix());
  for (unsigned int k = 1; k + 2 <= cnR; ++k) {
    // Step 5
    const double cfTElk = mT.Get(0, cnR - k - 1);
    const unsigned int cnOffsBase = a_mUi.back().GetM();
    unsigned int nUMDim = cnOffsBase;

    for (unsigned int i = nC; i >= 1; --i) {
      // filling in the new U_i
      DMatrix& mU = a_mUiTmp[i - 1];
      mU.InitZero(nUMDim, nUMDim + a_mUi[i - 1].GetN());
      for (unsigned int l = 0; l < nUMDim; ++l) mU.Set(l, l, cfTElk);

      // copying blocks
      unsigned int nAOffs = 0;
      VectorDM::const_reverse_iterator itU =
          static_cast<const VectorDM&>(a_mUi).rbegin();

      for (unsigned int nBOffs = cnOffsBase; nAOffs < nUMDim; ++itU) {
        const unsigned int cnDBOfs = itU->GetN();
        CopyBlk(mU, nAOffs, nBOffs, *itU);

        nAOffs = nBOffs;
        nBOffs += cnDBOfs;
      }
      nUMDim += a_mUi[i - 1].GetN();
    }
    a_mUi.swap(a_mUiTmp);
  }
  for (unsigned int i = 2; i <= nC; ++i) a_mUi[i - 1] *= i;
}

//-----------------------------------------------------------------------------
void RamaswamiPHCalculator::CreateLBlocks(VectorDM& dest, unsigned int nC,
                                          const DMatrix& mT) const {
  assert(mT.GetM() == mT.GetN());

  // Step 1
  const unsigned int cnR = mT.GetM();

  VectorDM& a_mLi = dest;  // L_i is stored at a_mLi[i]
  a_mLi.resize(nC);

  // Step 2. Initialization.
  for (unsigned int i = nC - 1; i + 1 > 0; --i)
    a_mLi[i].Init(1, 1, (nC - i) * mT.Get(cnR - 1, 0));

  // Step 3
  VectorDM a_mLiTmp(nC, DMatrix());
  for (unsigned int k = 1; k + 2 <= cnR; ++k) {
    // Step 5
    const unsigned int cnOffsBase = a_mLi.back().GetN();
    unsigned int nUNDim = cnOffsBase;

    for (unsigned i = nC - 1; i + 1 > 0; --i) {
      // filling in the new L_i
      DMatrix& mL = a_mLiTmp[i];
      mL.InitZero(nUNDim + a_mLi[i].GetM(), nUNDim);

      const double cfTEl = mT.Get(cnR - k - 1, 0);
      double fTElk = (nC - i) * cfTEl;

      // copying blocks
      unsigned int nAOffs = 0;
      unsigned int l = nAOffs;
      VectorDM::const_reverse_iterator itL = a_mLi.rbegin();

      for (unsigned int nBOffs = cnOffsBase; nAOffs < nUNDim; ++itL) {
        const unsigned int cnDBOfs = itL->GetM();
        CopyBlk(mL, nBOffs, nAOffs, *itL);

        for (; l < nBOffs; ++l) mL.Set(l, l, fTElk);
        fTElk -= cfTEl;

        nAOffs = nBOffs;
        nBOffs += cnDBOfs;
      }
      nUNDim += a_mLi[i].GetM();
    }
    a_mLi.swap(a_mLiTmp);
  }
}

//-----------------------------------------------------------------------------
void RamaswamiPHCalculator::CreatePBlocks(VectorDM& dest, unsigned int nC,
                                          const DMatrix& vBeta) const {
  LogAutoPusher logPusher(GetLogger(), ILogger::MA_LIB_DUMP_MINOR);

  assert(vBeta.GetM() == 1);

  const unsigned int cnM = vBeta.GetN();
  VectorDM& a_mPj = dest;  // P_j is stored at a_mPj[j]
  a_mPj.resize(nC);

  // Step 1.
  const double cfLastBeta = vBeta.GetV(cnM - 1);
  const double cfPreLastBeta = vBeta.GetV(cnM - 2);
  for (unsigned int j = 1; j + 1 <= nC; ++j) {
    DMatrix& mPj = a_mPj[j];
    mPj.InitZero(j + 1, j + 2);
    for (unsigned int i = 0; i < j + 1; ++i) {
      mPj.Set(i, i, cfPreLastBeta);
      mPj.Set(i, i + 1, cfLastBeta);
    }

    GetLogger() << "k = 0; P_[" << j << "] = {\n" << a_mPj[j] << "\n}\n";
  }

  // Step 2 etc.
  VectorDM a_mPjTmp(nC, DMatrix());
  for (unsigned int k = 1; k + 2 <= cnM; ++k) {
    // define new P_j size
    unsigned int nPMDim = k + 2;
    for (unsigned int j = 1; j + 1 <= nC; ++j) {
      DMatrix& mPj = a_mPjTmp[j];
      mPj.InitZero(nPMDim, nPMDim + a_mPj[j].GetN());

      for (unsigned int i = 1; i < k + 2; ++i)
        mPj.Set(0, i, vBeta.GetV(cnM - (k + 2) + i));
      const double cfEl = vBeta.GetV(cnM - (k + 2));
      for (unsigned int i = 0; i < nPMDim; ++i) mPj.Set(i, i, cfEl);

      // copying blocks
      unsigned int nAOffs = 1;
      VectorDM::const_iterator it = a_mPj.begin();
      ++it;

      for (unsigned int nBOffs = k + 2; nAOffs < nPMDim; ++it) {
        const unsigned int cnDBOfs = it->GetN();
        CopyBlk(mPj, nAOffs, nBOffs, *it);

        nAOffs = nBOffs;
        nBOffs += cnDBOfs;
      }

      nPMDim += a_mPj[j].GetN();

      GetLogger() << "k = " << k << "; P_[" << j << "] = {\n" << a_mPjTmp[j]
                  << "\n}\n";
    }
    a_mPj.swap(a_mPjTmp);
  }

  dest[0] = vBeta;
}

//-----------------------------------------------------------------------------
DMatrix RamaswamiPHCalculator::GetA(unsigned int nC, const DMatrix& mS) const {
  VectorDM a_mAi;
  CreateABlocks(a_mAi, nC, mS);
  return a_mAi.back();
}

//-----------------------------------------------------------------------------
// theorem 2, p.424
DMatrix RamaswamiPHCalculator::CalcDiagEntries(unsigned int nC,
                                               const DMatrix& mS) const {
  const unsigned int cnM = mS.GetM();
  VectorDM avU, avU1;
  avU.resize(cnM);
  avU1.resize(cnM);

  for (unsigned int i = 0; i < cnM; ++i) avU[i].Init(1, 1, mS.Get(i, i));

  for (unsigned int k = 2; k <= nC; ++k) {
    avU1[cnM - 1] = avU[cnM - 1];

    for (unsigned int i = cnM - 2; i + 1 > 0; --i) {
      DMatrix& vPrevU = avU1[i + 1];
      DMatrix& vNewU = avU1[i];
      DMatrix& vOldU = avU[i];

      vNewU.Init(1, vOldU.GetDLen() + vPrevU.GetDLen());
      unsigned int nDest = 0;
      for (unsigned int nSrc = 0; nSrc < vOldU.GetDLen(); ++nSrc, ++nDest)
        vNewU.SetV(nDest, vOldU.GetV(nSrc));
      for (unsigned int nSrc = 0; nSrc < vPrevU.GetDLen(); ++nSrc, ++nDest)
        vNewU.SetV(nDest, vPrevU.GetV(nSrc));
    }

    for (unsigned int i = 0; i < cnM; ++i)
      avU[i].Assign(avU1[i] + DMatrixConst(1, avU1[i].GetDLen(), mS.Get(i, i)));
  }

  unsigned int nDest = 0;
  for (unsigned int i = 0; i < cnM; ++i) nDest += avU[i].GetDLen();

  DMatrix mRet(1, nDest);
  nDest = 0;

  for (VectorDM::const_iterator it = avU.begin(); it != avU.end(); ++it) {
    for (unsigned int nSrc = 0; nSrc < it->GetDLen(); ++nSrc, ++nDest)
      mRet.SetV(nDest, it->GetV(nSrc));
  }

  return mRet;
}

}  // namespace QLib
