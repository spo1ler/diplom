/********************************************************************
        created:	8:11:2005   18:05
        filename: QBlockDMatrix.h
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#ifndef __QBlockDMatrix_h__
#define __QBlockDMatrix_h__

#include "QLib/QMatrix.h"

#include <vector>
#include <iterator>

namespace QLib {

/*!
  Structure holds an information about the row (column) of block matrix:
    its dimension and 0-based offset of its topmost (leftmost) entry in
    expanded form of block matrix

  \sa IMatrixStructInfo, IBlockDMatrix
*/
struct MatrixBlockInfo {
  //! Constructs a new MatrixBlockInfo structure holding given dimension and
  // offset values
  inline explicit MatrixBlockInfo(unsigned int dimension = 0,
                                  unsigned int offset = 0)
      : dim(dimension), off(offset) {}

  //! Returns the offset of the first entry to the right (to the bottom) after
  // the
  //!   last entry of the current block.
  inline unsigned int EndOffset() const { return off + dim; }

  //! The dimension of the block of a block matrix
  unsigned int dim;

  //! 0-based offset of the topmost (leftmost) entry of the block in expanded
  // form of a block matrix
  unsigned int off;
};

//! Base class for block matrices whose blocks are double-value matrices.
/*!
  Block matrix can be treated as matrix whose entries are matrices, not scalars.

  By substituting the matrix blocks to the block matrix we obtain the usual
  matrix
    with scalar values. We call it the expanded form of block matrix.

  When working with block matrices the term 'dimension' means the number of
  matrix rows
    (columns) of block matrix, not the number of rows (columns) of its expanded
  form. The
    dimension can be either finite or infinite. In practice all algorithms work
  only with finite
    part of infinite matrix, so in our case the infinite matrix means the matrix
  of dimension not
    greater than 2^32-1.

  Because of the block matrices currently are used only to represent the
  infinitesimal generators,
    the following assumptions are made:
    - Number of matrix rows of block matrix is equal to number of its matrix
  columns, or
      they are both infinite.
    - The diagonal entries of block matrix are square matrices. This property
  ensures that the number
      of columns of i-th matrix column is equal to the number of rows of i-th
  matrix row.
*/
class IBlockDMatrix {
 public:
  virtual ~IBlockDMatrix();

  //! \return The dimension (number of block rows or columns) of block matrix
  inline unsigned int GetDim() const { return m_dim; }

  //! Returns an information about the dimension and the offset of a matrix row
  //(column) in an
  //!   expanded form of the block matrix.
  /*!
    \param i number of row (column)
    \sa MatrixBlockInfo
  */
  virtual MatrixBlockInfo InfoAt(unsigned int i) const = 0;

  //! Returns an information about the first and last non-zero block in given
  // row of a block matrix.
  /*!
    \param row number of the row
    \param first address of variable to store the 0-based index of the first
    non-zero block of a row.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given row.
    \sa GetColInfo()
  */
  virtual unsigned int GetRowInfo(unsigned int row,
                                  unsigned int* first = NULL) const = 0;

  //! Returns an information about the first and last non-zero block in given
  // column of a block matrix.
  /*!
    \param col number of the column
    \param first address of variable to store the 0-based index of the first
    non-zero block of a column.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given column.
    \sa GetRowInfo()
  */
  virtual unsigned int GetColInfo(unsigned int col,
                                  unsigned int* first = NULL) const = 0;

  //! Checks whether the matrix stored at position (i, j) is zero.
  virtual bool IsZeroAt(unsigned int i, unsigned int j) const = 0;

  //! Returns the matrix stored at position (i, j).
  virtual DMatrix Get(unsigned int i, unsigned int j) const = 0;

  //! This constant must be returned instead of the infinity value.
  //! Compare the dimensions with this constant to check whether it is finite.
  static const unsigned int INF = -1;

 protected:
  //! Constructs the block matrix object and sets the dimension value
  IBlockDMatrix(unsigned int dim = 0);

  //! Sets the dimension value
  inline void SetDimension(unsigned int dim) { m_dim = dim; }

  //! Checks whether the block matrix is big enough to contain the entry
  //!   with given row and column. Throws an exception if the dimension is too
  // small.
  void CheckRange(unsigned int row, unsigned int col) const;

 private:
  //! The dimension (number of matrix rows and columns) of the block matrix
  unsigned int m_dim;
};

// IMatrixStructInfo
class IMatrixStructInfo {
 public:
  virtual ~IMatrixStructInfo() {};

  virtual MatrixBlockInfo At(unsigned int i) const = 0;
};

/*!
  Class accepts the sequence of the dimensions of successive matrix blocks
    of the block matrix, calculates their offsets and provides the
    information about them in form of MatrixBlockInfo structures.
  Can be used to overload the IBlockDMatrix::InfoAt() function.
  Can be used with both finite and infinite block matrices. In the latter case
    the last stored block dimension is supposed to be the dimension of the rest
  of
    the blocks.

  \sa IBlockDMatrix
*/
class MatrixStructInfoStoring : public IMatrixStructInfo {
  typedef std::vector<MatrixBlockInfo> Blocks;

 public:
  //! Constant iterator type to iterate through the stored dimensions and
  // offsets of the blocks.
  typedef Blocks::const_iterator ConstIterator;

  //! Constructs an empty object ready to accept the block dimensions.
  MatrixStructInfoStoring();

  //! Stores the dimension of the next matrix block.
  /*!
    Can be used after the Finalize() method without calling the Clear().
    Then the new dimension is added after the last currently stored one.
    Another call of Finalize() function will recalculate the offsets.
  */
  void PushDim(unsigned int dim);

  //! This method must be called after the last block dimension is stored.
  /*!
    Method calculates the offsets of the block using their dimensions
      and sets the flag determining whether the block matrix is finite.
    Without calling this method the At() function will return incorrect offset
    values.

    \param isFinite must be set to 'false' (default value is 'true') to
      work with infinite block matrix. In this case the dimension of the last
    stored block
      is supposed to be the dimension of the rest of the blocks.
  */
  void Finalize(bool isFinite = true);

  //! Clears the information about the blocks dimensions and offsets.
  void Clear();

  //! Calculates the total dimension of an expanded form of the block matrix.
  /*!
    For the infinite matrix returns IBlockDMatrix::INF constant.
  */
  unsigned int TotalDim() const;

  //! Returns the MatrixBlockInfo structure holding the dimension and the offset
  // of given matrix block.
  /*!
    Implements IMatrixStructInfo::At() method
    \param i number of block
    \sa MatrixBlockInfo
  */
  virtual MatrixBlockInfo At(unsigned int i) const;

  inline ConstIterator Begin() const { return m_blocks.begin(); }

  inline ConstIterator End() const { return m_blocks.end(); }

 private:
  bool m_isFinite;
  Blocks m_blocks;
};

/*!
  Class provides an information about the offsets and dimensions of the blocks
  of block matrix
    in case when all the blocks have the same size.
  Can be used to overload the IBlockDMatrix::InfoAt() function bot for finite
  and infinite block matrices.

  \sa IBlockDMatrix
*/
class MatrixStructInfoConst : public IMatrixStructInfo {
 public:
  //! Constructs MatrixStructInfoConst object with dimension and block size set
  // to 0
  MatrixStructInfoConst();

  //! Constructs MatrixStructInfoConst object
  /*!
    \param dim dimension of the block matrix. Use IBlockDMatrix::INF constant to
    specify the infinite dimension.
    \param blockSize dimension of each matrix block of the block matrix
  */
  MatrixStructInfoConst(unsigned int dim, unsigned int blockSize);

  //! initializes (reinitializes) MatrixStructInfoConst object
  /*!
    \param dim dimension of the block matrix. Use IBlockDMatrix::INF constant to
    specify the infinite dimension.
    \param blockSize dimension of each matrix block of the block matrix
  */
  void Init(unsigned int dim, unsigned int blockSize);

  //! Returns the MatrixBlockInfo structure holding the dimension and the offset
  // of given matrix block.
  /*!
    Implements IMatrixStructInfo::At() method
    \param i number of block
    \sa MatrixBlockInfo
  */
  virtual MatrixBlockInfo At(unsigned int i) const;

  //! Returns the dimension of the block matrix. IBlockDMatrix::INF means the
  // infinite dimension.
  inline unsigned int GetDim() const { return m_dim; }

  //! Returns the size of the block of the block matrix.
  inline unsigned int GetBlockSize() const { return m_blockSize; }

 private:
  //! Dimension of the block matrix. IBlockDMatrix::INF means the infinite
  // dimension.
  unsigned int m_dim;

  //! Size of the block of the block matrix.
  unsigned int m_blockSize;
};

// BlockDMatrixStoring
class BlockDMatrixStoring : public IBlockDMatrix {
 public:
  BlockDMatrixStoring();

  explicit BlockDMatrixStoring(const std::vector<unsigned int>& blkSizes) {
    Init(blkSizes);
  }

  template <class Iterator>
  BlockDMatrixStoring(Iterator blkSizeBegin, Iterator blkSizeEnd) {
    Init(blkSizeBegin, blkSizeEnd);
  }

  template <class Iterator>
  void Init(Iterator blkSizeBegin, Iterator blkSizeEnd) {
    std::vector<unsigned int> blkSizes(blkSizeBegin, blkSizeEnd);
    Init(blkSizes);
  }

  void Init(const std::vector<unsigned int>& blkSizes);

  void InitRegular(unsigned int dim, unsigned int blockSize);

  void Clear();

  void Set(unsigned int i, unsigned int j, const DMatrix& m);

  //! Returns an information about the first and last non-zero block in given
  // row of a block matrix.
  /*!
    Implements IBlockDMatrix::GetRowInfo() method
    \param row number of the row
    \param first address of variable to store the 0-based index of the first
    non-zero block of a row.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given row.
    \sa IBlockDMatrix, GetColInfo()
  */
  virtual unsigned int GetRowInfo(unsigned int row,
                                  unsigned int* first = NULL) const;

  //! Returns an information about the first and last non-zero block in given
  // column of a block matrix.
  /*!
    Implements IBlockDMatrix::GetColInfo() method
    \param col number of the column
    \param first address of variable to store the 0-based index of the first
    non-zero block of a column.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given column.
    \sa IBlockDMatrix, GetRowInfo()
  */
  virtual unsigned int GetColInfo(unsigned int col,
                                  unsigned int* first = NULL) const;

  //! Returns an information about the dimension and the offset of a matrix row
  //(column) in an
  //!   expanded form of the block matrix.
  /*!
    Implements IBlockDMatrix::InfoAt() method
    \param i number of row (column)
    \sa IBlockDMatrix, MatrixBlockInfo
  */
  virtual MatrixBlockInfo InfoAt(unsigned int i) const;

  //! Returns the matrix stored at position (i, j).
  /*!
    Implements IBlockDMatrix::Get() method
    \param i number of row (column)
    \sa IBlockDMatrix
  */
  virtual DMatrix Get(unsigned int i, unsigned int j) const;

  //! Checks whether the matrix stored at position (i, j) is zero.
  /*!
    Implements IBlockDMatrix::IsZeroAt() method
    \param i number of row (column)
    \sa IBlockDMatrix
  */
  virtual bool IsZeroAt(unsigned int i, unsigned int j) const;

 protected:
  struct FirstLastInfo {
    unsigned int iFirst;
    unsigned int iLast;

    inline FirstLastInfo() : iFirst(0), iLast(0) {}
  };

  void InitArrays(unsigned int dim);
  void UpdateStructInfoAfterStoring(unsigned int i, unsigned int j);
  unsigned int OffsetByIJ(unsigned int i, unsigned int j) const;

  unsigned int GetRCInfo(const std::vector<FirstLastInfo>& firstlast,
                         unsigned int idx, unsigned int* piFirst) const;

 private:
  std::vector<FirstLastInfo> m_aRowsInfo;  // rows-information array.
  std::vector<FirstLastInfo> m_aColsInfo;  // columns-information array.

  VectorDM m_storage;
  std::vector<bool> m_zeroes;

  MatrixStructInfoStoring m_info;
};

//! IDQBDGenerator,
//! Double-value Quasi-birth-and-death generator
class IDQBDGenerator : public IBlockDMatrix {
 public:
  //! Returns an information about the first and last non-zero block in given
  // row of a block matrix.
  /*!
    Implements IBlockDMatrix::GetRowInfo() method
    \param row number of the row
    \param first address of variable to store the 0-based index of the first
    non-zero block of a row.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given row.
    \sa IBlockDMatrix, GetColInfo()
  */
  virtual unsigned int GetRowInfo(unsigned int row,
                                  unsigned int* first = NULL) const;

  //! Returns an information about the first and last non-zero block in given
  // column of a block matrix.
  /*!
    Implements IBlockDMatrix::GetColInfo() method
    \param col number of the column
    \param first address of variable to store the 0-based index of the first
    non-zero block of a column.
      If set to NULL then this parameter must be ignored.
    \return 0-based index of first zero block after the last non-zero one in
    given column.
    \sa IBlockDMatrix, GetRowInfo()
  */
  virtual unsigned int GetColInfo(unsigned int col,
                                  unsigned int* first = NULL) const;

  //! Checks whether the matrix stored at position (i, j) is zero.
  /*!
    Implements IBlockDMatrix::IsZeroAt() method
    \param i number of row (column)
    \sa IBlockDMatrix
  */
  virtual bool IsZeroAt(unsigned int i, unsigned int j) const;

 protected:
  IDQBDGenerator();

  IDQBDGenerator(unsigned int maxMinus, unsigned int maxPlus,
                 unsigned int dim = IBlockDMatrix::INF);

  void Init(unsigned int maxMinus, unsigned int maxPlus,
            unsigned int dim = IBlockDMatrix::INF);

 private:
  unsigned int m_maxMinus;
  unsigned int m_maxPlus;
};

}  // namespace QLib

#endif  //__QBlockDMatrix_h__
