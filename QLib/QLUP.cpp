/********************************************************************
        created:	2006/02/21   02:27
        filename: QLUP.cpp
        author:		Dmitry Orlovsky

        purpose:
*********************************************************************/

#include "QLib/QLUP.h"

#include <cmath>
#include <memory>
#include <stdint.h>

#ifdef USE_BLAS
#include <cblas.h>
#include <lapacke.h>
#endif

#include "ELib/EXception.h"
#include "QLib/QMatrixViews.h"

namespace QLib {
using namespace ELib;

#ifdef USE_BLAS

namespace {
void CheckIsSquare(const DMatrix &m) {
  if (m.GetM() == 0 || m.GetM() != m.GetN()) {
    throw Exception("m[%dx%d] must be square non-empty matrix", m.GetM(),
                    m.GetN());
  }
}
}  // namespace

double Det(const DMatrix &m) {
  CheckIsSquare(m);
  DMatrix t;
  t.Clone(m);
  std::vector<int> pivots(t.GetM());

  LAPACKE_dgetrf(CblasRowMajor, t.GetM(), t.GetN(), t.Data(), t.GetN(),
                 &pivots[0]);
  double result = 1.;
  for (int i = 0; i < t.GetM(); ++i) {
    result *= t.At(i, i);
  }
  return result;
}

DMatrix Inversion(const DMatrix &m) {
  CheckIsSquare(m);
  DMatrix t;
  t.Clone(m);
  std::vector<int> pivots(t.GetM());

  const int info = LAPACKE_dgetrf(CblasRowMajor, t.GetM(), t.GetN(), t.Data(),
                                  t.GetN(), &pivots[0]);
  if (info > 0) {
    throw Exception("Unable to inverse a singular matrix");
  }
  LAPACKE_dgetri(CblasRowMajor, t.GetN(), t.Data(), t.GetN(), &pivots[0]);
  return t;
}

DMatrix LSolve(const DMatrix &m, const DMatrix &b) {
  CheckIsSquare(m);
  DMatrix t;
  t.Clone(m);
  std::vector<int> pivots(t.GetM());

  DMatrix result;
  result.Clone(b);

  const int info = LAPACKE_dgetrf(CblasRowMajor, t.GetM(), t.GetN(), t.Data(),
                                  t.GetN(), &pivots[0]);
  if (info > 0) {
    throw Exception("Unable to solve a system with singular matrix");
  }
  LAPACKE_dgetrs(CblasRowMajor, 'N', t.GetN(), 1, t.Data(), t.GetN(),
                 &pivots[0], result.Data(), t.GetN());
  return result;
}

// Solves linear system x*mM == 0 && x*vf == 1 and returns x
DMatrix LSolveRowNorm(const DMatrix &mM, const DMatrix &vf) {
  CheckIsSquare(mM);
  if (vf.GetN() != 1 || vf.GetM() != mM.GetM()) {
    throw Exception(
        "DLUP::LSolveRowNorm(const DMatrix &mM) failed: "
        "vf[%dx%d] must be a column vector of length %d",
        vf.GetM(), vf.GetN(), mM.GetM());
  }

  DMatrix t;
  t.Clone(mM);
  // Replace first column of the system matrix with vf.
  for (int i = 0; i < t.GetM(); ++i) {
    t.At(i, 0) = vf.GetV(i);
  }

  DMatrix result(1, t.GetM(), 0);
  // Right hand is all zeroes except to the first entry which is 1.
  result.SetV(0, 1.);

  std::vector<int> pivots(t.GetN());
  const int info = LAPACKE_dgetrf(CblasRowMajor, t.GetM(), t.GetN(), t.Data(),
                                  t.GetN(), &pivots[0]);
  if (info > 0) {
    throw Exception("Unable to solve a system with singular matrix");
  }
  LAPACKE_dgetrs(CblasRowMajor, 'T', t.GetN(), 1, t.Data(), t.GetN(),
                 &pivots[0], result.Data(), 1);
  return result;
}

#else  // USE_BLAS

class DLUP {
 public:
  //! Creates LUP-decomposition of matrix m
  DLUP(const DMatrix &m);

  //! Returns the system dimension
  inline unsigned int GetN() const { return m_mN; }

  //! Returns the determinant of the decomposed matrix
  inline double Det() const { return m_det; }

  //! Inverts itself and returns result
  DMatrix Inversion() const;

  //! Solves linear system self*x = b and returns x
  DMatrix LSolve(const DMatrix &b) const;

 private:
  DMatrix m_mLU;  // L && U decomposition matrices
  unsigned int m_mN;
  double m_det;
  std::vector<int> m_aP;  // Pivots array
};

DLUP::DLUP(const DMatrix &m) {
  if (m.GetM() != m.GetN())
    throw Exception(
        "DLUP::DLUP failed: trying to LUP-decompose non-square matrix.");

  m_det = 1.;
  m_mLU.Clone(m);
  m_mN = m.GetN();

  m_aP.reserve(GetN());
  for (unsigned int i = 0; i < GetN(); ++i) m_aP.push_back(i);

  // LUP-Decomposition
  DMatrix::iterator itKK = m_mLU.Begin();
  for (unsigned int k = 0; k < GetN(); ++k, itKK += (GetN() + 1)) {
    unsigned int j;
    double r = 0.;

    DMatrix::const_iterator citIK = itKK;
    for (unsigned int i = k; i < GetN(); ++i, citIK += GetN()) {
      const double val = fabs(*citIK);
      if (val > r) {
        r = val;
        j = i;
      }
    }

    if (r < 1e-200) {
      m_det = 0.;
      return;
    }

    if (k != j) {
      m_det = -m_det;
      std::swap(m_aP[k], m_aP[j]);

      MViewRow<DMatrix> kRow(m_mLU, k);
      MViewRow<DMatrix> jRow(m_mLU, j);
      std::swap_ranges(kRow.Begin(), kRow.End(), jRow.Begin());
    }

    const double leadKK = *itKK;
    m_det *= leadKK;

    DMatrix::iterator itIK = itKK + GetN();
    for (unsigned int i = k + 1; i < GetN(); ++i, itIK += GetN()) {
      (*itIK) /= leadKK;

      DMatrix::iterator itIL = itIK + 1;
      DMatrix::const_iterator itKL = itKK + 1;

      for (unsigned int l = k + 1; l < GetN(); ++l, ++itIL, ++itKL)
        (*itIL) -= (*itIK) * (*itKL);
    }
  }
}

DMatrix DLUP::Inversion() const {
  if (fabs(m_det) < 1e-200)
    throw Exception("Singular matrix is trying to be LUP-Inverted.");

  DMatrix mI(GetN(), GetN());

  DMatrix b(GetN(), 1, 0.);
  DMatrix y(GetN(), 1);

  DMatrix::const_iterator itcBeginB = b.Begin();
  DMatrix::iterator itBk = b.Begin();
  DMatrix::iterator itBeginY = y.Begin();
  DMatrix::iterator itINk = mI.Begin() + GetN() * (GetN() - 1);

  for (unsigned int k = 0; k < GetN(); ++k, ++itBk, ++itINk) {
    (*itBk) = 1.;

    DMatrix::const_iterator itcLUi0 = m_mLU.Begin();
    DMatrix::iterator itYi = itBeginY;

    for (unsigned int i = 0; i < GetN(); ++i, itcLUi0 += GetN(), ++itYi) {
      DMatrix::const_iterator itcLUij = itcLUi0;
      DMatrix::const_iterator itcYj = itBeginY;

      double r = 0;

      for (unsigned int j = 0; j + 1 <= i; ++j, ++itcLUij, ++itcYj)
        r += (*itcLUij) * (*itcYj);

      (*itYi) = *(itcBeginB + m_aP[i]) - r;
    }

    DMatrix::iterator itIik = itINk;
    DMatrix::const_iterator itcVi = y.End();
    DMatrix::const_iterator itcLUii = m_mLU.End() - 1;

    for (unsigned int i = GetN() - 1; i + 1 > 0;
         --i, itIik -= GetN(), itcLUii -= (GetN() + 1)) {
      --itcVi;

      double r = 0;
      DMatrix::const_iterator itcIjk = itIik;
      DMatrix::const_iterator itcLUij = itcLUii + 1;

      for (unsigned int j = i + 1; j < GetN(); ++j, ++itcLUij) {
        itcIjk += GetN();
        r += (*itcLUij) * (*itcIjk);
      }

      (*itIik) = (*itcVi - r) / (*itcLUii);
    }

    (*itBk) = 0.;
  }

  return mI;
}

DMatrix DLUP::LSolve(const DMatrix &b) const {
  if (b.GetM() != GetN() || b.GetN() != 1) {
    throw Exception(
        "DLUP::LSolve(DMatrix& x, const DMatrix& b) const failed:"
        "argument b must be a column-vector of length %d, not %d",
        GetN(), b.GetM());
  }

  if (fabs(m_det) < 1e-200) {
    throw Exception(
        "DLUP::LSolve(DMatrix& x, const DMatrix& b) const failed:"
        "linear system with singular matrix is trying to be LUP-Solved.");
  }

  DMatrix x(GetN(), 1);
  DMatrix y(GetN(), 1);

  for (unsigned int i = 0; i < GetN(); ++i) {
    double r = 0;

    for (unsigned int j = 0; j + 1 <= i; ++j) r += m_mLU.Get(i, j) * y.GetV(j);
    y.SetV(i, b.AtV(m_aP[i]) - r);
  }

  for (unsigned int i = GetN() - 1; i + 1 > 0; --i) {
    double r = 0;
    for (unsigned int j = i + 1; j < GetN(); ++j)
      r += m_mLU.Get(i, j) * x.GetV(j);
    x.SetV(i, (y.GetV(i) - r) / m_mLU.Get(i, i));
  }

  return x;
}

double Det(const DMatrix &m) { return DLUP(m).Det(); }

DMatrix Inversion(const DMatrix &mM) { return DLUP(mM).Inversion(); }

DMatrix LSolve(const DMatrix &mM, const DMatrix &b) {
  return DLUP(mM).LSolve(b);
}

// Solves linear system x*mM == 0 && x*vf == 1 and returns x
DMatrix LSolveRowNorm(const DMatrix &mM, const DMatrix &vf) {
  if (mM.GetM() == 0 || mM.GetM() != mM.GetN()) {
    throw Exception(
        "DLUP::LSolveRowNorm(const DMatrix &mM) failed: "
        "mM[%dx%d] must be square non-empty matrix",
        mM.GetM(), mM.GetN());
  }

  if (vf.GetN() != 1 || vf.GetM() != mM.GetM()) {
    throw Exception(
        "DLUP::LSolveRowNorm(const DMatrix &mM) failed: "
        "vf[%dx%d] must be a column vector of length %d",
        vf.GetM(), vf.GetN(), mM.GetM());
  }

  DMatrix mSys = mM.T();
  MViewRow<DMatrix> row0(mSys, 0);
  std::copy(vf.Begin(), vf.End(), row0.Begin());

  DMatrix vb(mSys.GetN(), 1, 0.);
  vb.SetV(0, 1.);

  return LSolve(mSys, vb).Transpose();
}

#endif  // USE_BLAS

// Solves linear system x*mM == 0 && t1(x) == 1 and returns x
DMatrix LSolveRowNorm(const DMatrix &mM) {
  return LSolveRowNorm(mM, DMatrix(mM.GetM(), 1, 1.));
}

}  // namespace QLib
