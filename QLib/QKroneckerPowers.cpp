/********************************************************************
  created:  28.11.06    10:59
  filename: QKroneckerPowers.cpp
  author:   Dmitry Orlovsky

  purpose:  Implementation of KroneckerPowers class
*********************************************************************/

#include "QLib/QKroneckerPowers.h"

#include "ELib/EXception.h"
#include "QLib/QMatrixArithm.h"

namespace QLib {
KroneckerPowers::KroneckerPowers()
    : m_growIfNeeded(false), m_operation(KO_NOT_SPECIFIED) {}

KroneckerPowers::KroneckerPowers(const DMatrix& mBase,
                                 KroneckerOperation operation,
                                 unsigned int initialMaxPower,
                                 bool growIfNeeded) {
  Init(mBase, operation, initialMaxPower, growIfNeeded);
}

void KroneckerPowers::Init(const DMatrix& mBase, KroneckerOperation operation,
                           unsigned int initialMaxPower, bool growIfNeeded) {
  const char szErr[] = "KroneckerPowers::Init(.) failed";

  m_operation = operation;

  if (operation == KO_NOT_SPECIFIED)
    throw ELib::Exception("%s: only OTIMES and OPLUS operations are allowed",
                          szErr);

  if (operation == KO_OPLUS) {
    if (mBase.GetM() != mBase.GetN() && mBase.GetM() != 1 &&
        mBase.GetN() != 1) {
      throw ELib::Exception(
          "%s: OPLUS operation not allowed for (%u x %u) matrices", szErr,
          mBase.GetM(), mBase.GetN());
    }

    m_dmOplusOperand.InitI(std::max(mBase.GetM(), mBase.GetN()));
  }

  m_growIfNeeded = growIfNeeded;

  if (!m_amPowers.empty()) {
    VectorDM empty;
    m_amPowers.swap(empty);
  }

  m_amPowers.reserve(initialMaxPower + 1);

  m_amPowers.push_back(DMatrix(1, 1, operation == KO_OTIMES ? 1. : 0.));
  m_amPowers.push_back(mBase);

  if (initialMaxPower > 1) CalculatePowersUpTo(initialMaxPower);
}

void KroneckerPowers::CalculatePowersUpTo(unsigned int power) {
  m_amPowers.reserve(power + 1);

  while (m_amPowers.size() < power + 1) {
    const DMatrix& cmB = m_amPowers.back();
    const DMatrix& cmM = m_amPowers[1];

    if (m_operation == KO_OTIMES)
      m_amPowers.push_back(cmB ^ cmM);
    else {
      const DMatrix mBnew =
          (cmB ^ m_dmOplusOperand) +
          (DMatrixCDiag::I(std::max(cmB.GetM(), cmB.GetN())) ^ cmM);
      m_amPowers.push_back(mBnew);
    }
  }
}

void KroneckerPowers::Clear() {
  m_operation = KO_NOT_SPECIFIED;
  m_growIfNeeded = false;

  VectorDM empty;
  m_amPowers.swap(empty);
}

const DMatrix& KroneckerPowers::Get(unsigned int power) {
  if (power + 1 > m_amPowers.size()) {
    if (m_growIfNeeded)
      CalculatePowersUpTo(power);
    else {
      throw ELib::Exception(
          "KroneckerPowers::Get(power[%u]) failed: "
          "power too big (MaxPower = %u) and growIfNeeded set to false",
          power, MaxPower());
    }
  }

  return m_amPowers.at(power);
}

}  // namespace QLib
