/********************************************************************
        created:	13:10:2005   21:37
        filename: QMatrixViews.h
        author:		Dmitry Orlovsky

        purpose:	iterators for generic matrix
*********************************************************************/

#ifndef __QMatrixViews_h__
#define __QMatrixViews_h__

#include <iterator>

#include "ELib/EXception.h"
#include "ELib/ETypedefs.h"
#include "QLib/QMatrix.h"

namespace QLib {

//! Base class for iterator-based iterators used in MView iterators
template <class MatrixType, bool IsConst>
class MViewIteratorBase {
  typedef MViewIteratorBase<MatrixType, IsConst> Self;

 public:
  typedef typename ELib::If<IsConst, typename MatrixType::const_pointer,
                            typename MatrixType::pointer>::T IteratorWrappedT;

  typedef std::iterator_traits<IteratorWrappedT> IteratorTraits;
  typedef typename IteratorTraits::value_type value_type;
  typedef typename IteratorTraits::pointer pointer;
  typedef typename IteratorTraits::reference reference;
  typedef typename IteratorTraits::difference_type difference_type;
  typedef std::random_access_iterator_tag iterator_category;

  MViewIteratorBase() {}

  MViewIteratorBase(const MViewIteratorBase<MatrixType, false>& it)
      : m_it(it.m_it) {}

  Self& operator=(const MViewIteratorBase<MatrixType, false>& it) {
    m_it = it.m_it;
    return *this;
  }

  bool operator==(const Self& it) const { return m_it == it.m_it; }

  bool operator!=(const Self& it) const { return m_it != it.m_it; }

  bool operator<(const Self& it) const { return m_it < it.m_it; }

  reference operator*() const { return *m_it; }

  pointer operator->() const { return &(operator*()); }

  IteratorWrappedT m_it;
};

//! Base for iterators with constant increment/decrement step.
template <class MatrixType, bool IsConst>
class MViewSingleStepIterator : public MViewIteratorBase<MatrixType, IsConst> {
  typedef MViewSingleStepIterator<MatrixType, IsConst> Self;

 public:
  using typename MViewIteratorBase<MatrixType, IsConst>::difference_type;

  MViewSingleStepIterator() = default;

  //! Initializes possibly Const iterator via the NonConst one
  MViewSingleStepIterator(const MViewSingleStepIterator<MatrixType, false>& it)
      : MViewIteratorBase<MatrixType, IsConst>(it) {}

  Self& operator=(const MViewSingleStepIterator<MatrixType, false>& it) {
    MViewIteratorBase<MatrixType, IsConst>::operator=(it);
    return *this;
  }

  Self& operator++() {
    ++m_it;
    return *this;
  }

  Self operator++(int) {
    Self tmp = *this;
    ++m_it;
    return tmp;
  }

  Self& operator--() {
    --m_it;
    return *this;
  }

  Self operator--(int) {
    Self tmp = *this;
    --m_it;
    return tmp;
  }

  Self operator+(difference_type n) const {
    Self tmp = *this;
    tmp.m_it += n;
    return tmp;
  }

  Self operator+=(difference_type n) {
    m_it += n;
    return *this;
  }

  Self operator-(difference_type n) const {
    Self tmp = *this;
    tmp.m_it -= n;
    return tmp;
  }

  Self operator-=(difference_type n) {
    m_it -= n;
    return *this;
  }

  difference_type operator-(const Self& it2) const { return m_it - it2.m_it; }

  using MViewIteratorBase<MatrixType, IsConst>::m_it;
};

//! Base for iterators with regular increment
template <class MatrixType, bool IsConst>
class MViewRegularIterator : public MViewIteratorBase<MatrixType, IsConst> {
  typedef MViewRegularIterator<MatrixType, IsConst> Self;

 public:
  using typename MViewIteratorBase<MatrixType, IsConst>::difference_type;

  MViewRegularIterator() = default;

  //! Initializes possibly Const iterator via the NonConst one
  MViewRegularIterator(const MViewRegularIterator<MatrixType, false>& it)
      : MViewIteratorBase<MatrixType, IsConst>(it),
        m_increment(it.m_increment) {}

  Self& operator=(const MViewRegularIterator<MatrixType, false>& it) {
    MViewIteratorBase<MatrixType, IsConst>::operator=(it);
    m_increment = it.m_increment;

    return *this;
  }

  Self& operator++() {
    m_it += m_increment;
    return *this;
  }

  Self operator++(int) {
    Self tmp = *this;
    m_it += m_increment;
    return tmp;
  }

  Self& operator--() {
    m_it -= m_increment;
    return *this;
  }

  Self operator--(int) {
    Self tmp = *this;
    m_it -= m_increment;
    return tmp;
  }

  Self operator+(difference_type n) const {
    Self tmp = *this;
    tmp.m_it += m_increment * n;
    return tmp;
  }

  Self operator+=(difference_type n) {
    m_it += m_increment * n;
    return *this;
  }

  Self operator-(difference_type n) const {
    Self tmp = *this;
    tmp.m_it -= m_increment * n;
    return tmp;
  }

  Self operator-=(difference_type n) {
    m_it -= m_increment * n;
    return *this;
  }

  difference_type operator-(const Self& it2) const {
    return (it2.m_it - m_it) / m_increment;
  }

  using MViewIteratorBase<MatrixType, IsConst>::m_it;
  difference_type m_increment;
};

///////////////////////////////////////////////////////
// Diagonal iterators and views
///////////////////////////////////////////////////////
//! View representing the matrix diagonal. Template for const and non-const
// views.
template <class MatrixType, bool IsConst>
class MViewDiagTemplate {
 public:
  typedef typename ELib::If<IsConst, const MatrixType&, MatrixType&>::T
      InitMatrixRef;
  typedef MViewRegularIterator<MatrixType, IsConst> IteratorType;
  typedef MViewRegularIterator<MatrixType, true> const_iterator;

  IteratorType Begin() const { return m_begin; }

  IteratorType End() const { return m_end; }

 protected:
  MViewDiagTemplate(InitMatrixRef m) {
    if (m.GetM() != m.GetN())
      throw ELib::Exception(
          "Can't create diagonal view for non-square (%dx%d) matrix", m.GetM(),
          m.GetN());

    m_begin.m_it = m.Data();
    m_begin.m_increment = m.GetN() + 1;
    m_end = m_begin + m.GetM();
  }

  IteratorType m_begin;
  IteratorType m_end;
};

//! View representing the matrix diagonal. Non-const partial specialization.
template <class MatrixType>
class MViewDiag : public MViewDiagTemplate<MatrixType, false> {
 public:
  typedef MViewRegularIterator<MatrixType, false> iterator;

  MViewDiag(MatrixType& m) : MViewDiagTemplate<MatrixType, false>(m) {}
};

//! View representing the matrix diagonal. Const partial specialization.
template <class MatrixType>
class MViewDiagConst : public MViewDiagTemplate<MatrixType, true> {
 public:
  MViewDiagConst(const MatrixType& m)
      : MViewDiagTemplate<MatrixType, true>(m) {}

  MViewDiagConst(const MViewDiag<MatrixType>& v)
      : MViewDiagTemplate<MatrixType, true>(v) {}
};

///////////////////////////////////////////////////////
// Row iterators and views
///////////////////////////////////////////////////////

//! View representing the matrix row. Template for const and non-const row
// views.
template <class MatrixType, bool IsConst>
class MViewRowTemplate {
 public:
  typedef typename ELib::If<IsConst, const MatrixType&, MatrixType&>::T
      InitMatrixRef;
  typedef MViewSingleStepIterator<MatrixType, IsConst> IteratorType;

  typedef MViewSingleStepIterator<MatrixType, true> const_iterator;

  IteratorType Begin() const { return m_begin; }

  IteratorType End() const { return m_end; }

 protected:
  MViewRowTemplate(InitMatrixRef m, unsigned int row) {
    if (row >= m.GetM())
      throw ELib::Exception("Failed to create view for row #%d of %d", row,
                            m.GetM());

    m_begin.m_it = m.Data() + row * m.GetN();
    m_end = m_begin + m.GetN();
  }

 private:
  IteratorType m_begin;
  IteratorType m_end;
};

//! View representing the matrix row. Non-const partial specialization.
template <class MatrixType>
class MViewRow : public MViewRowTemplate<MatrixType, false> {
 public:
  typedef MViewSingleStepIterator<MatrixType, false> iterator;

  MViewRow(MatrixType& m, unsigned int row)
      : MViewRowTemplate<MatrixType, false>(m, row) {}
};

//! View representing the matrix row. Const partial specialization.
template <class MatrixType>
class MViewRowConst : public MViewRowTemplate<MatrixType, true> {
 public:
  MViewRowConst(const MatrixType& m, unsigned int row)
      : MViewRowTemplate<MatrixType, true>(m, row) {}

  MViewRowConst(const MViewRow<MatrixType>& v)
      : MViewRowTemplate<MatrixType, true>(v) {}
};

///////////////////////////////////////////////////////
// Column iterators and views
///////////////////////////////////////////////////////
//! Template base for const and non-const diagonal iterators
template <class MatrixType, bool IsConst>
class MViewColumnTemplate {
 public:
  typedef typename ELib::If<IsConst, const MatrixType&, MatrixType&>::T
      InitMatrixRef;
  typedef MViewRegularIterator<MatrixType, IsConst> IteratorType;

  typedef MViewRegularIterator<MatrixType, true> const_iterator;

  IteratorType Begin() const { return m_begin; }

  IteratorType End() const { return m_end; }

 protected:
  MViewColumnTemplate(InitMatrixRef m, unsigned int column) {
    if (column >= m.GetM())
      throw ELib::Exception("Failed to create view for column #%d of %d",
                            column, m.GetM());

    m_begin.m_it = m.Begin() + column;
    m_begin.m_increment = m.GetN();
    m_end = m_begin + m.GetM();
  }

 private:
  IteratorType m_begin;
  IteratorType m_end;
};

//! View representing the matrix column. Non-const partial specialization.
template <class MatrixType>
class MViewColumn : public MViewColumnTemplate<MatrixType, false> {
 public:
  typedef MViewRegularIterator<MatrixType, false> iterator;

  MViewColumn(MatrixType& m, unsigned int column)
      : MViewColumnTemplate<MatrixType, false>(m, column) {}
};

//! View representing the matrix column. Const partial specialization.
template <class MatrixType>
class MViewColumnConst : public MViewColumnTemplate<MatrixType, true> {
 public:
  MViewColumnConst(const MatrixType& m, unsigned int column)
      : MViewColumnTemplate<MatrixType, true>(m, column) {}

  MViewColumnConst(const MViewColumn<MatrixType>& v)
      : MViewColumnTemplate<MatrixType, true>(v) {}
};

///////////////////////////////////////////////////////
// Block iterators and views
///////////////////////////////////////////////////////

//! Base template const and non-const block iterators
template <class MatrixType, bool IsConst>
class MViewBlockIteratorTemplate
    : public MViewIteratorBase<MatrixType, IsConst> {
  typedef MViewIteratorBase<MatrixType, IsConst> Base;
  typedef MViewBlockIteratorTemplate<MatrixType, IsConst> Self;
  using typename Base::IteratorWrappedT;

 public:
  using typename Base::difference_type;

  //! Difference between the beginning of current column to the beginning of the
  // next one
  MViewBlockIteratorTemplate() = default;

  //! Initializes possibly ConstTraits-ed iterator via the NonConstTraits-ed one
  MViewBlockIteratorTemplate(
      const MViewBlockIteratorTemplate<MatrixType, true>& it)
      : MViewIteratorBase<MatrixType, IsConst>(it),
        m_increment(it.m_increment),
        m_firstInRow(it.m_firstInRow),
        m_lastInRow(it.m_lastInRow) {}

  Self& operator=(const MViewBlockIteratorTemplate<MatrixType, true>& it) {
    MViewIteratorBase<MatrixType, IsConst>::operator=(it);
    m_increment = it.m_increment;
    m_firstInRow = it.m_firstInRow;
    m_lastInRow = it.m_lastInRow;
  }

  Self& operator++() {
    if (m_it != m_lastInRow)
      ++m_it;
    else {
      m_firstInRow += m_increment;
      m_lastInRow += m_increment;
      m_it = m_firstInRow;
    }
    return *this;
  }

  Self operator++(int) {
    Self tmp = *this;
    ++(*this);
    return tmp;
  }

  Self& operator--() {
    if (m_it != m_firstInRow)
      --m_it;
    else {
      m_firstInRow -= m_increment;
      m_lastInRow -= m_increment;
      m_it = m_lastInRow;
    }
    return *this;
  }

  Self operator--(int) {
    Self tmp = *this;
    --(*this);
    return tmp;
  }

  Self& operator+=(difference_type n) {
    n += (m_it - m_firstInRow);
    const difference_type d = (n / m_increment) * m_increment;

    m_firstInRow += d;
    m_lastInRow += d;
    m_it = m_firstInRow + (n - d);

    return *this;
  }

  Self operator+(difference_type n) const {
    Self tmp = *this;
    tmp += n;
    return tmp;
  }

  Self& operator-=(int n) {
    n += (m_lastInRow - m_it);
    const difference_type d = (n / m_increment) * m_increment;

    m_firstInRow -= d;
    m_lastInRow -= d;
    m_it = m_lastInRow - (n - d);

    return *this;
  }

  Self operator-(difference_type n) const {
    Self tmp = *this;
    tmp -= n;
    return tmp;
  }

  difference_type operator-(const Self& it2) const {
    return (it2.m_firstInRow - m_firstInRow) / m_increment *
               (m_lastInRow - m_firstInRow + 1) +
           (it2.m_it - it2.m_firstInRow) - (m_it - m_firstInRow);
  }

  using Base::m_it;
  difference_type m_increment;
  IteratorWrappedT m_firstInRow;
  IteratorWrappedT m_lastInRow;
};

//! View representing the matrix row. Template for const and non-const views.
template <class MatrixType, bool IsConst>
class MViewBlockTemplate {
 public:
  typedef typename ELib::If<IsConst, const MatrixType&, MatrixType&>::T
      InitMatrixRef;
  typedef MViewBlockIteratorTemplate<MatrixType, IsConst> IteratorType;

  typedef MViewBlockIteratorTemplate<MatrixType, true> const_iterator;

  IteratorType Begin() const { return m_begin; }

  IteratorType End() const { return m_end; }

 protected:
  MViewBlockTemplate(InitMatrixRef m, unsigned int m0, unsigned int n0,
                     unsigned int rows, unsigned int cols) {
    const char msg[] =
        "Failed to create block view: %s corner(%d,%d) is outside the matrix "
        "(%dx%d)";

    if (m0 >= m.GetM() || n0 >= m.GetN())
      throw ELib::Exception(msg, "upper left", m0, n0, m.GetM(), m.GetN());
    if (m0 + rows > m.GetM() || n0 + cols > m.GetN())
      throw ELib::Exception(msg, "lower right", m0 + rows, n0 + cols, m.GetM(),
                            m.GetN());

    m_begin.m_increment = m_end.m_increment = m.GetN();

    m_begin.m_it = m_begin.m_firstInRow = m.PtrAt(m0, n0);
    m_begin.m_lastInRow = m_begin.m_firstInRow + (cols - 1);

    const unsigned int endInc = m.GetN() * rows;

    m_end.m_it = m_begin.m_it + endInc;
    m_end.m_firstInRow = m_begin.m_firstInRow + endInc;
    m_end.m_lastInRow = m_begin.m_lastInRow + endInc;
  }

  IteratorType m_begin;
  IteratorType m_end;
};

template <class MatrixType>
class MViewBlock : public MViewBlockTemplate<MatrixType, false> {
 public:
  typedef MViewBlockTemplate<MatrixType, false> iterator;

  MViewBlock(MatrixType& m, unsigned int m0, unsigned int n0, unsigned int rows,
             unsigned int cols)
      : MViewBlockTemplate<MatrixType, false>(m, m0, n0, rows, cols) {}
};

//! View representing the matrix block. Const partial specialization.
template <class MatrixType>
class MViewBlockConst : public MViewBlockTemplate<MatrixType, true> {
 public:
  MViewBlockConst(const MatrixType& m, unsigned int m0, unsigned int n0,
                  unsigned int rows, unsigned int cols)
      : MViewBlockTemplate<MatrixType, true>(m, m0, n0, rows, cols) {}

  MViewBlockConst(const MViewBlock<MatrixType>& v)
      : MViewBlockTemplate<MatrixType, true>(v) {}
};

}  // namespace QLib

#endif  //__QMatrixViews_h__
