/********************************************************************
created:	18:11:2005   11:46
filename: QToeSolver.cpp
author:		Dmitry Orlovsky

purpose:	Quasi-Toeplits Markov chain solver
*********************************************************************/

#include "QLib/QToeSolver.h"

#include <assert.h>

#include "ELib/EXception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QLUP.h"
#include "QLib/QMatrixArithm.h"

namespace QLib {
using namespace ELib;

const ToeSolver::Parameters& ToeSolver::Parameters::GetDefault() {
  static Parameters wfs;

  wfs.precisionG = 1e-6;
  wfs.maxIterationsG = 20000;
  wfs.precisionPi = 1e-6;
  wfs.maxPi = 20000;

  return wfs;
}

ToeSolver::ToeSolver(ILogger& logger, IProgressor& progressor)
    : LogProg(logger, progressor) {}

void ToeSolver::ValidateQ(unsigned int& maxState) {
  // Determining the maximal state which can be reached from unique rows
  maxState = 0;

  for (unsigned int i = 0; i <= maxState && i < URows(); ++i) {
    // Maximal state which can be achieved from state i
    maxState = std::max(maxState, Q().GetRowInfo(i) - 1);
  }

  if (maxState < URows())
    throw Exception(
        "ToeSolver::ValidateQ(): macrostates with i > %d cannot be reached",
        maxState);
}

// Calculating G matrix, see (8) at p.10
// void ToeSolver::CalculateG(DMatrix& mG)
//{
//  //mG = DLUP::Inversion( -Q().Get(URows(), URows())*Q().Get(URows(), URows()
//  - 1) );
// DMatrix temp;
// int dim = Q().InfoAt(1).dim;
//    temp.InitZero(dim,dim);
// //for(int i=0; i<dim; i++){
// // temp.At(i,i) = 1.0;
// //}
// mG = temp;

//  const unsigned int rowEnd = Q().GetRowInfo(URows());

//  if (rowEnd > URows() + 1)
//  {
//    double errNorm = GetParameters().precisionG;
//    unsigned int iterCounter = 1;

//    DMatrix mGprev = mG;
//    DMatrix mGpow;
//
//    for (mGpow = mG;
//      errNorm >= GetParameters().precisionG && iterCounter <
//      GetParameters().maxIterationsG;
//      mGprev = mG, ++iterCounter)
//    {
//      mG = Q().Get(URows(), URows());

//      for (unsigned int i = URows() + 1; i < rowEnd; ++i)
////  for (unsigned int i = URows() - 1; i < rowEnd; ++i)
//      {
//        tpax(mG, Q().Get(URows(), i), mGpow);
//        mGpow *= mGprev;
//      }
//      errNorm = NormDiff(mGprev, mG);
//    }
// //GetLogger() << "dfg";
// //GetLogger() << mG;
//	GetLogger() << "Test"<<"\n";
//	GetLogger() << mG;
//    if (errNorm >= GetParameters().precisionG)
//    {
//      throw Exception("ToeSolver::CalculateG: target precision %g can't be
//      reached"
//        " in %d iterations. Precision %g reached. Try to lower target
//        precision"
//        " or raise  max. iterations counter.",
//        GetParameters().precisionG, GetParameters().maxIterationsG, errNorm);
//    }
//  }
//}

// Simple method
// void ToeSolver::CalculateG(DMatrix& mG)
//{
//	DMatrix firstMG;
//	int dim = Q().InfoAt(1).dim;
//	firstMG.InitZero(dim,dim);
//	for(int i=0; i<dim; i++){
//		firstMG.At(i,i) = 0.0;
//	}
//	mG = firstMG;// matrix I
//
//	DMatrix Z;
//	Z.InitZero(dim,dim);
//	for(int i=0; i<dim; i++){
//		Z.At(i,i) = -Q().Get(URows(), URows()).At(i,i);
//	}
//	DMatrix ZInverse;
//	ZInverse = QLib::Inversion(Z);
//
//	const unsigned int rowEnd = Q().GetRowInfo(URows());
//
//	if (rowEnd > URows() + 1)
//	{
//		double errNorm = GetParameters().precisionG;
//		unsigned int iterCounter = 1;
//
//		DMatrix mGprev = mG;
//		DMatrix mGpow;
//		for (mGpow = mG;
//			errNorm >= GetParameters().precisionG && iterCounter <
// GetParameters().maxIterationsG;
//			mGprev = mG, ++iterCounter){
//
//				mG = ZInverse*(Q().Get(URows(), URows() - 1) +
// Q().Get(URows(), URows() + 1)*mGprev*mGprev);
//				GetLogger() << mG <<"\n";
//				errNorm = NormDiff(mGprev, mG);
//		}
//		GetLogger() << "Test"<<"\n";
//		GetLogger() << mG;
//		if (errNorm >= GetParameters().precisionG)
//		{
//            throw Exception(
//                "ToeSolver::CalculateG: target precision %g  can't be reached in %d iterations. "
//                "Precision %g reached. Try to lower target precision"
//                " or raise  max. iterations counter.",
//                GetParameters().precisionG,
//                GetParameters().maxIterationsG, errNorm);
//		}
//	}
//}

void ToeSolver::CalculateG(DMatrix& mG) {
  DMatrix firstMG;
  int dim = Q().InfoAt(1).dim;
  firstMG.InitZero(dim, dim);
  for (int i = 0; i < dim; i++) {
    firstMG.At(i, i) = 0.0;
  }
  mG = firstMG;  // matrix I

  DMatrix IMatrix;
  IMatrix.InitZero(dim, dim);
  for (int i = 0; i < dim; i++) {
    IMatrix.At(i, i) = 1.0;
  }

  DMatrix Z;
  Z.InitZero(dim, dim);
  for (int i = 0; i < dim; i++) {
    Z.At(i, i) = -Q().Get(URows(), URows()).At(i, i);
  }
  DMatrix ZInverse = Inversion(Z);

  const unsigned int rowEnd = Q().GetRowInfo(URows());

  if (rowEnd > URows() + 1) {
    double errNorm = GetParameters().precisionG;
    unsigned int iterCounter = 1;

    DMatrix mGprev = mG;
    DMatrix mGpow = mG;
    while (errNorm >= GetParameters().precisionG &&
           iterCounter < GetParameters().maxIterationsG) {
      mG = ZInverse * Q().Get(URows(), URows() - 1) +
           (ZInverse * Q().Get(URows(), URows()) + IMatrix) * mGpow;
      mGpow = mGpow * mGprev;
      for (int k = URows() + 1; k < rowEnd; k++) {
        mG = mG + ZInverse * Q().Get(URows(), k) * mGpow;
        mGpow = mGpow * mGprev;
      }
      errNorm = NormDiff(mGprev, mG);

      mGpow = mG;
      mGprev = mG;
      iterCounter++;
      //	GetLogger() << "ToeSolver G: iteration " << iterCounter << ",
      // norm "  << errNorm << '\n';
    }
    // GetLogger() << "Test3"<<"\n";
    /*	mG.InitZero(3,3);
    mG.At(0,1)=1;
    for(int i=1;i< 3 ;i++){
    mG.At(i,2)=1;
    }*/
    // GetLogger() << mG <<"\n";
    for (int i = 0; i < mG.GetM(); i++) {
      double sum = 0.0;
      for (int j = 0; j < mG.GetN(); j++) {
        sum = sum + mG.At(i, j);
      }
      sum = sum;
    }
    // GetLogger() << mG.GetM();
    GetLogger() << mG.GetM() << "\n";
    GetLogger() << mG.GetN() << "\n";
    if (errNorm >= GetParameters().precisionG) {
      throw Exception(
          "ToeSolver::CalculateG: target precision %g can't be reached "
          "in %d iterations. Precision %g reached. Try to lower target "
          "precision or raise  max. iterations counter.",
          GetParameters().precisionG, GetParameters().maxIterationsG, errNorm);
    }
  }
}

void ToeSolver::CalculateGk(VectorDM& aGk) {
  GetProgressor().SetProgressMsg("ToeSolver::CalculateGk2");

  // Calculating G_i matrices, see (9) at p.10, G_{URows()} == G
  aGk.resize(URows() + 1);

  if (IsFinite())
    aGk.at(URows()).InitI(Q().InfoAt(URows()).dim);
  else
    CalculateG(aGk.at(URows()));
  // aGk.at(URows()).InitI(Q().InfoAt(URows()).dim);

  GetProgressor().SetProgressMsg("ToeSolver::CalculateGk");

  for (unsigned int i = URows() - 1; i + 1 > 0; --i) {
    const unsigned int dim = Q().InfoAt(i + 1).dim;
    DMatrix mSum(dim, dim, 0);
    DMatrix mGProd;

    const unsigned int lMax = Q().GetRowInfo(i + 1);
    for (unsigned int l = i + 2; l < lMax; ++l) {
      if (l == i + 2)
        mGProd = G(l - 1);
      else
        mGProd = G(l - 1) * mGProd;

      tpax(mSum, Q().Get(i + 1, l), mGProd);
    }
    mSum += Q().Get(i + 1, i + 1);

    aGk.at(i) = Inversion(Neg(mSum)) * Q().Get(i + 1, i);
  }
}

const DMatrix& ToeSolver::G(unsigned int i) const {
  assert(!IsFinite() || i < URows());

  return (i < URows()) ? m_aG[i] : m_aG[URows()];
}

void ToeSolver::CalculateFl(VectorDM& aFl) {
  GetProgressor().SetProgressMsg("ToeSolver::CalculateFl");
  // Calculating F_l matrices, until |F_l| >= Prec()

  // length of repeated row
  const unsigned int repRowLen = Q().GetRowInfo(URows()) - URows();

  // If repeated rows has no overdiagonal elements,
  // bool hasFiniteStatespace = IsFinite() || (repRowLen <= 2);
  bool hasFiniteStatespace = IsFinite();
  const unsigned int maxPi =
      hasFiniteStatespace ? MaxState() : GetParameters().maxPi;

  if (!IsFinite() && MaxState() > GetParameters().maxPi) {
    GetLogger()
        << FormatStr(
               "ToeSolver::CalculateFl: system is finite, maximal number"
               " of calculated probabilities is forced to %d (was %d). Target "
               "error "
               "norm is ignored.\n",
               MaxState(), GetParameters().maxPi);
  }

  // First non-unique row of \bar Q_{il} matrices will be stored to m_aStoredQil
  m_aStoredQil.resize(Q().GetRowInfo(repRowLen));

  double fNorm = GetParameters().precisionPi;

  for (unsigned int l = 0; l <= maxPi && fNorm >= GetParameters().precisionPi;
       ++l) {
    DMatrix mFl(Q().InfoAt(0).dim, Q().InfoAt(l).dim, 0.);

    for (unsigned int i = 0; i <= l; ++i) {
      const unsigned int rowEnd = Q().GetRowInfo(i);

      if (l < rowEnd) {
        DMatrix barQil;

        if (i > URows())
          barQil = m_aStoredQil.at(l - i);
        else {
          DMatrix mGProd;
          barQil = Q().Get(i, l);

          for (unsigned int m = l + 1; m < rowEnd; ++m) {
            if (m == l + 1)
              mGProd = G(l);
            else
              mGProd = G(m - 1) * mGProd;

            tpax(barQil, Q().Get(i, m), mGProd);
          }

          if (i == URows()) m_aStoredQil.at(l - i) = barQil;
        }

        if (i == 0)
          mFl = barQil;
        else if (i < l)
          tpax(mFl, m_aFl.at(i), barQil);
        else {
          DMatrix m = Inversion(barQil);
          Neg(m);
          mFl *= m;
        }
      }
    }
    m_aFl.push_back(mFl);

    if (hasFiniteStatespace)
      fNorm = (l < maxPi) ? GetParameters().precisionPi : 0;
    else
      fNorm = Norm(mFl);

    // GetLogger() << "ToeSolver: iteration " << l << ", norm "  << fNorm <<
    // '\n';
  }

  if (fNorm >= GetParameters().precisionPi) {
    throw Exception(
        "ToeSolver::CalculateFl: target precision %g can't be reached"
        " in %d iterations. Precision %g reached. Try lower target precision"
        " or higher max. iterations bound.",
        GetParameters().precisionPi, GetParameters().maxPi, fNorm);
  }
}

void ToeSolver::CalculatePi(VectorDM& aPi) {
  GetProgressor().SetProgressMsg("ToeSolver::CalculatePi");

  // Calculate \sum_{l=0}^{N+R} Fl \bf e
  DMatrix vSum(Fl(0).GetN(), 1, 1.);
  for (unsigned int l = 1; l < GetFlNumber(); ++l) vSum += t1(Fl(l));

  aPi.resize(GetFlNumber());
  aPi.at(0) = LSolveRowNorm(Fl(0), vSum);

  VectorDM::iterator it = aPi.begin() + 1;
  for (unsigned int l = 1; it != aPi.end(); ++it, ++l)
    it->Assign(aPi.front() * Fl(l));
}

//! Finds the stationary probability vectors of the Markov chain with finite
// infinitesimal generator
PiVector ToeSolver::Solve(const IBlockDMatrix& q) {
  PiVector tgtPi;

  m_pQ = &q;
  m_bIsFinite = true;
  m_nOfUniqueRows = q.GetDim() - 1;
  m_params = Parameters::GetDefault();

  ValidateQ(m_maxState);
  CalculateGk(m_aG);
  CalculateFl(m_aFl);
  CalculatePi(tgtPi);

  return tgtPi;
}

//! Finds the stationary probability vectors of the Markov chain with infinite
// infinitesimal generator
PiVector ToeSolver::Solve(const IBlockDMatrix& q, unsigned int uniqueRows,
                          const Parameters& params) {
  PiVector tgtPi;

  m_pQ = &q;
  m_bIsFinite = false;
  m_nOfUniqueRows = uniqueRows - 1;
  m_params = params;

  ValidateQ(m_maxState);
  CalculateGk(m_aG);
  CalculateFl(m_aFl);
  CalculatePi(tgtPi);

  return tgtPi;
}

}  // namespace QLib
