/********************************************************************
created:	16:11:2006   22:34
filename: QAQTMCSolver.h
author:	  Dmitry Orlovsky

purpose:  Asymptotically Quasi-Toeplitz Markov chain solver

based on: Klimenok, Dudin, Continuous-time Asymptotially Quasi-Toeplitz
          Markov Chains // Queues: Flows, systems, Networks, 22-24
          February, 2205, Minsk, V.18, P.76-84 (Yellow book)
*********************************************************************/

#ifndef __QAQTMCSolver_h__
#define __QAQTMCSolver_h__

#include <list>
#include <vector>

#include "ELib/ELogProg.h"
#include "QLib/QEngineStructures.h"

namespace QLib {
class IBlockDMatrix;

//! Calculates the stationary probabilities of asymptotically quasi-Toeplitz
// Markov chains
class AQTMCSolver : protected ELib::LogProg {
 public:
  //! Calculation algorithm parameters
  class Parameters : public ICfgObject {
   public:
    double precisionGi;  //! Error norm between G and G_{\tilde i - 1} matrix
    unsigned int maxTildeI;     //! Maximum possible value of \tilde i
    double errorGi;             //! Error while calculating G_i matrices
    unsigned int maxGiRefIter;  //! Maximum G_i refinement iterations
    unsigned int lStep;         //! Size of forward step while calculating F_l
    double precisionPi;         //! Estimated norm of last calculated Pi entry
    unsigned int maxPi;  //! Maximum number of Pi vectors to be calculated

    //! Retrieves the default processing settings for AQTMCSolver
    static const Parameters& GetDefault();

    //! Reads the parameters from ConfigFile section
    virtual void CfgGet(const ConfigFile& config, const std::string& section);

    //! Writes the parameters to ConfigFile section
    virtual void CfgSet(ConfigFile& config, const std::string& section) const;

    //! Writes the parameters to logger
    virtual void Dump(ELib::ILogger& logger) const;
  };

  //! Calculation results
  struct Result {
    //! Vectors of stationary probabilities
    PiVector avPi;

    //! Messages generated during the calculations
    std::vector<std::string> messages;
  };

  //! Constructs the AQTMCSolver object with given logger and progressor
  /*!
  \sa ELib::ILogger, ELib::IProgressor
  */
  AQTMCSolver(ELib::ILogger& logger, ELib::IProgressor& progressor);

  virtual ~AQTMCSolver() = default;

  //! Finds the stationary probability vectors of an asymptotically
  // quasi-Toeplitz Markov chain
  /*!
    \param bmA    Infinitesimal generator of the Markov chain
    \param mG     Neuts's matrix for the limiting Markov chain
    \param i0     i_0 from AQTMC definition (definition 1.3, P.78)
    \param k_0    k_0 from AQTMC definition (definition 1.3, P.78)
    \param params calculation parameters, see AQTMSolver::Parameters for details

    \return       AQTMCSolver::Result stucture with stationary probability
    vectors and messages
                    generated during the calculations

    \sa PiVector, IBlockDMatrix
  */
  Result Solve(const IBlockDMatrix& bmA, const DMatrix& mG, unsigned int i0,
               unsigned int k0,
               const Parameters& params = Parameters::GetDefault());

 protected:
  //! Returns reference to parameters structure
  inline const Parameters& Par() const;

  //! Returns infinitesimal generator
  inline const IBlockDMatrix& A() const;

  //! Returns Neuts's matrix
  inline const DMatrix& G() const;

  //! Returns i_0
  inline unsigned int I0() const;

  //! Returns k_0
  inline unsigned int K0() const;

  //! Returns l_{maxIrr}
  inline unsigned int LmaxIrr() const;

  //! Returns i_{min}(l), l = \overline{0, l_{maxIrr}}
  inline unsigned int IMin(unsigned int l) const;

  //! Returns k_{maxReg}
  inline unsigned int KmaxReg() const;

  //! Returns precalculated value for G^{pow}, pow >= 1
  DMatrix PowerOfG(unsigned int pow);

  //! Cleanups the internal structures
  virtual void Cleanup();

  //! Validates the generator ad input parameters
  virtual void Validate();

  //! Preliminary calculations
  virtual void PreCalc();

  //! Calculates G_i, i = 0 .. \tilde i - 1
  virtual void CalculateGi(std::vector<DMatrix>& ahGi,
                           std::vector<std::string>& messages);

 private:
  //! Returns G_i if i < \tilde i, G otherwise
  DMatrix Gi(unsigned int i);

  //! Calculates Fl matrices array, \sum F_i \bf e vector and \bar A_{0,0}
  // matrix
  void CalculateFl(std::vector<std::string>& messages);

  //! Calculates Pi matrices array
  void CalculatePi(PiVector& avPi);

  Parameters m_params;

  const IBlockDMatrix* m_pA = nullptr;
  unsigned int m_i0 = 0;
  unsigned int m_k0 = 0;

  unsigned int m_lMaxIrr = 0;
  unsigned int m_kMaxReg = 0;
  std::vector<unsigned int> m_aIMin;

  DMatrix m_mG;

  std::vector<DMatrix> m_ahPowersOfG;
  std::vector<DMatrix> m_ahGi;

  std::deque<DMatrix> m_ahFl;
  DMatrix m_vSumFie;
  DMatrix m_barA00;

  struct Adotl {
    unsigned int iMin;
    VectorDM amByI;
  };
};

//! Class to work with the limiting chain of an asymptotically quasi-Toeplitz
// Markov chain
/*!
  Takes the Y_k matrices of the limiting chain.
  May be used to verify the ergidicity condition and to calculate the Neuts's G
  matrix.
*/
class AQTMCLimitingChain : protected ELib::Logged {
 public:
  //! Constructs the AQTMCLimitingChain object with given logger
  AQTMCLimitingChain(ELib::ILogger& logger);

  //! (Re-)Initializes an object with a set of Y_k matrices
  void Init(const VectorDM& amYk);

  //     void InitViaGeneratorRow(IBlockDMatrix& bmA, unsigned int row);

  //! Checks whether the object is initialized with correct Y_k matrices
  void Validate() const;

  //! Checks the stability condition of an asymptotically quasi-Toeplitz Markov
  // chain
  /*!
    The stability condition is:
      x Y'(1) \bf 1 < 1,
    where x is the solution of the system
      x(Y(1) - I) = 0,
      x \bf 1 = 1.
  */
  bool CheckStability() const;

  //! Calculates the Neuts's G matrix
  /*!
    Iterational method is used to calculate the G matrix. The initial
    approximation must be
      a stochastic matrix. I matrix is correct.
    The iterations stop when the norm of a difference between the successive
    approximations
      becomes less than target precision, or when the maximum number of
    iterations is exceeded.
    If the maximum number of iterations is reached, but precision is still too
    low, the
      exception is thrown.

    \param mInitialG initial approximation of G matrix, must be a stochastic
    matrix
    \param rPrecision target precision
    \param iMaxIterations maximum number of iterations
    \param progressor object used to report the G matrix calculation progress

    \returns calculated G matrix
  */
  DMatrix CalculateG(const DMatrix& mInitialG, double rPrecision,
                     unsigned int iMaxIterations,
                     ELib::IProgressor& progressor) const;

 private:
  PolyDM m_amYk;
};

// ====== implementation =======
inline const AQTMCSolver::Parameters& AQTMCSolver::Par() const {
  return m_params;
}

inline const IBlockDMatrix& AQTMCSolver::A() const { return *m_pA; }

inline unsigned int AQTMCSolver::I0() const { return m_i0; }

inline unsigned int AQTMCSolver::K0() const { return m_k0; }

inline unsigned int AQTMCSolver::LmaxIrr() const { return m_lMaxIrr; }

inline unsigned int AQTMCSolver::KmaxReg() const { return m_kMaxReg; }

inline unsigned int AQTMCSolver::IMin(unsigned int l) const {
  if (l <= LmaxIrr())
    return m_aIMin[l];
  else if (l > KmaxReg())
    return l - KmaxReg();
  else
    return 0;
}

inline const DMatrix& AQTMCSolver::G() const { return m_mG; }

}  // namespace QLib

#endif  //__QAQTMCSolver_h__
