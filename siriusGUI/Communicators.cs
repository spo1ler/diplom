using System;
using System.Text;

using SiriusCLR;

namespace siriusGUI
{
  public class CommunicatorConsole : ICommunicator
  {
    public override void SetProgressMsg(String sCaption)
    {
      Console.WriteLine("#progress message: " + sCaption);
    }

    public override void SetProgress(int iProgress)
    {
      Console.WriteLine("#progress: {0:D}%", iProgress);
    }

    public override void SetProjectFile(String s)
    {
      Console.WriteLine("#project file: " + s);
    }

    public override void SetProjectName(String s)
    {
      Console.WriteLine("#project name: " + s);
    }
  }


  public interface ICommunicatorValues
  {
    String GetProgressMsg();
    int GetProgress();
    String GetPojectFile();
    String GetProjectName();
  }


  public class CommunicatorStoring : SiriusCLR.ICommunicator, ICommunicatorValues
  {
    public event EventHandler ValuesChanged;

    public override void SetProgressMsg(String sCaption)
    {
      m_progressMsg = sCaption;
      ValuesChanged(this, EventArgs.Empty);
    }

    public override void SetProgress(int iProgress)
    {
      m_progress = iProgress;
      ValuesChanged(this, EventArgs.Empty);
    }

    public override void SetProjectFile(String s)
    {
      m_projectFile = s;
      ValuesChanged(this, EventArgs.Empty);
    }

    public override void SetProjectName(String s)
    {
      m_projectName = s;
      ValuesChanged(this, EventArgs.Empty);
    }

    public String GetProgressMsg()
    {
      return m_progressMsg;
    }

    public int GetProgress()
    {
      return m_progress;
    }

    public String GetPojectFile()
    {
      return m_projectFile;
    }

    public String GetProjectName()
    {
      return m_projectName;
    }

    String m_progressMsg;
    int m_progress;
    String m_projectFile;
    String m_projectName;
  };
}
