using System;
using System.Windows.Forms;
using siriusGUI.Properties;

namespace siriusGUI
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(String[] args)
    {
//       Application.EnableVisualStyles();
//       Application.SetCompatibleTextRenderingDefault(false);

      if (args.Length == 0)
      {
        OpenFileDialog openDialog = new OpenFileDialog();

        openDialog.Title = Resources.openDialogTitle;
        openDialog.Filter = Resources.openDialogFilter;
        openDialog.Multiselect = true;

        if (openDialog.ShowDialog() == DialogResult.OK)
          args = openDialog.FileNames;
      }

      if (args.Length > 0)
      {
        CalcDocument doc = new CalcDocument();
        doc.SetExecutionPlan(args);

        CalcView view = new CalcView(doc);

        Application.Run(view);
      }

      Application.DoEvents();
    }
  }

}