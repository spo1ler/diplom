using System;
using System.Text;
using System.Threading;

using System.IO;
using System.Net.Mail;

namespace siriusGUI
{
  class CalcDocument : Document
  {
    public enum DocumentState { Idle, Executing, Suspended, Completed };
    public enum DocChangeType { CommValuesChanged, ThreadStateChanged };

    public class DocChangeArgs : EventArgs
    {
      public DocChangeArgs(DocChangeType changeType)
      {
        this.changeType = changeType;
      }

      public DocChangeType changeType;
    };

    public CalcDocument()
    {
      m_communicator = new CommunicatorStoring();
      m_communicator.ValuesChanged += OnCommValuesChanged;

      m_kernel = new SiriusCLR.Kernel();
      m_kernel.SetCommunicator(m_communicator);

      m_kernelThread = new Thread(new ThreadStart(this.ExecuteDelegate));
      m_kernelThread.IsBackground = true;

      m_kernelLock = new Object();
      m_docState = DocumentState.Idle;

      this.emailOnComletion = false;
    }

    // can return obsolete values when calling from outside the class,
    // inside the class use only within the lock(m_kernelLock) section
    public DocumentState DocState
    {
      get { return m_docState; }
      
      private set { 
        m_docState = value;

        if (m_docState == DocumentState.Completed && this.emailOnComletion)
        {
          SmtpClient client = new SmtpClient("smtp.mail.ru");
          MailAddress from = new MailAddress("odms@mail.ru", "Sirius-M");
          MailAddress to = new MailAddress(this.emailAddr);
          
          MailMessage message = new MailMessage(from, to);
          message.Subject = "Sirius-M calculation results";
          message.SubjectEncoding = System.Text.Encoding.UTF8;

          foreach (String fileName in this.m_executionPlan)
          {
            message.Body += "### " + fileName + " begin" + Environment.NewLine;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            StreamReader s = new StreamReader(fs);

            String str;
            while ((str = s.ReadLine()) != null)
              message.Body += str + Environment.NewLine;

            message.Body += "### " + fileName + " end" + Environment.NewLine;
          }

          message.BodyEncoding = System.Text.Encoding.ASCII;

          client.Send(message);
        }

        RaiseDocumentChanged(new DocChangeArgs(DocChangeType.ThreadStateChanged));
      }
    }

    public void SetExecutionPlan(String[] executionPlan)
    {
      lock (m_kernelLock)
      {
        if (this.DocState == DocumentState.Idle)
          m_executionPlan = executionPlan;
        else
          throw new Exception(Properties.Resources.excUnableChangeExecPlan);
      }
    }

    // Ignores execution attempts if already running
    public void Execute()
    {
      lock (m_kernelLock)
      {
        if (this.DocState == DocumentState.Idle)
        {
          m_kernelThread.Start();
          this.DocState = DocumentState.Executing;
        }
      }
    }

    protected void ExecuteDelegate()
    {
      foreach (String sFileName in m_executionPlan)
      {
        try
        {
          m_kernel.Execute(sFileName);
        }
        catch
        {
        }
      }

      lock (m_kernelLock)
      {
        this.DocState = DocumentState.Completed;
      }
    }

    public void SuspendExecution()
    {
      lock (m_kernelLock)
      {
        if (this.DocState == DocumentState.Executing)
        {
          m_kernelThread.Suspend();
          this.DocState = DocumentState.Suspended;
        }
      }
    }

    public void ResumeExecution()
    {
      lock (m_kernelLock)
      {
        if (this.DocState == DocumentState.Suspended)
        {
          m_kernelThread.Resume();
          this.DocState = DocumentState.Executing;
        }
      }
    }

    public ICommunicatorValues GetCommunicatorValues()
    {
      return m_communicator;
    }

    protected void OnCommValuesChanged(Object sender, EventArgs e)
    {
      RaiseDocumentChangedAsync(new DocChangeArgs(DocChangeType.CommValuesChanged));
    }

    public bool emailOnComletion;
    public String emailAddr;

    SiriusCLR.Kernel m_kernel;
    Thread m_kernelThread;

    Object m_kernelLock;
    DocumentState m_docState; 

    CommunicatorStoring m_communicator;

    String[] m_executionPlan;
  }
}
