namespace siriusGUI
{
  partial class CalcView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.Label labelProjectFile;
      System.Windows.Forms.Label labelProjectName;
      System.Windows.Forms.Label labelCalcState;
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalcView));
      this.textProjectFile = new System.Windows.Forms.TextBox();
      this.textProjectName = new System.Windows.Forms.TextBox();
      this.textCalcState = new System.Windows.Forms.TextBox();
      this.buttonStop = new System.Windows.Forms.Button();
      this.imageListButtons = new System.Windows.Forms.ImageList(this.components);
      this.progressBar = new System.Windows.Forms.ProgressBar();
      this.labelProgress = new System.Windows.Forms.Label();
      this.buttonPause = new System.Windows.Forms.Button();
      this.checkBoxEmail = new System.Windows.Forms.CheckBox();
      this.textBoxEMail = new System.Windows.Forms.TextBox();
      labelProjectFile = new System.Windows.Forms.Label();
      labelProjectName = new System.Windows.Forms.Label();
      labelCalcState = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // labelProjectFile
      // 
      labelProjectFile.AutoSize = true;
      labelProjectFile.Location = new System.Drawing.Point(36, 10);
      labelProjectFile.Name = "labelProjectFile";
      labelProjectFile.Size = new System.Drawing.Size(59, 13);
      labelProjectFile.TabIndex = 0;
      labelProjectFile.Text = "Project file:";
      labelProjectFile.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // labelProjectName
      // 
      labelProjectName.AutoSize = true;
      labelProjectName.Location = new System.Drawing.Point(23, 36);
      labelProjectName.Name = "labelProjectName";
      labelProjectName.Size = new System.Drawing.Size(72, 13);
      labelProjectName.TabIndex = 1;
      labelProjectName.Text = "Project name:";
      labelProjectName.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // labelCalcState
      // 
      labelCalcState.AutoSize = true;
      labelCalcState.Location = new System.Drawing.Point(7, 62);
      labelCalcState.Name = "labelCalcState";
      labelCalcState.Size = new System.Drawing.Size(88, 13);
      labelCalcState.TabIndex = 2;
      labelCalcState.Text = "Calculation state:";
      labelCalcState.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // textProjectFile
      // 
      this.textProjectFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textProjectFile.Location = new System.Drawing.Point(101, 7);
      this.textProjectFile.Name = "textProjectFile";
      this.textProjectFile.ReadOnly = true;
      this.textProjectFile.Size = new System.Drawing.Size(330, 20);
      this.textProjectFile.TabIndex = 3;
      this.textProjectFile.TabStop = false;
      // 
      // textProjectName
      // 
      this.textProjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textProjectName.Location = new System.Drawing.Point(101, 33);
      this.textProjectName.Name = "textProjectName";
      this.textProjectName.ReadOnly = true;
      this.textProjectName.Size = new System.Drawing.Size(385, 20);
      this.textProjectName.TabIndex = 4;
      this.textProjectName.TabStop = false;
      // 
      // textCalcState
      // 
      this.textCalcState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textCalcState.Location = new System.Drawing.Point(101, 59);
      this.textCalcState.Name = "textCalcState";
      this.textCalcState.ReadOnly = true;
      this.textCalcState.Size = new System.Drawing.Size(385, 20);
      this.textCalcState.TabIndex = 5;
      this.textCalcState.TabStop = false;
      // 
      // buttonStop
      // 
      this.buttonStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.buttonStop.ImageIndex = 2;
      this.buttonStop.ImageList = this.imageListButtons;
      this.buttonStop.Location = new System.Drawing.Point(464, 6);
      this.buttonStop.Name = "buttonStop";
      this.buttonStop.Size = new System.Drawing.Size(21, 21);
      this.buttonStop.TabIndex = 6;
      this.buttonStop.UseVisualStyleBackColor = true;
      this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
      // 
      // imageListButtons
      // 
      this.imageListButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButtons.ImageStream")));
      this.imageListButtons.TransparentColor = System.Drawing.Color.Transparent;
      this.imageListButtons.Images.SetKeyName(0, "btnSuspend.gif");
      this.imageListButtons.Images.SetKeyName(1, "btnResume.gif");
      this.imageListButtons.Images.SetKeyName(2, "btnAbort.gif");
      // 
      // progressBar
      // 
      this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.progressBar.Location = new System.Drawing.Point(10, 88);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new System.Drawing.Size(420, 13);
      this.progressBar.Step = 1;
      this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
      this.progressBar.TabIndex = 7;
      // 
      // labelProgress
      // 
      this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.labelProgress.AutoSize = true;
      this.labelProgress.Location = new System.Drawing.Point(451, 88);
      this.labelProgress.Name = "labelProgress";
      this.labelProgress.Size = new System.Drawing.Size(21, 13);
      this.labelProgress.TabIndex = 8;
      this.labelProgress.Text = "0%";
      this.labelProgress.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // buttonPause
      // 
      this.buttonPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonPause.BackColor = System.Drawing.SystemColors.Control;
      this.buttonPause.Enabled = false;
      this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.buttonPause.ImageIndex = 0;
      this.buttonPause.ImageList = this.imageListButtons;
      this.buttonPause.Location = new System.Drawing.Point(437, 6);
      this.buttonPause.Name = "buttonPause";
      this.buttonPause.Size = new System.Drawing.Size(21, 21);
      this.buttonPause.TabIndex = 9;
      this.buttonPause.UseVisualStyleBackColor = false;
      this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
      // 
      // checkBoxEmail
      // 
      this.checkBoxEmail.AutoSize = true;
      this.checkBoxEmail.Location = new System.Drawing.Point(10, 107);
      this.checkBoxEmail.Name = "checkBoxEmail";
      this.checkBoxEmail.Size = new System.Drawing.Size(123, 17);
      this.checkBoxEmail.TabIndex = 10;
      this.checkBoxEmail.Text = "Email on completion:";
      this.checkBoxEmail.UseVisualStyleBackColor = true;
      this.checkBoxEmail.CheckedChanged += new System.EventHandler(this.checkBoxEmail_CheckedChanged);
      // 
      // textBoxEMail
      // 
      this.textBoxEMail.Location = new System.Drawing.Point(139, 105);
      this.textBoxEMail.Name = "textBoxEMail";
      this.textBoxEMail.Size = new System.Drawing.Size(292, 20);
      this.textBoxEMail.TabIndex = 11;
      this.textBoxEMail.TextChanged += new System.EventHandler(this.textBoxEMail_TextChanged);
      // 
      // CalcView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(492, 143);
      this.Controls.Add(this.textBoxEMail);
      this.Controls.Add(this.checkBoxEmail);
      this.Controls.Add(this.buttonPause);
      this.Controls.Add(this.labelProgress);
      this.Controls.Add(this.progressBar);
      this.Controls.Add(this.buttonStop);
      this.Controls.Add(this.textCalcState);
      this.Controls.Add(this.textProjectName);
      this.Controls.Add(this.textProjectFile);
      this.Controls.Add(labelCalcState);
      this.Controls.Add(labelProjectName);
      this.Controls.Add(labelProjectFile);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(1280, 170);
      this.MinimumSize = new System.Drawing.Size(200, 170);
      this.Name = "CalcView";
      this.Padding = new System.Windows.Forms.Padding(4);
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
      this.Text = "Sirius-M";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textProjectFile;
    private System.Windows.Forms.TextBox textProjectName;
    private System.Windows.Forms.TextBox textCalcState;
    private System.Windows.Forms.Button buttonStop;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.ImageList imageListButtons;
    private System.Windows.Forms.Button buttonPause;
    private System.Windows.Forms.CheckBox checkBoxEmail;
    private System.Windows.Forms.TextBox textBoxEMail;

  }
}

