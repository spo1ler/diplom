using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace siriusGUI
{
  partial class CalcView : Form, IView
  {
    public CalcView(CalcDocument doc)
    {
      m_doc = doc;
      m_doc.AttachView(this);

      this.Load += delegate(Object sender, EventArgs e)
      {
        m_doc.Execute();
      };

      this.Closing += delegate(Object sender, CancelEventArgs e)
      {
        CheckedDetach();
      };

      InitializeComponent();
    }

    ~CalcView()
    {
      CheckedDetach();
    }

    private void CheckedDetach()
    {
      if (m_doc != null)
      {
        Document doc = m_doc;
        m_doc = null;
        doc.DetachView(this);
      }
    }

    public void OnDocumentChanged(Object sender, EventArgs e)
    {
      if (sender != m_doc)
        return;

      if (InvokeRequired)
        Invoke(new EventHandler(OnDocumentChanged), new Object[] { sender, e });
      else
      {
        switch (((CalcDocument.DocChangeArgs)e).changeType)
        {
          case CalcDocument.DocChangeType.CommValuesChanged:
            ICommunicatorValues commValues = m_doc.GetCommunicatorValues();

            this.textProjectFile.Text = commValues.GetPojectFile();
            this.textProjectName.Text = commValues.GetProjectName();
            this.textCalcState.Text = commValues.GetProgressMsg();
            this.progressBar.Value = commValues.GetProgress();
            this.labelProgress.Text = commValues.GetProgress().ToString() + "%";

            break;

          case CalcDocument.DocChangeType.ThreadStateChanged:
            switch (m_doc.DocState)
            {
              case CalcDocument.DocumentState.Executing:
                this.Text = "Sirius-M (executing)";
                this.buttonPause.ImageIndex = (int)ButtonImages.Suspend;
                this.buttonPause.Enabled = true;
                break;

              case CalcDocument.DocumentState.Suspended:
                this.Text = "Sirius-M (suspended)";
                this.buttonPause.ImageIndex = (int)ButtonImages.Resume;
                this.buttonPause.Enabled = true;
                break;

              case CalcDocument.DocumentState.Idle:
                this.Text = "Sirius-M";
                this.buttonPause.Enabled = false;
                break;

              case CalcDocument.DocumentState.Completed:
                Close();
                break;
            }
            break;
        }
      }
    }

    public void OnDocumentDetach(Document detachedDoc)
    {
      if (detachedDoc == m_doc)
      {
        m_doc = null;
        Close();
      }
    }

    private void buttonStop_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void buttonPause_Click(object sender, EventArgs e)
    {
      switch (m_doc.DocState)
      {
        case CalcDocument.DocumentState.Executing:
          m_doc.SuspendExecution();
          break;

        case CalcDocument.DocumentState.Suspended:
          m_doc.ResumeExecution();
          break;
      }
    }

    private enum ButtonImages
    {
      Suspend = 0,
      Resume = 1,
      Abort = 2
    }

    private CalcDocument m_doc;

    private void textBoxEMail_TextChanged(object sender, EventArgs e)
    {
      this.m_doc.emailAddr = this.textBoxEMail.Text;
    }

    private void checkBoxEmail_CheckedChanged(object sender, EventArgs e)
    {
      this.m_doc.emailOnComletion = this.checkBoxEmail.Checked;
    }
  }
}