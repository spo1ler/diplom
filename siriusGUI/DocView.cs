using System;
using System.Collections;

namespace siriusGUI
{
  class Document
  {
    public event EventHandler DocumentChanged;

    public Document()
    {
      this.attachedViews = new ArrayList();
    }

    public void AttachView(IView view)
    {
      if (!this.attachedViews.Contains(view))
      {
        this.attachedViews.Add(view);
        DocumentChanged += view.OnDocumentChanged;
      }
    }

    public void DetachView(IView view)
    {
      view.OnDocumentDetach(this);
      this.attachedViews.Remove(view);
      DocumentChanged -= view.OnDocumentChanged;
    }

    public void DetachAllViews()
    {
      foreach (IView view in this.attachedViews)
        DetachView(view);
    }

    ~Document()
    {
      DetachAllViews();
    }

    protected void RaiseDocumentChanged(EventArgs e)
    {
      DocumentChanged(this, e);
    }

    protected void RaiseDocumentChangedAsync(EventArgs e)
    {
      if (this.DocumentChanged != null)
      {
        foreach (EventHandler handler in this.DocumentChanged.GetInvocationList())
        {
          EventHandler h = handler;
          h.BeginInvoke(
            this, e,
            delegate(IAsyncResult result) { h.EndInvoke(result); },
            null);
        }
      }
    }

    private ArrayList attachedViews;
  };

  interface IView
  {
    void OnDocumentChanged(Object sender, EventArgs changeArgs);

    // Called before the view is detached from document
    void OnDocumentDetach(Document detachedDoc);
  };
}
