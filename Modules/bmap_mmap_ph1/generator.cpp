#include "generator.h"
#include "QLib\QMatrixArithm.h"

namespace BMAPMMAPPH1
{
    Generator::Generator()
    {

    }

    void Generator::SetDimensions()
    {
        requestBmapSize = P().requestBMAP.size();

        W = P().nW;
        V = P().nV;
        a = W * V;

        M1 = P().main1ServicePH.GetM();
        M2 = P().main2ServicePH.GetM();
        M3 = P().reserveServicePH.GetM();

        R1 = P().main1RepairPH.GetM();
        R2 = P().main2RepairPH.GetM();
    }

    QLib::DMatrix Generator::Q_00() const
    {
        QLib::DMatrix q_00{ a*(1 + R1 + R2 + R1 * R2), a*(1 + R1 + R2 + R1 * R2), 0 };

        QLib::CopyBlk(q_00, 0, 0, D(0) | H(0));
        QLib::CopyBlk(q_00, 0, a, I(W) ^ H(1) ^ tau_1());
        QLib::CopyBlk(q_00, 0, a*(1 + R1), I(W) ^ H(2) ^ tau_2());

        QLib::CopyBlk(q_00, a, 0, I(a) ^ T1_0());
        QLib::CopyBlk(q_00, a, a, D(0) | (H(0) + H(1)) | T1());
        QLib::CopyBlk(q_00, a, a*(1 + R1 + R2), I(W) ^ H(2) ^ I(R1) ^ tau_2());

        QLib::CopyBlk(q_00, a*(1 + R1), 0, I(a) ^ T2_0());
        QLib::CopyBlk(q_00, a*(1 + R1), a*(1 + R1), D(0) | (H(0) + H(2)) | T2());
        QLib::CopyBlk(q_00, a*(1 + R1), a*(1 + R1 + R2), I(W) ^ H(1) ^ tau_1() ^ I(R2));

        QLib::CopyBlk(q_00, a*(1 + R1 + R2), a, I(a) ^ I(R1) ^ T2_0());
        QLib::CopyBlk(q_00, a*(1 + R1 + R2), a*(1 + R1), I(a) ^ T1_0() ^ I(R2));
        QLib::CopyBlk(q_00, a*(1 + R1 + R2), a*(1 + R1 + R2), D(0) | HH() | T1() | T2());

        return q_00;
    }


    QLib::DMatrix Generator::Q_01() const
    {
        QLib::DMatrix q_01{ a*(1 + R1 + R2 + R1 * R2), a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_01, 0, 0, D(1) ^ I(V) ^ beta_1());

        QLib::CopyBlk(q_01, a, a*(M1 + M2), D(1) ^ I(V) ^ beta_2() ^ I(R1));

        QLib::CopyBlk(q_01, a*(1 + R1), a*(M1 + M2 + M2*R1), D(1) ^ I(V) ^ beta_1() ^ I(R2));

        QLib::CopyBlk(q_01, a*(1 + R1 + R2), a*(M1 + M2 + M2*R1 + M1*R2), D(1) ^ I(V) ^ beta_3() ^ I(R1) ^ I(R2));

        return q_01;
    }

    QLib::DMatrix Generator::Q_0k(int k) const
    {
        if(k <= 1)
        {
            throw ELib::Exception("Q_0,k: k <= 1");
        }

        QLib::DMatrix q_0k{ a*(1 + R1 + R2 + R1 * R2), a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_0k, 0, 0, D(k) ^ I(V) ^ beta_1() ^ beta_2());
        QLib::CopyBlk(q_0k, a, a*M1*M2, D(k) ^ I(V) ^ beta_2() ^ I(R1));
        QLib::CopyBlk(q_0k, a*(1 + R1), a*(M1*M2 + M2*R1), D(k) ^ I(V) ^ beta_1() ^ I(R2));
        QLib::CopyBlk(q_0k, a*(1 + R1 + R2), a*(M1*M2 + M2*R1 + M1*R2), D(k) ^ I(V) ^ beta_3() ^ I(R1) ^ I(R2));

        return q_0k;
    }

    QLib::DMatrix Generator::Q_10() const
    {
        QLib::DMatrix q_10{ a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2), a*(1 + R1 + R2 + R1*R2), 0 };

        QLib::CopyBlk(q_10, 0, 0, I(a) ^ S1_0());

        QLib::CopyBlk(q_10, a*M1, 0, I(a) ^ S2_0());

        QLib::CopyBlk(q_10, a*(M1 + M2), a, I(a) ^ S2_0() ^ I(R1));

        QLib::CopyBlk(q_10, a*(M1 + M2 + M2*R1), a*(1 + R1), I(a) ^ S1_0() ^ I(R2));

        QLib::CopyBlk(q_10, a*(M1 + M2 + M2*R1 + M1*R2), a*(1 + R1 + R2), I(a) ^ S3_0() ^ I(R1) ^ I(R2));

        return q_10;
    }

    QLib::DMatrix Generator::Q_11() const
    {
        QLib::DMatrix q_11{ a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1 *R2), a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_11, 0, 0, D(0) | H(0) | S1());
        QLib::CopyBlk(q_11, 0, a*(M1 + M2), I(W) ^ H(1) ^ E(M1) ^ beta_2() ^ tau_1());
        QLib::CopyBlk(q_11, 0, a*(M1 + M2 + M2*R1), I(W) ^ H(2) ^ I(M1) ^ tau_2());

        QLib::CopyBlk(q_11, a*M1, a*M1, D(0) | H(0) | S2());
        QLib::CopyBlk(q_11, a*M1, a*(M1 + M2), I(W) ^ H(1) ^ I(M2) ^ tau_1());
        QLib::CopyBlk(q_11, a*M1, a*(M1 + M2 + M2*R1), I(W) ^ H(2) ^ E(M2) ^ beta_1() ^ tau_2());

        QLib::CopyBlk(q_11, a*(M1 + M2), a*M1, I(a) ^ I(M2) ^ T1_0());
        QLib::CopyBlk(q_11, a*(M1 + M2), a*(M1 + M2), D(0) | (H(0) + H(1)) | S2() | T1());
        QLib::CopyBlk(q_11, a*(M1 + M2), a*(M1 + M2 + M2*R1 + M1*R2), I(W) ^ H(2) ^ E(M2) ^ beta_3() ^ I(R1) ^ tau_2());

        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1), 0, I(a) ^ I(M1) ^ T2_0());
        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1), a*(M1 + M2 + M2*R1), D(0) | (H(0) + H(2)) | S1() | T2());
        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1), a*(M1 + M2 + M2*R1 + M1*R2), I(W) ^ H(1) ^ E(M1) ^ beta_3() ^ tau_1() ^ I(R2));

        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1 + M1*R2), a*(M1 + M2), I(a) ^ beta_2() ^ E(M3) ^ I(R1) ^ T2_0());
        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1 + M1*R2), a*(M1 + M2 + M2*R1), I(a) ^ beta_1() ^ E(M3) ^ T1_0() ^ I(R2));
        QLib::CopyBlk(q_11, a*(M1 + M2 + M2*R1 + M1*R2), a*(M1 + M2 + M2*R1 + M1*R2), D(0) | (H(0) + H(2)) | S1() | T2());
        
        return q_11;
    }

    QLib::DMatrix Generator::Q_1k(int k) const
    {
        if(k <= 1)
        {
            throw ELib::Exception("Q_1,k: k <= 1");
        }

        QLib::DMatrix q_1k{ a*(M1 + M2 + M2*R1 + M1*R2 + a*M3*R1*R2), a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_1k, 0, 0, D(k - 1) ^ I(V) ^ I(M1) ^ beta_2());

        QLib::CopyBlk(q_1k, a*M1, 0, D(k - 1) ^ I(V) ^ beta_1() ^ I(M2));

        QLib::CopyBlk(q_1k, a*(M1 + M2), a*M1*M2, D(k - 1) ^ I(V) ^ I(M2) ^ I(R1));

        QLib::CopyBlk(q_1k, a*(M1 + M2 + M2*R1), a*(M1*M2 + M2*R1), D(k - 1) ^ I(V) ^ I(M1) ^ I(R2));

        QLib::CopyBlk(q_1k, a*(M1 + M2 + M2*R1 * a*M1*R2), a*(M1*M2 + M2*R1 + M1*R2), D(k - 1) ^ I(V) ^ I(M3) ^ I(R1) ^ I(R2));

        return q_1k;
    }

    QLib::DMatrix Generator::Q_21() const
    {
        QLib::DMatrix q_21{ a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_21, 0, 0, I(a) ^ I(M1) ^ S2_0());
        QLib::CopyBlk(q_21, 0, a*M1, I(a) ^ S1_0() ^ I(M2));

        QLib::CopyBlk(q_21, a*M1*M2, a*(M1 + M2), I(a) ^ (S2_0()*beta_2()) ^ I(R1));

        QLib::CopyBlk(q_21, a*(M1*M2 + M2*R1), a*(M1 + M2 + M2*R1), I(a) ^ (S2_0()*beta_2()) ^ I(R1));

        QLib::CopyBlk(q_21, a*(M1*M2 + M2*R1 + M1*R2), a*(M1 + M2 + M2*R1 + M1*R2), I(a) ^ (S3_0()*beta_3()) ^ I(R1) ^ I(R2));

        return q_21;
    }

    QLib::DMatrix Generator::Q_0() const
    {
        QLib::DMatrix q_0{ a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_0, 0, 0, I(a) ^ (S1_0()*beta_1() | S2_0()*beta_2()));

        QLib::CopyBlk(q_0, a*M1*M2, a*M1*M2, I(a) ^ S2_0()*beta_2() ^ I(R1));

        QLib::CopyBlk(q_0, a*(M1*M2 + M2*R1), a*(M1*M2 + M2*R1), I(a) ^ S1_0()*beta_1() ^ I(R2));

        QLib::CopyBlk(q_0, a*(M1*M2 + M2*R1 + M1*R2), a*(M1*M2 + M2*R1 + M1*R2), I(a) ^ S3_0()*beta_3() ^ I(R1) ^ I(R2));

        return q_0;
    }

    QLib::DMatrix Generator::Q_iim1(int i) const
    {
        return Q_0();
    }

    QLib::DMatrix Generator::Q_1() const
    {
        auto q_111 = Q_111();
        auto q_112 = Q_112();
        auto q_121 = Q_121();
        auto q_122 = Q_122();

        auto m1 = q_111.GetM();
        auto m2 = q_121.GetM();

        auto n1 = q_111.GetN();
        auto n2 = q_112.GetN();

        QLib::DMatrix q_1{m1 + m2, n1 + n2, 0};

        QLib::CopyBlk(q_1, 0, 0, q_111);
        QLib::CopyBlk(q_1, 0, n1, q_112);
        QLib::CopyBlk(q_1, m1, 0, q_121);
        QLib::CopyBlk(q_1, m1, n1, q_122);

        return q_1;
    }

    QLib::DMatrix Generator::Q_ii(int i) const
    {
        return Q_1();
    }

    QLib::DMatrix Generator::Q_111() const
    {
        QLib::DMatrix q_111{ a*(M1*M2 + M2*R1), a*(M1*M2 + M2*R1), 0 };

        QLib::CopyBlk(q_111, 0, 0, D(0) | H(0) | S1() | S2());
        QLib::CopyBlk(q_111, 0, a*M1*M2, I(W) ^ H(1) ^ E(M1) ^ I(M2) ^ tau_1());

        QLib::CopyBlk(q_111, a*M1*M2, 0, I(a) ^ beta_1() ^ I(M2) ^ T1_0());
        QLib::CopyBlk(q_111, a*M1*M2, a*M1*M2, D(0) | (H(0) + H(1)) | S2() | T1());
        
        return q_111;
    }

    QLib::DMatrix Generator::Q_112() const
    {
        QLib::DMatrix q_112{ a*(M1*M2 + M2*R1), a*(M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_112, 0, 0, I(W) ^ H(2) ^ I(M1) ^ E(M2) ^ tau_2());

        QLib::CopyBlk(q_112, a*M1*M2, a*M1*R2, I(W) ^ H(2) ^ E(M2) ^ beta_3() ^ I(R1) ^ tau_2());

        return q_112;
    }

    QLib::DMatrix Generator::Q_121() const
    {
        QLib::DMatrix q_121{ a*(M1*R2 + M3*R1*R2), a*(M1*M2 + M2*R1), 0 };

        QLib::CopyBlk(q_121, 0, 0, I(a) ^ I(M1) ^ beta_2() ^ T2_0());

        QLib::CopyBlk(q_121, a*M1*R2, a*M1*M2, I(a) ^ beta_2() ^ E(M3) ^ I(R1) ^ T2_0());

        return q_121;
    }

    QLib::DMatrix Generator::Q_122() const
    {
        QLib::DMatrix q_122{ a*(M1*R2 + M3*R1*R2), a*(M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_122, 0, 0, D(0) | (H(0) + H(2)) | S1() | S3() | T2());
        QLib::CopyBlk(q_122, 0, a*M1*R2, I(W) ^ H(1) ^ E(M1) ^ beta_3() ^ I(R2) ^ tau_1());

        QLib::CopyBlk(q_122, a*M1*R2, 0, I(a) ^ beta_1() ^ I(M3) ^ T1_0() ^ I(R2));
        QLib::CopyBlk(q_122, a*M1*R2, a*M1*R2, D(0) | HH() | S3() | T1() | T2());

        return q_122;
    }

    QLib::DMatrix Generator::Q_k(int k) const
    {
        if(k == 0)
        {
            return Q_0();
        }
        else if(k == 1)
        {
            return Q_1();
        }
        else
        {
            return Q_k2(k);
        }
    }

    QLib::DMatrix Generator::Q_k2(int k) const
    {
        QLib::DMatrix q_k{ a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), a*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_k, 0, 0, D(k - 1) ^ I(V) ^ I(M1) ^ I(M2));

        QLib::CopyBlk(q_k, a*M1*M2, a*M1*M2, D(k - 1) ^ I(V) ^ I(M2) ^ I(R1));

        QLib::CopyBlk(q_k, a*(M1*M2 + M2*R1), a*(M1*M2 + M2*R1), D(k - 1) ^ I(V) ^ I(M1) ^ I(R2));

        QLib::CopyBlk(q_k, a*(M1*M2 + M2*R1 + M1*R2), a*(M1*M2 + M2*R1 + M1*R2), D(k - 1) ^ I(V) ^ I(M3) ^ I(R1) ^ I(R2));
        
        return q_k;
    }

    QLib::DMatrix Generator::Q_iipk(int i, int k) const
    {
        if(i == 0)
        {
            throw ELib::Exception("Q_i,i+k: i == 0");
        }
        return Q_k2(k + 1);
    }

    QLib::Polynomial<QLib::DMatrix> Generator::FirstRow() const
    {
        auto result = QLib::PolyDM{};
        result.push_back(Q_00());
        result.push_back(Q_01());

        for (auto k = 2; k < requestBmapSize; ++k)
        {
            result.push_back(Q_0k(k));
        }

        return result;
    }

    QLib::Polynomial<QLib::DMatrix> Generator::SecondRow() const
    {
        auto result = QLib::PolyDM{};
        result.push_back(Q_10());
        result.push_back(Q_11());
        
        for (auto k = 1; k < requestBmapSize; ++k)
        {
            result.push_back(Q_iipk(1, k));
        }

        return result;
    }

    QLib::Polynomial<QLib::DMatrix> Generator::ThirdRow() const
    {
        auto result = QLib::PolyDM{};

        auto q_1 = Q_1();
        auto q_10 = Q_10();

        result.push_back(ZM(q_1.GetM(), q_10.GetN()));
        result.push_back(Q_21());

        for (auto k = 1; k < requestBmapSize - 1; ++k)
        {
            result.push_back(Q_iipk(1, k));
        }

        return result;
    }

    QLib::Polynomial<QLib::DMatrix> Generator::Qs() const
    {
        auto result = std::vector<QLib::DMatrix>(requestBmapSize + 1);

        for (int i = 0; i < requestBmapSize + 1; ++i)
        {
            result[i] = Q_k(i);
        }

        return QLib::PolyDM{ result };
    }

    QLib::DMatrix Generator::Q_minus() const
    {
        QLib::DMatrix q_minus{ V*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), V*(M1*M2 + M2*R1 + M1*R2 + M3*R1*R2), 0 };

        QLib::CopyBlk(q_minus, 0, 0, H(0) | S1() | S2());
        QLib::CopyBlk(q_minus, 0, V*M1*M2, H(1) ^ E(M1) ^ I(M2) ^ tau_1());
        QLib::CopyBlk(q_minus, 0, V*(M1*M2 + M1*R1), H(2) ^ I(M1) ^ E(M2) ^ tau_2());

        QLib::CopyBlk(q_minus, V*M1*M2, 0, I(V) ^ beta_1() ^ I(M2) ^ T1_0());
        QLib::CopyBlk(q_minus, V*M1*M2, V*M1*M2, (H(0) + H(1)) | S2() | T1());
        QLib::CopyBlk(q_minus, V*M1*M2, V*(M1*M2 + M1*R1 + M1*R2), H(2) ^ E(M2) ^ beta_2() ^ I(R1) ^ tau_2());

        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1), 0, I(V) ^ I(M1) ^ beta_2() ^ T2_0());
        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1), V*(M1*M2 + M1*R1), (H(0) + H(2)) | S1() | T2());
        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1), V*(M1*M2 + M1*R1 + M1*R2), H(1) ^ E(M1) ^ beta_3() ^ I(R2) ^ tau_1());

        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1 + M1*R2), V*M1*M2, I(V) ^ beta_2() ^ E(M3) ^ I(R1) ^ T2_0());
        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1 + M1*R2), V*(M1*M2 + M1*R1), I(V) ^ beta_1() ^ E(M3) ^ T1_0() ^ I(R2));
        QLib::CopyBlk(q_minus, V*(M1*M2 + M2*R1 + M1*R2), V*(M1*M2 + M1*R1 + M1*R2), HH() | S3() | T1() | T2());

        return q_minus;
    }
    
    QLib::DMatrix Generator::Gamma() const
    {
        auto gamma11 = Gamma11();
        auto gamma12 = Gamma12();
        auto gamma21 = Gamma21();
        auto gamma22 = Gamma22();

        return{};
    }


    QLib::DMatrix Generator::Gamma11() const
    {
        QLib::DMatrix gamma11{ V*(M1*M2 + M2*R1), V*(M1*M2 + M2*R1), 0 };

        //QLib::CopyBlk()

        return gamma11;
    }

    QLib::DMatrix Generator::Gamma12() const
    {
        QLib::DMatrix gamma11{ V*(M1*M2 + M2*R1), V*(M1*M2 + M2*R1), 0 };

        //QLib::CopyBlk()

        return gamma11;
    }

    QLib::DMatrix Generator::Gamma21() const
    {
        QLib::DMatrix gamma11{ V*(M1*M2 + M2*R1), V*(M1*M2 + M2*R1), 0 };

        //QLib::CopyBlk()

        return gamma11;
    }

    QLib::DMatrix Generator::Gamma22() const
    {
        QLib::DMatrix gamma11{ V*(M1*M2 + M2*R1), V*(M1*M2 + M2*R1), 0 };

        //QLib::CopyBlk()

        return gamma11;
    }

    void Generator::Fill(const EngineParameters& parameters)
    {
        SetParameters(parameters);
        SetDimensions();

        Init(1, requestBmapSize - 1);

        firstRow = FirstRow();
        secondRow = SecondRow();
        q = Qs();
    }

    QLib::DMatrix Generator::Get(unsigned int i, unsigned int j) const
    {
        if (i == 0)
        {
            return firstRow[j];
        }
        else if (i == 1)
        {
            return secondRow[j];
        }
        else if(i == 2)
        {
            if(j == 0)
            {
                throw ELib::Exception();
            }
            else if(j == 1)
            {
                return Q_21();
            }
            else
            {
                return q[j - i + 1];
            }
        }
        else
        {
            return q[j - i + 1];
        }
    }

    QLib::MatrixBlockInfo Generator::InfoAt(unsigned int i) const
    {
        if(i == 0)
        {
            return QLib::MatrixBlockInfo{ a*(1 + R1 + R2 + R1*R2), 0 };
        }
        else if(i == 1)
        {
            return QLib::MatrixBlockInfo{ a*(M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2), a*(1 + R1 + R2 + R1*R2) };
        }
        else
        {
            auto size = q[1].GetM();
            auto offset = a*(1 + R1 + R2 + R1*R2 + M1 + M2 + M2*R1 + M1*R2 + M3*R1*R2) + size*(i - 2);
            return QLib::MatrixBlockInfo{ size, offset };
        }
    }

    bool Generator::IsValid() const
    {
        auto CheckZero = [](QLib::DMatrix first, QLib::DMatrix others)
        {
            auto rowsSums = first * E(first.GetN()) + others * E(others.GetN());

            for (auto i = 0; i < rowsSums.GetN(); ++i)
            {
                if (rowsSums.At(0, i) > 1e-10)
                {
                    return false;
                }
            }

            return true;
        };

        return true;
            //CheckZero(qwave[0], qwave.GetSum(1)) // Check first row with qwave_0, qwave_1, ...
            //&& CheckZero(qcap0, q.GetSum(1)) // Seoncd row with qcap and q_1, q_2, ...
            //&& CheckZero(q[0], q.GetSum(1)); // Third row and so on with q_0, q_1, ...
    }

}