#include "module.h"
#include "engine.h"

#include "QLib/QConfigFile.h"

namespace BMAPMMAPPH1
{

std::string Module::Name()
{ 
    return "bmap_mmap_ph1";
}

void Module::Calc(QLib::ConfigFile& config, ELib::ILogger& logger, ELib::IProgressor& progressor) {
    const std::string sInfo = "Info";
    const std::string sResult = "Result";
    const std::string sResultPi = sResult + ".Pi";
    const std::string sWorkflow = "Workflow";

    EngineParameters enginePar;
    enginePar.CfgGet(config, Name());

    ELib::SubProgressor engProgressor(progressor, "Engine: ", PR_START_ENGINE, PR_WRITE_RESULTS);

    Engine engine(logger, engProgressor);

    const double solve = true;
    if (solve)
    {
        config.SectionDelete(sResult);
        config.SectionDelete(sResultPi);
        config.SectionDelete(sWorkflow);
        config.SectionDelete("Result.Performance");
        config.SectionDelete("Result.Debug.Rho1");
        config.SectionDelete("Result.Debug.Rho2");
        config.SectionDelete("Result.Test1");
        config.SectionDelete("Result.Test2");
        config.SectionDelete("Result.Debug");
        config.SectionDelete("Lambdas.LfromLambda");

        Performance perf = engine.Solve(enginePar);

        progressor.SetStatus("Writing results", PR_WRITE_RESULTS);

        perf.avPi.EnableIO(enginePar.reportPi);
        perf.avPi.ToggleReportOnlyDist(true);
        perf.CfgSet(config, sResult);
    }
}

};