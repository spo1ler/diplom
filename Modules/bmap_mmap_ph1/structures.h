#pragma once

#include "QLib/QBMAP.h"
#include "QLib/QEngineStructures.h"
#include "QLib/QPHDist.h"

namespace BMAPMMAPPH1
{

struct EngineParameters :
    public QLib::ICfgObject,
    public QLib::GenAndPiManagementParams
{
    QLib::BMAP requestBMAP;
    QLib::BMAP breakMMAP;
    QLib::PHDist main1ServicePH;
    QLib::PHDist main2ServicePH;
    QLib::PHDist reserveServicePH;
    QLib::PHDist main1RepairPH;
    QLib::PHDist main2RepairPH;

    unsigned int nW;   // Request BMAP
    unsigned int nV;   // Break MMAP (Marked Markovian Arrival Process)
    
    virtual void CfgGet(const QLib::ConfigFile& config, const std::string& section);
    virtual void CfgSet(QLib::ConfigFile& config, const std::string& section) const;
    virtual void Dump(ELib::ILogger& logger) const;
    virtual void CfgSetInfo(QLib::ConfigFile& config, const std::string& section) const;
    virtual void DumpInfo(ELib::ILogger& logger) const;
private:
    QLib::BMAP GenerateBMAP(const QLib::DMatrix& d0, const QLib::DMatrix& d, double q, int maxk) const;
    void AdjustIntensities(const QLib::ConfigFile& config, const std::string& section);
    void EngineParameters::Validate(bool condition, const std::string& err);
    QLib::BMAP GenerateSpecificBMAP(const QLib::ConfigFile& config, const std::string& section) const;
    QLib::BMAP GenerateSpecificMMAP(const QLib::ConfigFile& config, const std::string& section) const;
};

struct Performance : public QLib::ICfgObject {
    QLib::PiVector avPi;  //! Stationary distribution vector
    double requestIntensity; // BMAP intensity
    double main1ServiceIntensity;
    double main2ServiceIntensity;
    double reserveServiceIntensity;
	double repair1Intensity;
	double repair2Intensity;

    double systemLoad; // rho
    double averageNumberOfRequestsInSystem;

    double requestCorellation;
    double requestVariation;
    double main1ServiceVariation;
    double main2ServiceVariation;
    double reserveServiceVariation;
	double main1RepairVariation;
	double main2RepairVariation;
	
	virtual void CfgSet(QLib::ConfigFile& config, const std::string& section) const override;
};

}