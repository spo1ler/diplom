#include "structures.h"

#include "ELib/Exception.h"

#include "QLib/QConfigFile.h"
#include "QLib/QMatrixArithm.h" 

namespace BMAPMMAPPH1
{
    void EngineParameters::Validate(bool condition, const std::string& err)
    {
        if (!condition)
        {
            throw ELib::Exception(err);
        }
    }

    //EngineParameters::EngineParameters(const EngineParameters& other) :
    //    requestBMAP(other.requestBMAP),
    //    mainServicePH(other.mainServicePH),
    //    reserveServicePH(other.reserveServicePH),
    //    mainToReservePH(other.mainToReservePH),
    //    repairPH(other.repairPH),
    //    breakMAP(other.breakMAP)
    //{
    //}

    QLib::BMAP EngineParameters::GenerateSpecificBMAP(
        const QLib::ConfigFile& config, 
        const std::string& section) const
    {
        QLib::DMatrix d0{};
        QLib::DMatrix d{};

        auto sectionD = config.SectionByName(section + ".BMAP_GENERATOR");
        QLib::CfgGet(d0, sectionD, "D0");
        QLib::CfgGet(d, sectionD, "D");
        auto dq = QLib::CfgGetDbl(sectionD, "q");
        auto dmaxk = QLib::CfgGetInt(sectionD, "maxk");

        return GenerateBMAP(d0, d, dq, dmaxk);
    }

    QLib::BMAP EngineParameters::GenerateSpecificMMAP(const QLib::ConfigFile& config, const std::string& section) const
    {
        QLib::DMatrix h0{};
        QLib::DMatrix h{};

        auto sectionH = config.SectionByName(section + ".MMAP_GENERATOR");
        QLib::CfgGet(h0, sectionH, "H0");
        QLib::CfgGet(h, sectionH, "H");
        auto hq = QLib::CfgGetDbl(sectionH, "q");
        auto hmaxk = QLib::CfgGetInt(sectionH, "maxk");

        return GenerateBMAP(h0, h, hq, hmaxk);
    }

    void EngineParameters::CfgGet(const QLib::ConfigFile& config, const std::string& section)
    {
        requestBMAP = GenerateSpecificBMAP(config, section);
        breakMMAP = GenerateSpecificMMAP(config, section);

        //requestBMAP.CfgGet(config, section + ".RequestBMAP");
        //breakMMAP.CfgGet(config, section + ".BreakMMAP");

        main1ServicePH.CfgGet(config, section + ".Main1ServicePH");
        main2ServicePH.CfgGet(config, section + ".Main2ServicePH");
        reserveServicePH.CfgGet(config, section + ".ReserveServicePH");

        main1RepairPH.CfgGet(config, section + ".Main1RepairPH");
        main2RepairPH.CfgGet(config, section + ".Main2RepairPH");

        AdjustIntensities(config, section);

        nW = requestBMAP.GetDim();
        nV = breakMMAP.GetDim();
    }

    void EngineParameters::CfgSet(QLib::ConfigFile& config, const std::string& section) const
    {
        requestBMAP.CfgSet(config, section + ".RequestBMAP");
        breakMMAP.CfgSet(config, section + ".BreakMMAP");

        main1ServicePH.CfgSet(config, section + ".Main1ServicePH");
        main2ServicePH.CfgSet(config, section + ".Main2ServicePH");
        reserveServicePH.CfgSet(config, section + ".ReserveServicePH");

        main1RepairPH.CfgSet(config, section + ".Main1RepairPH");
        main2RepairPH.CfgSet(config, section + ".Main2RepairPH");
    }

    void EngineParameters::Dump(ELib::ILogger& logger) const
    {
        if (!logger.IsActive()) return;

        logger << "BMAP: ";
        requestBMAP.Dump(logger);

        logger << "break MMAP: ";
        breakMMAP.Dump(logger);

        logger << "Main 1 Service PH: ";
        main1ServicePH.Dump(logger);

        logger << "Main 2 Service PH: ";
        main2ServicePH.Dump(logger);

        logger << "Reserve Service PH: ";
        reserveServicePH.Dump(logger);

        logger << "Main Repair PH: ";
        main1RepairPH.Dump(logger);

        logger << "Reserve Repair PH: ";
        main2RepairPH.Dump(logger);
    }

    void EngineParameters::CfgSetInfo(QLib::ConfigFile& config, const std::string& section) const
    {
        /*requestBMAP.CfgSet(config, section + ".BMAP");
        breakMAP.CfgSet(config, section + ".BreakMAP");

        mainServicePH.CfgSet(config, section + ".MainServicePH");
        reserveServicePH.CfgSet(config, section + ".ReserveServicePH");

        mainToReservePH.CfgSet(config, section + ".MainToReserveSwitchPH");

        repairPH.CfgSet(config, section + ".RepairPH");*/
    }

    void EngineParameters::DumpInfo(ELib::ILogger& logger) const
    {
        if (!logger.IsActive()) return;

        logger << "BMAP: ";
        requestBMAP.Dump(logger);

        logger << "break MMAP: ";
        breakMMAP.Dump(logger);

        logger << "Main 1 Service PH: ";
        main1ServicePH.Dump(logger);

        logger << "Main 2 Service PH: ";
        main2ServicePH.Dump(logger);

        logger << "Reserve Service PH: ";
        reserveServicePH.Dump(logger);

        logger << "Main Repair PH: ";
        main1RepairPH.Dump(logger);

        logger << "Reserve Repair PH: ";
        main2RepairPH.Dump(logger);
    }

    QLib::BMAP EngineParameters::GenerateBMAP(const QLib::DMatrix& d0, const QLib::DMatrix& d, double q, int maxk) const
    {
        QLib::BMAP bmap{};

        bmap.push_back(d0);

        for (int k = 1; k <= maxk; ++k)
        {
            auto coeff = pow(q, k - 1)*(1 - q) / (1 - pow(q, maxk));
            auto dk = coeff * d;
            bmap.push_back(dk);
        }

        return bmap;
    }

    void EngineParameters::AdjustIntensities(const QLib::ConfigFile& config, const std::string& section)
    {
        auto sectionIntensity = config.SectionByName(section + ".intensity");
        auto lambda = QLib::CfgGetDbl(sectionIntensity, "lambda");
        auto h = QLib::CfgGetDbl(sectionIntensity, "h");
        auto mu1 = QLib::CfgGetDbl(sectionIntensity, "mu1");
        auto mu2 = QLib::CfgGetDbl(sectionIntensity, "mu2");
        auto mu3 = QLib::CfgGetDbl(sectionIntensity, "mu3");
        auto tau1 = QLib::CfgGetDbl(sectionIntensity, "tau1");
        auto tau2 = QLib::CfgGetDbl(sectionIntensity, "tau2");

        requestBMAP.SetIntensity(lambda);
        breakMMAP.SetIntensity(h);
        main1ServicePH.SetIntensity(mu1);
        main2ServicePH.SetIntensity(mu2);
        reserveServicePH.SetIntensity(mu3);
        main1RepairPH.SetIntensity(tau1);
        main2RepairPH.SetIntensity(tau2);
    }

    void Performance::CfgSet(QLib::ConfigFile& config, const std::string& section) const
    {
        QLib::CfgSetDbl(config, section + ".Performance", "lambda", requestIntensity);
        QLib::CfgSetDbl(config, section + ".Performance", "mu1", main1ServiceIntensity);
        QLib::CfgSetDbl(config, section + ".Performance", "mu2", main2ServiceIntensity);
        QLib::CfgSetDbl(config, section + ".Performance", "mu3", reserveServiceIntensity);
        QLib::CfgSetDbl(config, section + ".Performance", "tau1", repair1Intensity);
        QLib::CfgSetDbl(config, section + ".Performance", "tau2", repair2Intensity);

        QLib::CfgSetDbl(config, section + ".Performance.Statistics", "requestCorellation", requestCorellation);
        QLib::CfgSetDbl(config, section + ".Performance.Statistics", "requestVariation", requestVariation);
        //QLib::CfgSetDbl(config, section + ".Performance.Statistics", "breakCorellation", breakCorellation);
        //QLib::CfgSetDbl(config, section + ".Performance.Statistics", "breakVariation", breakVariation);
        //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "mainServiceCorellation", mainServiceCorellation);
        QLib::CfgSetDbl(config, section + ".Performance.Statistics", "main1ServiceVariation", main1ServiceVariation);
        QLib::CfgSetDbl(config, section + ".Performance.Statistics", "main2ServiceVariation", main2ServiceVariation);
        //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "reserveServiceCorellation", reserveServiceCorellation);
        QLib::CfgSetDbl(config, section + ".Performance.Statistics", "reserveServiceVariation", reserveServiceVariation);
        //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "switchCorellation", switchCorellation);
        //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "repaorCorellation", repairCorellation);
        //QLib::CfgSetDbl(config, section + ".Performance.Statistics", "repairVariation", repairVariation);

        if (systemLoad > 0)
        {
            QLib::CfgSetDbl(config, section + ".Performance", "rho1", systemLoad);
        }
        else
        {
            QLib::CfgSetStr(config, section + ".Debug", "Error", "Q(1) is not a generator");
        }
        
        if (systemLoad > 0 && systemLoad < 1)
        {
            avPi.CfgSet(config, section + ".Pi");

            QLib::CfgSetDbl(config, section + ".Performance", "L", averageNumberOfRequestsInSystem);
        }
    }

}