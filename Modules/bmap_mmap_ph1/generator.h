#pragma once

#include <vector>

#include "structures.h"

#include "QLib/QMatrix.h"
#include "QLib/QBlockDMatrix.h"
#include "QLib/QMatrixArithm.h"

namespace BMAPMMAPPH1
{

class Generator : public QLib::IDQBDGenerator,
protected QLib::ParameterHolder <EngineParameters>
{
public:
    Generator();
    void SetDimensions();

    void Fill(const EngineParameters& parameters);

    virtual QLib::DMatrix Get(unsigned int i, unsigned int j) const override;

    virtual QLib::MatrixBlockInfo InfoAt(unsigned int i) const override;

        bool IsValid() const;

    inline QLib::DMatrix I(int i) const
    {
        return QLib::DMatrixCDiag(i, 1).GetAsOrdinary();
    }

    static inline QLib::DMatrix E(int i)
    {
        return QLib::DMatrix(i, 1, 1);
    }

    static inline QLib::DMatrix ZM(int rows, int columns)
    {
        return QLib::DMatrix(rows, columns, 0);
    }

    inline QLib::DMatrix D(int i) const 
    {
        return P().requestBMAP.at(i);
    }

    inline QLib::DMatrix HH() const
    {
        return H(0) + H(1) + H(2);
    }

    inline QLib::DMatrix H(int i) const
    {   
        return P().breakMMAP.at(i);
    }

    inline QLib::DMatrix T1() const
    {
        return P().main1RepairPH.GetS();
    }

    inline QLib::DMatrix T2() const
    {
        return P().main2RepairPH.GetS();
    }

    inline QLib::DMatrix T1_0() const
    {
        return P().main1RepairPH.GetS0();
    }

    inline QLib::DMatrix T2_0() const
    {
        return P().main2RepairPH.GetS0();
    }

    inline QLib::DMatrix tau_1() const
    {
        return P().main1RepairPH.GetBeta();
    }
    
    inline QLib::DMatrix tau_2() const
    {
        return P().main2RepairPH.GetBeta();
    }

    inline QLib::DMatrix beta_1() const
    {
        return P().main1ServicePH.GetBeta();
    }

    inline QLib::DMatrix beta_2() const
    {
        return P().main2ServicePH.GetBeta();
    }

    inline QLib::DMatrix beta_3() const
    {
        return P().reserveServicePH.GetBeta();
    }

    inline QLib::DMatrix S1() const
    {
        return P().main1ServicePH.GetS();
    }
    
    inline QLib::DMatrix S2() const
    {
        return P().main2ServicePH.GetS();
    }

    inline QLib::DMatrix S3() const
    {
        return P().reserveServicePH.GetS();
    }

    inline QLib::DMatrix S1_0() const
    {
        return P().main1ServicePH.GetS0();
    }

    inline QLib::DMatrix S2_0() const
    {
        return P().main2ServicePH.GetS0();
    }

    inline QLib::DMatrix S3_0() const
    {
        return P().reserveServicePH.GetS0();
    }

    QLib::DMatrix Q_00() const;
    QLib::DMatrix Q_01() const;
    QLib::DMatrix Q_0k(int k) const;
    QLib::DMatrix Q_10() const;
    QLib::DMatrix Q_11() const;
    QLib::DMatrix Q_1k(int k) const;
    QLib::DMatrix Q_21() const;
    QLib::DMatrix Q_0() const;
    QLib::DMatrix Q_iim1(int i) const;
    QLib::DMatrix Q_1() const;
    QLib::DMatrix Q_ii(int i) const;
    QLib::DMatrix Q_111() const;
    QLib::DMatrix Q_112() const;
    QLib::DMatrix Q_121() const;
    QLib::DMatrix Q_122() const;
    QLib::DMatrix Q_k(int k) const;
    QLib::DMatrix Q_k2(int k) const;
    QLib::DMatrix Q_iipk(int i, int k) const;
    QLib::Polynomial<QLib::DMatrix> FirstRow() const;
    QLib::Polynomial<QLib::DMatrix> SecondRow() const;
    QLib::Polynomial<QLib::DMatrix> ThirdRow() const;
    QLib::Polynomial<QLib::DMatrix> Qs() const;
    QLib::DMatrix Q_minus() const;
    QLib::DMatrix Gamma11() const;
    QLib::DMatrix Gamma12() const;
    QLib::DMatrix Gamma21() const;
    QLib::DMatrix Gamma22() const;
    QLib::DMatrix Gamma() const;
    unsigned W;
    unsigned V;
    unsigned a;

    unsigned M1;
    unsigned M2;
    unsigned M3;

    unsigned R1;
    unsigned R2;
private:
    std::vector<QLib::DMatrix>::size_type requestBmapSize;
    QLib::Polynomial<QLib::DMatrix> firstRow;
    QLib::Polynomial<QLib::DMatrix> secondRow;
    QLib::Polynomial<QLib::DMatrix> q;
};

}
