#include <iostream>

#include "engine.h"
#include "generator.h"
#include "structures.h"

#include "QLib/QToeSolver.h"

namespace BMAPMMAPPH1
{
    Engine::Engine(ELib::ILogger& logger, ELib::IProgressor& progressor) : LogProg(logger, progressor)
    {

    }

    double Engine::Main1ServiceIntensity(const EngineParameters& engine_parameters)
    {
        return engine_parameters.main1ServicePH.GetIntensity();
    }

    double Engine::Main2ServiceIntensity(const EngineParameters& engine_parameters)
    {
        return engine_parameters.main2ServicePH.GetIntensity();
    }

    double Engine::ReserveServiceIntensity(const EngineParameters& engine_parameters)
    {
        return engine_parameters.reserveServicePH.GetIntensity();
    }

    double Engine::Repair1Intensity(const EngineParameters& engine_parameters)
    {
        return engine_parameters.main1RepairPH.GetIntensity();
    }

    double Engine::Repair2Intensity(const EngineParameters& engine_parameters)
    {
        return engine_parameters.main2RepairPH.GetIntensity();
    }

    void Engine::CalculateStatistics(const EngineParameters& parameters, Performance& performance)
    {
        performance.requestCorellation = parameters.requestBMAP.GetCorrelation();
        performance.requestVariation = parameters.requestBMAP.GetVariation();
        performance.main1ServiceVariation = parameters.main1ServicePH.GetVariation();
        performance.main2ServiceVariation = parameters.main2ServicePH.GetVariation();
        performance.reserveServiceVariation = parameters.reserveServicePH.GetVariation();
        performance.main1RepairVariation = parameters.main1RepairPH.GetVariation();
        performance.main2RepairVariation = parameters.main2RepairPH.GetVariation();
    }

    Performance Engine::Solve(const EngineParameters& params) const
    {
        ELib::SubProgressor subprog(GetProgressor(), "Filling generator:", PR_FILL_GENERATOR, PR_CHECK_ERGOD);

        Generator genQ;
        genQ.Fill(params);

        Performance perf;

        subprog.Configure("Calculating pi vectors:", PR_SOLVE, PR_PERFORMANCE);
        QLib::ToeSolver solver(GetLogger(), GetProgressor());

        auto solverParameters = QLib::ToeSolver::Parameters::GetDefault();
        solverParameters.precisionG = 1e-8;
        solverParameters.precisionPi = 1e-8;
        solverParameters.maxIterationsG *= 1000;
        solverParameters.maxPi *= 1000;

        perf.requestIntensity = RequestIntensity(params);
        perf.reserveServiceIntensity = ReserveServiceIntensity(params);
        perf.repair1Intensity = Repair1Intensity(params);
        perf.repair2Intensity = Repair2Intensity(params);

        if (perf.systemLoad < 1)
        {
            perf.avPi = Pi(solver, genQ, solverParameters);
            perf.averageNumberOfRequestsInSystem = AverageNumberOfRequestsInSystem(perf.avPi);
        }

        CalculateStatistics(params, perf);

        return perf;
    }

    QLib::PiVector Engine::Pi(QLib::ToeSolver& solver, Generator& generator, const QLib::ToeSolver::Parameters& solverParameters)
    {
        return solver.Solve(generator, 3, solverParameters);
    }

    double Engine::RequestIntensity(const EngineParameters& parameters)
    {
        return parameters.requestBMAP.GetIntensity();
    }

    double Engine::AverageNumberOfRequestsInSystem(const QLib::PiVector& piVector)
    {
        double result = 0;

        auto marginalDist = piVector.MarginalDist();

        std::cout.precision(10);
        std::cout << "====== Average number of requests in system ======" << std::endl;
        std::cout << "i\tresult_i\tp_i" << std::endl;

        for (auto i = 0; i < marginalDist.GetN(); ++i)
        {
            double p_i = marginalDist.Get(0, i);
            result += i * p_i;

            std::cout << i << "\t" << result << "\t" << p_i << std::endl;
        }

        return result;
    }

    double Engine::SystemLoad(const EngineParameters& parameters, const Generator& g, Performance& performance)
    {
        return 0.0;
    }
}
