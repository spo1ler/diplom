#include <iostream>
#include <iomanip>

#include "engine.h"
#include "generator.h"
#include "structures.h"

#include "QLib/QMatrixArithm.h"
#include "QLib/QLUP.h"
#include "QLib/QToeSolver.h"
#include <ELib/EStrUtils.h>

namespace BMAPPH1
{
Engine::Engine(ELib::ILogger& logger, ELib::IProgressor& progressor) :
    LogProg(logger, progressor)
{

}

double Engine::SwitchIntensity(const EngineParameters& engine_parameters)
{
    return engine_parameters.mainToReservePH.GetIntensity();
}

double Engine::BreakIntensity(const EngineParameters& engine_parameters)
{
    return engine_parameters.breakMAP.GetIntensity();
}

double Engine::MainServiceIntensity(const EngineParameters& engine_parameters)
{
    return engine_parameters.mainSvcPH.GetIntensity();
}

double Engine::ReserveServiceIntensity(const EngineParameters& engine_parameters)
{
    return engine_parameters.reserveSvcPH.GetIntensity();
}

double Engine::RepairIntensity(const EngineParameters& engine_parameters)
{
    return engine_parameters.repairPH.GetIntensity();
}

void Engine::CalculateStatistics(const EngineParameters& parameters, Performance& performance)
{
    performance.requestCorellation = parameters.requestBMAP.GetCorrelation();
    performance.requestVariation = parameters.requestBMAP.GetVariation();
    performance.breakCorellation = parameters.breakMAP.GetCorrelation();
    performance.breakVariation = parameters.breakMAP.GetVariation();
    //performance.mainServiceCorellation = parameters.mainServicePH.GetCorrelation();
    performance.mainServiceVariation = parameters.mainSvcPH.GetVariation();
    //performance.reserveServiceCorellation = parameters.reserveServicePH.GetCorrelation();
    performance.reserveServiceVariation = parameters.reserveSvcPH.GetVariation();
    //performance.switchCorellation = parameters.mainToReservePH.GetCorrelation();
    performance.switchVariation = parameters.mainToReservePH.GetVariation();
    //performance.repairCorellation = parameters.repairPH.GetCorrelation();
    performance.repairVariation = parameters.repairPH.GetVariation();
}

Performance Engine::Solve(const EngineParameters& params)
{
    ELib::SubProgressor subprog(GetProgressor(), "Filling generator:",
        PR_FILL_GENERATOR, PR_CHECK_ERGOD);

    Generator genQ;

    genQ.Fill(params);

    Performance perf;
    
    perf.generatorDebug.Qwave = genQ.Qwave();
    perf.generatorDebug.Q = genQ.Q();
    perf.generatorDebug.Qcap0 = genQ.Qcap0();

    subprog.Configure("Calculating pi vectors:", PR_SOLVE, PR_PERFORMANCE);
    QLib::ToeSolver solver(GetLogger(), GetProgressor());
    
    auto solverParameters = QLib::ToeSolver::Parameters::GetDefault();
    solverParameters.precisionG = 1e-8;
    solverParameters.precisionPi = 1e-8;
    solverParameters.maxIterationsG *= 1000;
    solverParameters.maxPi *= 1000;

    perf.requestIntensity = RequestIntensity(params);
    perf.breakIntensity = BreakIntensity(params);
    perf.mainServiceIntensity = MainServiceIntensity(params);
    perf.reserveServiceIntensity = ReserveServiceIntensity(params);
    perf.repairIntensity = RepairIntensity(params);
    perf.switchIntensity = SwitchIntensity(params);

    perf.systemLoad = SystemLoad(params, genQ, perf);
    
    perf.systemLoad2 = SystemLoad2(params, genQ, perf);

    if (perf.systemLoad < 0.981)
    {
        perf.avPi = Pi(solver, genQ, solverParameters);
        perf.averageNumberOfRequestsInSystem = AverageNumberOfRequestsInSystem(perf.avPi);
        Test1(params, perf);
        Test2(params, perf);
    }
    else if (perf.systemLoad < 1)
    {
        perf.averageNumberOfRequestsInSystem = -2;
    }

    CalculateStatistics(params, perf);

    return perf;
}

void Engine::Test1(const EngineParameters& parameters, Performance& performanceOut)
{
    QLib::DMatrix dAt1 = parameters.requestBMAP.GetSum();

    Performance::Test1& test1 = performanceOut.test1;

    test1.theta = QLib::LSolveRowNorm(dAt1);
    
    const auto W = parameters.nW;
    const auto V = parameters.nV;
    const auto a = W * V;

    const auto M1 = parameters.mainSvcPH.GetM();
    const auto M2 = parameters.reserveSvcPH.GetM();

    const auto L = parameters.mainToReservePH.GetM();

    const auto R = parameters.repairPH.GetM();

    auto I = [](int i)
    {
        return QLib::DMatrixCDiag(i, 1).GetAsOrdinary();
    };

    auto E = [](int i)
    {
        return QLib::DMatrix(i, 1, 1);
    };

    QLib::DMatrix a0 = QLib::DMatrix(a*(R+1), W, 0);
    QLib::CopyBlk(a0, 0, 0, I(W) ^ E(V));
    QLib::CopyBlk(a0, a, 0, I(W) ^ E(V) ^ E(R));

    QLib::DMatrix ai = QLib::DMatrix(a*(M1 + R*(M2 + L)), W, 0);
    QLib::CopyBlk(ai, 0, 0, I(W) ^ E(V) ^ E(M1));
    QLib::CopyBlk(ai, a*M1, 0, I(W) ^ E(V) ^ E(R) ^ E(M2));
    QLib::CopyBlk(ai, a*(M1 + R*M2), 0, I(W) ^ E(V) ^ E(R) ^ E(L));

    test1.x = performanceOut.avPi[0] * a0;

    for (auto i = 1; i < performanceOut.avPi.size(); ++i)
    {
       test1.x += performanceOut.avPi[i] * ai;
    }
}

void Engine::Test2(const EngineParameters& parameters, Performance& performanceOut)
{
    const auto W = parameters.nW;
    const auto V = parameters.nV;

    const auto M1 = parameters.mainSvcPH.GetM();
    const auto M2 = parameters.reserveSvcPH.GetM();
    
    const auto R = parameters.repairPH.GetM();

    QLib::DMatrix& yFromX = performanceOut.test2.yFromX;

    yFromX = QLib::DMatrix(performanceOut.rho1Debug.y.GetM(), performanceOut.rho1Debug.y.GetN());

    QLib::CopyBlk(yFromX, 0, 0, performanceOut.test1.theta ^ performanceOut.rho2Debug.x1);
    QLib::CopyBlk(yFromX, 0, W*V*M1, performanceOut.test1.theta ^ performanceOut.rho2Debug.x2);
    QLib::CopyBlk(yFromX, 0, W*V*(M1 + R*M2), performanceOut.test1.theta ^ performanceOut.rho2Debug.x3);

    performanceOut.test2.yDiff = performanceOut.rho1Debug.y - yFromX;
}

QLib::PiVector Engine::Pi(QLib::ToeSolver& solver, Generator& generator, const QLib::ToeSolver::Parameters& solverParameters)
{
    return solver.Solve(generator, 3, solverParameters);
}

    double Engine::SystemLoad(const EngineParameters& parameters, const Generator& generator, Performance& performance)
{
    int maxK = parameters.requestBMAP.size();

    QLib::PolyDM q = generator.Q();

    QLib::DMatrix qAt1 = q.GetSum();

    int m = qAt1.GetM();
    int n = qAt1.GetN();

    auto e = QLib::DMatrix(m, 1, 1);

    performance.rho1Debug.y = QLib::LSolveRowNorm(qAt1);

    auto check = performance.rho1Debug.y * qAt1;

    for (auto i = 0; i < check.GetN(); ++i)
    {
        if (check.At(0, i) > 1e-10)
        {
            return -1;
        }
    }

    auto rowsSums = qAt1 * e;

    for (auto i = 0; i < rowsSums.GetN(); ++i)
    {
        if (rowsSums.At(0, i) > 1e-10)
        {
            return -1;
        }
    }

    auto leftM = QLib::DMatrix(m, n, 0);

    for (auto i = 2; i <= maxK; ++i)
    {
        leftM += (i - 1) *q[i];
    }

    auto left = performance.rho1Debug.y * leftM * e;
    auto right = performance.rho1Debug.y * q[0] * e;

    performance.rho1Debug.numerator = left.At(0, 0);
    performance.rho1Debug.denominator = right.At(0, 0);

    auto ro = performance.rho1Debug.numerator / performance.rho1Debug.denominator;

    return ro;
}

double Engine::SystemLoad2(const EngineParameters& parameters, const Generator& g, Performance& performance)
{
    const auto V = parameters.nV;

    const auto M1 = parameters.mainSvcPH.GetM();
    const auto M2 = parameters.reserveSvcPH.GetM();

    const auto L = parameters.mainToReservePH.GetM();

    const auto R = parameters.repairPH.GetM();

    QLib::DMatrix gamma(V*(M1 + R*(M2 + L)), V*(M1 + R*(M2 + L)), 0);

    // First row
    QLib::CopyBlk(gamma, 0, 0, (g.H(0) | g.S_1()) + (g.I(V) ^ g.S0_1() * g.beta_1()));
    QLib::CopyBlk(gamma, 0, V*(M1 + R*M2), g.H(1) ^ g.E(M1) ^ g.tau() ^ g.alpha());

    // Second row
    QLib::CopyBlk(gamma, V*M1, 0, g.I(V) ^ g.T0() ^ g.E(M2) ^ g.beta_1());
    QLib::CopyBlk(gamma, V*M1, V*M1, (g.HH() | g.T() | g.S_2()) + (g.I(V*R) ^ (g.S0_2()*g.beta_2())));

    // Third row
    QLib::CopyBlk(gamma, V*(M1 + R*M2), 0, g.I(V) ^ g.T0() ^ g.E(L) ^ g.beta_1());
    QLib::CopyBlk(gamma, V*(M1 + R*M2), V*M1, g.I(V*R) ^ g.A0() ^ g.beta_2());
    QLib::CopyBlk(gamma, V*(M1 + R*M2), V*(M1 + R*M2), g.HH() | g.T() | g.A());

    auto x = QLib::LSolveRowNorm(gamma);

    performance.rho2Debug.x1 = QLib::DMatrix(1, V*M1, 0);
    performance.rho2Debug.x2 = QLib::DMatrix(1, V*R*M2, 0);
    performance.rho2Debug.x3 = QLib::DMatrix(1, V*R*L, 0);

    QLib::CopyBlk(performance.rho2Debug.x1, 0, 0, x, 0, 0, 1, V*M1);
    QLib::CopyBlk(performance.rho2Debug.x2, 0, 0, x, 0, V*M1, 1, V*R*M2);
    QLib::CopyBlk(performance.rho2Debug.x3, 0, 0, x, 0, V*(M1 + R*M2), 1, V*R*L);

    performance.rho2Debug.pi1 = performance.rho2Debug.x1 * (g.E(V) ^ g.I(M1));
    performance.rho2Debug.pi2 = performance.rho2Debug.x2 * (g.E(V) ^ g.E(R) ^ g.I(M2));
    performance.rho2Debug.denumerator = (performance.rho2Debug.pi1*g.S0_1()).At(0, 0) + (performance.rho2Debug.pi2*g.S0_2()).At(0, 0);
    
    auto rho = parameters.requestBMAP.GetIntensity() / performance.rho2Debug.denumerator;

    return rho;
}

double Engine::RequestIntensity(const EngineParameters& parameters)
{
    return parameters.requestBMAP.GetIntensity();
}

double Engine::AverageNumberOfRequestsInSystem(const QLib::PiVector& piVector)
{
    double result = 0;

    auto marginalDist = piVector.MarginalDist();

    std::cout.precision(10);
    std::cout << "====== Average number of requests in system ======" << std::endl;
    std::cout << "i\tresult_i\tp_i" << std::endl;

    for (auto i = 0; i < marginalDist.GetN(); ++i)
    {
        double p_i = marginalDist.Get(0, i);
        result += i * p_i;

        std::cout << i << "\t" << result << "\t" << p_i << std::endl;
    }

    return result;
}

double Engine::P0(const EngineParameters& parameters, QLib::PiVector& piVector)
{
    const auto W = parameters.nW;
    const auto V = parameters.nV;
    const auto a = W * V;

    const auto M1 = parameters.mainSvcPH.GetM();

    double result = 0;
    for (int i = 0; i < a; ++i)
    {
        result += piVector[0].At(0, i);
    }

    for (int j = 1; j < piVector.size(); ++j)
    {
        for (int i = 0; i < a*M1; ++i)
        {
            result += piVector[j].At(0, i);
        }
    }

    return result;
}

LfromLambda Engine::LsFromLambda(double lambda1, double lambda2, int count, EngineParameters& parameters)
{
    double oldIntensity = parameters.requestBMAP.GetIntensity();

    double delta;

    if (count == 1)
    {
        delta = 0;
    }
    else
    {
        delta = (lambda2 - lambda1) / (count - 1);
    }
    

    LfromLambda result;

    result.lambdas = std::vector<double>(count);
    result.Ls = std::vector<double>(count);
    result.rhos = std::vector<double>(count);
    result.pis = std::vector<QLib::DMatrix>(count);

    for (auto i = 0; i < count; ++i)
    {
        GetLogger() << ELib::FormatStr("lambda_%d", i);

        double lambda = lambda1 + delta*i;
        result.lambdas[i] = lambda;

        parameters.requestBMAP.SetIntensity(lambda);

        auto performance = Solve(parameters);
        
        result.rhos[i] = performance.systemLoad2;

        if (performance.systemLoad2 > 1)
        {
            result.Ls[i] = -1;
        }
        else
        {
            result.Ls[i] = performance.averageNumberOfRequestsInSystem;
        }

        result.pis[i] = performance.avPi.MarginalDist();
    }

    parameters.requestBMAP.SetIntensity(oldIntensity);

    return result;
}

}