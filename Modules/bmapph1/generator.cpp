#include "generator.h"
#include "QLib\QMatrixArithm.h"

namespace BMAPPH1
{
Generator::Generator() 
{

}

void Generator::Fill(const EngineParameters& parameters)
{
    SetParameters(parameters);

    const auto requestBmapSize = P().requestBMAP.size();

    const auto W = P().nW;
    const auto V = P().nV;
    const auto a = W * V;

    const auto M1 = P().mainSvcPH.GetM();
    const auto M2 = P().reserveSvcPH.GetM();

    const auto L = P().mainToReservePH.GetM();

    const auto R = P().repairPH.GetM();

    Init(1, requestBmapSize - 1);

    // ~Q_0
    QLib::DMatrix qwave_0(a*(R + 1), a*(R + 1), 0);

    {
        // First row
        QLib::CopyBlk(qwave_0, 0, 0, D(0) | H(0));
        QLib::CopyBlk(qwave_0, 0, a, I(W) ^ H(1) ^ tau());

        //Second row
        QLib::CopyBlk(qwave_0, a, 0, I(a) ^ T0());
        QLib::CopyBlk(qwave_0, a, a, D(0) | HH() | T());
    }
     
    qwave.push_back(qwave_0);

    for (auto i = 1; i <= requestBmapSize - 1; ++i)
    {
        QLib::DMatrix qwave_i(a*(R + 1), a*(M1 + R*(M2 + L)), 0);

        // First row
        QLib::CopyBlk(qwave_i, 0, 0, D(i) ^ I(W) ^ beta_1());

        // Second row
        QLib::CopyBlk(qwave_i, a, a*M1, D(i) ^ I(V * R) ^ beta_2());

        qwave.push_back(qwave_i);
    }

    // ^Q_0
    qcap0 = QLib::DMatrix(a*(M1 + R*(M2 + L)), a*(R + 1), 0);

    {
        // First row
        QLib::CopyBlk(qcap0, 0, 0, I(a) ^ S0_1());

        //Second row
        QLib::CopyBlk(qcap0, a*M1, a, I(a*R) ^ S0_2());
    }

    // Q_0
    QLib::DMatrix q0(a*(M1 + R*(M2 + L)), a*(M1 + R*(M2 + L)), 0);

    {
        // First row
        QLib::CopyBlk(q0, 0, 0, I(a) ^ (S0_1()*beta_1()));

        //Second row
        QLib::CopyBlk(q0, a*M1, a*M1, I(a*R) ^ S0_2()*beta_2());
    }

    q.push_back(q0);

    // Q_1
    QLib::DMatrix q1(a*(M1 + R*(M2 + L)), a*(M1 + R*(M2 + L)), 0);
    {
        // First row
        QLib::CopyBlk(q1, 0, 0, D(0) | H(0) | S_1());
        QLib::CopyBlk(q1, 0, a*(M1 + R*M2), I(W) ^ H(1) ^ E(M1) ^ tau() ^ alpha());

        // Second row
        QLib::CopyBlk(q1, a*M1, 0, I(a) ^ T0() ^ E(M2) ^ beta_1());
        QLib::CopyBlk(q1, a*M1, a*M1, D(0) | HH() | T() | S_2());

        // Third row
        QLib::CopyBlk(q1, a*(M1 + R*M2), 0, I(a) ^ T0() ^ E(L) ^ beta_1());
        QLib::CopyBlk(q1, a*(M1 + R*M2), a*M1, I(a*R) ^ A0() ^ beta_2());
        QLib::CopyBlk(q1, a*(M1 + R*M2), a*(M1 + R*M2), D(0) | HH() | T() | A());
    }

    q.push_back(q1);

    for (auto i = 2; i <= requestBmapSize; ++i)
    {
        QLib::DMatrix qi(a*(M1 + R*(M2 + L)), a*(M1 + R*(M2 + L)), 0);

        // 1st diagonal
        QLib::CopyBlk(qi, 0, 0, D(i - 1) ^ I(V * M1));
        // 2nd diagonal
        QLib::CopyBlk(qi, a*M1, a*M1, D(i - 1) ^ I(V * R * M2));
        // 3rd diagonal
        QLib::CopyBlk(qi, a*(M1 + R*M2), a*(M1 + R*M2), D(i - 1) ^ I(V * R * L));

        q.push_back(qi);
    }

    if (!IsValid())
    {
        throw ELib::Exception("Non-zero rows detected");
    }
}

QLib::DMatrix Generator::Get(unsigned int i, unsigned int j) const
{
    if (i == 0)
    {
        return qwave[j];
    }

    if (i == 1)
    {
        return j == 0 ? qcap0 : q[j];
    }

    return q[j - i + 1];
}

QLib::MatrixBlockInfo Generator::InfoAt(unsigned int i) const
{
    auto W = P().nW;
    auto V = P().nV;
    auto a = W * V;

    auto M1 = P().mainSvcPH.GetM();
    auto M2 = P().reserveSvcPH.GetM();

    auto L = P().mainToReservePH.GetM();

    auto R = P().repairPH.GetM();

    if (i == 0)
    {
        return QLib::MatrixBlockInfo(a*(R+1), 0);
    }
    else
    {
        return QLib::MatrixBlockInfo(a*(M1 + R*(M2 + L)), a*(R + 1) + (i - 1)*a*(M1 + R*(M2 + L)));
    }    
}

const QLib::PolyDM& Generator::Qwave() const
{
    return qwave;
}

const QLib::PolyDM& Generator::Q() const
{
    return q;
}

const QLib::DMatrix& Generator::Qcap0() const
{
    return qcap0;
}

bool Generator::IsValid() const
{
    auto CheckZero = [](QLib::DMatrix first, QLib::DMatrix others)
    {
        auto rowsSums = first * E(first.GetN()) + others * E(others.GetN());

        for (auto i = 0; i < rowsSums.GetN(); ++i)
        {
            if (rowsSums.At(0, i) > 1e-10)
            {
                return false;
            }
        }

        return true;
    };
    
    return 
           CheckZero(qwave[0], qwave.GetSum(1)) // Check first row with qwave_0, qwave_1, ...
        && CheckZero(qcap0, q.GetSum(1)) // Seoncd row with qcap and q_1, q_2, ...
        && CheckZero(q[0], q.GetSum(1)); // Third row and so on with q_0, q_1, ...
}

}