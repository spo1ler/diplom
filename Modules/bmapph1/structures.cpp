#include "structures.h"

#include "ELib/Exception.h"
#include "ELib/EStrUtils.h"

#include "QLib/QMatrixArithm.h"
#include "QLib/QConfigFile.h"

namespace BMAPPH1
{
void EngineParameters::Validate(bool condition, const std::string& err) 
{
    if (!condition)
    {
        throw ELib::Exception(err);
    }
}

//EngineParameters::EngineParameters(const EngineParameters& other) :
//    requestBMAP(other.requestBMAP),
//    mainServicePH(other.mainServicePH),
//    reserveServicePH(other.reserveServicePH),
//    mainToReservePH(other.mainToReservePH),
//    repairPH(other.repairPH),
//    breakMAP(other.breakMAP)
//{
//}

void EngineParameters::CfgGet(const QLib::ConfigFile& config, const std::string& section)
{
    requestBMAP.CfgGet(config, section + ".BMAP");
    breakMAP.CfgGet(config, section + ".BreakMAP");

    mainSvcPH.CfgGet(config, section + ".MainServicePH");
    reserveSvcPH.CfgGet(config, section + ".ReserveServicePH");

    mainToReservePH.CfgGet(config, section + ".MainToReservePH");

    repairPH.CfgGet(config, section + ".RepairPH");
    
    nW = requestBMAP.GetDim();
    nV = breakMAP.GetDim();

    nM1 = mainSvcPH.GetM();
    nM2 = reserveSvcPH.GetM();

    nL = mainToReservePH.GetM();

    nR = repairPH.GetM();
}

void EngineParameters::CfgSet(QLib::ConfigFile& config, const std::string& section) const
{
    requestBMAP.CfgSet(config, section + ".BMAP");
    breakMAP.CfgSet(config, section + ".BreakMAP");

    mainSvcPH.CfgSet(config, section + ".MainServicePH");
    reserveSvcPH.CfgSet(config, section + ".ReserveServicePH");

    mainToReservePH.CfgSet(config, section + ".MainToReserveSwitchPH");

    repairPH.CfgSet(config, section + ".RepairPH");
}

void EngineParameters::Dump(ELib::ILogger& logger) const
{
    if (!logger.IsActive()) return;

    logger << "BMAP: ";
    requestBMAP.Dump(logger);

    logger << "break MAP: ";
    breakMAP.Dump(logger);

    logger << "Main Service PH: ";
    mainSvcPH.Dump(logger);

    logger << "Reserve Service PH: ";
    reserveSvcPH.Dump(logger);

    logger << "Main to Reserve Switch PH: ";
    mainToReservePH.Dump(logger);

    logger << "Repair PH: ";
    repairPH.Dump(logger);
}

void EngineParameters::CfgSetInfo(QLib::ConfigFile& config, const std::string& section) const
{
    /*requestBMAP.CfgSet(config, section + ".BMAP");
    breakMAP.CfgSet(config, section + ".BreakMAP");

    mainServicePH.CfgSet(config, section + ".MainServicePH");
    reserveServicePH.CfgSet(config, section + ".ReserveServicePH");

    mainToReservePH.CfgSet(config, section + ".MainToReserveSwitchPH");

    repairPH.CfgSet(config, section + ".RepairPH");*/
}

void EngineParameters::DumpInfo(ELib::ILogger& logger) const
{
    if (!logger.IsActive()) return;

    logger << "BMAP: ";
    requestBMAP.Dump(logger);

    logger << "break MAP: ";
    breakMAP.Dump(logger);

    logger << "Main Service PH: ";
    mainSvcPH.Dump(logger);

    logger << "Reserve Service PH: ";
    reserveSvcPH.Dump(logger);
    
    logger << "Main to Reserve Switch PH: ";
    mainToReservePH.Dump(logger);

    logger << "Repair PH: ";
    repairPH.Dump(logger);
}

void Performance::CfgSet(QLib::ConfigFile& config, const std::string& section) const
{
    const double debug = false;

    QLib::CfgSetDbl(config, section + ".Performance", "lambda", requestIntensity);
    QLib::CfgSetDbl(config, section + ".Performance", "mu1", mainServiceIntensity);
    QLib::CfgSetDbl(config, section + ".Performance", "mu2", reserveServiceIntensity);
    QLib::CfgSetDbl(config, section + ".Performance", "tau", repairIntensity);
    QLib::CfgSetDbl(config, section + ".Performance", "h", breakIntensity);
    QLib::CfgSetDbl(config, section + ".Performance", "a", switchIntensity);

    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "requestCorellation", requestCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "requestVariation", requestVariation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "breakCorellation", breakCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "breakVariation", breakVariation);
    //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "mainServiceCorellation", mainServiceCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "mainServiceVariation", mainServiceVariation);
    //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "reserveServiceCorellation", reserveServiceCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "reserveServiceVariation", reserveServiceVariation);
    ///QLib::CfgSetDlb(config, section + ".Performance.Statistics", "switchCorellation", switchCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "switchVariation", switchVariation);
    //QLib::CfgSetDlb(config, section + ".Performance.Statistics", "repaorCorellation", repairCorellation);
    QLib::CfgSetDbl(config, section + ".Performance.Statistics", "repairVariation", repairVariation);

    if (systemLoad > 0)
    {
        QLib::CfgSetDbl(config, section + ".Performance", "rho1", systemLoad);
    }
    else
    {
        QLib::CfgSetStr(config, section + ".Debug", "Error", "Q(1) is not a generator");
    }

    QLib::CfgSetDbl(config, section + ".Performance", "rho2", systemLoad2);

    // Debug info

    if (debug)
    {
        //// Rho1 debug info
        QLib::CfgSet(config, section + ".Debug.Rho1", "y", rho1Debug.y);
        QLib::CfgSetDbl(config, section + ".Debug.Rho1", "numerator", rho1Debug.numerator);
        QLib::CfgSetDbl(config, section + ".Debug.Rho1", "denominator", rho1Debug.denominator);

        // Rho2 debug info
        QLib::CfgSet(config, section + ".Debug.Rho2", "x1", rho2Debug.x1);
        QLib::CfgSet(config, section + ".Debug.Rho2", "x2", rho2Debug.x2);
        QLib::CfgSet(config, section + ".Debug.Rho2", "x3", rho2Debug.x3);

        QLib::CfgSet(config, section + ".Debug.Rho2", "pi1", rho2Debug.pi1);
        QLib::CfgSet(config, section + ".Debug.Rho2", "pi2", rho2Debug.pi2);

        QLib::CfgSetDbl(config, section + ".Debug.Rho2", "denumerator", rho2Debug.denumerator);
    }

    QLib::CfgSet(config, section + ".Test1", "theta", test1.theta);
    QLib::CfgSet(config, section + ".Test1", "x", test1.x);

    QLib::CfgSet(config, section + ".Test2", "y_fromX", test2.yFromX);
    QLib::CfgSet(config, section + ".Test2", "y_diff", test2.yDiff);

    if (systemLoad > 0 && systemLoad < 1)
    {
        avPi.CfgSet(config, section + ".Pi");

        QLib::CfgSetDbl(config, section + ".Performance", "L", averageNumberOfRequestsInSystem);
    }

    if (debug)
    {
        for (auto i = 0; i < generatorDebug.Qwave.size(); ++i)
        {
            QLib::CfgSet(config, section + ".Debug", ELib::FormatStr("Qwave[%d]", i), generatorDebug.Qwave[i]);
        }

        for (auto i = 0; i < generatorDebug.Q.size(); ++i)
        {
            QLib::CfgSet(config, section + ".Debug", ELib::FormatStr("Q[%d]", i), generatorDebug.Q[i]);
        }

        QLib::CfgSet(config, section + ".Debug", "Qcap0", generatorDebug.Qcap0);

        QLib::CfgSet(config, section + ".Debug", "Q(1)", generatorDebug.Q.GetSum());
    }
}

void LfromLambda::CfgSet(QLib::ConfigFile& config, const std::string& section)
{
    QLib::DMatrix lambdasVector(1, lambdas.size(), lambdas);
    QLib::DMatrix LsVector(1, Ls.size(), Ls);
    QLib::DMatrix rhosVector(1, rhos.size(), rhos);
    
    QLib::CfgSet(config, section + ".LfromLambda", "lambdas", lambdasVector);
    QLib::CfgSet(config, section + ".LfromLambda", "Ls", LsVector);
    QLib::CfgSet(config, section + ".LfromLambda", "rhos", rhosVector);

    for (int i = 0; i < pis.size(); ++i)
    {
        QLib::CfgSet(config, section + ".LfromLambda", ELib::FormatStr("pi_%d", i), pis[i]);
    }
}

void P0FromH::CfgSet(QLib::ConfigFile& config, const std::string& section)
{
    QLib::DMatrix hsVector(1, hs.size(), hs);
    QLib::DMatrix p0sVector(1, p0s.size(), p0s);
    QLib::DMatrix rhosVector(1, rhos.size(), rhos);

    QLib::CfgSet(config, section + ".LfromLambda", "hs", hsVector);
    QLib::CfgSet(config, section + ".LfromLambda", "p0s", p0sVector);
    QLib::CfgSet(config, section + ".LfromLambda", "rhos", rhosVector);
}
}