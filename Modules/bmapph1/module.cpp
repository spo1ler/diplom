#include "module.h"
#include "engine.h"

#include "QLib/QConfigFile.h"
#include <ELib/EStrUtils.h>

namespace BMAPPH1
{

std::string Module::Name() 
{ 
    return "bmapph1";
}

void Module::Calc(QLib::ConfigFile& config, ELib::ILogger& logger, ELib::IProgressor& progressor) {
    const std::string sInfo = "Info";
    const std::string sResult = "Result";
    const std::string sResultPi = sResult + ".Pi";
    const std::string sWorkflow = "Workflow";

    EngineParameters enginePar;
    enginePar.CfgGet(config, Name());

    ELib::SubProgressor engProgressor(progressor, "Engine: ", PR_START_ENGINE, PR_WRITE_RESULTS);

    Engine engine(logger, engProgressor);

    const double solve = false;
    if (solve)
    {
        config.SectionDelete(sResult);
        config.SectionDelete(sResultPi);
        config.SectionDelete(sWorkflow);
        config.SectionDelete("Result.Performance");
        config.SectionDelete("Result.Debug.Rho1");
        config.SectionDelete("Result.Debug.Rho2");
        config.SectionDelete("Result.Test1");
        config.SectionDelete("Result.Test2");
        config.SectionDelete("Result.Debug");
        config.SectionDelete("Lambdas.LfromLambda");

        Performance perf = engine.Solve(enginePar);

        progressor.SetStatus("Writing results", PR_WRITE_RESULTS);

        perf.avPi.EnableIO(enginePar.reportPi);
        perf.avPi.ToggleReportOnlyDist(true);
        perf.CfgSet(config, sResult);
    }

    bool iterate = true;

    if (iterate)
    {
        LfromLambda lFromLambda;

        auto lambdasSection = config.SectionByName("Lambdas");

        QLib::DMatrix lambdas1Matrix;
        QLib::CfgGet(lambdas1Matrix, lambdasSection, "lambdas1");

        QLib::DMatrix lambdas2Matrix;
        QLib::CfgGet(lambdas2Matrix, lambdasSection, "lambdas2");

        int count = QLib::CfgGetInt(lambdasSection, "count");

        QLib::DMatrix hsMatrix;
        QLib::CfgGet(hsMatrix, lambdasSection, "h");

        auto lambdas1Row = lambdas1Matrix.GetRow(0);
        auto lambdas2Row = lambdas2Matrix.GetRow(0);
        auto hsRow = hsMatrix.GetRow(0);

        std::vector<double> hs(hsRow.Begin(), hsRow.End());
        std::vector<double> lambdas1(lambdas1Row.Begin(), lambdas1Row.End());
        std::vector<double> lambdas2(lambdas2Row.Begin(), lambdas2Row.End());

        if (lambdas1.size() != lambdas2.size() || lambdas1.size() != hs.size())
        {
            throw std::exception();
        }

        for (auto i = 0; i < hs.size(); ++i)
        {
            logger << ELib::FormatStr("h%d", i);

            enginePar.breakMAP.SetIntensity(hs[i]);
            LfromLambda ls = engine.LsFromLambda(lambdas1[i], lambdas2[i], count, enginePar);
            ls.CfgSet(config, ELib::FormatStr("Lambdas_h%d", i));
        }
    }

    bool iteratehs = false;

    if (iteratehs)
    {
        P0FromH p0fromH;

        auto hsSection = config.SectionByName("Hs");

        QLib::DMatrix hsMatrix;
        QLib::CfgGet(hsMatrix, hsSection, "h");

        double lambda = QLib::CfgGetDbl(hsSection, "lambda");

        double oldIntensity = enginePar.requestBMAP.GetIntensity();
        enginePar.requestBMAP.SetIntensity(lambda);

        auto hsRow = hsMatrix.GetRow(0);

        std::vector<double> hs(hsRow.Begin(), hsRow.End());
        
        for (auto i = 0; i < hs.size(); ++i)
        {
            logger << ELib::FormatStr("h%d", i);

            enginePar.breakMAP.SetIntensity(hs[i]);
            auto solution = engine.Solve(enginePar);
            double p0 = engine.P0(enginePar, solution.avPi);

            p0fromH.hs.push_back(solution.breakIntensity);
            p0fromH.p0s.push_back(p0);
            p0fromH.rhos.push_back(solution.systemLoad);
        }

        enginePar.requestBMAP.SetIntensity(oldIntensity);

        p0fromH.CfgSet(config, "P0fromH");
    }
}

};