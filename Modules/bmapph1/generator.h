#pragma once

#include <vector>

#include "structures.h"

#include "ELib/ELogProg.h"
#include "QLib/QMatrix.h"
#include "QLib/QBlockDMatrix.h"
#include "QLib/QMatrixArithm.h"

namespace BMAPPH1
{

class Generator : public QLib::IDQBDGenerator,
protected QLib::ParameterHolder <EngineParameters> 
{
public:
    Generator();

    void Fill(const EngineParameters& parameters);

    virtual QLib::DMatrix Get(unsigned int i, unsigned int j) const;

    virtual QLib::MatrixBlockInfo InfoAt(unsigned int i) const;

    const QLib::PolyDM& Qwave() const;
    const QLib::PolyDM& Q() const;
    const QLib::DMatrix& Qcap0() const;

    bool IsValid() const;

    inline QLib::DMatrix I(int i) const
    {
        return QLib::DMatrixCDiag(i, 1).GetAsOrdinary();
    }

    static inline QLib::DMatrix E(int i)
    {
        return QLib::DMatrix(i, 1, 1);
    }

    static inline QLib::DMatrix ZM(int rows, int columns)
    {
        return QLib::DMatrix(rows, columns, 0);
    }

    inline QLib::DMatrix D(int i) const 
    {
        return P().requestBMAP.at(i);
    }

    inline QLib::DMatrix H(int i) const
    {
        return P().breakMAP.at(i);
    }

    inline QLib::DMatrix HH() const
    {
        return H(0) + H(1);
    }

    inline QLib::DMatrix T() const
    {
        return P().repairPH.GetS();
    }

    inline QLib::DMatrix T0() const
    {
        return P().repairPH.GetS0();
    }

    inline QLib::DMatrix tau() const
    {
        return P().repairPH.GetBeta();
    }

    inline QLib::DMatrix beta_1() const
    {
        return P().mainSvcPH.GetBeta();
    }

    inline QLib::DMatrix beta_2() const
    {
        return P().reserveSvcPH.GetBeta();
    }

    inline QLib::DMatrix S_1() const
    {
        return P().mainSvcPH.GetS();
    }

    inline QLib::DMatrix S_2() const
    {
        return P().reserveSvcPH.GetS();
    }
    inline QLib::DMatrix S0_1() const
    {
        return P().mainSvcPH.GetS0();
    }
    inline QLib::DMatrix S0_2() const
    {
        return P().reserveSvcPH.GetS0();
    }
    
    inline QLib::DMatrix alpha() const
    {
        return P().mainToReservePH.GetBeta();
    }


    inline QLib::DMatrix A() const
    {
        return P().mainToReservePH.GetS();
    }

    inline QLib::DMatrix A0() const
    {
        return P().mainToReservePH.GetS0();
    }

    QLib::PolyDM qwave;
    QLib::DMatrix qcap0;
    QLib::PolyDM q;
};

}
