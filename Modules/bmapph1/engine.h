#pragma once

#include "ELib/ELogProg.h"
#include "ELib/ECopyProtected.h"
#include "structures.h"
#include "QLib/QEngineStructures.h"
#include <QLib/QToeSolver.h>

namespace BMAPPH1
{
class Generator;

class Engine :
    public ELib::CopyProtected,
    public ELib::LogProg,
    public QLib::ParameterHolder<EngineParameters>
{
public:
    enum {
        PR_BEGIN = 0,
        PR_FILL_GENERATOR = 1,
        PR_CHECK_ERGOD = 5,
        PR_CALC_G = 15,
        PR_SOLVE = 35,
        PR_PERFORMANCE = 95,
        PR_END = 100
    };

    Engine(ELib::ILogger& logger, ELib::IProgressor& progressor);

    double SwitchIntensity(const EngineParameters& engine_parameters);
    double BreakIntensity(const EngineParameters& engine_parameters);
    double MainServiceIntensity(const EngineParameters& engine_parameters);
    double ReserveServiceIntensity(const EngineParameters& engine_parameters);
    double RepairIntensity(const EngineParameters& engine_parameters);
    void CalculateStatistics(const EngineParameters& parameters, Performance& performance);
    Performance Solve(const EngineParameters& par);

    void Test1(const EngineParameters& parameters, Performance& performanceOut);
    void Test2(const EngineParameters& parameters, Performance& performanceOut);

    QLib::PiVector Pi(QLib::ToeSolver& solve, Generator& generator, const QLib::ToeSolver::Parameters& solverParameters);

    double SystemLoad(const EngineParameters& parameters, const Generator& generator, Performance& performance);
    double SystemLoad2(const EngineParameters& parameters, const Generator& g, Performance& performance);
    double RequestIntensity(const EngineParameters& parameters);
    double AverageNumberOfRequestsInSystem(const QLib::PiVector& piVector);
    double P0(const EngineParameters& parameters, QLib::PiVector& piVector);


    LfromLambda LsFromLambda(double lambda1, double lambda2, int count, EngineParameters& parameters);
};


}