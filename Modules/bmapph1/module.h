#pragma once

#include "QLib/QCalcModule.h"

namespace BMAPPH1
{
class Engine;

class Module : public QLib::ICalcModule
{
public:
    static std::string Name();

    virtual void Calc(QLib::ConfigFile& config, ELib::ILogger& logger, ELib::IProgressor& progressor);
private:
    enum {
        PR_BEGIN = 0,
        PR_START_ENGINE = 2,
        PR_WRITE_RESULTS = 98,
        PR_END = 100
    };
};

}
