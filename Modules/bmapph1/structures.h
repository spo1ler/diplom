#pragma once

#include "QLib/QBMAP.h"
#include "QLib/QEngineStructures.h"
#include "QLib/QPHDist.h"

namespace BMAPPH1
{

struct EngineParameters :
    public QLib::ICfgObject,
    public QLib::GenAndPiManagementParams
{
    // EngineParameters(const EngineParameters& other);

    QLib::BMAP requestBMAP;
    QLib::PHDist mainSvcPH;
    QLib::PHDist reserveSvcPH;
    QLib::PHDist mainToReservePH;
    QLib::PHDist repairPH;
    QLib::BMAP breakMAP;

    unsigned int nW;   // BMAP
    unsigned int nV;   // MAP
    unsigned int nM1;  // main service ph
    unsigned int nM2;  // reserve service ph
    unsigned int nL;  // main to reserve switch ph
    unsigned int nR;   // repair ph

    virtual void CfgGet(const QLib::ConfigFile& config, const std::string& section);
    virtual void CfgSet(QLib::ConfigFile& config, const std::string& section) const;
    virtual void Dump(ELib::ILogger& logger) const;
    virtual void CfgSetInfo(QLib::ConfigFile& config, const std::string& section) const;
    virtual void DumpInfo(ELib::ILogger& logger) const;
private:
    void EngineParameters::Validate(bool condition, const std::string& err);
};

struct Performance : public QLib::ICfgObject {
    QLib::PiVector avPi;  //! Stationary distribution vector
    double requestIntensity; // BMAP intensity
    double breakIntensity;
    double mainServiceIntensity;
    double reserveServiceIntensity;
    double switchIntensity;
    double repairIntensity;

    double systemLoad; // rho
    double systemLoad2; //rho
    double averageNumberOfRequestsInSystem;

    double requestCorellation;
    double requestVariation;
    double breakCorellation;
    double breakVariation;
    //double mainServiceCorellation;
    double mainServiceVariation;
    //double reserveServiceCorellation;
    double reserveServiceVariation;
    //double switchCorellation;
    double switchVariation;
    //double repairCorellation;
    double repairVariation;
    
    // Test
    struct Test1
    {
        QLib::DMatrix theta;
        QLib::DMatrix x;
    };
    
    struct Test2
    {
        QLib::DMatrix yFromX;
        QLib::DMatrix yDiff;
    };

    struct GeneratorDebug
    {
        QLib::PolyDM Q;
        QLib::PolyDM Qwave;
        QLib::DMatrix Qcap0;
    };

    struct Rho1Debug
    {
        QLib::DMatrix y;

        double numerator;
        double denominator;
    };

    struct Rho2Debug
    {
        QLib::DMatrix x1;
        QLib::DMatrix x2;
        QLib::DMatrix x3;

        QLib::DMatrix pi1;
        QLib::DMatrix pi2;

        double denumerator;
    };

    Test1 test1;
    Test2 test2;
    Rho1Debug rho1Debug;
    Rho2Debug rho2Debug;
    GeneratorDebug generatorDebug;

    virtual void CfgSet(QLib::ConfigFile& config, const std::string& section) const;
    
};

struct LfromLambda : public QLib::ICfgObject
{
    std::vector<double> lambdas;
    std::vector<double> Ls;
    std::vector<double> rhos;
    std::vector<QLib::DMatrix> pis;

    virtual void CfgSet(QLib::ConfigFile& config, const std::string& section);
};

struct P0FromH : public QLib::ICfgObject
{
    std::vector<double> hs;
    std::vector<double> p0s;
    std::vector<double> rhos;

    virtual void CfgSet(QLib::ConfigFile& config, const std::string& section);
};

}