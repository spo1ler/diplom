/********************************************************************
        created:	3:4:2006   13:11
        author:		Dmitry Orlovsky
*********************************************************************/

#ifndef __ModulesRegistration_h__
#define __ModulesRegistration_h__

#include <memory>
#include <string>

namespace QLib {
class ICalcModule;
}  // namespace QLib

namespace Modules {

std::unique_ptr<QLib::ICalcModule> ModuleByName(const std::string& name);

}  // namespace Modules

#endif  //__ModulesRegistration_h__
