//  created:	3:4:2006   13:12

#include "Modules/ModulesRegistration.h"

#include <functional>
#include <map>
#include <memory>

#include "QLib/QCalcModule.h"

#include "Modules/bmap_mmap_ph1/module.h"

namespace Modules {

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

typedef std::function<std::unique_ptr<QLib::ICalcModule>()> ModuleFactory;
typedef std::map<std::string, ModuleFactory> FactoryMap;

std::unique_ptr<QLib::ICalcModule> ModuleByName(const std::string& name) {
    static const FactoryMap factory_map = {
        /*{ 
            BMAPPH1::Module::Name(), 
            [] { return make_unique<BMAPPH1::Module>(); } 
        },*/
        {
            BMAPMMAPPH1::Module::Name(),
            [] { return make_unique<BMAPMMAPPH1::Module>(); }
        }
    };

  if (factory_map.count(name) != 0) {
    return factory_map.find(name)->second();
  } else {
    return std::unique_ptr<QLib::ICalcModule>();
  }
}
};
