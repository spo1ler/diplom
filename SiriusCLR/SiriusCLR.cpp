// This is the main DLL file.

#include "SiriusCLR.h"

#include "KernelLib/KKernel.h"

#include <string>
#include <vector>
#include <vcclr.h>

using namespace System::Runtime::InteropServices;
using namespace System::Threading;

namespace SiriusCLR
{
  namespace
  {
    std::string StringToAnsi(String^ s)
    {
#ifndef WIN32
#  error 32-bit architecture only is currently supported
#endif //WIN32

      char* szString = (char*)Marshal::StringToHGlobalAnsi(s).ToInt32();
      std::string sStr = szString;
      Marshal::FreeHGlobal(IntPtr((int)szString));

      return sStr;
    }

    String^ AnsiToString(const std::string& s)
    {
#ifndef WIN32
#  error 32-bit architecture only is currently supported
#endif //WIN32
      return Marshal::PtrToStringAnsi(IntPtr((int)s.c_str()), s.length());
    }

    // KLib::ICommunicator adaptor ---------------------------------------------------------------
    class CommunicatorAdaptor: public KLib::ICommunicator
    {
    public:
      CommunicatorAdaptor(SiriusCLR::ICommunicator^ hComm)
      : m_hComm(hComm)
      {
      }

      virtual void SetProgressMsg(const std::string& sCaption)
      {
        m_hComm->SetProgressMsg(AnsiToString(sCaption));
      }

      virtual void SetProgress(int iProgress)
      {
        m_hComm->SetProgress(iProgress);
      }

      virtual void SetProjectFile(const std::string& s)
      {
        m_hComm->SetProjectFile(AnsiToString(s));
      }

      virtual void SetProjectName(const std::string& s)
      {
        m_hComm->SetProjectName(AnsiToString(s));
      }

      SiriusCLR::ICommunicator^ GetCommunicator()
      {
        return m_hComm;
      }

    private:
      gcroot<SiriusCLR::ICommunicator^> m_hComm;
    };

  }


  // IProgressor implementation ----------------------------------------------------------------
  void IProgressor::SetStatus(String^ sCaption, int iProgress)
  {
    SetProgressMsg(sCaption);
    SetProgress(iProgress);
  }

  // KLib::Kernel pointer managed holder -------------------------------------------------------
  private ref class KernelPtrHolder
  {
  public:
    KernelPtrHolder(): m_pKernel(new KLib::Kernel)
    {
    }

    ~KernelPtrHolder()
    {
      delete m_pKernel;
    }

    inline KLib::Kernel* Get()
    {
      return m_pKernel;
    }

  protected:
    !KernelPtrHolder()
    {
      delete m_pKernel;
    }

  private:
    KLib::Kernel* m_pKernel;
  };

  // Managed communicator adapter holder ----------------------------------------
  private ref class CommAdaptorPtrHolder
  {
  public:
    CommAdaptorPtrHolder()
    : m_pAdaptor(new CommunicatorAdaptor(gcnew NullCommunicator()))
    {
    }

    CommAdaptorPtrHolder(SiriusCLR::ICommunicator^ comm) 
    : m_pAdaptor(new CommunicatorAdaptor(comm))
    {
    }

    ~CommAdaptorPtrHolder()
    {
      delete m_pAdaptor;
    }

    inline CommunicatorAdaptor* Get()
    {
      return m_pAdaptor;
    }

  protected:
    !CommAdaptorPtrHolder()
    {
      delete m_pAdaptor;
    }

  private:
    CommunicatorAdaptor* m_pAdaptor;
  };

  // Kernel implementation ---------------------------------------------------------------------
  Kernel::Kernel()
  : m_hCommAdaptor(gcnew CommAdaptorPtrHolder()),
    m_hKernelPtr(gcnew KernelPtrHolder()),
    m_hKernelLock(gcnew Object())
  {
  }

  void Kernel::SetCommunicator(ICommunicator^ communicator)
  {
    Monitor::Enter(m_hKernelLock);

    try
    {
      m_hCommunicator = communicator;
      m_hCommAdaptor = gcnew CommAdaptorPtrHolder(communicator);
      m_hKernelPtr->Get()->SetCommunicator(*m_hCommAdaptor->Get());
    }
    finally
    {
      Monitor::Exit(m_hKernelLock);
    }
  }

  ICommunicator^ Kernel::GetCommunicator()
  {
    return m_hCommunicator;
  }

  void Kernel::Execute(String^ fileName)
  {
    Monitor::Enter(m_hKernelLock);
    
    try
    {
      m_hKernelPtr->Get()->Execute(StringToAnsi(fileName));
    }
    finally
    {
      Monitor::Exit(m_hKernelLock);
    }
  }

} // namespace SiriusCLR
