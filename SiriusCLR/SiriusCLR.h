/********************************************************************
  created:  19/09/06	18:55
  filename: SiriusCLR.h
  author:   Dmitry Orlovsky

  purpose:  CLR interfaces for sirius
*********************************************************************/

#pragma once

using namespace System;

namespace SiriusCLR
{

  /// <summary>
  ///  Progress information acceptor class
  /// </summary>
  public ref class IProgressor abstract
  {
  public:
    virtual void SetProgressMsg(String^ sCaption) abstract;
    virtual void SetProgress(int iProgress) abstract;

    void SetStatus(String^ sCaption, int iProgress);
  };

  /// CLR ICommunicator interface
  public ref class ICommunicator abstract : public IProgressor
  {
  public:
    virtual void SetProjectFile(String^ s) abstract;
    virtual void SetProjectName(String^ s) abstract;
  };

  /// Null communicator skipping all messages
  public ref class NullCommunicator : public ICommunicator
  {
  public:
    virtual void SetProgressMsg(String^ sCaption) override {}
    virtual void SetProgress(int iProgress) override {}

    virtual void SetProjectFile(String^ s) override {}
    virtual void SetProjectName(String^ s) override {}
  };

  ref class KernelPtrHolder;
  ref class CommAdaptorPtrHolder;

  /// CLR Kernel wrapper
  public ref class Kernel
  {
  public:
    Kernel();

    void SetCommunicator(ICommunicator^ communicator);
    ICommunicator^ GetCommunicator();

    void Execute(String^ fileName);

  private:
    Object^ m_hKernelLock;
    KernelPtrHolder^ m_hKernelPtr;
    ICommunicator^ m_hCommunicator;
    CommAdaptorPtrHolder^ m_hCommAdaptor;
  };


}
