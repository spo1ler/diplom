/********************************************************************
  created:  02/10/06  18:25
  filename: KKernel.cpp
  author:   Dmitry Orlovsky

  purpose:  Sirius Kernel definition
*********************************************************************/

#include "KernelLib/KKernel.h"

#include <ctime>
#include <memory>
#include <string>

#include "ELib/ELogger.h"
#include "ELib/EStrUtils.h"
#include "Modules/ModulesRegistration.h"
#include "QLib/QCalcModule.h"
#include "QLib/QConfigFile.h"

namespace KLib {
using namespace ELib;
using namespace QLib;

namespace {
struct ProjectData {
  std::string moduleName;
  std::string projectName;
  std::string logMode;
  std::string logFile;
};
}

Kernel::Kernel() : m_pCommunicator(NULL) {}

void Kernel::SetCommunicator(ICommunicator& comm) { m_pCommunicator = &comm; }

void Kernel::Execute(const std::string& fileName) {
  Comm().SetProjectFile(fileName);
  ConfigFile config(fileName);

  config.Read();
  config.SectionCleanup("Workflow");

  std::unique_ptr<ILogger> pLogger(new DummyLogger());

  try {
    ProjectData project;

    if (ConfigSection* pProjectSection = config.SectionByName("Project")) {
      project.projectName = CfgGetStr(pProjectSection, "Name", "<noname>");
      project.moduleName = CfgGetStr(pProjectSection, "Module");
      project.logMode = CfgGetStr(pProjectSection, "LogMode", "");
      project.logFile = CfgGetStr(pProjectSection, "LogFile", "sirius.log");
    } else
      throw Exception("Kernel::Execute: 'Project' section not found in config");

    if (project.logMode != "NoLog") {
      pLogger.reset(new OFStreamLogger(project.logFile));

      if (project.logMode == "LogAll")
        pLogger->SetAcceptMask(ILogger::LM_ALL);
      else if (project.logMode == "LogMajor")
        pLogger->SetAcceptMask(ILogger::LM_MAJOR);
      else {
        (*pLogger)
            << "Project.LogMode key not found of have unspecified value. "
               "Setting LM_MAJOR logging mode by default.\n";
        pLogger->SetAcceptMask(ILogger::LM_MAJOR);
      }
    }

    //   (*pLogger) << "Registered modules list:\n";
    //   Registry().DumpRegisteredFactories(*pLogger);

    Comm().SetProjectName(project.projectName);

    const std::unique_ptr<ICalcModule> pCalcModule =
        Modules::ModuleByName(project.moduleName);
    if (pCalcModule != nullptr) {
      //  (*pLogger) << "SiriusApp::Main: starting module " <<
      //  project.moduleName << '\n';

      std::clock_t clkModuleStart = std::clock();
      (*pLogger) << "Start : " << clkModuleStart << "\n";
      pCalcModule->Calc(config, *pLogger, Comm());
      std::clock_t clkModuleDone = std::clock();
      (*pLogger) << "Finish : " << clkModuleDone << "\n";
      config.Write("Workflow", "Calc_Time_MS",
                   FormatStr("%u", static_cast<unsigned int>(
                                       1000 * (clkModuleDone - clkModuleStart) /
                                       CLOCKS_PER_SEC)));
    } else
      throw Exception("SiriusApp::Main: module '%s' not registered\n",
                      project.moduleName.c_str());
  }
  catch (const Exception& e) {
    FormatStr sErr("Exception <ELib::Exception> caught: %s", e.what());

    (*pLogger) << sErr << '\n';
    CfgSetStr(config, "Workflow", "Necrologue", sErr);
  }
  catch (const std::bad_alloc& e) {
    FormatStr sErr("Exception <std::bad_alloc> caught: %s", e.what());

    (*pLogger) << sErr << '\n';
    CfgSetStr(config, "Workflow", "Necrologue", sErr);
  }
  catch (const std::exception& e) {
    FormatStr sErr("Exception <std::exception> caught: %s", e.what());

    (*pLogger) << sErr << '\n';
    CfgSetStr(config, "Workflow", "Necrologue", sErr);
  }
  catch (...) {
    const char szErr[] = "Exception <unknown> caught.";
    (*pLogger) << szErr << '\n';
    CfgSetStr(config, "Workflow", "Necrologue", szErr);
  }
}

}  // namespace KLib