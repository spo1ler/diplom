/********************************************************************
  created:  02/10/06	18:18
  filename: KKernel.h
  author:   Dmitry Orlovsky

  purpose:  Sirius execution Kernel
*********************************************************************/

#ifndef __KKernel_h__
#define __KKernel_h__

#include <string>

#include "ELib/EProgressor.h"

namespace KLib {
// communicator class
// -------------------------------------------------------------------------
class ICommunicator : public ELib::IProgressor {
 public:
  virtual void SetProjectFile(const std::string& s) = 0;
  virtual void SetProjectName(const std::string& s) = 0;
};

// Kernel class
// -------------------------------------------------------------------------------
class Kernel {
 public:
  Kernel();

  void SetCommunicator(ICommunicator& comm);

  void Execute(const std::string& fileName);

 protected:
  inline ICommunicator& Comm() { return *m_pCommunicator; }

 private:
  Kernel(const Kernel&) = delete;
  Kernel& operator=(const Kernel&) = delete;

  ICommunicator* m_pCommunicator;
};

}  // namespace KLib

#endif  //__KKernel_h__
