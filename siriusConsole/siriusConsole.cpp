/********************************************************************
created:  11.04.07    00:04
filename: siriusConsole.cpp
author:   Dmitry Orlovsky

purpose:
*********************************************************************/

#include <iostream>

#include "KernelLib/KKernel.h"

class ConCommunicator : public KLib::ICommunicator {
 public:
  ConCommunicator() : progress(0) {}

  virtual void SetProgressMsg(const std::string& sCaption) {
    msg = sCaption;
    Print();
  }

  virtual void SetProgress(int iProgress) {
    progress = iProgress;
    Print();
  }

  virtual void SetProjectFile(const std::string& s) {
    std::cout << "Project file: " << s.c_str() << std::endl;
  }

  virtual void SetProjectName(const std::string& s) {
    std::cout << "Project name: " << s.c_str() << std::endl;
  }

 private:
  void Print() {
    msg = msg.substr(0, 100);
    msg.reserve(100);

    while (msg.length() < 100) msg += ' ';

    std::cout << progress << "% : " << msg.c_str() << '\xD';
  }

  std::string msg;
  int progress;
};

int main(int argc, char* argv[]) {
  /*if (argc != 2) {
    std::cout << "Usage: siriusConsole.exe <filename>.spp" << std::endl;
    return -1;
  }*/

  ConCommunicator comm;
  KLib::Kernel ker;
  ker.SetCommunicator(comm);
  ker.Execute("D:/CW/sirius2.spp");
  //ker.Execute(argv[1]);

  return 0;
}

